from tkinter import END

import numpy as np
import cv2
import shapely.geometry
import zmq
from msgpack import packb, unpackb
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
import xml.etree.cElementTree as ET
import csv
import matplotlib.pyplot as plt
import cv2.aruco as aruco
from collections import defaultdict
import sympy
import time
import os
import datetime
from datetime import datetime
import json
import requests
import tkinter as tk
from queue import Queue
from threading import Thread


##############################################
# Classes
##############################################

class Eyes:
    """ Eyes describes the position of the eyes on the x- and y-axes of the recording picture."""

    def __init__(self):
        self.x = 0
        self.y = 0

    def setX(self, x):
        self.x = int(x)

    def setY(self, y):
        self.y = int(y)

    def getCoo(self):
        # print(self.x, self.y)
        return self.x, self.y


class Point:
    def __init__(self, x, y):
        self.x = self.roundNumber(x)
        self.y = self.roundNumber(y)

    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def roundPoint(self):
        self.x = int(round(self.x))
        self.y = int(round(self.y))

    def roundNumber(self, z):
        return int(round(z))


class Aruco:
    def __init__(self, id, rect):
        self.id = id
        self.rect = rect

    def getCenter(self):
        """Calculates the center of the aruco's rectangle"""
        p1 = self.rect[0]
        p4 = self.rect[3]
        x = (p1.x + p4.x) / 2
        y = (p1.y + p4.y) / 2
        pCenter = Point(x, y)
        return pCenter


class TaskElementPerFrame:
    def __init__(self, arucoPair, task):
        self.arucoPair = arucoPair
        self.task = task
        self.id = self.getId()
        self.arucoLeft, self.arucoRight = self.getArucoPos()
        self.rect = self.getRectParallel()
        self.focused = False

    def getPolygon(self):
        p0 = self.rect[0]
        p1 = self.rect[1]
        p2 = self.rect[2]
        p3 = self.rect[3]
        polygon = np.array([[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]], np.int32)
        polygon = polygon.reshape((-1, 1, 2))
        return polygon

    def getId(self):
        return self.arucoPair[0].id

    def getArucoPos(self):
        """Sorts aruco marker pair from left to right"""
        aruco1 = self.arucoPair[0]
        aruco2 = self.arucoPair[1]
        if aruco1.getCenter().x < aruco2.getCenter().x:
            return [aruco1, aruco2]
        else:
            return [aruco2, aruco1]

    def checkIfFocused(self, eyes):
        """
        Verifies if the position of eyes is in the TaskElementPerFrame rectangle.
        """
        point = shapely.geometry.Point(eyes.x, eyes.y)
        polygon = shapely.geometry.Polygon(
            [[self.rect[0].x, self.rect[0].y], [self.rect[1].x, self.rect[1].y], [self.rect[2].x, self.rect[2].y],
             [self.rect[3].x, self.rect[3].y]])
        self.focused = point.within(polygon)
        # print("checkIfFocused=", self.focused)

    def getRectParallel(self):
        aRect = self.arucoLeft.rect
        bRect = self.arucoRight.rect

        sup_p1 = aRect[1]  # supporter Point, um die parallele Gerade zu zeichnen
        sup_p2 = bRect[0]

        qr_rect_top = self.task.qrRectPtsTopSorted  # Top-QR-Code reicht, um Hilfsgeraden zu erzeugen
        sup_l1_horizontal = (qr_rect_top[0], qr_rect_top[1])  # supporter Line, um die parallele Gerade zu zeichnen
        sup_l2_vertical = (qr_rect_top[0], qr_rect_top[2])
        p1 = parallel_line(sup_p1, sup_l1_horizontal, sup_p2, sup_l2_vertical)

        sup_p3 = aRect[1]
        sup_p4 = bRect[2]
        p2 = parallel_line(sup_p3, sup_l2_vertical, sup_p4, sup_l1_horizontal)

        p0 = aRect[1]
        p3 = bRect[2]

        return [p1, p0, p2, p3]


class PupilRecordingGroupFolders:
    def __init__(self, path):
        self.path = path
        self.prgf_list = []
        self.prf_list = []

    def createPupilRecordingGroupFolders(self):
        # print("createPupilRecordingGroupFolders...")
        for folderName in os.listdir(self.path):
            path = self.path + folderName + "/"
            prgf = PupilRecordingGroupFolder(path)
            self.prgf_list.append(prgf)

    def printRecordings(self):
        for prgf in self.prgf_list:
            print(" " + prgf.name)
            for prf in prgf.pupilRecordingFolders:
                print(" -" + prf.name)

    def createPaths(self):
        pdfepcs = []  # PupilDataFishEyePathCollections
        for prgf in self.prgf_list:
            for prf in prgf.pupilRecordingFolders:
                name = prgf.name + "/" + prf.name
                exportsPath = prf.path
                videoPath = prf.getiMotionsVideoPath()
                gazeTLVPath = prf.getiMotionsTLVPath()
                pdfepc = PupilDataFishEyePathCollection(name, exportsPath, videoPath, gazeTLVPath, prf)
                pdfepcs.append(pdfepc)
        return pdfepcs


class PupilRecordingGroupFolder:  # z.B. Pilotstudie
    def __init__(self, path):
        self.path = path
        self.name = os.path.basename(os.path.dirname(path))
        self.pupilRecordingFolders = []
        # print("PupilRecordingGroupFolder: " + self.name, self.path)
        self.createPupilRecordingFolders()
        self.purgeInvalidFolders()

    def purgeInvalidFolders(self):
        # print("purgeInvalidFolders...")
        newPupilRecordingFolders = []
        for i, prf in enumerate(self.pupilRecordingFolders):
            # print(prf.name + " is " + str(prf.valid))
            if prf.valid:
                # print("add:", prf.name)
                newPupilRecordingFolders.append(prf)
        self.pupilRecordingFolders = newPupilRecordingFolders

    def createPupilRecordingFolders(self):
        # print("CreatePupilRecordingFolders...")
        for folderName in os.listdir(self.path):
            prf_path = self.path + folderName
            prf = PupilRecordingFolder(path=prf_path, name=folderName, prgf=self)
            self.pupilRecordingFolders.append(prf)
            # print(prf_path)

    def getFirstExportedFolder_AllRecordingFolders(self):
        # print("getFirstExportedFolder_AllRecordingFolders...")
        for prf in self.pupilRecordingFolders:
            prf.getFirstExportedFolder()


class PupilRecordingFolder:
    def __init__(self, path, name, prgf):
        self.path = path
        self.name = name
        self.prgf = prgf
        self.valid = None
        self.exportedFolder = None
        self.iMotionsFolder = None
        self.iMotions_info = None
        self.routine()

    def routine(self):
        # print("PupilRecordingFolder: " + self.name + "------------------")
        try:
            self.exportedFolder = self.getFirstExportedFolder()
            self.iMotionsFolder = self.getiMotionsFolder()
            self.iMotions_info = self.getiMotions_info()
            self.valid = True
            # print(self.name + " to " + str(self.valid))
            # print("Successfully created PupilRecordingFolder.")
        except Exception as e:
            # print("Couldn't create PupilRecordingFolder properly.")
            # print(e)
            self.valid = False
            # print(self.name + " to " + str(self.valid))

    def getiMotionsTLVPath(self):
        return self.iMotionsFolder + "gaze.tlv"

    def getiMotionsVideoPath(self):
        return self.iMotionsFolder + "scene.mp4"

    def getFirstExportedFolder(self):
        # print("getFirstExportedFolder...")
        exportsPath = self.path + "/exports/"
        # print(self.path)
        exportedFolder = os.listdir(exportsPath)[0]
        exportedFolderPath = exportsPath + exportedFolder + "/"
        # print(exportedFolderPath)
        return exportedFolderPath

    def getiMotionsFolder(self):
        # print("getiMotionsFolder...")
        for folderName in os.listdir(self.exportedFolder):
            if folderName[0:8] == "iMotions":
                iMotionsFolderPath = self.exportedFolder + folderName + "/"
                # print("iMotionsFolderPath:", iMotionsFolderPath)
                return iMotionsFolderPath

    def getiMotions_info(self):
        # print("getiMotions_info")
        csvPath = str(self.iMotionsFolder) + "iMotions_info.csv"

        with open(csvPath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=",")
            header = next(csv_reader)
            dict = {}
            for line in csv_reader:
                key = line[0]
                value = line[1]
                dict[key] = value
            return dict

    def convertPupilTimeToSystemTime(self, pupil_timestamp):
        dict = self.getiMotions_info()

        start_time_system = float(dict["Start Time (System)"])  # System Time at recording start
        start_time_synced = float(dict["Start Time (Synced)"])  # Pupil Time at recording start

        # Calculate the fixed offset between System and Pupil Time
        offset = start_time_system - start_time_synced

        # Add the fixed offset to the timestamp(s) we wish to convert
        pupiltime_in_systemtime = pupil_timestamp + offset

        pupiltime_in_systemtime_ms = int(pupiltime_in_systemtime * 1000)
        return pupiltime_in_systemtime_ms


class TaskPerFrame:
    def __init__(self, id, qrRecPtsTop, qrRecPtsBot, timestamp):
        self.id = id
        self.timestamp = timestamp
        self.qrRecPtsTop = qrRecPtsTop
        self.qrRecPtsBot = qrRecPtsBot
        self.qrRectPtsTopSorted = self.getSortedPointsFromQrRect(self.qrRecPtsTop)
        self.qrRectPtsBotSorted = self.getSortedPointsFromQrRect(self.qrRecPtsBot)
        self.rect = self.createTaskRec()
        self.focused = False
        self.elements = []
        self.arucos = []

    def drawTaskElements(self, dframe, drawRectPts=True):
        for element in self.elements:
            element.rect = sortPoints(element.rect)
            topLeft, topRight, botLeft, botRight = element.rect

            # Draw Rectangle
            if element.focused:
                color = (0, 255, 0)  # grün
            else:
                color = (0, 0, 255)  # rot

            pts = np.array([[topLeft.x, topLeft.y], [topRight.x, topRight.y], [botRight.x, botRight.y],
                            [botLeft.x, botLeft.y]], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], isClosed=True, color=color, thickness=5)  # Rechteck malen

            # Draw Rectangle-Points
            if drawRectPts:
                color = (255, 150, 20)
                cv2.putText(dframe, "tr", (topRight.x - 15, topRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                            color, 2)
                cv2.putText(dframe, "br", (botRight.x - 15, botRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                            color, 2)
                cv2.putText(dframe, "bl", (botLeft.x - 15, botLeft.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color,
                            2)
                cv2.putText(dframe, "tl", (topLeft.x - 15, topLeft.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color,
                            2)

            # Draw Arucos
            arucos = [element.arucoLeft, element.arucoRight]
            for aruco in arucos:
                p0, p1, p2, p3 = aruco.rect

    def drawArucos(self, dframe):
        """
        Draws a rectangle and the decoded index of all aruco markers
        """
        for aruco in self.arucos:
            topLeft, topRight, botLeft, botRight = aruco.rect

            # Draw Rectangle
            pts = np.array([[topLeft.x, topLeft.y], [topRight.x, topRight.y], [botRight.x, botRight.y],
                            [botLeft.x, botLeft.y]], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], isClosed=True, color=(0, 0, 255), thickness=5)  # Rechteck malen

            # Draw Id
            cv2.putText(dframe, str(aruco.id), (botLeft.x + 15, botLeft.y + 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                        (0, 255, 255), 2)

            # Draw Point
            x = aruco.getCenter().x
            y = aruco.getCenter().y
            cv2.circle(dframe, (x, y), 5, (255, 255, 0), -1)

    def createTaskElementPerFrames(self):
        """
        This Method creates TaskElements from this arucos-List after all ArUCo-Markers were recognized and have been
        filtered for this specific TaskPerFrame-Instanz
        """
        dict = defaultdict(list)
        elements = self.elements

        for i in self.arucos:
            dict[i.id].append(i)  # dict with elements of the list to fill by key

        for i in dict:  # go through every i-Key
            if len(dict[i]) == 2:
                # print("Index", i, "hat", len(dict[i]), "Element(e).")
                pair = [dict[i][0], dict[i][1]]
                element = TaskElementPerFrame(pair, self)
                elements.append(element)

    def checkIfFocused(self, eyes):
        """
        Schaut, ob die Position von Eyes in dem TaskPerFrame liegt.
        """
        point = shapely.geometry.Point(eyes.x, eyes.y)
        polygon = shapely.geometry.Polygon(
            [[self.rect[0].x, self.rect[0].y], [self.rect[1].x, self.rect[1].y], [self.rect[2].x, self.rect[2].y],
             [self.rect[3].x, self.rect[3].y]])
        self.focused = point.within(polygon)

    def createTaskRec(self):
        """
        Erzeugt aus der Position der QR-Codes die Viereck-Fläche des TaskPerFrame.
        """
        # print("Erzeuge Task-Area")
        line1 = (self.qrRectPtsTopSorted[0], self.qrRectPtsTopSorted[1])
        line2 = (self.qrRectPtsBotSorted[0], self.qrRectPtsBotSorted[2])
        p1 = line_intersection(line1, line2)

        line1 = (self.qrRectPtsTopSorted[1], self.qrRectPtsTopSorted[3])
        line2 = (self.qrRectPtsBotSorted[2], self.qrRectPtsBotSorted[3])
        p2 = line_intersection(line1, line2)

        point1 = self.qrRectPtsTopSorted[1]
        point2 = self.qrRectPtsBotSorted[2]
        p0 = Point(point1.x, point1.y)
        p3 = Point(point2.x, point2.y)
        return [p1, p0, p2, p3]

    def getSortedPointsFromQrRect(self, qrRecPts):
        """
        Die Punkteliste des erkannten QR-Code-Rechtecks ist immer in einer anderen Reihenfolge. Deshalb ist es notwendig
        sich die Codes zu sortieren. Diese Funktion schlüsselt die QR-Code-Punkte auf und bedient sich anschließend der
        sortPoints()-Funktion.
        """
        # Punkte für Task-Rechteck berechnen
        ## Punkte aus Barcode zwischenspeichern
        ptBarcode1 = Point(qrRecPts[0].x, qrRecPts[0].y)  # Rechteck-Punkte aus QR-Code extrahieren
        ptBarcode2 = Point(qrRecPts[1].x, qrRecPts[1].y)
        ptBarcode3 = Point(qrRecPts[2].x, qrRecPts[2].y)
        ptBarcode4 = Point(qrRecPts[3].x, qrRecPts[3].y)
        ptList = [ptBarcode1, ptBarcode2, ptBarcode3, ptBarcode4]
        ptListSorted = self.sortPoints(ptList)
        return ptListSorted

    def sortPoints(self, pointList):
        """
        Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück.
        Es wird nach der Perspektive der Points sortiert: 0) oben links 1) oben rechts 2) unten links 3) unten rechts
        """
        pointListX = sorted(pointList, key=lambda point: point.x)  # Punkte nach x-Achse sortieren von links nach rechts

        topLeft, botLeft, topRight, botRight = 0, 0, 0, 0  # Punkte gruppieren: beide linken und beide rechten Punkte
        if pointListX[0].y < pointListX[
            1].y:  # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster Stelle kommen
            topLeft = pointListX[0]
            botLeft = pointListX[1]
        else:
            botLeft = pointListX[0]  # bot left
            topLeft = pointListX[1]  # top left

        if pointListX[2].y < pointListX[3].y:
            topRight = pointListX[2]
            botRight = pointListX[3]
        else:
            botRight = pointListX[2]
            topRight = pointListX[3]

        rectSorted = [topLeft, topRight, botLeft, botRight]
        return rectSorted

    def getPolygon(self):
        p0, p1, p2, p3 = self.rect[0], self.rect[1], self.rect[2], self.rect[3]
        polygon = np.array(
            [[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]],
            np.int32)
        polygon = polygon.reshape((-1, 1, 2))
        return polygon


class TasksPerFrame:
    """
    Diese Klasse analysiert eine Liste von QR-Codes und gibt am Ende eine Liste von Tasks aus, die es dann später
    gilt mit schon gefundenen Tasks abzugleichen, was in dieser Klasse selber nicht passieren soll, denn eine Instanz
    dieser Klasse lebt nur einen Frame lang.
    """

    def __init__(self, frameNumber):
        self.taskList = []
        self.arucos = []
        self.frameNumber = frameNumber

    def findPairs(self, l):
        l.sort()
        l_tupel = []  # Der QR-Code aufgeschlüsselt in ID und top/bot
        l_pairs = []  # Gefundene Paare
        for element in l:
            l_tupel.append(element.split())
        for i in range(len(l_tupel)):
            for j in range(i + 1, len(l_tupel)):
                if l_tupel[i][0] == l_tupel[j][0]:
                    l_pairs.append([l_tupel[i], l_tupel[j]])
        return l_pairs

    def drawTaskElements_ForAllTasks(self, dframe):
        for task in self.taskList:
            color = (0, 0, 0)
            content = str(len(task.elements)) + " Elements detected"
            task.drawTaskElements(dframe)
            x = task.rect[0].x
            y = task.rect[0].y
            cv2.putText(dframe, content, (x + 30, y - 30), 1, 1.4, color, 1)
            # cv2.putText(dframe, "br", (botRight.x - 15, botRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color, 2)

    def drawArucos_ForAllTasks(self, dframe):
        for task in self.taskList:
            task.drawArucos(dframe)

    def drawRecsQrCodes_ForAllTasks(self, dframe):
        """ Diese Methode malt für alle Tasks des Frames ein Rechteck um die QR-Codes """
        color = (255, 0, 255)
        for task in self.taskList:
            qrList = [task.qrRecPtsTop, task.qrRecPtsBot]
            for qrRecPts in qrList:
                pts = np.array([qrRecPts], np.int32)
                pts = pts.reshape((-1, 1, 2))
                cv2.polylines(dframe, [pts], True, color, 5)

    def drawID_ForAllTasks(self, dframe):
        color = (0, 0, 255)
        for task in self.taskList:
            content = task.id
            point = task.rect[1]
            cv2.putText(dframe, content, (int(point.x + 30), int(point.y - 30)), 1, 1.4, color, 1)

    def drawRect_ForAllTasks(self, dframe):
        color = (0, 0, 0)
        for task in self.taskList:
            if task.focused:
                color = (0, 255, 0)  # grün
            else:
                color = (0, 0, 255)  # rot
            cv2.polylines(dframe, [task.getPolygon()], True, color, 5)  # Rechteck malen

    def drawRectPoint_ForAllTasks(self, dframe):
        color = (123, 255, 132)
        for task in self.taskList:
            pts = task.getPolygon()
            for i, point in enumerate(pts):  # iteriert durch alle 4 Punkte des QR-Codes
                px = point[0][0]
                py = point[0][1]
                pointText = str(i) + " : " + str(px) + " | " + str(py)
                cv2.putText(dframe, pointText, (px + 30, py + 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color, 1)

    def checkIfFocused_ForAllTasks(self, eyes):
        for task in self.taskList:
            task.checkIfFocused(eyes)
            for element in task.elements:
                # print("Check Elements if focused...")
                element.checkIfFocused(eyes)

    def decodeFrame(self, decodedFrame, timestamp):
        # print("decode...")
        # Finde Pärchen
        ## Extrahiere String aus QR-Code
        listQRStrings = []
        for qrCode in decodedFrame:
            decodedText = qrCode.data.decode("UTF-8")
            listQRStrings.append(decodedText)

        ## Finde String-Pärchen
        listQRStringsPaired = self.findPairs(listQRStrings)
        # print(len(listQRStringsPaired), "pair(s) found.")

        ## Gehe durch decodedFrame aber nur durch Pärchen
        for pairElement in listQRStringsPaired:  # durch alle Paar-Element-Strings iterieren
            # Temporary-Task - Attributes
            id_tta = ""
            qrRecPtsTop_tta = ""
            qrRecPtsBot_tta = ""

            for qrCode in decodedFrame:
                decodedText = qrCode.data.decode("UTF-8")
                decodedPts = qrCode.polygon
                # print("Dekodierter Text =", decodedText)
                # print("Dekodierte Punkte =", decodedPts)
                for singleElement in pairElement:  # for Bot und Top
                    singleElementFullString = " ".join([str(singleElement[0]), str(singleElement[1])])
                    # print(singleElementFullString)
                    if singleElementFullString == decodedText:  # abgleichen aller gefundener QR-Codes mit Paar-Element-String-Liste
                        id_tta = singleElementFullString[:-4]  # lösche das bot / top Suffix
                        # print("id_tta", id_tta)
                        posType = singleElement[1]  # bot oder top
                        # print("PosType:", posType)
                        if posType == "top":
                            qrRecPtsTop_tta = decodedPts  # Punkte des einzelnen QR-Code-Rechtecks
                            # print("Points Top:", qrRecPtsTop_tta)
                        elif posType == "bot":
                            qrRecPtsBot_tta = decodedPts
                            # print("POints Bot", qrRecPtsBot_tta)

            taskPerFrame = TaskPerFrame(id_tta, qrRecPtsTop_tta, qrRecPtsBot_tta, timestamp)
            # taskPerFrame.createTaskRec()
            self.taskList.append(taskPerFrame)

    def decodeArucoMarkers(self, frame, dframe, markerSize=4, totalMarkers=1000):
        """
        Diese Methode dekodiert alle Aruco-Marker innerhalb eines Frames und legt entsprechend Aruco-Instanzen an. Dabei
        sind die Aruco-Marker noch nicht danach gefiltert, ob sie wirklich innerhalb einer Task liegen.
        """
        # print("Decode Aruco Markers")

        key = getattr(aruco, f'DICT_{markerSize}X{markerSize}_{totalMarkers}')
        arucoDict = aruco.Dictionary_get(key)
        arucoParam = aruco.DetectorParameters_create()
        corners, ids, rejectedMarkers = aruco.detectMarkers(frame, arucoDict, parameters=arucoParam)
        cv2.putText(dframe, "MarkerCount = " + str(len(corners)), (int(30), int(60)), 1, 1.4, (0, 0, 255), 1)

        if len(corners) > 0:
            # print("Corner:")
            counter = 0
            for i, corner in enumerate(corners):
                counter += 1
                topLeft = Point(int(corners[i][0][0][0]), int(corners[i][0][0][1]))  # topLeft
                topRight = Point(int(corners[i][0][1][0]), int(corners[i][0][1][1]))  # topRight
                botRight = Point(int(corners[i][0][2][0]), int(corners[i][0][2][1]))  # botRight
                botLeft = Point(int(corners[i][0][3][0]), int(corners[i][0][3][1]))  # botLeft
                rectangle = [topLeft, topRight, botLeft, botRight]

                id = ids[i][0]
                # print("id=" + str(id))
                arucoMarker = Aruco(id, rectangle)
                self.arucos.append(arucoMarker)

    def assignArucosToTasks(self):
        """
        Nachdem die Aruco-Marker erkannt wurden wird mit dieser Methode geschaut, ob sich ein Marker innerhalb einer Task befindet.
        """
        for task in self.taskList:
            for aruco in self.arucos:
                aruco_center = aruco.getCenter()
                point = shapely.geometry.Point(aruco_center.x, aruco_center.y)
                p1 = task.rect[0]
                p2 = task.rect[1]
                p3 = task.rect[2]
                p4 = task.rect[3]
                polygon = shapely.geometry.Polygon([[p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y], [p4.x, p4.y]])
                arucoIsInTask = point.within(polygon)
                if arucoIsInTask:
                    # print("element " + str(element.id) + " is in task " + task.id)
                    task.arucos.append(aruco)

    def createTaskElementPerFrames_ForAllTasks(self):
        for task in self.taskList:
            task.createTaskElementPerFrames()


class TimelineElementsPerFrame:
    """
    Creates TimelineElementPerFrame for every TaskPerFrame and adds them to the list of this class. Sends
    TimelineElementsPerFrame to the Server.
    """

    def __init__(self, httpRequestPostUrl, out_q):
        self.timeline = []
        self.timelineChangedOnly = []
        self.lastTimelineElement = None
        self.httpRequestPostUrl = httpRequestPostUrl

        self.out_q = out_q

    def saveToJson(self, timelineElement):
        startTimestamp = timelineElement.startTimestamp
        te_json = timelineElement.toJson()

        folder_path = "outputs/"
        filename = str(startTimestamp) + ".json"
        file_path = folder_path + filename

        try:
            with open(file_path, "r") as jsonFile:
                data = json.load(jsonFile)
                data["inputs"].append(timelineElement.toDict())
                with open(file_path, "w") as jsonFile:
                    json.dump(data, jsonFile, indent=4, sort_keys=True)
        except IOError:
            print("File doesn't exist")
            data = {
                "inputs": []
            }
            data["inputs"].append(timelineElement.toDict())
            with open(file_path, "w") as jsonFile:
                json.dump(data, jsonFile, indent=4, sort_keys=True)
            return 0

    def importTasksPerFrame(self, tasksPerFrame, userId, startTimestamp, timestamp):
        """
        In this method all focused TasksPerFrame are getting analysed to create one TimelineElementPerFrame for
        the specific TaskPerFrame which is focused (because it's only possible that one Task is focused) and add those
        to this class's timeline-list. This method also calls the method "importToChangedOnly".
        """
        timelineElement = TimelineElementPerFrame()
        timelineElement.userId = userId
        timelineElement.startTimestamp = startTimestamp
        timelineElement.timestamp = timestamp

        for tpf in tasksPerFrame.taskList:
            if tpf.focused:
                timelineElement.taskId = tpf.id
                for e in tpf.elements:
                    if e.focused:
                        timelineElement.elementId = int(e.id)

        self.timeline.append(timelineElement)
        self.importToChangedOnly(timelineElement)
        self.lastTimelineElement = timelineElement

    def importToChangedOnly(self, timelineElementPerFrame):
        """
        In this method every created TimelineElementPerFrame is getting controlled about if its attributes have the same
        values than the the ones of the previous TimelineElementPerFrame-Instanz. If the values don't match it means
        that something in the task-recognition or the focused has changed. Not all but all different
        TimelineElementPerFrame instances should be sent to the L2P-Service. This reduces the count of transmitted data.
        """
        url = self.httpRequestPostUrl
        data = timelineElementPerFrame.toJson()

        if len(self.timelineChangedOnly) == 0:  # for the very first TimelineElementPerFrame
            self.timelineChangedOnly.append(timelineElementPerFrame)
            print("append first element")
            self.out_q.put((url, data))
            # sendToL2P(url, data)
            # self.saveToJson(timelineElement)
        else:
            taskId = str(timelineElementPerFrame.taskId)
            elementId = str(timelineElementPerFrame.elementId)
            lastTaskId = str(self.lastTimelineElement.taskId)
            lastElementId = str(self.lastTimelineElement.elementId)
            if not (taskId == lastTaskId and elementId == lastElementId):
                print("changed focus")
                self.timelineChangedOnly.append(timelineElementPerFrame)
                self.out_q.put((url, data))
                # sendToL2P(url, data)
                # self.saveToJson(timelineElement)


def sendToL2P(url, data):
    r = requests.post(url=url, data=data)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")


class TimelineElementPerFrame:
    """
    Diese Klasse bildet Instanzen eines TimelineElementes pro Frame. Jeder Frame soll ein TimelineElement
    besitzen. So ein TimelineElement beinhaltet alle wichtigen Attribute für ein REST-API-Request.
    """

    def __init__(self):
        self.userId = None
        self.studyName = None
        self.startTimestamp = None
        self.timestamp = None
        self.taskId = None
        self.elementId = None

    def toJson(self):
        data = {
            "userId": str(self.userId),
            "studyName": str(self.studyName),
            "startTimestamp": str(self.startTimestamp),
            "timestamp": str(self.timestamp),
            "taskId": str(self.taskId),
            "taskElementId": str(self.elementId)
        }

        json_data = json.dumps(data)
        # print("toJson:")
        # print(json_data)
        return json_data

    def toDict(self):
        data = {
            "userId": str(self.userId),
            "studyName": str(self.studyName),
            "startTimestamp": str(self.startTimestamp),
            "timestamp": str(self.timestamp),
            "taskId": str(self.taskId),
            "taskElementId": str(self.elementId)
        }
        return data


class ElementRecord():
    def __init__(self, id):
        self.id = id
        self.timeline = []
        self.focusedCounter = 0
        self.unfocusedCounter = 0

    def countFocusedFrames(self):
        counter = 0
        for record in self.timeline:
            if record[1] == True:
                counter += 1
        # count = self.timeline.count((True, True))
        return counter

    def countUnfocusedFrames(self):
        counter = 0
        for record in self.timeline:
            if record[1] == False:
                counter += 1
        # count = self.timeline.count((True, False))
        return counter


class ElementRecords():
    def __init__(self):
        self.list = []

    def add(self, element):
        self.list.append(element)

    def importTaskElementsPerFrame(self, taskElementsPerFrame, frameNumber):
        for tepf in taskElementsPerFrame:
            elementRecordExists = any(tepf.id == taskElementRecord.id for taskElementRecord in self.list)

            # elementRecordExists = True
            if elementRecordExists == False:
                print("Element-Record für", tepf.id, "existiert noch nicht. Lege neuen Record an")
                elementRecord = ElementRecord(tepf.id)
                self.list.append(elementRecord)
            else:
                for elementRecord in self.list:
                    if tepf.focused == True:
                        elementRecord.timeline.append((frameNumber, True))
                        elementRecord.focusedCounter += 1
                    else:
                        elementRecord.timeline.append((frameNumber, False))
                        elementRecord.unfocusedCounter += 1


class TaskRecord():
    """
    Diese Klasse beinhaltet alle gespeicherten Daten einer einzelnen Task und ist in der Klasse "TaskRecords" als Liste.
    """

    def __init__(self, id):
        self.id = id
        self.frameCounter = 1
        self.focusedFrameCounter = 1
        self.unfocusedFrameCounter = 1
        self.elementRecords = ElementRecords()
        self.timeline = []

    def toString(self):
        return "id = " + str(self.id) + ", frameCounter = " + str(self.frameCounter)


class TaskRecords:
    """
    In dieser Klasse sollen alle Tasks gespeichert werden, welche innerhalb einer Session gefunden werden.
    Dazu wird die Sammlung an TasksPerFrame importiert und abgeglichen, ob sich in
    der TaskRecords bereits diese Tasks befinden und Daten hinzugefügt.
    """

    def __init__(self):
        self.list = []

    def add(self, task):
        self.list.append(task)

    def printAllTaskRecords(self):
        for record in self.list:
            print(record.toString())

    def importTasksPerFrame(self, tasksPerFrame, frameNumber):
        """
        In dieser Methode werden die "PerFrame"-Daten mit den persistenten Daten abgeglichen. Falls eine neue Aufgabe
        hinzugekommen ist soll auch eine neue Aufgabe, also Task-Instanz, angelegt werden. Falls die Aufgabe schon mal
        erkannt worden ist, dann werden die Werte der Task-Attribute entsprechend geändert, bzw. hochgezählt.
        """
        for tpf in tasksPerFrame.taskList:
            taskRecordExists = any(tpf.id == taskRecord.id for taskRecord in self.list)
            if taskRecordExists == False:  # Record anlegen
                print("Record für", tpf.id, "existiert noch nicht. Lege einen neuen Record an...")
                singleTaskRecord = TaskRecord(tpf.id)
                self.list.append(singleTaskRecord)
            else:  # angelegten Record updaten
                # Elements
                ###################################################
                taskElementsPerFrame = tpf.elements
                for taskRecord in self.list:
                    elementRecords = taskRecord.elementRecords
                    elementRecords.importTaskElementsPerFrame(taskElementsPerFrame, frameNumber)
                ###################################################

                # Tasks
                for taskRecord in self.list:
                    if taskRecord.id == tpf.id:
                        taskRecord.frameCounter += 1
                        taskRecord.timeline.append(1)
                if tpf.focused == False:
                    taskRecord.unfocusedFrameCounter += 1
                elif tpf.focused == True:
                    taskRecord.focusedFrameCounter += 1
                else:
                    taskRecord.timeline.append(0)

    def drawTasks(self, dframe):
        """
        In dieser Methode werden die persistenten Aufgaben (Task-Instanzen) visuell im Videobild dargestellt.
        """
        index = 0
        color = (140, 22, 99)
        for taskRecord in self.list:
            index += 1
            id_short = taskRecord.id[0:5] + "..." + taskRecord.id[len(taskRecord.id) - 5: len(taskRecord.id)]
            string = str(index) + " id=" + str(id_short) + " frameCounter=" + str(
                taskRecord.frameCounter) + " fcFocused=" + str(taskRecord.focusedFrameCounter) + " fcUnfocused=" + str(
                taskRecord.unfocusedFrameCounter)
            for j, elementRecord in enumerate(taskRecord.elementRecords.list):
                string += "| e" + str(j) + ": "
                # string += "fc=" + str(elementRecord.countFocusedFrames())
                # string += ", ufc=" + str(elementRecord.countUnfocusedFrames())
                string += "fc=" + str(elementRecord.focusedCounter)
                string += ", ufc=" + str(elementRecord.unfocusedCounter)
            # print(string)
            cv2.putText(dframe, string, (index, 50 * index), 1, 1.4, color, 2)

    def drawTimeline(self, frameTimeline):
        plt.style.use("fivethirtyeight")
        numberOfRecognizedTasks = len(self.list)
        if numberOfRecognizedTasks > 0:
            fig, axs = plt.subplots(numberOfRecognizedTasks, sharex=True)
            fig.suptitle("Title")
            for i, taskRecord in enumerate(self.list):
                x = frameTimeline
                y = taskRecord.timeline
                axs[i].plot(x, y)
            plt.show()


##############################################
# Funktionen
##############################################

def getPolygon(rect):
    """
    Erzeugt aus vier Punkt-Objekten ein Polygon für cv2, um es zu zeichnen
    """
    p0 = rect[0]
    p1 = rect[1]
    p2 = rect[2]
    p3 = rect[3]
    polygon = np.array([[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]], np.int32)
    polygon = polygon.reshape((-1, 1, 2))
    return polygon


def sortPoints(pointList):
    """
    Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück.
    Es wird nach der Perspektive der Points sortiert: 0) oben links 1) oben rechts 2) unten links 3) unten rechts
    """
    pointListX = sorted(pointList, key=lambda point: point.x)  # Punkte nach x-Achse sortieren von links nach rechts

    topLeft, botLeft, topRight, botRight = 0, 0, 0, 0  # Punkte gruppieren: beide linken und beide rechten Punkte
    if pointListX[0].y < pointListX[
        1].y:  # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster Stelle kommen
        topLeft = pointListX[0]
        botLeft = pointListX[1]
    else:
        botLeft = pointListX[0]  # bot left
        topLeft = pointListX[1]  # top left

    if pointListX[2].y < pointListX[3].y:
        topRight = pointListX[2]
        botRight = pointListX[3]
    else:
        botRight = pointListX[2]
        topRight = pointListX[3]

    rectSorted = [topLeft, topRight, botLeft, botRight]
    return rectSorted


def line_intersection(l1, l2):
    p1_l1 = sympy.Point(l1[0].x, l1[0].y)
    p2_l1 = sympy.Point(l1[1].x, l1[1].y)
    l1 = sympy.Line(p1_l1, p2_l1)

    p1_l2 = sympy.Point(l2[0].x, l2[0].y)
    p2_l2 = sympy.Point(l2[1].x, l2[1].y)
    l2 = sympy.Line(p1_l2, p2_l2)

    intersection = l1.intersection(l2)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def parallel_line(pt_h, l_h, pt_v, l_v):
    # horizontal
    point_h = sympy.Point(pt_h.x, pt_h.y)

    p1_l_h = sympy.Point(l_h[0].x, l_h[0].y)
    p2_l_h = sympy.Point(l_h[1].x, l_h[1].y)
    line_h = sympy.Line(p1_l_h, p2_l_h)
    parallelLine_h = line_h.parallel_line(point_h)

    # vertical
    point_v = sympy.Point(pt_v.x, pt_v.y)

    p1_l_v = sympy.Point(l_v[0].x, l_v[0].y)
    p2_l_v = sympy.Point(l_v[1].x, l_v[1].y)
    line_v = sympy.Line(p1_l_v, p2_l_v)
    parallelLine_v = line_v.parallel_line(point_v)

    intersection = parallelLine_h.intersection(parallelLine_v)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def parallel_from_point(pt, l, l2):
    point = sympy.Point(pt.x, pt.y)

    p1_l = sympy.Point(l[0].x, l[0].y)
    p2_l = sympy.Point(l[1].x, l[1].y)
    line = sympy.Line(p1_l, p2_l)

    parallelLine = line.parallel_line(point)

    p1_l2 = sympy.Point(l2[0].x, l2[0].y)
    p2_l2 = sympy.Point(l2[1].x, l2[1].y)
    l2 = sympy.Line(p1_l2, p2_l2)

    intersection = parallelLine.intersection(l2)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def scaleImage(img, scale_percent=60):
    """ Skaliert ein Bild um x Prozent. """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img


def createXMLFromTaskRecords(taskRecords):
    session = ET.Element("session")
    person = ET.SubElement(session, "person")
    mail = ET.SubElement(person, "mail").text = "surname.lastname@stud.htwk-leipzig.de"
    records = ET.SubElement(session, "records")
    tasks = ET.SubElement(records, "tasks")
    for tr in taskRecords.list:
        task = ET.SubElement(tasks, "task")
        id = ET.SubElement(task, "id").text = tr.id
        framecounters = ET.SubElement(task, "framecounters")
        visable = ET.SubElement(framecounters, "visable").text = str(tr.frameCounter)
        focused = ET.SubElement(framecounters, "focused").text = str(tr.focusedFrameCounter)
        unfocused = ET.SubElement(framecounters, "unfocused").text = str(tr.unfocusedFrameCounter)

    tree = ET.ElementTree(session)
    folder = "outputs"
    try:
        tree.write(folder + "/session.xml")
    except ValueError:
        print("Folder", folder, "could not be found.")


def drawControls(dframe):
    wOffset = 20
    h, w, _ = dframe.shape
    # print("width="+ str(w), "height=" + str(h))
    color = (255, 255, 0)
    quit_content = "Press q to quit"
    xml_content = "Press x to create xml-file"
    changeCamera_content = "Press numbers from 0 to 4 to change camera input"
    cv2.putText(dframe, changeCamera_content, (wOffset, h - 90), 1, 1.9, color, 1)
    cv2.putText(dframe, quit_content, (wOffset, h - 60), 1, 1.9, color, 1)
    cv2.putText(dframe, xml_content, (wOffset, h - 30), 1, 1.9, color, 1)


class GazeTLVFile:
    def __init__(self, path):
        self.path = path

    def getColumnByName(self, columnName):
        """
        Get a list of values of the column's name you give into this function.
        """

        def getMatch(columnName, header):
            """
            Match the demanded columnName with all header elements to get the index of the demanded header list's item.
            """
            for i, headerElement in enumerate(header):
                if str(header[i]) == columnName:
                    return i

        allowedColumnNames = ["GazeTimeStamp", "MediaTimeStamp", "MediaFrameIndex", "Gaze3dX", "Gaze3dY", "Gaze3dZ",
                              "Gaze2dX", "Gaze2dY", "PupilDiaLeft", "PupilDiaRight", "Confidence"]
        if columnName not in allowedColumnNames:
            raise ValueError("No valid column name selected.")
        column = []
        world_index = -1
        with open(self.path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter="\t")
            header = next(csv_reader)
            i = getMatch(columnName, header)
            for line in csv_reader:
                mediaFrameIndex = int(line[2])
                if world_index == mediaFrameIndex:
                    try:
                        next(csv_reader)
                    except:
                        print("End of CSV reached")
                        break
                else:
                    world_index += 1
                    element = float(line[i])
                    column.append(element)
        return column


def importGazePosition(path):
    x = []
    y = []
    world_index = -1
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for line in csv_reader:
            print(line)
            if (world_index == int(line[1])):
                next(csv_reader)
                print(world_index)
            else:
                world_index += 1
                world_index = int(line[1])
                norm_pos_x = float(line[3])
                norm_pos_y = float(line[4])
                print(norm_pos_x, norm_pos_y)
                x.append(norm_pos_x)
                y.append(norm_pos_y)
    return x, y


class PupilDataFishEyePathCollection:
    def __init__(self, name, exportsPath, videoPath, gazeTLVPath, pupilRecordingFolder):
        self.name = name
        self.exportsPath = exportsPath
        self.videoPath = videoPath
        self.gazeTLVPath = gazeTLVPath
        self.pupilRecordingFolder = pupilRecordingFolder


def openPupilDataFishEye(exportsPath, videoPath, gazeTLVPath, pupilRecordingFolder):
    # capture = cv2.VideoCapture(videoPath)
    capture = cv2.VideoCapture(pupilRecordingFolder.getiMotionsVideoPath())

    res_x = int(capture.get(3))
    res_y = int(capture.get(4))
    print("Video resolution is: " + str(res_x) + "x" + str(res_y))

    eyes = Eyes()
    gazeTLVFile = GazeTLVFile(gazeTLVPath)
    xList = gazeTLVFile.getColumnByName("Gaze2dX")
    yList = gazeTLVFile.getColumnByName("Gaze2dY")
    timestampList = gazeTLVFile.getColumnByName("GazeTimeStamp")
    startTimestamp_ms = pupilRecordingFolder.getiMotions_info()["Start Time (System)"]

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    frameNumber = 0
    fps = 0
    prev_frame_time = 0

    frameTimeline = []

    while capture.isOpened:  # existiert die Datei, bzw. der Livestream (noch)
        success, frame = capture.read()
        if not success:
            print("Video has ended.")
            capture.release()
            cv2.destroyWindow("frame")
            break
        dframe = frame.copy()

        frameNumber += 1
        frameTimeline.append(frameNumber)

        windowsize = 80
        xPos = xList[frameNumber]
        yPos = yList[frameNumber]
        # print(xPos, yPos)
        eyes.setX(xPos)
        eyes.setY(yPos)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame,
                                     pupilRecordingFolder.convertPupilTimeToSystemTime(timestampList[frameNumber]))

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)
        cv2.putText(dframe, "fps=" + str(fps), (res_x - 500, res_y - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowsize))

        # Controls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            cv2.destroyWindow("frame")
            capture.release()
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        if pressedKey == ord('s'):  # createXML
            taskRecords.drawTimeline(frameNumber)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)


def openPupilData(pathFolderRecording="resources/recording_24_03/"):
    exportsPath = pathFolderRecording + "exports/000/"
    capture = cv2.VideoCapture(exportsPath + "world.mp4")
    # capture.set(3, 1280)  # ID bei set für die Breite ist 3
    # capture.set(4, 720)

    eyes = Eyes()  # Die Augen sind in der Mitte der Bild-Aufnahme
    # xList, yList = importGazePosition(exportsPath)
    xList, yList = importGazePosition(
        "D:/Git-Clones/digitalMentor/ressources/recording_24_03/exports/000/gaze_positions.csv")

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    frameNumber = 0
    fps = 0
    prev_frame_time = 0

    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        dframe = frame.copy()

        frameNumber += 1
        frameTimeline.append(frameNumber)

        timestamp = int(datetime.now().strftime("%s%f")) / 1000
        print(timestamp)

        windowsize = 100
        xPos = xList[frameNumber] * 1280 / (windowsize / 100)
        yPos = 720 - yList[frameNumber] * 720 / (windowsize / 100)
        print(xPos, yPos)
        eyes.setX(xPos)
        eyes.setY(yPos)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame, timestamp)

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)
        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowsize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            cv2.destroyWindow("frame")
            capture.release()
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        if pressedKey == ord('s'):  # createXML
            taskRecords.drawTimeline(frameNumber)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)
    capture.release()


_sentinel = object()


def runEyeTracking(producerFunction, *args):
    """
    This method is called to create a producer and a consumer thread. The consumer thread
    is for sending JSON-Statements to the L2P-Service. The producer function is e.g.
    openLiveWebcam, etc. This method exists to call all functions that analyse the video
    and gaze material as producer functions.
    """
    q = Queue()
    t1 = Thread(target=producerFunction, args=args + (q,))
    t2 = Thread(target=consumer, args=(q,))
    t1.start()
    t2.start()
    t1.join()  # join makes the threads wait for each other if one is faster/slower than the other
    t2.join()


def consumer(in_q):
    while True:
        data = in_q.get()
        json_data = None
        url = None
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        else:
            url = data[0]
            json_data = data[1]
            print(json_data)
        sendToL2P(url, json_data)
        in_q.task_done()


def openLivePupilCore(windowSize, userId, httpRequestPostUrl, out_q):
    # Methods
    def notify(notification):
        """Sends ``notification`` to Pupil Remote"""
        topic = "notify." + notification["subject"]
        payload = packb(notification, use_bin_type=True)
        req.send_string(topic, flags=zmq.SNDMORE)
        req.send(payload)
        return req.recv_string()

    # port setup
    addr = "127.0.0.1"  # open a req port to talk to pupil; remote ip or localhost
    req_port = "50020"  # same as in the pupil remote gui
    context = zmq.Context()
    req = context.socket(zmq.REQ)
    req.connect("tcp://{}:{}".format(addr, req_port))
    req.send_string("SUB_PORT")  # ask for the sub port
    sub_port = req.recv_string()
    sub = context.socket(zmq.SUB)  # open a sub port to listen to pupil
    sub.connect("tcp://{}:{}".format(addr, sub_port))
    sub.setsockopt_string(zmq.SUBSCRIBE, "frame.")
    sub.setsockopt_string(zmq.SUBSCRIBE, "gaze.")

    def recv_from_sub():
        """Recv a message with topic, payload.
        Topic is a utf-8 encoded string. Returned as unicode object.
        Payload is a msgpack serialized dict. Returned as a python dict.
        Any addional message frames will be added as a list
        in the payload dict with key: '__raw_data__' .
        """
        topic = sub.recv_string()
        payload = unpackb(sub.recv(), raw=False)
        extra_frames = []
        while sub.get(zmq.RCVMORE):
            extra_frames.append(sub.recv())
        if extra_frames:
            payload["__raw_data__"] = extra_frames
        return topic, payload

    def has_new_data_available():
        # Returns True as long subscription socket has received data queued for processing
        return sub.get(zmq.EVENTS) & zmq.POLLIN

    fisheye_properties = {
        "(1920, 1080)": {
            "dist_coefs": [
                [
                    -0.13648546769272826,
                    -0.0033787366635030644,
                    -0.002343859061730869,
                    0.001926274947199097,
                ]
            ],
            "camera_matrix": [
                [793.8052697386686, 0.0, 953.2237035923064],
                [0.0, 792.3104221704713, 572.5036513432223],
                [0.0, 0.0, 1.0],
            ],
            "cam_type": "fisheye"
        }
    }
    D = fisheye_properties["(1920, 1080)"]["dist_coefs"]
    D = np.array(D)
    K = fisheye_properties["(1920, 1080)"]["camera_matrix"]
    # print(K, type(K))
    K = np.array(K).reshape(3, 3)

    def undistort(img):
        """
        Undistortes an image based on the camera model.
        :param img: Distorted input image
        :return: Undistorted image
        """
        R = np.eye(3)

        map1, map2 = cv2.fisheye.initUndistortRectifyMap(
            np.array(K),
            np.array(D),
            R,
            np.array(K),
            resolution,
            cv2.CV_16SC2,
        )

        undistorted_img = cv2.remap(
            img,
            map1,
            map2,
            interpolation=cv2.INTER_LINEAR,
            borderMode=cv2.BORDER_CONSTANT,
        )

        return undistorted_img

    def projectPoints(object_points, rvec=None, tvec=None, use_distortion=True):
        """
        Projects a set of points onto the camera plane as defined by the camera model.
        :param object_points: Set of 3D world points
        :param rvec: Set of vectors describing the rotation of the camera when recording
            the corresponding object point
        :param tvec: Set of vectors describing the translation of the camera when
            recording the corresponding object point
        :return: Projected 2D points
        """
        skew = 0

        input_dim = object_points.ndim

        object_points = object_points.reshape((1, -1, 3))

        if rvec is None:
            rvec = np.zeros(3).reshape(1, 1, 3)
        else:
            rvec = np.array(rvec).reshape(1, 1, 3)

        if tvec is None:
            tvec = np.zeros(3).reshape(1, 1, 3)
        else:
            tvec = np.array(tvec).reshape(1, 1, 3)

        if use_distortion:
            _D = D
        else:
            _D = np.asarray([[1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0]])

        image_points, jacobian = cv2.fisheye.projectPoints(
            object_points, rvec, tvec, K, _D, alpha=skew
        )

        if input_dim == 2:
            image_points.shape = (-1, 2)
        elif input_dim == 3:
            image_points.shape = (-1, 1, 2)
        return image_points

    def unprojectPoints(pts_2d, use_distortion=True, normalize=False):
        """
        Undistorts points according to the camera model. cv2.fisheye.undistortPoints
        does *NOT* perform the same unprojection step the original cv2.unprojectPoints
        does. Thus we implement this function ourselves.
        :param pts_2d, shape: Nx2
        :return: Array of unprojected 3d points, shape: Nx3
        """

        pts_2d = np.array(pts_2d, dtype=np.float32)

        # Delete any posibly wrong 3rd dimension
        if pts_2d.ndim == 1 or pts_2d.ndim == 3:
            pts_2d = pts_2d.reshape((-1, 2))

        eps = np.finfo(np.float32).eps
        # print("hier:", K, type(K))
        f = np.array((K[0, 0], K[1, 1])).reshape(1, 2)
        c = np.array((K[0, 2], K[1, 2])).reshape(1, 2)
        if use_distortion:
            k = D.ravel().astype(np.float32)
        else:
            k = np.asarray(
                [1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0], dtype=np.float32
            )

        pi = pts_2d.astype(np.float32)
        pw = (pi - c) / f

        theta_d = np.linalg.norm(pw, ord=2, axis=1)
        theta = theta_d
        for j in range(10):
            theta2 = theta ** 2
            theta4 = theta2 ** 2
            theta6 = theta4 * theta2
            theta8 = theta6 * theta2
            theta = theta_d / (
                    1 + k[0] * theta2 + k[1] * theta4 + k[2] * theta6 + k[3] * theta8
            )

        scale = np.tan(theta) / (theta_d + eps)

        pts_2d_undist = pw * scale.reshape(-1, 1)

        pts_3d = cv2.convertPointsToHomogeneous(pts_2d_undist)
        pts_3d.shape = -1, 3

        if normalize:
            pts_3d /= np.linalg.norm(pts_3d, axis=1)[:, np.newaxis]
        return pts_3d

    def denormalize(pos, size, flip_y=False):
        """
        denormalize
        """
        width, height = size
        x = pos[0]
        y = pos[1]
        x *= width
        if flip_y:
            y = 1 - y
        y *= height
        return x, y

    def undist2d2pixel(global_norm_pos_x, global_norm_pos_y, resolution):
        pixel_pos = denormalize(
            (global_norm_pos_x, global_norm_pos_y), resolution, flip_y=True
        )
        undistorted3d = unprojectPoints(pixel_pos)
        undistorted2d = projectPoints(
            undistorted3d, use_distortion=False
        )
        width, height = resolution
        x, y = (int(undistorted2d[0][0]), int(undistorted2d[0][1]))
        if x > width:
            x = width
        if y > height:
            y = height
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        return (x, y)

    _sentinel = object()
    global_norm_pos_x = 0
    global_norm_pos_y = 0
    frame = None
    res_x = 0
    res_y = 0
    resolution = (res_x, res_y)
    records = []
    start_time = time.time()
    FRAME_FORMAT = "bgr"
    notify({"subject": "frame_publishing.set_format",
            "format": FRAME_FORMAT})  # Set the frame format via the Network API plugin

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme
    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # timestamps
    startTimestamp_raw = time.time()  # in seconds but float
    startTimestamp_ms = int(startTimestamp_raw * 1000)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    try:
        while True:
            while has_new_data_available():

                topic, msg = recv_from_sub()

                if topic.startswith("gaze."):  # get gaze
                    global_norm_pos_x, global_norm_pos_y = msg['norm_pos']

                if topic.startswith("frame.") and msg["format"] != FRAME_FORMAT:
                    print(
                        f"different frame format ({msg['format']}); "
                        f"skipping frame from {topic}"
                    )
                    continue

                if topic == "frame.world":  # get video
                    frame = np.frombuffer(
                        msg["__raw_data__"][0],
                        dtype=np.uint8).reshape(msg["height"], msg["width"], 3)
                    res_x = msg['width']
                    res_y = msg['height']
                    resolution = (res_x, res_y)
            if frame is not None and global_norm_pos_x is not 0:
                img_undist = undistort(frame)
                frame = img_undist
                dframe = frame.copy()

                gaze_pixel = undist2d2pixel(global_norm_pos_x, global_norm_pos_y, resolution)

                # timestamps
                cTimestamp_raw = time.time()  # in seconds but float
                cTimestamp_ms = int(cTimestamp_raw * 1000)

                # frames
                frameNumber += 1
                frameTimeline.append(frameNumber)

                # qr code scanner
                decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
                tasksPerFrame = TasksPerFrame(frameNumber)
                tasksPerFrame.decodeFrame(decodedFrame, cTimestamp_ms)

                # AruCo scanner
                tasksPerFrame.decodeArucoMarkers(frame, dframe)
                tasksPerFrame.assignArucosToTasks()
                tasksPerFrame.createTaskElementPerFrames_ForAllTasks()

                # focused?
                eyes.setX(gaze_pixel[0])
                eyes.setY(gaze_pixel[1])
                tasksPerFrame.checkIfFocused_ForAllTasks(eyes)

                # save results
                taskRecords.importTasksPerFrame(tasksPerFrame, frameNumber)
                taskRecords.drawTasks(dframe)
                timeline.importTasksPerFrame(tasksPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

                # draw
                tasksPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
                tasksPerFrame.drawRect_ForAllTasks(dframe)
                tasksPerFrame.drawRectPoint_ForAllTasks(dframe)
                tasksPerFrame.drawID_ForAllTasks(dframe)
                ## arucos
                tasksPerFrame.drawArucos_ForAllTasks(dframe)
                tasksPerFrame.drawTaskElements_ForAllTasks(dframe)
                ## gaze point
                cv2.circle(dframe, (eyes.x, eyes.y), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking
                ## text
                cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
                drawControls(dframe)
                cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
                drawControls(dframe)
                cv2.imshow("frame", scaleImage(dframe, windowSize))

                # controls
                pressedKey = cv2.waitKey(1) & 0xFF
                if pressedKey == ord('q'):  # close
                    out_q.put(_sentinel)
                    cv2.destroyWindow("frame")
                    break

    except KeyboardInterrupt:
        pass
    finally:
        cv2.destroyAllWindows()


def openLiveWebcam(camIndex, videoRes, windowSize, userId, httpRequestPostUrl, out_q):
    # camera settings
    capture = cv2.VideoCapture(camIndex, cv2.CAP_DSHOW)
    xRes, yRes = videoRes
    capture.set(3, xRes)  # ID bei set für die Breite ist 3
    capture.set(4, yRes)
    res_x = int(capture.get(3))
    res_y = int(capture.get(4))
    if res_x == 0 and res_y == 0:
        tk.messagebox.showinfo("Camera Index", "No Camera connected at this index.")
        return

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # timestamps
    startTimestamp_raw = time.time()  # in seconds but float
    startTimestamp_ms = int(startTimestamp_raw * 1000)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        if not success:
            tk.messagebox.showinfo("Camera", "The camera is disconnected.")
            capture.release()
            cv2.destroyWindow("frame")
            return
        dframe = frame.copy()

        # Timestamps
        cTimestamp_raw = time.time()  # in seconds but float
        cTimestamp_ms = int(cTimestamp_raw * 1000)

        frameNumber += 1
        frameTimeline.append(frameNumber)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        tasksPerFrame = TasksPerFrame(frameNumber)
        tasksPerFrame.decodeFrame(decodedFrame, cTimestamp_ms)

        # AruCo-Scanner
        tasksPerFrame.decodeArucoMarkers(frame, dframe)
        tasksPerFrame.assignArucosToTasks()
        tasksPerFrame.createTaskElementPerFrames_ForAllTasks()

        # Sind Tasks fokussiert?
        tasksPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(tasksPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)
        timeline.importTasksPerFrame(tasksPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

        # Zeichne
        tasksPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        tasksPerFrame.drawRect_ForAllTasks(dframe)
        tasksPerFrame.drawRectPoint_ForAllTasks(dframe)
        tasksPerFrame.drawID_ForAllTasks(dframe)

        tasksPerFrame.drawArucos_ForAllTasks(dframe)
        tasksPerFrame.drawTaskElements_ForAllTasks(dframe)

        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowSize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            out_q.put(_sentinel)
            cv2.destroyWindow("frame")
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        def mousePoints(event, x, y, flags, params):
            if event == cv2.EVENT_LBUTTONDOWN:
                eyes.setX(x / (windowSize / 100))
                eyes.setY(y / (windowSize / 100))
                print(x, y)

        cv2.setMouseCallback("frame", mousePoints)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    out_q.put(_sentinel)
    capture.release()


def openPupilDataFishEye2(pupilRecordingFolder, windowSize, userId, httpRequestPostUrl, out_q):
    # camera settings
    capture = cv2.VideoCapture(pupilRecordingFolder.getiMotionsVideoPath())
    res_x = int(capture.get(3))
    res_y = int(capture.get(4))

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme
    gazeTLVFile = GazeTLVFile(pupilRecordingFolder.getiMotionsTLVPath())
    xList = gazeTLVFile.getColumnByName("Gaze2dX")
    yList = gazeTLVFile.getColumnByName("Gaze2dY")
    timestampList = gazeTLVFile.getColumnByName("GazeTimeStamp")
    startTimestamp_ms = pupilRecordingFolder.getiMotions_info()["Start Time (System)"]

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        if not success:
            print("Video has ended.")
            capture.release()
            cv2.destroyWindow("frame")
            break
        dframe = frame.copy()

        # Frames
        frameNumber += 1
        frameTimeline.append(frameNumber)

        # Eyes
        xPos = xList[frameNumber]
        yPos = yList[frameNumber]
        eyes.setX(xPos)
        eyes.setY(yPos)
        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualize eye-gaze

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame,
                                     pupilRecordingFolder.convertPupilTimeToSystemTime(timestampList[frameNumber]))

        # AruCo-Scanner
        taskListPerFrame.decodeArucoMarkers(frame, dframe)
        taskListPerFrame.assignArucosToTasks()
        taskListPerFrame.createTaskElementPerFrames_ForAllTasks()

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)
        cTimestamp_ms = pupilRecordingFolder.convertPupilTimeToSystemTime(timestampList[frameNumber])
        timeline.importTasksPerFrame(taskListPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)

        taskListPerFrame.drawArucos_ForAllTasks(dframe)
        taskListPerFrame.drawTaskElements_ForAllTasks(dframe)

        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowSize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            out_q.put(_sentinel)
            cv2.destroyWindow("frame")
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        def mousePoints(event, x, y, flags, params):
            if event == cv2.EVENT_LBUTTONDOWN:
                eyes.setX(x / (windowSize / 100))
                eyes.setY(y / (windowSize / 100))
                print(x, y)

        cv2.setMouseCallback("frame", mousePoints)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    out_q.put(_sentinel)
    capture.release()
