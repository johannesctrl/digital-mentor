import tkinter as tk
import tkinter.font as font
from tkinter import filedialog, END
import ttClasses
import requests

LARGE_FONT = ("Helvetica", 12)
font_standard = ("Helvetica", 10)


def runRoutine(function, url):
    if checkL2PConnection(url):
        function()
    else:
        return


def checkL2PConnection(url):
    print("checkl2pconnection")
    try:
        r = requests.get(url=url, timeout=5.0)
        if r.ok:
            print("Posting Data to L2P was successful.")
            tk.messagebox.showinfo("L2P Connectivity Check", "Connection to L2P was successful.")
            return True
        else:
            print(r)
            print("Posting Data to L2P wasn't successful.")
            tk.messagebox.showinfo("L2P Connectivity Check", "Connection to L2P wasn't successful.")
    except requests.Timeout:
        tk.messagebox.showinfo("L2P Connectivity Check", "Connection Timeout.")
        return False
    except requests.ConnectionError:
        return False


class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.shared_data = {
            "userId": tk.StringVar(),
            "studyId": tk.StringVar(),
            "httpRequestPostUrl": tk.StringVar(),
            "httpRequestGetUrlCheckConnection": tk.StringVar()
        }

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.fontStyle = font.Font(family='Helvetica')

        self.frames = {}
        for f in (StartPage, LiveWebcam, LivePupilCore, PupilDataFishEyeRecording, Options):
            frame = f(container, self)
            self.frames[f] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, controller):
        frame = self.frames[controller]
        frame.tkraise()


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        lb_header = tk.Label(self, text="Start Page", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, pady=10, padx=10)
        btn_liveWebcam = tk.Button(self, text="Live Webcam (simulation)", font=font_standard, padx=10,
                                   pady=10,
                                   fg="white", width=20, bg="#ffcccc",
                                   command=lambda: controller.show_frame(LiveWebcam))
        btn_liveWebcam.grid(row=1, column=0)

        btn_livePupilCore = tk.Button(self, text="Live PupilCore", font=font_standard, padx=10,
                                      pady=10,
                                      fg="white", width=20, bg="#ffcccc",
                                      command=lambda: controller.show_frame(LivePupilCore))
        btn_livePupilCore.grid(row=2, column=0)

        btn_pupilDataFishEye = tk.Button(self, text="PupilData Fish Eye Recording", font=font_standard, padx=10,
                                         pady=10,
                                         fg="white", width=20, bg="#ffcccc",
                                         command=lambda: controller.show_frame(PupilDataFishEyeRecording))
        btn_pupilDataFishEye.grid(row=3, column=0)

        btn_options = tk.Button(self, text="Options", font=font_standard, padx=10,
                                pady=10,
                                fg="white", width=20, bg="#ffcccc",
                                command=lambda: controller.show_frame(Options))
        btn_options.grid(row=4, column=0)

        btn_close = tk.Button(self, text="Close", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                              command=lambda: self.quit())
        btn_close.grid(row=99, column=0)


class Options(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        lb_header = tk.Label(self, text="Options", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        lb_userId = tk.Label(self, text="UserId", font=font_standard)
        lb_userId.grid(row=1, column=0, pady=10, padx=10)

        entry_userId = tk.Entry(self, textvariable=controller.shared_data["userId"])
        entry_userId.grid(row=1, column=1, pady=10, padx=10)
        entry_userId.insert(0, "999999")

        lb_studyId = tk.Label(self, text="studyId", font=font_standard)
        lb_studyId.grid(row=2, column=0, pady=10, padx=10)

        entry_studyId = tk.Entry(self, textvariable=controller.shared_data["studyId"])
        entry_studyId.grid(row=2, column=1, pady=10, padx=10)
        entry_studyId.insert(0, "Test_study")

        lb_httpRequestPostUrl = tk.Label(self, text="httpRequestPostUrl", font=font_standard)
        lb_httpRequestPostUrl.grid(row=2, column=0, pady=10, padx=10)

        entry_lb_httpRequestPostUrl = tk.Entry(self, textvariable=controller.shared_data["httpRequestPostUrl"])
        entry_lb_httpRequestPostUrl.grid(row=2, column=1, pady=10, padx=10)
        entry_lb_httpRequestPostUrl.insert(0,
                                           "https://las2peer.tech4comp.dbis.rwth-aachen.de/eyeTrackingProxyService/postEyeTracking")

        lb_httpRequestPostUrl = tk.Label(self, text="httpRequestGetUrlCheckL2PConnectivity", font=font_standard)
        lb_httpRequestPostUrl.grid(row=3, column=0, pady=10, padx=10)

        entry_lb_httpRequestGetUrlCheckConnection = tk.Entry(self, textvariable=controller.shared_data[
            "httpRequestGetUrlCheckConnection"])
        entry_lb_httpRequestGetUrlCheckConnection.grid(row=3, column=1, pady=10, padx=10)
        entry_lb_httpRequestGetUrlCheckConnection.insert(0,
                                                         "https://las2peer.tech4comp.dbis.rwth-aachen.de/eyeTrackingProxyService/getCheckConnection")

        btn_checkL2PConnection = tk.Button(self, text="CheckL2PConnection", font=font_standard, padx=10,
                                           pady=10,
                                           fg="white", width=20, bg="#ffcccc",
                                           command=lambda: checkL2PConnection(
                                               url=controller.shared_data["httpRequestGetUrlCheckConnection"].get()
                                           ))
        btn_checkL2PConnection.grid(row=4, column=0)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class LiveWebcam(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        lb_header = tk.Label(self, text="Live Webcam (simulation)", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        lb_camIndex = tk.Label(self, text="Camera Index", font=font_standard)
        lb_camIndex.grid(row=1, column=0, pady=10, padx=10)
        camIndex = tk.IntVar()
        camIndex.set(0)
        entry_camIndex = tk.Entry(self, text=camIndex)
        entry_camIndex.grid(row=1, column=1, pady=10, padx=10)

        lb_windowSize = tk.Label(self, text="Window Size (in %)", font=font_standard)
        lb_windowSize.grid(row=2, column=0, pady=10, padx=10)
        windowSize = tk.IntVar()
        windowSize.set(20)  # default value
        entry_windowSize = tk.Entry(self, text=windowSize)
        entry_windowSize.grid(row=2, column=1, pady=10, padx=10)

        xRes = tk.StringVar()
        xRes.set(1920)  # default value
        entry_xRes = tk.Entry(self, text=xRes)
        entry_xRes.grid(row=3, column=1)
        yRes = tk.StringVar()
        yRes.set(1080)  # default value
        entry_yRes = tk.Entry(self, text=yRes)
        entry_yRes.grid(row=4, column=1)
        videoRes = (int(xRes.get()), int(yRes.get()))

        lb_xRes = tk.Label(self, text="x", font=font_standard)
        lb_xRes.grid(row=3, column=0, pady=10, padx=10)
        lb_yRes = tk.Label(self, text="y", font=font_standard)
        lb_yRes.grid(row=4, column=0, pady=10, padx=10)

        btn_startLiveWebcam = tk.Button(self, text="Start Live Webcam", font=font_standard, padx=10, pady=10,
                                        fg="white",
                                        width=20, bg="#ffcccc",
                                        command=lambda: runRoutine(
                                            lambda: ttClasses.runEyeTracking(
                                                ttClasses.openLiveWebcam,
                                                camIndex.get(),
                                                videoRes,
                                                windowSize.get(),
                                                controller.shared_data["userId"].get(),
                                                controller.shared_data["httpRequestPostUrl"].get()
                                            ), controller.shared_data["httpRequestGetUrlCheckConnection"].get()))
        btn_startLiveWebcam.grid(row=98, column=0, columnspan=2)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class LivePupilCore(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        lb_header = tk.Label(self, text="Live PupilCore", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        lb_windowSize = tk.Label(self, text="Window Size (in %)", font=font_standard)
        lb_windowSize.grid(row=2, column=0, pady=10, padx=10)
        windowSize = tk.IntVar()
        windowSize.set(20)  # default value
        entry_windowSize = tk.Entry(self, text=windowSize)
        entry_windowSize.grid(row=2, column=1, pady=10, padx=10)

        xRes = tk.StringVar()
        xRes.set(1920)  # default value
        entry_xRes = tk.Entry(self, text=xRes)
        entry_xRes.grid(row=3, column=1)
        yRes = tk.StringVar()
        yRes.set(1080)  # default value
        entry_yRes = tk.Entry(self, text=yRes)
        entry_yRes.grid(row=4, column=1)
        videoRes = (int(xRes.get()), int(yRes.get()))

        lb_xRes = tk.Label(self, text="x", font=font_standard)
        lb_xRes.grid(row=3, column=0, pady=10, padx=10)
        lb_yRes = tk.Label(self, text="y", font=font_standard)
        lb_yRes.grid(row=4, column=0, pady=10, padx=10)

        btn_startLiveWebcam = tk.Button(self, text="Start Live Webcam", font=font_standard, padx=10, pady=10,
                                        fg="white",
                                        width=20, bg="#ffcccc",
                                        command=lambda: runRoutine(
                                            lambda: ttClasses.runEyeTracking(
                                                ttClasses.openLivePupilCore,
                                                windowSize.get(),
                                                controller.shared_data["userId"].get(),
                                                controller.shared_data["httpRequestPostUrl"].get()
                                            ), controller.shared_data["httpRequestGetUrlCheckConnection"].get()))
        btn_startLiveWebcam.grid(row=98, column=0, columnspan=2)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class PupilDataFishEyeRecording(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        lb_header = tk.Label(self, text="PupilDataFishEye Recording", font=LARGE_FONT)
        lb_header.grid(row=0, column=0)

        btn_folderPicker = tk.Button(self, text="Pick Folder", padx=10, pady=10, fg="white", width=20, bg="#ffcccc",
                                     command=lambda: self.pdfepcs == self.pupilDataFishEye_pickFolder(
                                         self.pickFolder(), listbox))
        btn_folderPicker.grid(row=1, column=0)

        listbox = tk.Listbox(self, selectmode=tk.MULTIPLE)
        listbox.grid(row=2, column=0)

        btn_startScanning = tk.Button(self, text="Start Scanning", padx=10, pady=10, fg="white", width=20,
                                      bg="#ffcccc",
                                      command=lambda: self.startScanning(listbox))
        btn_startScanning.grid(row=3, column=0)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0)

        # global Variables
        self.pdfepcs = None

    def pickFolder(self):
        folderPath = tk.StringVar()
        folder_selected = filedialog.askdirectory()
        folderPath.set(folder_selected)
        return folderPath.get() + "/"
        # ttClasses.openPupilDataFishEye(folderPath.get() + "/")

    def pupilDataFishEye_pickFolder(self, pathFolderRecording, listbox):
        pupil_rgfs = ttClasses.PupilRecordingGroupFolders(path=pathFolderRecording)
        pupil_rgfs.createPupilRecordingGroupFolders()
        # pupil_rgfs.printRecordings()
        self.pdfepcs = pupil_rgfs.createPaths()
        # listbox.delete(0, listbox.size())
        for pdfepc in self.pdfepcs:
            # print(pdfepc.name)
            listbox.insert(END, pdfepc.name)

    def startScanning(self, listbox):
        print("startScanning")
        for i in listbox.curselection():
            # print(i)
            ep = self.pdfepcs[i].exportsPath
            vp = self.pdfepcs[i].videoPath
            gp = self.pdfepcs[i].gazeTLVPath
            pupilRecordingFolders = self.pdfepcs[i].pupilRecordingFolder
            ttClasses.openPupilDataFishEye(ep, vp, gp, pupilRecordingFolders)


app = App()
app.mainloop()
