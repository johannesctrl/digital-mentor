const sendGetHttpRequest = function(url) {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", url)
        xhr.responseType = "text"
        xhr.onload = function() { //onload -> wenn Response kommt (auch bei "Fail"-Response)
            resolve(xhr.response)
        }
        xhr.onerror = function() { //onerror -> wenn gar keine Response kommt
            reject("No response from the server :-(")
        }
        xhr.send()
    })
    return promise
}

const get_category_level = function(study_id, category_name) {
    const url = "http://127.0.0.1:5000/category-level?study-id=" + String(study_id) + "&category-name=" + String(category_name)
    console.log(url)

    sendGetHttpRequest(url).then(responseData => {
        console.log("success!")
        console.log(responseData)
        math_level = responseData.math
        return math_level
    }).catch(err => {
        console.log("error!")
        console.log(err)
        window.alert(err)
    })
}

const getLevelOfMath = function() {
    let study_id = window.prompt("Bitte trage deine Studien-ID in das Feld.")
    const math_level = get_category_level(study_id, "math")

    window.alert("This student has a math level of " + math_level)
}

const changeVariables = function() {
    const div_inputs = document.createElement("div")
    div_inputs.style.backgroundColor = "orange"
    const div_inputs_html = `
    <form>
        <label for="input01">input01</label><br>
        <input type="text" id="input01" name="input01" class="inputs"><br>
        <label for="input02">input02</label><br>
        <input type="text" id="input02" name="input02" class="inputs">
    </form> 
    `
    div_inputs.innerHTML = div_inputs_html

    const inputs = div_inputs.getElementsByTagName("input")
    console.log(inputs.length)
    console.log("Add EventListeners")
    for (i=0, len=inputs.length; i<len; i++) {
        inputs[i].addEventListener('input', function() {
            myTimer()
        }, true)
    }
    const item_container = document.getElementsByClassName("item-container")[0]
    item_container.appendChild(div_inputs)
    myTimer()
}

window.onload = function() {
    changeVariables()
    getLevelOfMath()
}

const myTimer = function() {
    const a = document.getElementById("input01")
    const b = document.getElementById("input02")
    xdw_a = document.getElementById("xdw_a").getElementsByTagName('input')[0]
    xdw_a.value = parseInt(a.value)
    xdw_a.readOnly = true

    xdw_b = document.getElementById("xdw_b").getElementsByTagName('input')[0] 
    xdw_b.value = parseInt(b.value)
    xdw_b.readOnly = true
}

