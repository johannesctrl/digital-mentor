var check = function(){

	iframeIsLoaded = false
    var body = document.body;

	if(body != null) {
		iframeIsLoaded = true
	}

    if(iframeIsLoaded){ //Das iFrame ist jetzt geladen. Alles was man nun coden möchte muss in diesen if-Zweig.
        console.log("iframe geladen");

        var item = String(document.getElementsByClassName("item-container")[0].innerHTML);
        console.log(item);
        
        var start = item.search("<span>Erreicht: ") + 6; //<span> besteht aus 6 Zeichen
        var end = item.slice(start, -1).search(/(?:\r\n|\r|\n)/g) + start; //Findet den ersten Zeilenumbruch
        resultString = item.slice(start, end);

        resultArray = resultString.split(" ");

        console.log("ResultArray" + resultArray);
        console.log(resultArray[0]);

        if(resultArray[0] != "Erreicht:"){ //Check ob wir uns auf der Auswertungsseite befinden und nicht auf der Frage-Stellungsseite, denn das JavaScript-Script wird auf beiden Seiten aufgerufen
            alert("Wir befinden uns noch auf der Frage-Stellungsseite. Deshalb gibt es hier noch keinen Score.")
        }else{
            scoredPoints = resultArray[1];
            totalPoints = resultArray[3];

            console.log("Start: " + start);
            console.log("End: ", end);
            alert("Sie haben " + scoredPoints + " von " + totalPoints + " erreicht.");
        }
        

        console.log("Code finished");
    }
    else {
        console.log("iframe ist noch nicht geladen.");
        setTimeout(check, 500); // check again in a half a second
        //Dies ist ein rekursiver Aufruf: Check so häufig ob du ein HTML-Body-Element findest bis du es findest und somit das iFrame der Aufgabe geladen ist, denn erst dann ist es möglich Daten aus der HTML zu extrahieren.
    }
}

check();