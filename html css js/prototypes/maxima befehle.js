(
    is (
        equal ({LEARNERRESPONSE_GAP_1}, {a})
    ) and
    is (
        equal({LEARNERRESPONSE_GAP_2}, {b})
    ) and
    is (
        equal({LEARNERRESPONSE_GAP_3}, {c})
    )
);


(
    is (
        equal (
            {LEARNERRESPONSE}, {LEARNERRESPONSE_GAP_1} + {LEARNERRESPONSE_GAP_2}
        )
    )
);


  (
    is (
      equal (
        ev(LEARNERRESPONSE), ev(LEARNERRESPONSE_GAP_2)
      )
    )
  );



{LEARNERRESPONSE_GAP_1} + {LEARNERRESPONSE_GAP_2}