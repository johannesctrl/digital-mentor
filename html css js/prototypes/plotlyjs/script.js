
const getLocalJson = function(url) {
    var obj
    fetch("eyetracking_example.json")
        .then(res => res.json())
        .then(data => {
            drawGraph(data)
            // console.log("Data:")
            console.log("Data:", data)
        })
    return obj
}

class GraphData {
    constructor(timestamp, taskId, taskElementId) {
        this.timestamp = timestamp
        this.taskId = taskId
        this.taskElementId = taskElementId
    }
}

class GraphDataDuration {
    constructor(timestamp, taskId, taskElementId) {
        this.timestamp = timestamp
        this.taskId = taskId
        this.taskElementId = taskElementId
    }
}

class GraphDataDurations {
    constructor() {
        this.list = []
    }
}

class GraphDatas {
    constructor() {
        this.list = []
        this.dict = []
        this.interpolated = []

        this.timestamps = []
        this.taskIds = []
        this.taskElementIds = []
    }
    interpolate() {
        console.log("interpolate2()")
        const key_array = Object.keys(this.dict)
        const first = Object.getOwnPropertyNames(this.dict)[0]
        const last = Object.getOwnPropertyNames(this.dict)[key_array.length-1]
        let last_taskId
        let last_taskElementId 
        for(let i = first; i<last; i++) {
            // console.log(i)
            // console.log(this.dict[i])

            if(this.dict.hasOwnProperty(i)) {
                const timestamp = i
                const taskId = this.dict[i][1]
                const taskElementId = this.dict[i][2]

                this.timestamps.push(new Date(timestamp*1000))
                this.taskIds.push(taskId)
                this.taskElementIds.push(taskElementId)
                
                last_taskId = taskId
                last_taskElementId = taskElementId
                
            } else {
                this.timestamps.push(new Date(i*1000))
                this.taskIds.push(last_taskId)
                this.taskElementIds.push(last_taskElementId)
            }
        }
        console.log(this.timestamps)
        console.log(this.taskIds)
        console.log(this.taskElementIds)
    }
    clock() {
        // Diese Method analysiert die Liste aller GraphData-Instanzen und taktet diese auf eine Sekunde. Falls zwei oder mehr Instanzen,
        // dieselbe Sekunde haben wird die jeweils Ältere von der Neueren überschrieben.
        for(i=0, len=this.list.length; i<len; i++) {
            const graphData = this.list[i]
            // attribute values
            const timestamp = graphData.timestamp
            const taskId = graphData.taskId
            const taskElementId = graphData.taskElementId
            // console.log("tupel:", timestamp, taskId, taskElementId)
            // add to dict
            const key = Math.round(graphData.timestamp / 1000)
            const tupel = [graphData.timestamp, graphData.taskId, graphData.taskElementId]
            this.dict[key] = tupel

            

        }
        console.log("dict:", this.dict)
    }
}

const extractFromJson = function(data) {
    graphDatas = new GraphDatas()
    for(i=0, len=data.inputs.length; i<len; i++) {
        timestamp = new Date(parseInt(data.inputs[i].timestamp))
        taskId = data.inputs[i].taskId
        taskElementId = data.inputs[i].taskElementId
        graphData = new GraphData(timestamp, taskId, taskElementId)
        // console.log(graphData)
        graphDatas.list.push(graphData)
    }
    console.log(graphDatas)
    return graphDatas
}

const extractFromJsonToGraphDataDurations = function(data) {
    graphDataDurations = new GraphDataDurations()
    for(i=0, len=data.inputs.length; i<len-1 ; i++) {
        timestamp_start = new Date(parseInt(data.inputs[i].timestamp))
        timestamp_end = new Date(parseInt(data.inputs[i+1].timestamp))
        timestamp = (timestamp_start, timestamp_end)
        
        taskId_start = data.inputs[i].taskId
        taskId_end = data.inputs[i+1].taskId
        taskId = (taskId_start, taskId_end)

        
        taskElementId_start = data.inputs[i].taskElementId
        taskElementId_end = data.inputs[i+1].taskElementId
        taskElementId = (taskElementId_start, taskElementId_end)

        graphDataduration = new GraphDataDuration(timestamp, taskId, taskElementId)
        console.log(graphDataDurations)
        graphDatas.list.push(graphData)
    }
    console.log("graphData:", graphData)
}

const drawGraph = function(data) {
    const graphDatas = extractFromJson(data)
    graphDatas.clock()
    graphDatas.interpolate()
    let x_eyeFocus_tasks = graphDatas.timestamps
    let y_eyeFocus_tasks = graphDatas.taskIds

    let x_eyeFocus_Elements = graphDatas.timestamps
    let y_eyeFocus_elements = graphDatas.taskElementIds
    
    var data = [
        {
            x: x_eyeFocus_tasks,
            y: y_eyeFocus_tasks,
            mode: "lines+markers",
            name: "Eye Focus Tasks",
            type: "scatter",
        },
        {
            x: x_eyeFocus_Elements,
            y: y_eyeFocus_elements,
            mode: "lines+markers",
            name: "Eye Focus Elements",
            type: "scatter",
        }
    ]
    var layout = { 
        title: "User Timeline",
        font: {size: 12},
        grid: {rows: 1, columns: 2, pattern: "independent"},
        xaxis: {
            title: {
              text: "Time",
              font: {
              }
            },
          },
          yaxis: {
            title: {
                text: "Tasks & Elements",
                font: {
                }
            }
          }
    }
    var config = {
        responsive: true
    }
    Plotly.newPlot("gd_plotly", data, layout, config);

}

getLocalJson()


/*
var obj_chart_months = document.getElementById("myChart1")
data_blue = [1,2,3,4,5]
var chart_months = new Chart(obj_chart_months, {
    type: "line",
    data: {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [
            {
                label: "dataset 1",
                backgroundColor: "rgba(65,105,225,0.5)",
                borderColor: "rgba(65,105,225,1)",
                data: data_blue
            },
            {
                label: "dataset 2",
                backgroundColor: "rgba(255,105,225,0.5)",
                borderColor: "rgba(255,105,225,1)",
                data: [4,3,2,1,7]
            }
        ]
    },
    options: {
        title: {
            text: "Chart Months",
            display: true
        },
        scales: {
            yAxes: {
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return yLabels[value];
                    }
                }
            }
        }
    }
})

var myChartObject2 = document.getElementById("myChart2")
const data_a = [
    {x: 1627825221472, y: 0},
    {x: 1627825233725, y: 1},
    {x: 1627825234658, y: 2},
]

const data_b = [
    {x:new Date(15-04-2018),y:10},
    {x:new Date(16-04-2018),y:20},
    {x:new Date(17-04-2018),y:30}
   ]

var chart2 = new Chart(myChartObject2, {
    type: 'line',
    data: {
        datasets: [
            {
                label: "dataset 1",
                backgroundColor: "rgba(65,105,225,0.5)",
                borderColor: "rgba(65,105,225,1)",
                data: data_b
            }
        ],
    },
    options: {
        title: {
            text: "Chart 2",
            display: true
        },
        scales: {
            x: {
                type: 'time',
                time: {
                    displayFormats: {hour: "HH:mm"}
                }
            }
        }
    }
})
*/