let win

const main = function() {
    createOverlay()
    window.prompt("Bitte trage deine User-ID in das Feld. Die User-ID bleibt so lange gespeichert, wie der Tab offen bleibt.")
    getStudyId("http://127.0.0.1:5000/postStudyId")
}

const createOverlay = function() {

    // overlay
    const div_overlay = document.createElement("div")
    document.body.appendChild(div_overlay)
    div_overlay.setAttribute("id", "overlay")
    const overlay_style = `
        #overlay {
            position: fixed; /* Sit on top of the page content */
            display: block; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
        }
        h2 {
            color: blue;
        }
        .oAuthButton {
            width: 20%;
            padding: 10px;
            background-color: pink;

            margin: auto;
            position: absolute;
            left: 50%;
            top: 50%;
            -ms-transform: translate(-50%);
            transform: translate(-50%);
        }
    `

    const styleSheet = document.createElement("style")
    styleSheet.setAttribute("type", "text/css")
    styleSheet.innerHTML = overlay_style
    document.head.appendChild(styleSheet)

    // button
    div_overlay.innerHTML = `
        <p id="studyId">studyId = </p>
        <p id="studyId">studyId = </p>
        <p id="studyId">studyId = </p>
        <p id="studyId">studyId = </p>
        <button class="oAuthButton" type="button" onclick="openWindow()">open oAuth login window</button>
        <button type="button" onclick="closeWindow()">close window  </button>
    `
}

const openWindow = function() {
    const url = "http://127.0.0.1:5000/getStudyId"
    win = window.open(url, "_blank", "location=yes, height=570, width=520, scrollbars=yes, status=yes") 
}

const closeWindow = function() {
    win.closeWindow()
}

function on() {
    document.getElementById("overlay").style.display = "block";
}
  
function off() {
    document.getElementById("overlay").style.display = "none";
}

window.onload = function() {
    main()
}

const sendAuthRequest = function(url) {
    console.log("POST...")
    const promise = new Promise((resolve, reject) => {
        var clientId = "johannesalbrechtmail@gmail.com"
        var clientSecret = "asdfasdf"
        // var authorizationBasic = $.base64.btoa(clientId + ':' + clientSecret);
        var authorizationBasic = window.btoa(clientId + ':' + clientSecret)

        const xhr = new XMLHttpRequest()
        xhr.open("POST", url)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        xhr.setRequestHeader('Authorization', 'Basic ' + authorizationBasic);
        xhr.setRequestHeader('Accept', 'application/json')
        xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5000');
        xhr.responseType = "text"
        // xhr.setRequestHeader = "Content-Type", "application/json"
        xhr.setRequestHeader = "Content-Type", "text/plain"
        xhr.onload = function() { //onload -> wenn Response kommt
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() {
            reject("Keine Antwort vom Server :-(")
        }
        xhr.send("payload test")
        // xhr.send("username=johannesalbrechtmail@gmail.com&password=asdfasdf&grant_type=password")
    })
    return promise
}

const getStudyId = function(url) {
    sendAuthRequest(url).then(responseData => {
        localStorage.setItem("studyId", responseData)
    }).catch(err => {
        console.log(err)
    })
}
