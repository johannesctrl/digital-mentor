"use strict";
/****************************
 * Importierte Bibliotheken
 ****************************/
exports.__esModule = true;
/*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */
var $ = require("jquery");
var script = document.createElement('script'); 
script.src = '//code.jquery.com/jquery-1.11.0.min.js'; 
document.getElementsByTagName('head')[0].appendChild(script);  
/****************************
 * Variablen
 ****************************/
var start = Date.now();
var radionInputsGlobal = [];
/****************************
 * Methods
 ****************************/
var appendAsFirstChild = function (parent, newChild) {
    parent.before(newChild, parent.firstChild);
};
var createPElement = function (text) {
    var pElement = document.createElement("p");
    var textNode = document.createTextNode("\"" + text + "\"");
    pElement.appendChild(textNode);
    pElement.style.color = "red";
    return pElement;
};
var jsonDownloadEvent = function (exportObj, exportName, clickElement) {
    clickElement.addEventListener("click", function () {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
        var downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download", exportName + ".json");
        document.body.appendChild(downloadAnchorNode);
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    });
};
/****************************
 * Klassen
 ****************************/
var Radios = /** @class */ (function () {
    function Radios() {
        this.radioList = [];
        this.timelineInputs = [];
        this.timelineInputsNumbers = [];
    }
    return Radios;
}());
var Radio = /** @class */ (function () {
    function Radio(element, radioValue, elementName) {
        this.element = element;
        this.pValue = "";
        this.radioValue = radioValue;
        this.elementName = elementName;
        this.inputs = [];
    }
    return Radio;
}());
var RadioInput = /** @class */ (function () {
    function RadioInput(number, timestamp) {
        this.number = number;
        this.pValue = "";
        this.timestamp = timestamp;
    }
    return RadioInput;
}());
var Textfields = /** @class */ (function () {
    function Textfields() {
        this.textfieldList = [];
        this.timelineInputs = [];
    }
    return Textfields;
}());
var Textfield = /** @class */ (function () {
    function Textfield(element, number) {
        this.element = element;
        this.number = number;
        this.inputs = [];
    }
    return Textfield;
}());
var TextfieldInput = /** @class */ (function () {
    function TextfieldInput(currentString, timestamp) {
        this.currentString = currentString;
        this.timestamp = timestamp;
    }
    return TextfieldInput;
}());
var TaskRadio = /** @class */ (function () {
    function TaskRadio() {
        this.answerOptions = [];
    }
    return TaskRadio;
}());
var Tasks = /** @class */ (function () {
    function Tasks(taskList) {
        this.taskList = taskList;
    }
    Tasks.prototype.createJsonForAllTasks = function () {
        console.log("CreateJsonForAllTasks " + this.taskList.length);
        for (var i = 0, len = this.taskList.length; i < len; i++) {
            var task = this.taskList[i];
            task.createJson();
        }
    };
    return Tasks;
}());
var Task = /** @class */ (function () {
    function Task(elementItemContainer) {
        this.elementItemContainer = elementItemContainer;
        this.id = null;
        this.title = "";
        this.typeIndex = "";
        this.typeName = "";
        this.typeId = "";
        this.elementInteractionContainers = this.elementItemContainer.getElementsByClassName("interaction-container");
        this.interactionElement = null;
        this.interaction = [];
        this.radioInteractions = [];
        this.jsonInteractionFormat = null;
    }
    Task.prototype.pPrintTaskInfo = function () {
        var pElement = createPElement(this.title + " | " + this.typeName);
        var element_item_content = this.elementItemContainer.getElementsByClassName("item-content")[0];
        appendAsFirstChild(element_item_content, pElement);
    };
    Task.prototype.createJson = function () {
        console.log("Create single json for " + this.title);
        var jsonObj = {
            "id": this.id,
            "title": this.title,
            "typeIndex": this.typeIndex,
            "typeName": this.typeName,
            "interaction": {
                radios: {
                    timelineInputs: {}
                },
                textfields: {
                    timeline: {}
                }
            }
        };
        // Pack Interaction Tree
        if (this.typeIndex == "0") { // Auswahlaufgabe
            var radios = this.interactionElement;
            console.log("Radio-Timeline zeigen:" + radios.timelineInputs.length);
            // jsonObj.interaction["radios"] = {}
            jsonObj.interaction.radios["timelineInputs"] = {};
            var ti = radios.timelineInputs;
            for (var i = 0, len = radios.timelineInputs.length; i < len; i++) {
                // const input = radios.timelineInputs[i]
                // const pValue = input.pValue
                // const timestamp = input.timestamp
                jsonObj.interaction.radios.timelineInputs = ["testasdf"];
            }
            jsonObj.interaction.radios.timelineInputs = radionInputsGlobal.length;
        }
        if (this.typeIndex == "1") { //Lückentext
            // jsonObj.interaction["textfields"] = {}
            jsonObj.interaction.textfields["timeline"] = {};
            var textfields = this.interactionElement;
            var timelineInputs = textfields.timelineInputs;
            jsonObj.interaction.textfields.timeline = timelineInputs;
            // console.log(textfields.length)
            for (var i = 0, len = textfields.textfieldList.length; i < len; i++) { //iteriere durch Textfields
                console.log("Textfield Number " + i);
                var textfield = textfields[i];
                var textfieldName = "textfield_" + i;
                jsonObj.interaction.textfields[textfieldName] = {
                    "number": textfield.number,
                    "inputs": {}
                };
                jsonObj.interaction.textfields[textfieldName]["inputs"] = textfield.inputs;
                //console.log("ACHTUNG", textfield.inputs.length)
                for (var j = 0, len_1 = textfield.inputs.length; j < len_1; j++) { //iteriere durch inputs eines Textfields
                    var singleInput = textfield.inputs[j];
                    var singleInputValue = [singleInput.string, singleInput.timestamp];
                    jsonObj.interaction[textfieldName]["inputs"] = singleInputValue;
                }
            }
        }
        // Download
        var exportObj = jsonObj;
        var exportName = this.title;
        var downloadAnchorNodeFinish = document.getElementById("finish");
        var forward = document.getElementById("forward");
        jsonDownloadEvent(exportObj, exportName, forward);
        jsonDownloadEvent(exportObj, exportName, downloadAnchorNodeFinish);
    };
    Task.prototype.toString = function () {
        console.log("toString()");
        console.log("title=" + this.title);
        console.log("id=" + this.id);
        console.log("typeName=" + this.typeName);
        console.log("typeId=" + this.typeId);
    };
    Task.prototype.extractEverything = function () {
        this.extractId();
        this.extractTitle();
        this.extractTaskType();
        this.extractInteractionElement();
        // this.extractRadioAnswers()
    };
    Task.prototype.extractId = function () {
        var a_array = this.elementItemContainer.getElementsByTagName("a");
        var id = a_array[0].getAttribute("name");
        if (id.substring(0, 2) == "id") {
            this.id = id;
        }
    };
    Task.prototype.extractTitle = function () {
        var element_content_header = this.elementItemContainer.getElementsByClassName("content-header");
        var element_h2 = element_content_header[0].getElementsByTagName("h2");
        this.title = element_h2[0].textContent;
    };
    Task.prototype.extractTaskType = function () {
        var generalTaskTypes = [
            ["0", "Auswahlaufgabe", "interaction-choice", "0"],
            ["1", "Lückentext | Nummerische Eingabe (Lückentext) | Berechnung | Formelvergleich | Regulärer Ausdruck", "interaction-textentry", "1"],
            ["2", "Freitext | Programmieraufgabe", "interaction-extendedtext", "0"],
            ["3", "Einfache Zuordnung Drag&Drop", "interaction-match", "0"],
            ["4", "Mehrfache Zuordnung (Matrix)", "interaction-matrix", "0"],
            ["5", "Reihenfolge Aufgabe", "interaction-match order", "0"],
            ["6", "Fehlertext", "hottextInteraction", "1"],
            ["7", "Textboxaufgabe", "dropdown inline-input", "1"],
            ["8", "Uploadaufgabe", "uploadwrapper", "0"]
        ];
        var ic = this.elementInteractionContainers[0];
        for (var i = 0, len = generalTaskTypes.length; i < len; i++) { //iteriere durch die Generalliste
            var taskTypeIndex = generalTaskTypes[i][0];
            var taskTypeName = generalTaskTypes[i][1];
            var taskTypeClassName = generalTaskTypes[i][2];
            var taskTypeId = generalTaskTypes[i][3];
            // console.log(taskTypeName, taskTypeClassName, taskTypeId)
            var match = ic.getElementsByClassName(taskTypeClassName);
            console.log("Match mit", this.title, match.length);
            if (match.length > 0) {
                console.log("Aktuell untersuchte task " + this.title + " hat den Klassennamen " + taskTypeClassName);
                // console.log("== 1", "taskTypesName = " + taskTypesName + " taskTypesClassName = " + taskTypesClassName + " taskTypeId=" + taskTypeId + " hinzugefügt.")
                this.typeIndex = taskTypeIndex;
                this.typeName = taskTypeName;
                this.typeId = taskTypeId;
            }
        }
    };
    Task.prototype.extractRadioAnswers = function () {
        console.log("Extracting radio answers ...");
        if (this.typeIndex == "0") {
            var ic = this.elementInteractionContainers[0];
            var element_p_array = ic.getElementsByTagName("p");
            var taskRadio = new TaskRadio();
            for (var i = 0, len = element_p_array.length; i < len; i++) { //iteriere durch alle p-Elemente
                var string = element_p_array[i].innerHTML;
                taskRadio.answerOptions.push(string);
            }
        }
    };
    Task.prototype.extractInteractionElement = function () {
        console.log("Extracting Interaction Elements...");
        if (this.typeIndex == "0") { //Auswahlaufgabe
            var radios = new Radios();
            var element_input_array = this.elementInteractionContainers[0].getElementsByTagName("input");
            for (var i = 0, len = element_input_array.length; i < len; i++) {
                var input = element_input_array[i];
                var elementName = input.getAttribute("name");
                var radioValue = input.getAttribute("value");
                var radio = new Radio(input, radioValue, elementName);
                // pValue zu radio hinzufügen
                var ic = this.elementInteractionContainers[0];
                var element_p_array = ic.getElementsByTagName("p");
                // console.log("Kindelemente Anzahl = " + element_p_array[0].children.length)
                if (element_p_array[0].children.length == 0) {
                    var pValue = this.elementInteractionContainers[0].getElementsByTagName("p")[i].innerHTML;
                    radio.pValue = pValue;
                    console.log(pValue);
                }
                // console.log("new Radio:", i, radioValue, elementName)
                radios.radioList.push(radio); // strukturiert nach radio
            }
            this.interactionElement = radios;
        }
        if (this.typeIndex == "1") { //Lückentext
            var textfields = new Textfields();
            for (var i = 0, len = this.elementInteractionContainers.length; i < len; i++) {
                var element_input = this.elementInteractionContainers[i].getElementsByTagName("input")[0];
                var number = i;
                var textfield = new Textfield(element_input, number);
                // console.log(textfield.number)
                textfields.textfieldList[i] = textfield;
            }
            this.interactionElement = textfields;
            console.log("Number erstes Textfield-Elementes", textfields.textfieldList[0].number);
        }
        console.log("End extracting interaction elements");
    };
    Task.prototype.addListeners = function () {
        console.log("adding listeners...");
        if (this.typeIndex == "0") { // Auswahlaufgabe
            var eventListenerInteractionElement_1 = this.interactionElement;
            var inputNameTag = eventListenerInteractionElement_1.radioList[0].elementName;
            var ic = this.elementInteractionContainers[0];
            var ri = this.radioInteractions;
            // console.log("inputTagName", inputNameTag)
            $("input[name=\"" + inputNameTag + "\"]").change(function () {
                // console.log(this.id)
                var self = this;
                var radios = eventListenerInteractionElement_1;
                var checkNumber = parseInt(self.value.substring(5, self.value.length));
                var checkNumberFirstRadioValue = parseInt(radios.radioList[0].radioValue.substring(5, self.value.length));
                var number = checkNumber - checkNumberFirstRadioValue;
                // console.log("checkNumberFirstRadioValue", checkNumberFirstRadioValue)
                // console.log("this.value", this.value)
                // console.log("checkNumber", checkNumber)
                // console.log("number", number)
                console.log("radios.radios.length", radios.radioList.length);
                // pValue zu radioInput hinzufügen
                /*
                const element_p_array = ic.getElementsByTagName("p")
                console.log(element_p_array.length)
                if(element_p_array.length != 0) {
                    // const pValue = document.querySelectorAll("label[for="+ this.id +"]")[0].getElementsByTagName("p")[0].innerHTML
                    const pValue = "none"
                    radioInput.pValue = pValue
                }
                */
                var radioInput = new RadioInput(number, Date.now() - start);
                radios.timelineInputs.push(radioInput);
                radionInputsGlobal.push([number, Date.now() - start]);
                for (var i = 0, len = radios.timelineInputs.length; i < len; i++) {
                    console.log(radios.timelineInputs[i]);
                }
            });
        }
        if (this.typeIndex == "1") { // Lückentext
            var element_input_array = this.elementItemContainer.getElementsByTagName("input");
            console.log("Die Aufgabe besteht aus " + element_input_array.length + " Eingabefeldern.");
            var _loop_1 = function (i, len) {
                var input = element_input_array[i];
                console.log(input.getAttribute("type"));
                var eventListenerInteractionElement = this_1.interactionElement;
                input.addEventListener("input", function () {
                    var inputNumber = i;
                    var currentValue = input.value;
                    var timestamp = Date.now() - start;
                    // Add current Input-Values to Attribute - without JSON-Formatting
                    // Ordered by textfield
                    var textfields = eventListenerInteractionElement;
                    var textfield = textfields.textfieldList[i];
                    var textfieldInput = new TextfieldInput(currentValue, timestamp);
                    textfield.inputs.push(textfieldInput);
                    // Ordered by time for timelineInputs
                    var timelineInputs = textfields.timelineInputs;
                    console.log("Save to timelineInputs-Attribute: " + inputNumber, currentValue, timestamp);
                    timelineInputs.push([inputNumber, currentValue, timestamp]);
                }, true);
            };
            var this_1 = this;
            //var textfieldList = []
            //this.interaction = textfieldList
            for (var i = 0, len = element_input_array.length; i < len; i++) {
                _loop_1(i, len);
            }
        }
    };
    return Task;
}());
/****************************
 * Executed Code
 ****************************/
var checkMarker_v2 = function () {
    var body = document.body;
    if (body != null) { // Wenn Body des iFrames aufgebaut ist
        console.log("iframe geladen");
        // Create Tasks
        var element_interaction_containers = document.getElementsByClassName("item-container");
        var taskList = [];
        for (var i = 0, len = element_interaction_containers.length; i < len; i++) {
            var task = new Task(element_interaction_containers[i]);
            task.extractEverything();
            task.addListeners();
            // task.pPrintTaskInfo()
            taskList.push(task);
        }
        var tasks = new Tasks(taskList);
        tasks.createJsonForAllTasks();
        // Hide Elements
        var element_btn_testAbschliessen2 = document.getElementsByClassName("btn btn-highlight")[0];
        if (element_btn_testAbschliessen2 != undefined) {
            element_btn_testAbschliessen2.style.visibility = "hidden";
        }
        /*
        const element_navContainer = document.getElementsByTagName("nav")
        console.log("Navigationscontainer-Länge=" + element_navContainer.length)
        if(element_navContainer != undefined) {
            element_navContainer[0].style.visibility = "hidden"
        }
        */
        var element_btn_antwortAbgeben = document.getElementsByClassName("btn btn-outline-secondary");
        for (var i = 0, len = element_btn_antwortAbgeben.length; i < len; i++) {
            element_btn_antwortAbgeben[i].style.visibility = "hidden";
        }
        var element_btn_weiter = document.getElementsByClassName("btn btn-outline-secondary forward");
        if (element_btn_weiter.length > 0) {
            element_btn_weiter[0].style.visibility = "visible";
        }
        console.log("Code finished.");
    }
    else {
        console.log("iframe ist noch nicht geladen.");
        setTimeout(checkMarker_v2, 2500); // check again in a second // this is a recursive call of the method "check"
    }
};
checkMarker_v2();
