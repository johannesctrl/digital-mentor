
const xhr_json = function() {

    var body = document.body

    if(body != null) { // Wenn Body des iFrames aufgebaut ist
        console.log("iframe geladen")

        sendHttpPostRequest = function(url, data) {
            return fetch(url, {
                method: "POST", 
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                },
            }).then(response => {
                if (response.status >= 400) {
                    return response.json().then(errResData => {
                        const error = new Error("Something went wrong!")
                        error.data = errResData
                        throw error
                    })
                }
                return response.json()
            })
        }

        const getData = function() {
            sendHttpRequest("GET", "https://reqres.in/api/users")
                .then(responseData => {
                    console.log(responseData)
                })
        }

        const postData = function() {
            const jsonObj = {
                email: "sydney@fife",
                password: "password"
            }
            sendHttpPostRequest("https://reqres.in/api/register", jsonObj)
                .then(responseData => {
                    console.log(responseData)
                })
                .catch(err => {
                    console.log(err, err.data)
                })
        }

        const addButton = function(innerHTML, func) {
            const element_pageHeader = document.getElementsByClassName("page-header-additional")[0]
            const btn = document.createElement("button")
            btn.setAttribute("type", "button")
            btn.innerHTML = innerHTML
            btn.addEventListener("click", func)
            element_pageHeader.appendChild(btn)
        }
        addButton("GET", getData)
        addButton("POST", postData)
        
    }
    else {
        console.log("iframe ist noch nicht geladen.")
        setTimeout(xhr_json, 100) // check again in a second // this is a recursive call of the method "check"
    }
}

xhr_json()