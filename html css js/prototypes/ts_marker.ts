/****************************
 * Importierte Bibliotheken
 ****************************/

/*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */
import * as $ from "jquery"

/****************************
 * Variablen
 ****************************/

const start = Date.now()
const radionInputsGlobal = []

/****************************
 * Methods
 ****************************/

const appendAsFirstChild = function(parent, newChild) {
    parent.before(newChild, parent.firstChild)
}

const createPElement = function(text) {
    const pElement = document.createElement("p")
    const textNode = document.createTextNode("\"" + text + "\"")
    pElement.appendChild(textNode)
    pElement.style.color = "red"
    return pElement
}


const jsonDownloadEvent = function(exportObj, exportName, clickElement) {
    clickElement.addEventListener("click", function() {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj))
        var downloadAnchorNode = document.createElement('a')
        downloadAnchorNode.setAttribute("href", dataStr)
        downloadAnchorNode.setAttribute("download", exportName + ".json")
        document.body.appendChild(downloadAnchorNode)
        downloadAnchorNode.click()
        downloadAnchorNode.remove()
    })
}

/****************************
 * Klassen
 ****************************/

class Radios {
    radioList: Radio[]
    timelineInputs: any[]
    timelineInputsNumbers: any[]

    constructor() {
        this.radioList = []
        this.timelineInputs = []
        this.timelineInputsNumbers = []
    }
}
class Radio {
    element: Node
    pValue: string
    radioValue: string
    elementName: string
    inputs: any[]

    constructor(element, radioValue, elementName) {
        this.element = element
        this.pValue = ""
        this.radioValue = radioValue
        this.elementName = elementName
        this.inputs = []
    }
}
class RadioInput {
    number: number
    pValue: string
    timestamp: number


    constructor(number, timestamp) {
        this.number = number
        this.pValue = ""
        this.timestamp = timestamp
    }
}

class Textfields {
    textfieldList: Textfield[]
    timelineInputs: any[]

    constructor() {
        this.textfieldList = []
        this.timelineInputs = []
    }
}
class Textfield {
    element: Node
    number: number
    inputs: any[]

    constructor(element, number) {
        this.element = element
        this.number = number
        this.inputs = []
    }
}
class TextfieldInput {
    currentString: string
    timestamp: number

    constructor(currentString, timestamp) {
        this.currentString = currentString
        this.timestamp = timestamp
    }
}

class TaskRadio {
    answerOptions: string[]

    constructor() {
        this.answerOptions = []
    }
}
class Tasks {
    taskList: Task[]

    constructor(taskList) {
        this.taskList = taskList
    }
    createJsonForAllTasks() {
        console.log("CreateJsonForAllTasks " + this.taskList.length)
        for(let i=0, len=this.taskList.length; i<len; i++) {
            const task = this.taskList[i]
            task.createJson()
        }
    }
}

class Task {
    elementItemContainer: Element
    id: string
    title: string
    typeIndex: string
    typeName: string
    typeId: string
    elementInteractionContainers: HTMLCollectionOf<Element>

    interactionElement: unknown
    interaction: any[]
    radioInteractions: any[]
    jsonInteractionFormat: any[]

    constructor(elementItemContainer) {
        this.elementItemContainer = elementItemContainer

        this.id = null
        this.title = ""
        this.typeIndex = ""
        this.typeName = ""
        this.typeId = ""
        this.elementInteractionContainers = this.elementItemContainer.getElementsByClassName("interaction-container")
        
        this.interactionElement = null
        this.interaction = []
        this.radioInteractions = []
        this.jsonInteractionFormat = null
    }
    pPrintTaskInfo() {
        const pElement = createPElement(this.title + " | " + this.typeName)
        const element_item_content = this.elementItemContainer.getElementsByClassName("item-content")[0]
        appendAsFirstChild(element_item_content, pElement)
        
    }
    createJson() {
        console.log("Create single json for " + this.title)

        let jsonObj = {
            "id": this.id,
            "title": this.title,
            "typeIndex": this.typeIndex,
            "typeName": this.typeName,
            "interaction": {
                radios: {
                    timelineInputs: {}
                },
                textfields: {
                    timeline: {}
                }
            }
        }

        // Pack Interaction Tree
        if(this.typeIndex == "0") { // Auswahlaufgabe
            const radios = this.interactionElement as Radios
            console.log("Radio-Timeline zeigen:" + radios.timelineInputs.length)
            // jsonObj.interaction["radios"] = {}
            jsonObj.interaction.radios["timelineInputs"] = {}
            const ti = radios.timelineInputs
            for(let i=0, len=radios.timelineInputs.length; i<len; i++) {
                // const input = radios.timelineInputs[i]
                // const pValue = input.pValue
                // const timestamp = input.timestamp
                jsonObj.interaction.radios.timelineInputs = ["testasdf"]
            }
            jsonObj.interaction.radios.timelineInputs = radionInputsGlobal.length
        }
        if(this.typeIndex == "1") { //Lückentext
            // jsonObj.interaction["textfields"] = {}
            jsonObj.interaction.textfields["timeline"] = {}
            const textfields = this.interactionElement as Textfields
            const timelineInputs = textfields.timelineInputs
            jsonObj.interaction.textfields.timeline = timelineInputs
            
            // console.log(textfields.length)
            for(let i=0, len=textfields.textfieldList.length; i<len; i++) { //iteriere durch Textfields
                console.log("Textfield Number " + i)
                const textfield = textfields[i] as Textfield
                const textfieldName = "textfield_" + i
                jsonObj.interaction.textfields[textfieldName] = {
                    "number": textfield.number,
                    "inputs": {}
                }
                jsonObj.interaction.textfields[textfieldName]["inputs"] = textfield.inputs

                //console.log("ACHTUNG", textfield.inputs.length)
                for(let j=0, len=textfield.inputs.length; j<len; j++) { //iteriere durch inputs eines Textfields
                    const singleInput = textfield.inputs[j]
                    const singleInputValue = [singleInput.string, singleInput.timestamp]
                    jsonObj.interaction[textfieldName]["inputs"] = singleInputValue
                }
            }
        }

        // Download
        const exportObj = jsonObj
        const exportName = this.title
        var downloadAnchorNodeFinish = document.getElementById("finish")
        var forward = document.getElementById("forward")
        jsonDownloadEvent(exportObj, exportName, forward)
        jsonDownloadEvent(exportObj, exportName, downloadAnchorNodeFinish)
    }
    toString() {
        console.log("toString()")
        console.log("title=" + this.title)
        console.log("id=" + this.id)
        console.log("typeName=" + this.typeName)
        console.log("typeId=" + this.typeId)
    }
    extractEverything() {
        this.extractId()
        this.extractTitle()
        this.extractTaskType()
        this.extractInteractionElement()
        // this.extractRadioAnswers()
    }
    extractId() {
        const a_array = this.elementItemContainer.getElementsByTagName("a")
        const id = a_array[0].getAttribute("name")
        if(id.substring(0,2) == "id") {
            this.id = id
        }
    }
    extractTitle() {
        const element_content_header = this.elementItemContainer.getElementsByClassName("content-header")
        const element_h2 = element_content_header[0].getElementsByTagName("h2")
        this.title = element_h2[0].textContent
    }
    extractTaskType() {
        const generalTaskTypes = [
            ["0", "Auswahlaufgabe", "interaction-choice", "0"],
            ["1", "Lückentext | Nummerische Eingabe (Lückentext) | Berechnung | Formelvergleich | Regulärer Ausdruck", "interaction-textentry", "1"],
            ["2", "Freitext | Programmieraufgabe", "interaction-extendedtext", "0"],
            ["3", "Einfache Zuordnung Drag&Drop", "interaction-match", "0"],
            ["4", "Mehrfache Zuordnung (Matrix)", "interaction-matrix", "0"],
            ["5", "Reihenfolge Aufgabe", "interaction-match order", "0"],
            ["6", "Fehlertext", "hottextInteraction", "1"],
            ["7", "Textboxaufgabe", "dropdown inline-input", "1"],
            ["8", "Uploadaufgabe", "uploadwrapper", "0"]
        ]
        const ic = this.elementInteractionContainers[0]
        for (let i=0, len=generalTaskTypes.length; i<len; i++) { //iteriere durch die Generalliste
            const taskTypeIndex = generalTaskTypes[i][0]
            const taskTypeName = generalTaskTypes[i][1]
            const taskTypeClassName = generalTaskTypes[i][2]
            const taskTypeId = generalTaskTypes[i][3]
            // console.log(taskTypeName, taskTypeClassName, taskTypeId)

            const match = ic.getElementsByClassName(taskTypeClassName)
            console.log("Match mit", this.title, match.length)
            if(match.length > 0) {
                console.log("Aktuell untersuchte task " + this.title + " hat den Klassennamen " + taskTypeClassName)
                // console.log("== 1", "taskTypesName = " + taskTypesName + " taskTypesClassName = " + taskTypesClassName + " taskTypeId=" + taskTypeId + " hinzugefügt.")
                this.typeIndex = taskTypeIndex
                this.typeName = taskTypeName
                this.typeId = taskTypeId
            }
        }
    }
    extractRadioAnswers() {
        console.log("Extracting radio answers ...")
        if(this.typeIndex == "0") {
            const ic = this.elementInteractionContainers[0]
            const element_p_array = ic.getElementsByTagName("p")
            const taskRadio = new TaskRadio()

            for(let i=0, len=element_p_array.length; i<len; i++) { //iteriere durch alle p-Elemente
                const string = element_p_array[i].innerHTML
                taskRadio.answerOptions.push(string)
            }
        }
    }
    extractInteractionElement() {
        console.log("Extracting Interaction Elements...")
        if(this.typeIndex == "0") { //Auswahlaufgabe
            const radios = new Radios()
            const element_input_array = this.elementInteractionContainers[0].getElementsByTagName("input")
            for(let i=0, len=element_input_array.length; i<len; i++) {
                const input = element_input_array[i]
                const elementName = input.getAttribute("name")
                const radioValue = input.getAttribute("value")
                const radio = new Radio(input, radioValue, elementName)
                
                // pValue zu radio hinzufügen
                const ic = this.elementInteractionContainers[0]
                const element_p_array = ic.getElementsByTagName("p")
                // console.log("Kindelemente Anzahl = " + element_p_array[0].children.length)
                if(element_p_array[0].children.length == 0) {
                    const pValue = this.elementInteractionContainers[0].getElementsByTagName("p")[i].innerHTML
                    radio.pValue = pValue
                    console.log(pValue)
                }
                
                // console.log("new Radio:", i, radioValue, elementName)
                radios.radioList.push(radio) // strukturiert nach radio
            }
            this.interactionElement = radios
        }
        if(this.typeIndex == "1") { //Lückentext
            const textfields = new Textfields()
            for(let i=0, len=this.elementInteractionContainers.length; i<len; i++) {
                const element_input = this.elementInteractionContainers[i].getElementsByTagName("input")[0]
                const number = i
                const textfield = new Textfield(element_input, number)
                // console.log(textfield.number)
                textfields.textfieldList[i] = textfield
            }
            this.interactionElement = textfields as Textfields
            console.log("Number erstes Textfield-Elementes", textfields.textfieldList[0].number)
        }
        console.log("End extracting interaction elements")
        
    }
    addListeners() {
        console.log("adding listeners...")

        if(this.typeIndex == "0") { // Auswahlaufgabe

            const eventListenerInteractionElement: Radios = this.interactionElement as Radios
            const inputNameTag = eventListenerInteractionElement.radioList[0].elementName
            const ic = this.elementInteractionContainers[0]
            const ri = this.radioInteractions
            // console.log("inputTagName", inputNameTag)
            $("input[name=\"" + inputNameTag + "\"]").change(function() {
                // console.log(this.id)
                let self = this as HTMLInputElement
                const radios = eventListenerInteractionElement
                const checkNumber = parseInt(self.value.substring(5, self.value.length))
                const checkNumberFirstRadioValue = parseInt(radios.radioList[0].radioValue.substring(5, self.value.length))
                const number = checkNumber - checkNumberFirstRadioValue
                // console.log("checkNumberFirstRadioValue", checkNumberFirstRadioValue)
                // console.log("this.value", this.value)
                // console.log("checkNumber", checkNumber)
                // console.log("number", number)

                console.log("radios.radios.length", radios.radioList.length)

                // pValue zu radioInput hinzufügen
                /*
                const element_p_array = ic.getElementsByTagName("p")
                console.log(element_p_array.length)
                if(element_p_array.length != 0) {
                    // const pValue = document.querySelectorAll("label[for="+ this.id +"]")[0].getElementsByTagName("p")[0].innerHTML
                    const pValue = "none"
                    radioInput.pValue = pValue
                }
                */

                const radioInput = new RadioInput(number, Date.now() - start)
                radios.timelineInputs.push(radioInput)
                radionInputsGlobal.push([number, Date.now() - start])

                for(let i=0, len=radios.timelineInputs.length; i<len; i++) {
                    console.log(radios.timelineInputs[i])
                }
            })
        }
        
        if(this.typeIndex == "1") { // Lückentext
            const element_input_array = this.elementItemContainer.getElementsByTagName("input")
            console.log("Die Aufgabe besteht aus " + element_input_array.length + " Eingabefeldern.")
            
            //var textfieldList = []
            //this.interaction = textfieldList
            for(let i=0, len = element_input_array.length; i<len; i++) {
                const input = element_input_array[i]
                console.log(input.getAttribute("type"))
                const eventListenerInteractionElement = this.interactionElement

                input.addEventListener("input", function() {
                    const inputNumber = i
                    const currentValue = input.value
                    const timestamp = Date.now() - start
                    
                    // Add current Input-Values to Attribute - without JSON-Formatting
                    // Ordered by textfield
                    const textfields = eventListenerInteractionElement as Textfields
                    const textfield = textfields.textfieldList[i]
                    const textfieldInput = new TextfieldInput(currentValue, timestamp)
                    textfield.inputs.push(textfieldInput)

                    // Ordered by time for timelineInputs
                    const timelineInputs = textfields.timelineInputs
                    console.log("Save to timelineInputs-Attribute: " + inputNumber, currentValue, timestamp)
                    timelineInputs.push([inputNumber, currentValue, timestamp])
                }, true)
            }
        }
    }
}

/****************************
 * Executed Code
 ****************************/

const checkMarker_v2 = function() {

    var body = document.body

    if(body != null) { // Wenn Body des iFrames aufgebaut ist
        console.log("iframe geladen")
        

        // Create Tasks
        const element_interaction_containers = document.getElementsByClassName("item-container")
        const taskList = []
        for (let i=0, len=element_interaction_containers.length; i<len; i++) {
            const task = new Task(element_interaction_containers[i])
            task.extractEverything()
            task.addListeners()
            // task.pPrintTaskInfo()
            taskList.push(task)
        }
        const tasks = new Tasks(taskList)
        tasks.createJsonForAllTasks()


        // Hide Elements
        const element_btn_testAbschliessen2 = document.getElementsByClassName("btn btn-highlight")[0] as HTMLElement
        if(element_btn_testAbschliessen2 != undefined) {
            element_btn_testAbschliessen2.style.visibility = "hidden"
        }
        /*
        const element_navContainer = document.getElementsByTagName("nav")
        console.log("Navigationscontainer-Länge=" + element_navContainer.length)
        if(element_navContainer != undefined) {
            element_navContainer[0].style.visibility = "hidden"
        }
        */
       const element_btn_antwortAbgeben = document.getElementsByClassName("btn btn-outline-secondary") as HTMLCollectionOf<HTMLElement>
       for(let i=0, len=element_btn_antwortAbgeben.length; i<len; i++) {
           element_btn_antwortAbgeben[i].style.visibility = "hidden"
       }
       const element_btn_weiter = document.getElementsByClassName("btn btn-outline-secondary forward") as HTMLCollectionOf<HTMLElement>
       if(element_btn_weiter.length > 0) {
           element_btn_weiter[0].style.visibility = "visible"
       }

        console.log("Code finished.")
    }
    else {
        console.log("iframe ist noch nicht geladen.")
        setTimeout(checkMarker_v2, 2500) // check again in a second // this is a recursive call of the method "check"
    }
}

checkMarker_v2()

