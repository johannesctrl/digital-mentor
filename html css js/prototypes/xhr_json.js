
const xhr_json = function() {

    var body = document.body

    if(body != null) { // Wenn Body des iFrames aufgebaut ist
        console.log("iframe geladen")

        sendGetHttpRequest = function(url) {
            console.log("GET...")
            const promise = new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest()
                xhr.open("GET", url)
                xhr.responseType = "json"
                xhr.onload = function() { //onload -> wenn Response kommt
                    resolve(xhr.response)
                }
                xhr.onerror = function() {
                    reject("Something went wrong!")
                }
                xhr.send()
            })
            return promise
        }

        const sendPostHttpRequest = function(url, data) {
            console.log("POST...")
            const promise = new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest()
                xhr.open("POST", url)
                xhr.responseType = "json"
                xhr.setRequestHeader = "Content-Type", "application/json"
                xhr.onload = function() { //onload -> wenn Response kommt
                    if(xhr.status >= 400) {
                        reject(xhr.response)
                    }
                    resolve(xhr.response)
                }
                xhr.onerror = function() {
                    reject("Something went wrong!")
                }
                xhr.send(JSON.stringify(data))
            })
            return promise
        }

        const getData = function() {
            sendGetHttpRequest("https://reqres.in/api/users").then(responseData => {
                console.log(responseData)
            }).catch(err => {
                console.log(err)
            })
        }

        const postData = function() {
            const jsonObj = {
                email: "test@test.com",
                password: "password"
            }
            sendPostHttpRequest("https://reqres.in/api/register", jsonObj)
        }

        const element_pageHeader = document.getElementsByClassName("page-header-additional")[0]
        const getBtn = document.createElement("button")
        getBtn.setAttribute("type", "button")
        getBtn.innerHTML = "GET"
        getBtn.addEventListener("click", getData)
        element_pageHeader.appendChild(getBtn)

        const postBtn = document.createElement("button")
        postBtn.setAttribute("type", "button")
        postBtn.innerHTML = "POST"
        postBtn.addEventListener("click", postData)
        element_pageHeader.appendChild(postBtn)


        
    }
    else {
        console.log("iframe ist noch nicht geladen.")
        setTimeout(xhr_json, 100) // check again in a second // this is a recursive call of the method "check"
    }
}

xhr_json()