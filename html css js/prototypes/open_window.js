
const main = function() {
    create_overlay()
}

const create_overlay = function() {

    // overlay
    const div_overlay = document.createElement("div")
    document.body.appendChild(div_overlay)
    div_overlay.setAttribute("id", "overlay")
    const overlay_style = `
        #overlay {
            position: fixed; /* Sit on top of the page content */
            display: block; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
        }
        .oAuthForm {
            width: 20%;
            padding: 10px;
            background-color: pink;
            margin: auto;
            position: absolute;
            left: 50%;
            top: 50%;
            -ms-transform: translate(-50%);
            transform: translate(-50%);
        }
    `

    const styleSheet = document.createElement("style")
    styleSheet.setAttribute("type", "text/css")
    styleSheet.innerHTML = overlay_style
    document.head.appendChild(styleSheet)

    // button
    div_overlay.innerHTML = `
        
    <div class="oAuthForm">
        <label for="email">Email Address</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
        <button type="button" onClick=get_study_id("http://127.0.0.1:5000/postStudyId")>Send</button>
        <button type="button" onClick=open_window("http://127.0.0.1:5000/sign-up")>Create account</button>
    </div>
    `
}

const open_window = function(url) {
    window.open(url, "_blank", "location=yes, height=570, width=520, scrollbars=yes, status=yes") 
}

function on() {
    document.getElementById("overlay").style.display = "block";
}
  
function off() {
    document.getElementById("overlay").style.display = "none";
}

window.onload = function() {
    main()
}

const send_auth_request = function(url) {
    console.log("POST...")
    const promise = new Promise((resolve, reject) => {

        var clientId = document.getElementById('email').value
        var clientSecret = document.getElementById('password').value
        var authorizationBasic = window.btoa(clientId + ':' + clientSecret)

        const xhr = new XMLHttpRequest()
        xhr.open("POST", url)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        xhr.setRequestHeader('Authorization', 'Basic ' + authorizationBasic);
        xhr.setRequestHeader('Accept', 'application/json')
        xhr.setRequestHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5000')
        xhr.setRequestHeader = ('Content-Type', 'text/plain')
        xhr.onload = function() { //onload -> wenn Response kommt
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() {
            reject("Keine Antwort vom Server :-(")
        }
        xhr.send("payload test")
    })
    return promise
}

const get_study_id = function(url) {
    send_auth_request(url).then(responseData => {
        console.log("responseData = ")
        console.log(responseData)
        if(responseData != "false") {
            localStorage.setItem("studyId", responseData)
            off()
        }
    }).catch(err => {
        alert("Wrong login data")
        console.log(err)
    })
}
