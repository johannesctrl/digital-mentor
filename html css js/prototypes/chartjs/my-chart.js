var obj_chart_months = document.getElementById("myChart")
data_blue = [1,2,3,4,5]
var chart_months = new Chart(obj_chart_months, {
    type: "line",
    data: {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [
            {
                label: "dataset 1",
                backgroundColor: "rgba(65,105,225,0.5)",
                borderColor: "rgba(65,105,225,1)",
                data: data_blue
            },
            {
                label: "dataset 2",
                backgroundColor: "rgba(255,105,225,0.5)",
                borderColor: "rgba(255,105,225,1)",
                data: [4,3,2,1,7]
            }
        ]
    },
    options: {
        title: {
            text: "Chart Months",
            display: true
        },
        scales: {
            yAxes: {
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return yLabels[value];
                    }
                }
            }
        }
    }
})

var myChartObject2 = document.getElementById("myChart2")
var myChartObject3 = document.getElementById("myChart3")
const data = [
    {x: 1627825221472, y: 1},
    {x: 1627825233725, y: 1},
    {x: 1627825234658, y: 2},
]

const data2 = [
    {x:new date(15-04-2018),y:10},
    {x:new date(16-04-2018),y:20},
    {x:new date(17-04-2018),y:30}
   ]

var chart2 = new Chart(myChartObject2, {
    type: 'line',
    data: [1,2,3,4,5],
    options: {
        title: {
            text: "Chart 2",
            display: true
        },
        scales: {
            x: {
                type: 'time',
                time: {
                    unit: 'second'
                }
            }
        }
    }
})

