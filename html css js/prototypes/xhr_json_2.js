
const xhr_json = function() {

    console.log("iframe geladen")

    const jsonData = {
        "userId":"999999",
        "studyName":"test_study",
        
        "fragebogenTitle":"Aufgabensammlung",
        "taskId":"idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
        "taskTitle":"no title",
        "taskTypeIndex":1,
        "taskElementId":1,

        "inputFieldIndex":1,
        "taskInputValue":"check0",
        
        "timestamp":"1627565441592",
        "startTimestamp":"1627565441591",
        "jsonVersion":1,
    }

    sendGetHttpRequest = function(url) {
        console.log("GET...")
        const promise = new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest()
            xhr.open("GET", url)
            xhr.responseType = "text"
            xhr.onload = function() { //onload -> wenn Response kommt (auch bei "Fail"-Response)
                resolve(xhr.response)
            }
            xhr.onerror = function() { //onerror -> wenn gar keine Response kommt
                reject("No response from the server :-(")
            }
            xhr.send()
        })
        return promise
    }

    const sendPostHttpRequest = function(url, data) {
        console.log("POST...")
        const promise = new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest()
            xhr.open("POST", url)
            xhr.responseType = "text"
            // xhr.setRequestHeader = "Content-Type", "application/json"
            xhr.setRequestHeader = "Content-Type", "text/plain"
            xhr.onload = function() { //onload -> wenn Response kommt
                if(xhr.status >= 400) {
                    reject(xhr.response)
                }
                resolve(xhr.response)
            }
            xhr.onerror = function() {
                reject("Keine Antwort vom Server :-(")
            }
            xhr.send(JSON.stringify(data))
        })
        return promise
    }

    const getData = function(url) {
        sendGetHttpRequest(url).then(responseData => {
            console.log(responseData)
        }).catch(err => {
            console.log(err)
        })
    }

    const postData = function(url, data) {
        sendPostHttpRequest(url, data)
    }

    const element_pageHeader = document.getElementsByClassName("page-header-additional")[0]

    const getLocalBtn = document.createElement("button")
    getLocalBtn.setAttribute("type", "button")
    getLocalBtn.innerHTML = "GET Localhost"
    getLocalBtn.addEventListener("click", function() {
        getData("http://localhost:8080/uitProxy/get")
    })
    element_pageHeader.appendChild(getLocalBtn)

    const getL2PBtn = document.createElement("button")
    getL2PBtn.setAttribute("type", "button")
    getL2PBtn.innerHTML = "GET L2P"
    getL2PBtn.addEventListener("click", function() {
        getData("https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/get")
    })
    element_pageHeader.appendChild(getL2PBtn)

    const postLocalBtn = document.createElement("button")
    postLocalBtn.setAttribute("type", "button")
    postLocalBtn.innerHTML = "POST Localhost"
    postLocalBtn.addEventListener("click", function() {
        postData("http://localhost:8080/uitProxy/postInputs", jsonData)
    })
    element_pageHeader.appendChild(postLocalBtn)

    const postL2PBtn = document.createElement("button")
    postL2PBtn.setAttribute("type", "button")
    postL2PBtn.innerHTML = "POST L2P"
    postL2PBtn.addEventListener("click", function() {
        postData("https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postInputs", jsonData)
    })
    element_pageHeader.appendChild(postL2PBtn)
}

window.onload = function() {
xhr_json()
}
