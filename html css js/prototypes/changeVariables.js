
const changeVariables = function() {
    const div_inputs = document.createElement("div")
    div_inputs.style.backgroundColor = "orange"
    const div_inputs_html = `
    <form>
        <label for="input01">input01</label><br>
        <input type="text" id="input01" name="input01" class="inputs"><br>
        <label for="input02">input02</label><br>
        <input type="text" id="input02" name="input02" class="inputs">
    </form> 
    `
    div_inputs.innerHTML = div_inputs_html

    const inputs = div_inputs.getElementsByTagName("input")
    console.log(inputs.length)
    console.log("Add EventListeners")
    for (i=0, len=inputs.length; i<len; i++) {
        inputs[i].addEventListener('input', function() {
            myTimer()
        }, true)
    }
    const item_container = document.getElementsByClassName("item-container")[0]
    item_container.appendChild(div_inputs)
    myTimer()
}

window.onload = function() {
    changeVariables()
}

const myTimer = function() {
    const a = document.getElementById("input01")
    const b = document.getElementById("input02")
    const xdw_a = document.getElementById("xdw_a").getElementsByTagName('input')[0]
    xdw_a.value = parseInt(a.value)
    xdw_a.readOnly = true

    const xdw_b = document.getElementById("xdw_b").getElementsByTagName('input')[0] 
    xdw_b.value = parseInt(b.value)
    xdw_b.readOnly = true
}

