const get_category_level_now = function() {
    const url = "http://127.0.0.1:5000/category-level?study-id=999999&category-name=math"
    fetch(url)
        .then(res => res.json())
        .then(data => console.log(data))

}

const post_category_level_now = function() {    

}

const main = function() {
    const element_pageHeader = document.getElementsByClassName("page-header-additional")[0]

    const get_category_level = document.createElement("button")
    get_category_level.setAttribute("type", "button")
    get_category_level.innerHTML = "GET category level"
    get_category_level.addEventListener("click", function() {
        get_category_level_now()
    })
    element_pageHeader.appendChild(get_category_level)

    const post_category_level = document.createElement("button")
    post_category_level.setAttribute("type", "button")
    post_category_level.innerHTML = "POST category level"
    post_category_level.addEventListener("click", function() {
        post_category_level_now()
    })
    element_pageHeader.appendChild(post_category_level)
  
}


window.onload = function() {
    main()
}
    
