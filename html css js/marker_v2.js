/****************************
 * Importierte Bibliotheken
 ****************************/

/*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(C,e){"use strict";var t=[],r=Object.getPrototypeOf,s=t.slice,g=t.flat?function(e){return t.flat.call(e)}:function(e){return t.concat.apply([],e)},u=t.push,i=t.indexOf,n={},o=n.toString,v=n.hasOwnProperty,a=v.toString,l=a.call(Object),y={},m=function(e){return"function"==typeof e&&"number"!=typeof e.nodeType},x=function(e){return null!=e&&e===e.window},E=C.document,c={type:!0,src:!0,nonce:!0,noModule:!0};function b(e,t,n){var r,i,o=(n=n||E).createElement("script");if(o.text=e,t)for(r in c)(i=t[r]||t.getAttribute&&t.getAttribute(r))&&o.setAttribute(r,i);n.head.appendChild(o).parentNode.removeChild(o)}function w(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?n[o.call(e)]||"object":typeof e}var f="3.5.1",S=function(e,t){return new S.fn.init(e,t)};function p(e){var t=!!e&&"length"in e&&e.length,n=w(e);return!m(e)&&!x(e)&&("array"===n||0===t||"number"==typeof t&&0<t&&t-1 in e)}S.fn=S.prototype={jquery:f,constructor:S,length:0,toArray:function(){return s.call(this)},get:function(e){return null==e?s.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=S.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return S.each(this,e)},map:function(n){return this.pushStack(S.map(this,function(e,t){return n.call(e,t,e)}))},slice:function(){return this.pushStack(s.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},even:function(){return this.pushStack(S.grep(this,function(e,t){return(t+1)%2}))},odd:function(){return this.pushStack(S.grep(this,function(e,t){return t%2}))},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(0<=n&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:u,sort:t.sort,splice:t.splice},S.extend=S.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||m(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(S.isPlainObject(r)||(i=Array.isArray(r)))?(n=a[t],o=i&&!Array.isArray(n)?[]:i||S.isPlainObject(n)?n:{},i=!1,a[t]=S.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},S.extend({expando:"jQuery"+(f+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==o.call(e))&&(!(t=r(e))||"function"==typeof(n=v.call(t,"constructor")&&t.constructor)&&a.call(n)===l)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e,t,n){b(e,{nonce:t&&t.nonce},n)},each:function(e,t){var n,r=0;if(p(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},makeArray:function(e,t){var n=t||[];return null!=e&&(p(Object(e))?S.merge(n,"string"==typeof e?[e]:e):u.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:i.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var r,i,o=0,a=[];if(p(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&a.push(i);return g(a)},guid:1,support:y}),"function"==typeof Symbol&&(S.fn[Symbol.iterator]=t[Symbol.iterator]),S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){n["[object "+t+"]"]=t.toLowerCase()});var d=function(n){var e,d,b,o,i,h,f,g,w,u,l,T,C,a,E,v,s,c,y,S="sizzle"+1*new Date,p=n.document,k=0,r=0,m=ue(),x=ue(),A=ue(),N=ue(),D=function(e,t){return e===t&&(l=!0),0},j={}.hasOwnProperty,t=[],q=t.pop,L=t.push,H=t.push,O=t.slice,P=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},R="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",I="(?:\\\\[\\da-fA-F]{1,6}"+M+"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",W="\\["+M+"*("+I+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+I+"))|)"+M+"*\\]",F=":("+I+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+W+")*)|.*)\\)|)",B=new RegExp(M+"+","g"),$=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),_=new RegExp("^"+M+"*,"+M+"*"),z=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp(M+"|>"),X=new RegExp(F),V=new RegExp("^"+I+"$"),G={ID:new RegExp("^#("+I+")"),CLASS:new RegExp("^\\.("+I+")"),TAG:new RegExp("^("+I+"|[*])"),ATTR:new RegExp("^"+W),PSEUDO:new RegExp("^"+F),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+R+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/HTML$/i,Q=/^(?:input|select|textarea|button)$/i,J=/^h\d$/i,K=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ee=/[+~]/,te=new RegExp("\\\\[\\da-fA-F]{1,6}"+M+"?|\\\\([^\\r\\n\\f])","g"),ne=function(e,t){var n="0x"+e.slice(1)-65536;return t||(n<0?String.fromCharCode(n+65536):String.fromCharCode(n>>10|55296,1023&n|56320))},re=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ie=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},oe=function(){T()},ae=be(function(e){return!0===e.disabled&&"fieldset"===e.nodeName.toLowerCase()},{dir:"parentNode",next:"legend"});try{H.apply(t=O.call(p.childNodes),p.childNodes),t[p.childNodes.length].nodeType}catch(e){H={apply:t.length?function(e,t){L.apply(e,O.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function se(t,e,n,r){var i,o,a,s,u,l,c,f=e&&e.ownerDocument,p=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==p&&9!==p&&11!==p)return n;if(!r&&(T(e),e=e||C,E)){if(11!==p&&(u=Z.exec(t)))if(i=u[1]){if(9===p){if(!(a=e.getElementById(i)))return n;if(a.id===i)return n.push(a),n}else if(f&&(a=f.getElementById(i))&&y(e,a)&&a.id===i)return n.push(a),n}else{if(u[2])return H.apply(n,e.getElementsByTagName(t)),n;if((i=u[3])&&d.getElementsByClassName&&e.getElementsByClassName)return H.apply(n,e.getElementsByClassName(i)),n}if(d.qsa&&!N[t+" "]&&(!v||!v.test(t))&&(1!==p||"object"!==e.nodeName.toLowerCase())){if(c=t,f=e,1===p&&(U.test(t)||z.test(t))){(f=ee.test(t)&&ye(e.parentNode)||e)===e&&d.scope||((s=e.getAttribute("id"))?s=s.replace(re,ie):e.setAttribute("id",s=S)),o=(l=h(t)).length;while(o--)l[o]=(s?"#"+s:":scope")+" "+xe(l[o]);c=l.join(",")}try{return H.apply(n,f.querySelectorAll(c)),n}catch(e){N(t,!0)}finally{s===S&&e.removeAttribute("id")}}}return g(t.replace($,"$1"),e,n,r)}function ue(){var r=[];return function e(t,n){return r.push(t+" ")>b.cacheLength&&delete e[r.shift()],e[t+" "]=n}}function le(e){return e[S]=!0,e}function ce(e){var t=C.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function fe(e,t){var n=e.split("|"),r=n.length;while(r--)b.attrHandle[n[r]]=t}function pe(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function de(t){return function(e){return"input"===e.nodeName.toLowerCase()&&e.type===t}}function he(n){return function(e){var t=e.nodeName.toLowerCase();return("input"===t||"button"===t)&&e.type===n}}function ge(t){return function(e){return"form"in e?e.parentNode&&!1===e.disabled?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&ae(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function ve(a){return le(function(o){return o=+o,le(function(e,t){var n,r=a([],e.length,o),i=r.length;while(i--)e[n=r[i]]&&(e[n]=!(t[n]=e[n]))})})}function ye(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}for(e in d=se.support={},i=se.isXML=function(e){var t=e.namespaceURI,n=(e.ownerDocument||e).documentElement;return!Y.test(t||n&&n.nodeName||"HTML")},T=se.setDocument=function(e){var t,n,r=e?e.ownerDocument||e:p;return r!=C&&9===r.nodeType&&r.documentElement&&(a=(C=r).documentElement,E=!i(C),p!=C&&(n=C.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",oe,!1):n.attachEvent&&n.attachEvent("onunload",oe)),d.scope=ce(function(e){return a.appendChild(e).appendChild(C.createElement("div")),"undefined"!=typeof e.querySelectorAll&&!e.querySelectorAll(":scope fieldset div").length}),d.attributes=ce(function(e){return e.className="i",!e.getAttribute("className")}),d.getElementsByTagName=ce(function(e){return e.appendChild(C.createComment("")),!e.getElementsByTagName("*").length}),d.getElementsByClassName=K.test(C.getElementsByClassName),d.getById=ce(function(e){return a.appendChild(e).id=S,!C.getElementsByName||!C.getElementsByName(S).length}),d.getById?(b.filter.ID=function(e){var t=e.replace(te,ne);return function(e){return e.getAttribute("id")===t}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n=t.getElementById(e);return n?[n]:[]}}):(b.filter.ID=function(e){var n=e.replace(te,ne);return function(e){var t="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return t&&t.value===n}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),b.find.TAG=d.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):d.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},b.find.CLASS=d.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&E)return t.getElementsByClassName(e)},s=[],v=[],(d.qsa=K.test(C.querySelectorAll))&&(ce(function(e){var t;a.appendChild(e).innerHTML="<a id='"+S+"'></a><select id='"+S+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&v.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||v.push("\\["+M+"*(?:value|"+R+")"),e.querySelectorAll("[id~="+S+"-]").length||v.push("~="),(t=C.createElement("input")).setAttribute("name",""),e.appendChild(t),e.querySelectorAll("[name='']").length||v.push("\\["+M+"*name"+M+"*="+M+"*(?:''|\"\")"),e.querySelectorAll(":checked").length||v.push(":checked"),e.querySelectorAll("a#"+S+"+*").length||v.push(".#.+[+~]"),e.querySelectorAll("\\\f"),v.push("[\\r\\n\\f]")}),ce(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=C.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&v.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&v.push(":enabled",":disabled"),a.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&v.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),v.push(",.*:")})),(d.matchesSelector=K.test(c=a.matches||a.webkitMatchesSelector||a.mozMatchesSelector||a.oMatchesSelector||a.msMatchesSelector))&&ce(function(e){d.disconnectedMatch=c.call(e,"*"),c.call(e,"[s!='']:x"),s.push("!=",F)}),v=v.length&&new RegExp(v.join("|")),s=s.length&&new RegExp(s.join("|")),t=K.test(a.compareDocumentPosition),y=t||K.test(a.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},D=t?function(e,t){if(e===t)return l=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n||(1&(n=(e.ownerDocument||e)==(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!d.sortDetached&&t.compareDocumentPosition(e)===n?e==C||e.ownerDocument==p&&y(p,e)?-1:t==C||t.ownerDocument==p&&y(p,t)?1:u?P(u,e)-P(u,t):0:4&n?-1:1)}:function(e,t){if(e===t)return l=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e==C?-1:t==C?1:i?-1:o?1:u?P(u,e)-P(u,t):0;if(i===o)return pe(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?pe(a[r],s[r]):a[r]==p?-1:s[r]==p?1:0}),C},se.matches=function(e,t){return se(e,null,null,t)},se.matchesSelector=function(e,t){if(T(e),d.matchesSelector&&E&&!N[t+" "]&&(!s||!s.test(t))&&(!v||!v.test(t)))try{var n=c.call(e,t);if(n||d.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(e){N(t,!0)}return 0<se(t,C,null,[e]).length},se.contains=function(e,t){return(e.ownerDocument||e)!=C&&T(e),y(e,t)},se.attr=function(e,t){(e.ownerDocument||e)!=C&&T(e);var n=b.attrHandle[t.toLowerCase()],r=n&&j.call(b.attrHandle,t.toLowerCase())?n(e,t,!E):void 0;return void 0!==r?r:d.attributes||!E?e.getAttribute(t):(r=e.getAttributeNode(t))&&r.specified?r.value:null},se.escape=function(e){return(e+"").replace(re,ie)},se.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},se.uniqueSort=function(e){var t,n=[],r=0,i=0;if(l=!d.detectDuplicates,u=!d.sortStable&&e.slice(0),e.sort(D),l){while(t=e[i++])t===e[i]&&(r=n.push(i));while(r--)e.splice(n[r],1)}return u=null,e},o=se.getText=function(e){var t,n="",r=0,i=e.nodeType;if(i){if(1===i||9===i||11===i){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=o(e)}else if(3===i||4===i)return e.nodeValue}else while(t=e[r++])n+=o(t);return n},(b=se.selectors={cacheLength:50,createPseudo:le,match:G,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(te,ne),e[3]=(e[3]||e[4]||e[5]||"").replace(te,ne),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||se.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&se.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return G.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=h(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(te,ne).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=m[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&m(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(n,r,i){return function(e){var t=se.attr(e,n);return null==t?"!="===r:!r||(t+="","="===r?t===i:"!="===r?t!==i:"^="===r?i&&0===t.indexOf(i):"*="===r?i&&-1<t.indexOf(i):"$="===r?i&&t.slice(-i.length)===i:"~="===r?-1<(" "+t.replace(B," ")+" ").indexOf(i):"|="===r&&(t===i||t.slice(0,i.length+1)===i+"-"))}},CHILD:function(h,e,t,g,v){var y="nth"!==h.slice(0,3),m="last"!==h.slice(-4),x="of-type"===e;return 1===g&&0===v?function(e){return!!e.parentNode}:function(e,t,n){var r,i,o,a,s,u,l=y!==m?"nextSibling":"previousSibling",c=e.parentNode,f=x&&e.nodeName.toLowerCase(),p=!n&&!x,d=!1;if(c){if(y){while(l){a=e;while(a=a[l])if(x?a.nodeName.toLowerCase()===f:1===a.nodeType)return!1;u=l="only"===h&&!u&&"nextSibling"}return!0}if(u=[m?c.firstChild:c.lastChild],m&&p){d=(s=(r=(i=(o=(a=c)[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===k&&r[1])&&r[2],a=s&&c.childNodes[s];while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if(1===a.nodeType&&++d&&a===e){i[h]=[k,s,d];break}}else if(p&&(d=s=(r=(i=(o=(a=e)[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===k&&r[1]),!1===d)while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if((x?a.nodeName.toLowerCase()===f:1===a.nodeType)&&++d&&(p&&((i=(o=a[S]||(a[S]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]=[k,d]),a===e))break;return(d-=v)===g||d%g==0&&0<=d/g}}},PSEUDO:function(e,o){var t,a=b.pseudos[e]||b.setFilters[e.toLowerCase()]||se.error("unsupported pseudo: "+e);return a[S]?a(o):1<a.length?(t=[e,e,"",o],b.setFilters.hasOwnProperty(e.toLowerCase())?le(function(e,t){var n,r=a(e,o),i=r.length;while(i--)e[n=P(e,r[i])]=!(t[n]=r[i])}):function(e){return a(e,0,t)}):a}},pseudos:{not:le(function(e){var r=[],i=[],s=f(e.replace($,"$1"));return s[S]?le(function(e,t,n,r){var i,o=s(e,null,r,[]),a=e.length;while(a--)(i=o[a])&&(e[a]=!(t[a]=i))}):function(e,t,n){return r[0]=e,s(r,null,n,i),r[0]=null,!i.pop()}}),has:le(function(t){return function(e){return 0<se(t,e).length}}),contains:le(function(t){return t=t.replace(te,ne),function(e){return-1<(e.textContent||o(e)).indexOf(t)}}),lang:le(function(n){return V.test(n||"")||se.error("unsupported lang: "+n),n=n.replace(te,ne).toLowerCase(),function(e){var t;do{if(t=E?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return(t=t.toLowerCase())===n||0===t.indexOf(n+"-")}while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var t=n.location&&n.location.hash;return t&&t.slice(1)===e.id},root:function(e){return e===a},focus:function(e){return e===C.activeElement&&(!C.hasFocus||C.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:ge(!1),disabled:ge(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!b.pseudos.empty(e)},header:function(e){return J.test(e.nodeName)},input:function(e){return Q.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:ve(function(){return[0]}),last:ve(function(e,t){return[t-1]}),eq:ve(function(e,t,n){return[n<0?n+t:n]}),even:ve(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:ve(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:ve(function(e,t,n){for(var r=n<0?n+t:t<n?t:n;0<=--r;)e.push(r);return e}),gt:ve(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=b.pseudos.eq,{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})b.pseudos[e]=de(e);for(e in{submit:!0,reset:!0})b.pseudos[e]=he(e);function me(){}function xe(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function be(s,e,t){var u=e.dir,l=e.next,c=l||u,f=t&&"parentNode"===c,p=r++;return e.first?function(e,t,n){while(e=e[u])if(1===e.nodeType||f)return s(e,t,n);return!1}:function(e,t,n){var r,i,o,a=[k,p];if(n){while(e=e[u])if((1===e.nodeType||f)&&s(e,t,n))return!0}else while(e=e[u])if(1===e.nodeType||f)if(i=(o=e[S]||(e[S]={}))[e.uniqueID]||(o[e.uniqueID]={}),l&&l===e.nodeName.toLowerCase())e=e[u]||e;else{if((r=i[c])&&r[0]===k&&r[1]===p)return a[2]=r[2];if((i[c]=a)[2]=s(e,t,n))return!0}return!1}}function we(i){return 1<i.length?function(e,t,n){var r=i.length;while(r--)if(!i[r](e,t,n))return!1;return!0}:i[0]}function Te(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Ce(d,h,g,v,y,e){return v&&!v[S]&&(v=Ce(v)),y&&!y[S]&&(y=Ce(y,e)),le(function(e,t,n,r){var i,o,a,s=[],u=[],l=t.length,c=e||function(e,t,n){for(var r=0,i=t.length;r<i;r++)se(e,t[r],n);return n}(h||"*",n.nodeType?[n]:n,[]),f=!d||!e&&h?c:Te(c,s,d,n,r),p=g?y||(e?d:l||v)?[]:t:f;if(g&&g(f,p,n,r),v){i=Te(p,u),v(i,[],n,r),o=i.length;while(o--)(a=i[o])&&(p[u[o]]=!(f[u[o]]=a))}if(e){if(y||d){if(y){i=[],o=p.length;while(o--)(a=p[o])&&i.push(f[o]=a);y(null,p=[],i,r)}o=p.length;while(o--)(a=p[o])&&-1<(i=y?P(e,a):s[o])&&(e[i]=!(t[i]=a))}}else p=Te(p===t?p.splice(l,p.length):p),y?y(null,t,p,r):H.apply(t,p)})}function Ee(e){for(var i,t,n,r=e.length,o=b.relative[e[0].type],a=o||b.relative[" "],s=o?1:0,u=be(function(e){return e===i},a,!0),l=be(function(e){return-1<P(i,e)},a,!0),c=[function(e,t,n){var r=!o&&(n||t!==w)||((i=t).nodeType?u(e,t,n):l(e,t,n));return i=null,r}];s<r;s++)if(t=b.relative[e[s].type])c=[be(we(c),t)];else{if((t=b.filter[e[s].type].apply(null,e[s].matches))[S]){for(n=++s;n<r;n++)if(b.relative[e[n].type])break;return Ce(1<s&&we(c),1<s&&xe(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace($,"$1"),t,s<n&&Ee(e.slice(s,n)),n<r&&Ee(e=e.slice(n)),n<r&&xe(e))}c.push(t)}return we(c)}return me.prototype=b.filters=b.pseudos,b.setFilters=new me,h=se.tokenize=function(e,t){var n,r,i,o,a,s,u,l=x[e+" "];if(l)return t?0:l.slice(0);a=e,s=[],u=b.preFilter;while(a){for(o in n&&!(r=_.exec(a))||(r&&(a=a.slice(r[0].length)||a),s.push(i=[])),n=!1,(r=z.exec(a))&&(n=r.shift(),i.push({value:n,type:r[0].replace($," ")}),a=a.slice(n.length)),b.filter)!(r=G[o].exec(a))||u[o]&&!(r=u[o](r))||(n=r.shift(),i.push({value:n,type:o,matches:r}),a=a.slice(n.length));if(!n)break}return t?a.length:a?se.error(e):x(e,s).slice(0)},f=se.compile=function(e,t){var n,v,y,m,x,r,i=[],o=[],a=A[e+" "];if(!a){t||(t=h(e)),n=t.length;while(n--)(a=Ee(t[n]))[S]?i.push(a):o.push(a);(a=A(e,(v=o,m=0<(y=i).length,x=0<v.length,r=function(e,t,n,r,i){var o,a,s,u=0,l="0",c=e&&[],f=[],p=w,d=e||x&&b.find.TAG("*",i),h=k+=null==p?1:Math.random()||.1,g=d.length;for(i&&(w=t==C||t||i);l!==g&&null!=(o=d[l]);l++){if(x&&o){a=0,t||o.ownerDocument==C||(T(o),n=!E);while(s=v[a++])if(s(o,t||C,n)){r.push(o);break}i&&(k=h)}m&&((o=!s&&o)&&u--,e&&c.push(o))}if(u+=l,m&&l!==u){a=0;while(s=y[a++])s(c,f,t,n);if(e){if(0<u)while(l--)c[l]||f[l]||(f[l]=q.call(r));f=Te(f)}H.apply(r,f),i&&!e&&0<f.length&&1<u+y.length&&se.uniqueSort(r)}return i&&(k=h,w=p),c},m?le(r):r))).selector=e}return a},g=se.select=function(e,t,n,r){var i,o,a,s,u,l="function"==typeof e&&e,c=!r&&h(e=l.selector||e);if(n=n||[],1===c.length){if(2<(o=c[0]=c[0].slice(0)).length&&"ID"===(a=o[0]).type&&9===t.nodeType&&E&&b.relative[o[1].type]){if(!(t=(b.find.ID(a.matches[0].replace(te,ne),t)||[])[0]))return n;l&&(t=t.parentNode),e=e.slice(o.shift().value.length)}i=G.needsContext.test(e)?0:o.length;while(i--){if(a=o[i],b.relative[s=a.type])break;if((u=b.find[s])&&(r=u(a.matches[0].replace(te,ne),ee.test(o[0].type)&&ye(t.parentNode)||t))){if(o.splice(i,1),!(e=r.length&&xe(o)))return H.apply(n,r),n;break}}}return(l||f(e,c))(r,t,!E,n,!t||ee.test(e)&&ye(t.parentNode)||t),n},d.sortStable=S.split("").sort(D).join("")===S,d.detectDuplicates=!!l,T(),d.sortDetached=ce(function(e){return 1&e.compareDocumentPosition(C.createElement("fieldset"))}),ce(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||fe("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),d.attributes&&ce(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||fe("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ce(function(e){return null==e.getAttribute("disabled")})||fe(R,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),se}(C);S.find=d,S.expr=d.selectors,S.expr[":"]=S.expr.pseudos,S.uniqueSort=S.unique=d.uniqueSort,S.text=d.getText,S.isXMLDoc=d.isXML,S.contains=d.contains,S.escapeSelector=d.escape;var h=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&S(e).is(n))break;r.push(e)}return r},T=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},k=S.expr.match.needsContext;function A(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var N=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function D(e,n,r){return m(n)?S.grep(e,function(e,t){return!!n.call(e,t,e)!==r}):n.nodeType?S.grep(e,function(e){return e===n!==r}):"string"!=typeof n?S.grep(e,function(e){return-1<i.call(n,e)!==r}):S.filter(n,e,r)}S.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?S.find.matchesSelector(r,e)?[r]:[]:S.find.matches(e,S.grep(t,function(e){return 1===e.nodeType}))},S.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(S(e).filter(function(){for(t=0;t<r;t++)if(S.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)S.find(e,i[t],n);return 1<r?S.uniqueSort(n):n},filter:function(e){return this.pushStack(D(this,e||[],!1))},not:function(e){return this.pushStack(D(this,e||[],!0))},is:function(e){return!!D(this,"string"==typeof e&&k.test(e)?S(e):e||[],!1).length}});var j,q=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(S.fn.init=function(e,t,n){var r,i;if(!e)return this;if(n=n||j,"string"==typeof e){if(!(r="<"===e[0]&&">"===e[e.length-1]&&3<=e.length?[null,e,null]:q.exec(e))||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof S?t[0]:t,S.merge(this,S.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:E,!0)),N.test(r[1])&&S.isPlainObject(t))for(r in t)m(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return(i=E.getElementById(r[2]))&&(this[0]=i,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):m(e)?void 0!==n.ready?n.ready(e):e(S):S.makeArray(e,this)}).prototype=S.fn,j=S(E);var L=/^(?:parents|prev(?:Until|All))/,H={children:!0,contents:!0,next:!0,prev:!0};function O(e,t){while((e=e[t])&&1!==e.nodeType);return e}S.fn.extend({has:function(e){var t=S(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(S.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&S(e);if(!k.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?-1<a.index(n):1===n.nodeType&&S.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(1<o.length?S.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?i.call(S(e),this[0]):i.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(S.uniqueSort(S.merge(this.get(),S(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),S.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return h(e,"parentNode")},parentsUntil:function(e,t,n){return h(e,"parentNode",n)},next:function(e){return O(e,"nextSibling")},prev:function(e){return O(e,"previousSibling")},nextAll:function(e){return h(e,"nextSibling")},prevAll:function(e){return h(e,"previousSibling")},nextUntil:function(e,t,n){return h(e,"nextSibling",n)},prevUntil:function(e,t,n){return h(e,"previousSibling",n)},siblings:function(e){return T((e.parentNode||{}).firstChild,e)},children:function(e){return T(e.firstChild)},contents:function(e){return null!=e.contentDocument&&r(e.contentDocument)?e.contentDocument:(A(e,"template")&&(e=e.content||e),S.merge([],e.childNodes))}},function(r,i){S.fn[r]=function(e,t){var n=S.map(this,i,e);return"Until"!==r.slice(-5)&&(t=e),t&&"string"==typeof t&&(n=S.filter(t,n)),1<this.length&&(H[r]||S.uniqueSort(n),L.test(r)&&n.reverse()),this.pushStack(n)}});var P=/[^\x20\t\r\n\f]+/g;function R(e){return e}function M(e){throw e}function I(e,t,n,r){var i;try{e&&m(i=e.promise)?i.call(e).done(t).fail(n):e&&m(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}S.Callbacks=function(r){var e,n;r="string"==typeof r?(e=r,n={},S.each(e.match(P)||[],function(e,t){n[t]=!0}),n):S.extend({},r);var i,t,o,a,s=[],u=[],l=-1,c=function(){for(a=a||r.once,o=i=!0;u.length;l=-1){t=u.shift();while(++l<s.length)!1===s[l].apply(t[0],t[1])&&r.stopOnFalse&&(l=s.length,t=!1)}r.memory||(t=!1),i=!1,a&&(s=t?[]:"")},f={add:function(){return s&&(t&&!i&&(l=s.length-1,u.push(t)),function n(e){S.each(e,function(e,t){m(t)?r.unique&&f.has(t)||s.push(t):t&&t.length&&"string"!==w(t)&&n(t)})}(arguments),t&&!i&&c()),this},remove:function(){return S.each(arguments,function(e,t){var n;while(-1<(n=S.inArray(t,s,n)))s.splice(n,1),n<=l&&l--}),this},has:function(e){return e?-1<S.inArray(e,s):0<s.length},empty:function(){return s&&(s=[]),this},disable:function(){return a=u=[],s=t="",this},disabled:function(){return!s},lock:function(){return a=u=[],t||i||(s=t=""),this},locked:function(){return!!a},fireWith:function(e,t){return a||(t=[e,(t=t||[]).slice?t.slice():t],u.push(t),i||c()),this},fire:function(){return f.fireWith(this,arguments),this},fired:function(){return!!o}};return f},S.extend({Deferred:function(e){var o=[["notify","progress",S.Callbacks("memory"),S.Callbacks("memory"),2],["resolve","done",S.Callbacks("once memory"),S.Callbacks("once memory"),0,"resolved"],["reject","fail",S.Callbacks("once memory"),S.Callbacks("once memory"),1,"rejected"]],i="pending",a={state:function(){return i},always:function(){return s.done(arguments).fail(arguments),this},"catch":function(e){return a.then(null,e)},pipe:function(){var i=arguments;return S.Deferred(function(r){S.each(o,function(e,t){var n=m(i[t[4]])&&i[t[4]];s[t[1]](function(){var e=n&&n.apply(this,arguments);e&&m(e.promise)?e.promise().progress(r.notify).done(r.resolve).fail(r.reject):r[t[0]+"With"](this,n?[e]:arguments)})}),i=null}).promise()},then:function(t,n,r){var u=0;function l(i,o,a,s){return function(){var n=this,r=arguments,e=function(){var e,t;if(!(i<u)){if((e=a.apply(n,r))===o.promise())throw new TypeError("Thenable self-resolution");t=e&&("object"==typeof e||"function"==typeof e)&&e.then,m(t)?s?t.call(e,l(u,o,R,s),l(u,o,M,s)):(u++,t.call(e,l(u,o,R,s),l(u,o,M,s),l(u,o,R,o.notifyWith))):(a!==R&&(n=void 0,r=[e]),(s||o.resolveWith)(n,r))}},t=s?e:function(){try{e()}catch(e){S.Deferred.exceptionHook&&S.Deferred.exceptionHook(e,t.stackTrace),u<=i+1&&(a!==M&&(n=void 0,r=[e]),o.rejectWith(n,r))}};i?t():(S.Deferred.getStackHook&&(t.stackTrace=S.Deferred.getStackHook()),C.setTimeout(t))}}return S.Deferred(function(e){o[0][3].add(l(0,e,m(r)?r:R,e.notifyWith)),o[1][3].add(l(0,e,m(t)?t:R)),o[2][3].add(l(0,e,m(n)?n:M))}).promise()},promise:function(e){return null!=e?S.extend(e,a):a}},s={};return S.each(o,function(e,t){var n=t[2],r=t[5];a[t[1]]=n.add,r&&n.add(function(){i=r},o[3-e][2].disable,o[3-e][3].disable,o[0][2].lock,o[0][3].lock),n.add(t[3].fire),s[t[0]]=function(){return s[t[0]+"With"](this===s?void 0:this,arguments),this},s[t[0]+"With"]=n.fireWith}),a.promise(s),e&&e.call(s,s),s},when:function(e){var n=arguments.length,t=n,r=Array(t),i=s.call(arguments),o=S.Deferred(),a=function(t){return function(e){r[t]=this,i[t]=1<arguments.length?s.call(arguments):e,--n||o.resolveWith(r,i)}};if(n<=1&&(I(e,o.done(a(t)).resolve,o.reject,!n),"pending"===o.state()||m(i[t]&&i[t].then)))return o.then();while(t--)I(i[t],a(t),o.reject);return o.promise()}});var W=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;S.Deferred.exceptionHook=function(e,t){C.console&&C.console.warn&&e&&W.test(e.name)&&C.console.warn("jQuery.Deferred exception: "+e.message,e.stack,t)},S.readyException=function(e){C.setTimeout(function(){throw e})};var F=S.Deferred();function B(){E.removeEventListener("DOMContentLoaded",B),C.removeEventListener("load",B),S.ready()}S.fn.ready=function(e){return F.then(e)["catch"](function(e){S.readyException(e)}),this},S.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--S.readyWait:S.isReady)||(S.isReady=!0)!==e&&0<--S.readyWait||F.resolveWith(E,[S])}}),S.ready.then=F.then,"complete"===E.readyState||"loading"!==E.readyState&&!E.documentElement.doScroll?C.setTimeout(S.ready):(E.addEventListener("DOMContentLoaded",B),C.addEventListener("load",B));var $=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===w(n))for(s in i=!0,n)$(e,t,s,n[s],!0,o,a);else if(void 0!==r&&(i=!0,m(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(S(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},_=/^-ms-/,z=/-([a-z])/g;function U(e,t){return t.toUpperCase()}function X(e){return e.replace(_,"ms-").replace(z,U)}var V=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function G(){this.expando=S.expando+G.uid++}G.uid=1,G.prototype={cache:function(e){var t=e[this.expando];return t||(t={},V(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[X(t)]=n;else for(r in t)i[X(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][X(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(X):(t=X(t))in r?[t]:t.match(P)||[]).length;while(n--)delete r[t[n]]}(void 0===t||S.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!S.isEmptyObject(t)}};var Y=new G,Q=new G,J=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,K=/[A-Z]/g;function Z(e,t,n){var r,i;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(K,"-$&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n="true"===(i=n)||"false"!==i&&("null"===i?null:i===+i+""?+i:J.test(i)?JSON.parse(i):i)}catch(e){}Q.set(e,t,n)}else n=void 0;return n}S.extend({hasData:function(e){return Q.hasData(e)||Y.hasData(e)},data:function(e,t,n){return Q.access(e,t,n)},removeData:function(e,t){Q.remove(e,t)},_data:function(e,t,n){return Y.access(e,t,n)},_removeData:function(e,t){Y.remove(e,t)}}),S.fn.extend({data:function(n,e){var t,r,i,o=this[0],a=o&&o.attributes;if(void 0===n){if(this.length&&(i=Q.get(o),1===o.nodeType&&!Y.get(o,"hasDataAttrs"))){t=a.length;while(t--)a[t]&&0===(r=a[t].name).indexOf("data-")&&(r=X(r.slice(5)),Z(o,r,i[r]));Y.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof n?this.each(function(){Q.set(this,n)}):$(this,function(e){var t;if(o&&void 0===e)return void 0!==(t=Q.get(o,n))?t:void 0!==(t=Z(o,n))?t:void 0;this.each(function(){Q.set(this,n,e)})},null,e,1<arguments.length,null,!0)},removeData:function(e){return this.each(function(){Q.remove(this,e)})}}),S.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=Y.get(e,t),n&&(!r||Array.isArray(n)?r=Y.access(e,t,S.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=S.queue(e,t),r=n.length,i=n.shift(),o=S._queueHooks(e,t);"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,function(){S.dequeue(e,t)},o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return Y.get(e,n)||Y.access(e,n,{empty:S.Callbacks("once memory").add(function(){Y.remove(e,[t+"queue",n])})})}}),S.fn.extend({queue:function(t,n){var e=2;return"string"!=typeof t&&(n=t,t="fx",e--),arguments.length<e?S.queue(this[0],t):void 0===n?this:this.each(function(){var e=S.queue(this,t,n);S._queueHooks(this,t),"fx"===t&&"inprogress"!==e[0]&&S.dequeue(this,t)})},dequeue:function(e){return this.each(function(){S.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=S.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=Y.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var ee=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,te=new RegExp("^(?:([+-])=|)("+ee+")([a-z%]*)$","i"),ne=["Top","Right","Bottom","Left"],re=E.documentElement,ie=function(e){return S.contains(e.ownerDocument,e)},oe={composed:!0};re.getRootNode&&(ie=function(e){return S.contains(e.ownerDocument,e)||e.getRootNode(oe)===e.ownerDocument});var ae=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&ie(e)&&"none"===S.css(e,"display")};function se(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return S.css(e,t,"")},u=s(),l=n&&n[3]||(S.cssNumber[t]?"":"px"),c=e.nodeType&&(S.cssNumber[t]||"px"!==l&&+u)&&te.exec(S.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)S.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,S.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var ue={};function le(e,t){for(var n,r,i,o,a,s,u,l=[],c=0,f=e.length;c<f;c++)(r=e[c]).style&&(n=r.style.display,t?("none"===n&&(l[c]=Y.get(r,"display")||null,l[c]||(r.style.display="")),""===r.style.display&&ae(r)&&(l[c]=(u=a=o=void 0,a=(i=r).ownerDocument,s=i.nodeName,(u=ue[s])||(o=a.body.appendChild(a.createElement(s)),u=S.css(o,"display"),o.parentNode.removeChild(o),"none"===u&&(u="block"),ue[s]=u)))):"none"!==n&&(l[c]="none",Y.set(r,"display",n)));for(c=0;c<f;c++)null!=l[c]&&(e[c].style.display=l[c]);return e}S.fn.extend({show:function(){return le(this,!0)},hide:function(){return le(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){ae(this)?S(this).show():S(this).hide()})}});var ce,fe,pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,he=/^$|^module$|\/(?:java|ecma)script/i;ce=E.createDocumentFragment().appendChild(E.createElement("div")),(fe=E.createElement("input")).setAttribute("type","radio"),fe.setAttribute("checked","checked"),fe.setAttribute("name","t"),ce.appendChild(fe),y.checkClone=ce.cloneNode(!0).cloneNode(!0).lastChild.checked,ce.innerHTML="<textarea>x</textarea>",y.noCloneChecked=!!ce.cloneNode(!0).lastChild.defaultValue,ce.innerHTML="<option></option>",y.option=!!ce.lastChild;var ge={thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};function ve(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&A(e,t)?S.merge([e],n):n}function ye(e,t){for(var n=0,r=e.length;n<r;n++)Y.set(e[n],"globalEval",!t||Y.get(t[n],"globalEval"))}ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td,y.option||(ge.optgroup=ge.option=[1,"<select multiple='multiple'>","</select>"]);var me=/<|&#?\w+;/;function xe(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===w(o))S.merge(p,o.nodeType?[o]:o);else if(me.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+S.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;S.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&-1<S.inArray(o,r))i&&i.push(o);else if(l=ie(o),a=ve(f.appendChild(o),"script"),l&&ye(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}var be=/^key/,we=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Te=/^([^.]*)(?:\.(.+)|)/;function Ce(){return!0}function Ee(){return!1}function Se(e,t){return e===function(){try{return E.activeElement}catch(e){}}()==("focus"===t)}function ke(e,t,n,r,i,o){var a,s;if("object"==typeof t){for(s in"string"!=typeof n&&(r=r||n,n=void 0),t)ke(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=Ee;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return S().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=S.guid++)),e.each(function(){S.event.add(this,t,i,r,n)})}function Ae(e,i,o){o?(Y.set(e,i,!1),S.event.add(e,i,{namespace:!1,handler:function(e){var t,n,r=Y.get(this,i);if(1&e.isTrigger&&this[i]){if(r.length)(S.event.special[i]||{}).delegateType&&e.stopPropagation();else if(r=s.call(arguments),Y.set(this,i,r),t=o(this,i),this[i](),r!==(n=Y.get(this,i))||t?Y.set(this,i,!1):n={},r!==n)return e.stopImmediatePropagation(),e.preventDefault(),n.value}else r.length&&(Y.set(this,i,{value:S.event.trigger(S.extend(r[0],S.Event.prototype),r.slice(1),this)}),e.stopImmediatePropagation())}})):void 0===Y.get(e,i)&&S.event.add(e,i,Ce)}S.event={global:{},add:function(t,e,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Y.get(t);if(V(t)){n.handler&&(n=(o=n).handler,i=o.selector),i&&S.find.matchesSelector(re,i),n.guid||(n.guid=S.guid++),(u=v.events)||(u=v.events=Object.create(null)),(a=v.handle)||(a=v.handle=function(e){return"undefined"!=typeof S&&S.event.triggered!==e.type?S.event.dispatch.apply(t,arguments):void 0}),l=(e=(e||"").match(P)||[""]).length;while(l--)d=g=(s=Te.exec(e[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=S.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=S.event.special[d]||{},c=S.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&S.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(t,r,h,a)||t.addEventListener&&t.addEventListener(d,a)),f.add&&(f.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),S.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Y.hasData(e)&&Y.get(e);if(v&&(u=v.events)){l=(t=(t||"").match(P)||[""]).length;while(l--)if(d=g=(s=Te.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d){f=S.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,v.handle)||S.removeEvent(e,d,v.handle),delete u[d])}else for(d in u)S.event.remove(e,d+t[l],n,r,!0);S.isEmptyObject(u)&&Y.remove(e,"handle events")}},dispatch:function(e){var t,n,r,i,o,a,s=new Array(arguments.length),u=S.event.fix(e),l=(Y.get(this,"events")||Object.create(null))[u.type]||[],c=S.event.special[u.type]||{};for(s[0]=u,t=1;t<arguments.length;t++)s[t]=arguments[t];if(u.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,u)){a=S.event.handlers.call(this,u,l),t=0;while((i=a[t++])&&!u.isPropagationStopped()){u.currentTarget=i.elem,n=0;while((o=i.handlers[n++])&&!u.isImmediatePropagationStopped())u.rnamespace&&!1!==o.namespace&&!u.rnamespace.test(o.namespace)||(u.handleObj=o,u.data=o.data,void 0!==(r=((S.event.special[o.origType]||{}).handle||o.handler).apply(i.elem,s))&&!1===(u.result=r)&&(u.preventDefault(),u.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,u),u.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&1<=e.button))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?-1<S(i,this).index(l):S.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(t,e){Object.defineProperty(S.Event.prototype,t,{enumerable:!0,configurable:!0,get:m(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(e){return e[S.expando]?e:new S.Event(e)},special:{load:{noBubble:!0},click:{setup:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&Ae(t,"click",Ce),!1},trigger:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&Ae(t,"click"),!0},_default:function(e){var t=e.target;return pe.test(t.type)&&t.click&&A(t,"input")&&Y.get(t,"click")||A(t,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},S.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},S.Event=function(e,t){if(!(this instanceof S.Event))return new S.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?Ce:Ee,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&S.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[S.expando]=!0},S.Event.prototype={constructor:S.Event,isDefaultPrevented:Ee,isPropagationStopped:Ee,isImmediatePropagationStopped:Ee,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=Ce,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=Ce,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=Ce,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},S.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(e){var t=e.button;return null==e.which&&be.test(e.type)?null!=e.charCode?e.charCode:e.keyCode:!e.which&&void 0!==t&&we.test(e.type)?1&t?1:2&t?3:4&t?2:0:e.which}},S.event.addProp),S.each({focus:"focusin",blur:"focusout"},function(e,t){S.event.special[e]={setup:function(){return Ae(this,e,Se),!1},trigger:function(){return Ae(this,e),!0},delegateType:t}}),S.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,i){S.event.special[e]={delegateType:i,bindType:i,handle:function(e){var t,n=e.relatedTarget,r=e.handleObj;return n&&(n===this||S.contains(this,n))||(e.type=r.origType,t=r.handler.apply(this,arguments),e.type=i),t}}}),S.fn.extend({on:function(e,t,n,r){return ke(this,e,t,n,r)},one:function(e,t,n,r){return ke(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,S(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=Ee),this.each(function(){S.event.remove(this,e,n,t)})}});var Ne=/<script|<style|<link/i,De=/checked\s*(?:[^=]|=\s*.checked.)/i,je=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function qe(e,t){return A(e,"table")&&A(11!==t.nodeType?t:t.firstChild,"tr")&&S(e).children("tbody")[0]||e}function Le(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function He(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Oe(e,t){var n,r,i,o,a,s;if(1===t.nodeType){if(Y.hasData(e)&&(s=Y.get(e).events))for(i in Y.remove(t,"handle events"),s)for(n=0,r=s[i].length;n<r;n++)S.event.add(t,i,s[i][n]);Q.hasData(e)&&(o=Q.access(e),a=S.extend({},o),Q.set(t,a))}}function Pe(n,r,i,o){r=g(r);var e,t,a,s,u,l,c=0,f=n.length,p=f-1,d=r[0],h=m(d);if(h||1<f&&"string"==typeof d&&!y.checkClone&&De.test(d))return n.each(function(e){var t=n.eq(e);h&&(r[0]=d.call(this,e,t.html())),Pe(t,r,i,o)});if(f&&(t=(e=xe(r,n[0].ownerDocument,!1,n,o)).firstChild,1===e.childNodes.length&&(e=t),t||o)){for(s=(a=S.map(ve(e,"script"),Le)).length;c<f;c++)u=e,c!==p&&(u=S.clone(u,!0,!0),s&&S.merge(a,ve(u,"script"))),i.call(n[c],u,c);if(s)for(l=a[a.length-1].ownerDocument,S.map(a,He),c=0;c<s;c++)u=a[c],he.test(u.type||"")&&!Y.access(u,"globalEval")&&S.contains(l,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?S._evalUrl&&!u.noModule&&S._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")},l):b(u.textContent.replace(je,""),u,l))}return n}function Re(e,t,n){for(var r,i=t?S.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||S.cleanData(ve(r)),r.parentNode&&(n&&ie(r)&&ye(ve(r,"script")),r.parentNode.removeChild(r));return e}S.extend({htmlPrefilter:function(e){return e},clone:function(e,t,n){var r,i,o,a,s,u,l,c=e.cloneNode(!0),f=ie(e);if(!(y.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||S.isXMLDoc(e)))for(a=ve(c),r=0,i=(o=ve(e)).length;r<i;r++)s=o[r],u=a[r],void 0,"input"===(l=u.nodeName.toLowerCase())&&pe.test(s.type)?u.checked=s.checked:"input"!==l&&"textarea"!==l||(u.defaultValue=s.defaultValue);if(t)if(n)for(o=o||ve(e),a=a||ve(c),r=0,i=o.length;r<i;r++)Oe(o[r],a[r]);else Oe(e,c);return 0<(a=ve(c,"script")).length&&ye(a,!f&&ve(e,"script")),c},cleanData:function(e){for(var t,n,r,i=S.event.special,o=0;void 0!==(n=e[o]);o++)if(V(n)){if(t=n[Y.expando]){if(t.events)for(r in t.events)i[r]?S.event.remove(n,r):S.removeEvent(n,r,t.handle);n[Y.expando]=void 0}n[Q.expando]&&(n[Q.expando]=void 0)}}}),S.fn.extend({detach:function(e){return Re(this,e,!0)},remove:function(e){return Re(this,e)},text:function(e){return $(this,function(e){return void 0===e?S.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Pe(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||qe(this,e).appendChild(e)})},prepend:function(){return Pe(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=qe(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Pe(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Pe(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(S.cleanData(ve(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return S.clone(this,e,t)})},html:function(e){return $(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!Ne.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=S.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(S.cleanData(ve(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var n=[];return Pe(this,arguments,function(e){var t=this.parentNode;S.inArray(this,n)<0&&(S.cleanData(ve(this)),t&&t.replaceChild(e,this))},n)}}),S.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,a){S.fn[e]=function(e){for(var t,n=[],r=S(e),i=r.length-1,o=0;o<=i;o++)t=o===i?this:this.clone(!0),S(r[o])[a](t),u.apply(n,t.get());return this.pushStack(n)}});var Me=new RegExp("^("+ee+")(?!px)[a-z%]+$","i"),Ie=function(e){var t=e.ownerDocument.defaultView;return t&&t.opener||(t=C),t.getComputedStyle(e)},We=function(e,t,n){var r,i,o={};for(i in t)o[i]=e.style[i],e.style[i]=t[i];for(i in r=n.call(e),t)e.style[i]=o[i];return r},Fe=new RegExp(ne.join("|"),"i");function Be(e,t,n){var r,i,o,a,s=e.style;return(n=n||Ie(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||ie(e)||(a=S.style(e,t)),!y.pixelBoxStyles()&&Me.test(a)&&Fe.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function $e(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}!function(){function e(){if(l){u.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",l.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",re.appendChild(u).appendChild(l);var e=C.getComputedStyle(l);n="1%"!==e.top,s=12===t(e.marginLeft),l.style.right="60%",o=36===t(e.right),r=36===t(e.width),l.style.position="absolute",i=12===t(l.offsetWidth/3),re.removeChild(u),l=null}}function t(e){return Math.round(parseFloat(e))}var n,r,i,o,a,s,u=E.createElement("div"),l=E.createElement("div");l.style&&(l.style.backgroundClip="content-box",l.cloneNode(!0).style.backgroundClip="",y.clearCloneStyle="content-box"===l.style.backgroundClip,S.extend(y,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),o},pixelPosition:function(){return e(),n},reliableMarginLeft:function(){return e(),s},scrollboxSize:function(){return e(),i},reliableTrDimensions:function(){var e,t,n,r;return null==a&&(e=E.createElement("table"),t=E.createElement("tr"),n=E.createElement("div"),e.style.cssText="position:absolute;left:-11111px",t.style.height="1px",n.style.height="9px",re.appendChild(e).appendChild(t).appendChild(n),r=C.getComputedStyle(t),a=3<parseInt(r.height),re.removeChild(e)),a}}))}();var _e=["Webkit","Moz","ms"],ze=E.createElement("div").style,Ue={};function Xe(e){var t=S.cssProps[e]||Ue[e];return t||(e in ze?e:Ue[e]=function(e){var t=e[0].toUpperCase()+e.slice(1),n=_e.length;while(n--)if((e=_e[n]+t)in ze)return e}(e)||e)}var Ve=/^(none|table(?!-c[ea]).+)/,Ge=/^--/,Ye={position:"absolute",visibility:"hidden",display:"block"},Qe={letterSpacing:"0",fontWeight:"400"};function Je(e,t,n){var r=te.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function Ke(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=S.css(e,n+ne[a],!0,i)),r?("content"===n&&(u-=S.css(e,"padding"+ne[a],!0,i)),"margin"!==n&&(u-=S.css(e,"border"+ne[a]+"Width",!0,i))):(u+=S.css(e,"padding"+ne[a],!0,i),"padding"!==n?u+=S.css(e,"border"+ne[a]+"Width",!0,i):s+=S.css(e,"border"+ne[a]+"Width",!0,i));return!r&&0<=o&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))||0),u}function Ze(e,t,n){var r=Ie(e),i=(!y.boxSizingReliable()||n)&&"border-box"===S.css(e,"boxSizing",!1,r),o=i,a=Be(e,t,r),s="offset"+t[0].toUpperCase()+t.slice(1);if(Me.test(a)){if(!n)return a;a="auto"}return(!y.boxSizingReliable()&&i||!y.reliableTrDimensions()&&A(e,"tr")||"auto"===a||!parseFloat(a)&&"inline"===S.css(e,"display",!1,r))&&e.getClientRects().length&&(i="border-box"===S.css(e,"boxSizing",!1,r),(o=s in e)&&(a=e[s])),(a=parseFloat(a)||0)+Ke(e,t,n||(i?"border":"content"),o,r,a)+"px"}function et(e,t,n,r,i){return new et.prototype.init(e,t,n,r,i)}S.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=Be(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=X(t),u=Ge.test(t),l=e.style;if(u||(t=Xe(s)),a=S.cssHooks[t]||S.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"===(o=typeof n)&&(i=te.exec(n))&&i[1]&&(n=se(e,t,i),o="number"),null!=n&&n==n&&("number"!==o||u||(n+=i&&i[3]||(S.cssNumber[s]?"":"px")),y.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=X(t);return Ge.test(t)||(t=Xe(s)),(a=S.cssHooks[t]||S.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=Be(e,t,r)),"normal"===i&&t in Qe&&(i=Qe[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),S.each(["height","width"],function(e,u){S.cssHooks[u]={get:function(e,t,n){if(t)return!Ve.test(S.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?Ze(e,u,n):We(e,Ye,function(){return Ze(e,u,n)})},set:function(e,t,n){var r,i=Ie(e),o=!y.scrollboxSize()&&"absolute"===i.position,a=(o||n)&&"border-box"===S.css(e,"boxSizing",!1,i),s=n?Ke(e,u,n,a,i):0;return a&&o&&(s-=Math.ceil(e["offset"+u[0].toUpperCase()+u.slice(1)]-parseFloat(i[u])-Ke(e,u,"border",!1,i)-.5)),s&&(r=te.exec(t))&&"px"!==(r[3]||"px")&&(e.style[u]=t,t=S.css(e,u)),Je(0,t,s)}}}),S.cssHooks.marginLeft=$e(y.reliableMarginLeft,function(e,t){if(t)return(parseFloat(Be(e,"marginLeft"))||e.getBoundingClientRect().left-We(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),S.each({margin:"",padding:"",border:"Width"},function(i,o){S.cssHooks[i+o]={expand:function(e){for(var t=0,n={},r="string"==typeof e?e.split(" "):[e];t<4;t++)n[i+ne[t]+o]=r[t]||r[t-2]||r[0];return n}},"margin"!==i&&(S.cssHooks[i+o].set=Je)}),S.fn.extend({css:function(e,t){return $(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=Ie(e),i=t.length;a<i;a++)o[t[a]]=S.css(e,t[a],!1,r);return o}return void 0!==n?S.style(e,t,n):S.css(e,t)},e,t,1<arguments.length)}}),((S.Tween=et).prototype={constructor:et,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||S.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(S.cssNumber[n]?"":"px")},cur:function(){var e=et.propHooks[this.prop];return e&&e.get?e.get(this):et.propHooks._default.get(this)},run:function(e){var t,n=et.propHooks[this.prop];return this.options.duration?this.pos=t=S.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):et.propHooks._default.set(this),this}}).init.prototype=et.prototype,(et.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=S.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){S.fx.step[e.prop]?S.fx.step[e.prop](e):1!==e.elem.nodeType||!S.cssHooks[e.prop]&&null==e.elem.style[Xe(e.prop)]?e.elem[e.prop]=e.now:S.style(e.elem,e.prop,e.now+e.unit)}}}).scrollTop=et.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},S.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},S.fx=et.prototype.init,S.fx.step={};var tt,nt,rt,it,ot=/^(?:toggle|show|hide)$/,at=/queueHooks$/;function st(){nt&&(!1===E.hidden&&C.requestAnimationFrame?C.requestAnimationFrame(st):C.setTimeout(st,S.fx.interval),S.fx.tick())}function ut(){return C.setTimeout(function(){tt=void 0}),tt=Date.now()}function lt(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=ne[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function ct(e,t,n){for(var r,i=(ft.tweeners[t]||[]).concat(ft.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function ft(o,e,t){var n,a,r=0,i=ft.prefilters.length,s=S.Deferred().always(function(){delete u.elem}),u=function(){if(a)return!1;for(var e=tt||ut(),t=Math.max(0,l.startTime+l.duration-e),n=1-(t/l.duration||0),r=0,i=l.tweens.length;r<i;r++)l.tweens[r].run(n);return s.notifyWith(o,[l,n,t]),n<1&&i?t:(i||s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l]),!1)},l=s.promise({elem:o,props:S.extend({},e),opts:S.extend(!0,{specialEasing:{},easing:S.easing._default},t),originalProperties:e,originalOptions:t,startTime:tt||ut(),duration:t.duration,tweens:[],createTween:function(e,t){var n=S.Tween(o,l.opts,e,t,l.opts.specialEasing[e]||l.opts.easing);return l.tweens.push(n),n},stop:function(e){var t=0,n=e?l.tweens.length:0;if(a)return this;for(a=!0;t<n;t++)l.tweens[t].run(1);return e?(s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l,e])):s.rejectWith(o,[l,e]),this}}),c=l.props;for(!function(e,t){var n,r,i,o,a;for(n in e)if(i=t[r=X(n)],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=S.cssHooks[r])&&"expand"in a)for(n in o=a.expand(o),delete e[r],o)n in e||(e[n]=o[n],t[n]=i);else t[r]=i}(c,l.opts.specialEasing);r<i;r++)if(n=ft.prefilters[r].call(l,o,c,l.opts))return m(n.stop)&&(S._queueHooks(l.elem,l.opts.queue).stop=n.stop.bind(n)),n;return S.map(c,ct,l),m(l.opts.start)&&l.opts.start.call(o,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),S.fx.timer(S.extend(u,{elem:o,anim:l,queue:l.opts.queue})),l}S.Animation=S.extend(ft,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return se(n.elem,e,te.exec(t),n),n}]},tweener:function(e,t){m(e)?(t=e,e=["*"]):e=e.match(P);for(var n,r=0,i=e.length;r<i;r++)n=e[r],ft.tweeners[n]=ft.tweeners[n]||[],ft.tweeners[n].unshift(t)},prefilters:[function(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&ae(e),v=Y.get(e,"fxshow");for(r in n.queue||(null==(a=S._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,S.queue(e,"fx").length||a.empty.fire()})})),t)if(i=t[r],ot.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!v||void 0===v[r])continue;g=!0}d[r]=v&&v[r]||S.style(e,r)}if((u=!S.isEmptyObject(t))||!S.isEmptyObject(d))for(r in f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=v&&v.display)&&(l=Y.get(e,"display")),"none"===(c=S.css(e,"display"))&&(l?c=l:(le([e],!0),l=e.style.display||l,c=S.css(e,"display"),le([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===S.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1,d)u||(v?"hidden"in v&&(g=v.hidden):v=Y.access(e,"fxshow",{display:l}),o&&(v.hidden=!g),g&&le([e],!0),p.done(function(){for(r in g||le([e]),Y.remove(e,"fxshow"),d)S.style(e,r,d[r])})),u=ct(g?v[r]:0,r,p),r in v||(v[r]=u.start,g&&(u.end=u.start,u.start=0))}],prefilter:function(e,t){t?ft.prefilters.unshift(e):ft.prefilters.push(e)}}),S.speed=function(e,t,n){var r=e&&"object"==typeof e?S.extend({},e):{complete:n||!n&&t||m(e)&&e,duration:e,easing:n&&t||t&&!m(t)&&t};return S.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in S.fx.speeds?r.duration=S.fx.speeds[r.duration]:r.duration=S.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){m(r.old)&&r.old.call(this),r.queue&&S.dequeue(this,r.queue)},r},S.fn.extend({fadeTo:function(e,t,n,r){return this.filter(ae).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(t,e,n,r){var i=S.isEmptyObject(t),o=S.speed(e,n,r),a=function(){var e=ft(this,S.extend({},t),o);(i||Y.get(this,"finish"))&&e.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(i,e,o){var a=function(e){var t=e.stop;delete e.stop,t(o)};return"string"!=typeof i&&(o=e,e=i,i=void 0),e&&this.queue(i||"fx",[]),this.each(function(){var e=!0,t=null!=i&&i+"queueHooks",n=S.timers,r=Y.get(this);if(t)r[t]&&r[t].stop&&a(r[t]);else for(t in r)r[t]&&r[t].stop&&at.test(t)&&a(r[t]);for(t=n.length;t--;)n[t].elem!==this||null!=i&&n[t].queue!==i||(n[t].anim.stop(o),e=!1,n.splice(t,1));!e&&o||S.dequeue(this,i)})},finish:function(a){return!1!==a&&(a=a||"fx"),this.each(function(){var e,t=Y.get(this),n=t[a+"queue"],r=t[a+"queueHooks"],i=S.timers,o=n?n.length:0;for(t.finish=!0,S.queue(this,a,[]),r&&r.stop&&r.stop.call(this,!0),e=i.length;e--;)i[e].elem===this&&i[e].queue===a&&(i[e].anim.stop(!0),i.splice(e,1));for(e=0;e<o;e++)n[e]&&n[e].finish&&n[e].finish.call(this);delete t.finish})}}),S.each(["toggle","show","hide"],function(e,r){var i=S.fn[r];S.fn[r]=function(e,t,n){return null==e||"boolean"==typeof e?i.apply(this,arguments):this.animate(lt(r,!0),e,t,n)}}),S.each({slideDown:lt("show"),slideUp:lt("hide"),slideToggle:lt("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,r){S.fn[e]=function(e,t,n){return this.animate(r,e,t,n)}}),S.timers=[],S.fx.tick=function(){var e,t=0,n=S.timers;for(tt=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||S.fx.stop(),tt=void 0},S.fx.timer=function(e){S.timers.push(e),S.fx.start()},S.fx.interval=13,S.fx.start=function(){nt||(nt=!0,st())},S.fx.stop=function(){nt=null},S.fx.speeds={slow:600,fast:200,_default:400},S.fn.delay=function(r,e){return r=S.fx&&S.fx.speeds[r]||r,e=e||"fx",this.queue(e,function(e,t){var n=C.setTimeout(e,r);t.stop=function(){C.clearTimeout(n)}})},rt=E.createElement("input"),it=E.createElement("select").appendChild(E.createElement("option")),rt.type="checkbox",y.checkOn=""!==rt.value,y.optSelected=it.selected,(rt=E.createElement("input")).value="t",rt.type="radio",y.radioValue="t"===rt.value;var pt,dt=S.expr.attrHandle;S.fn.extend({attr:function(e,t){return $(this,S.attr,e,t,1<arguments.length)},removeAttr:function(e){return this.each(function(){S.removeAttr(this,e)})}}),S.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?S.prop(e,t,n):(1===o&&S.isXMLDoc(e)||(i=S.attrHooks[t.toLowerCase()]||(S.expr.match.bool.test(t)?pt:void 0)),void 0!==n?null===n?void S.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=S.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!y.radioValue&&"radio"===t&&A(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(P);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),pt={set:function(e,t,n){return!1===t?S.removeAttr(e,n):e.setAttribute(n,n),n}},S.each(S.expr.match.bool.source.match(/\w+/g),function(e,t){var a=dt[t]||S.find.attr;dt[t]=function(e,t,n){var r,i,o=t.toLowerCase();return n||(i=dt[o],dt[o]=r,r=null!=a(e,t,n)?o:null,dt[o]=i),r}});var ht=/^(?:input|select|textarea|button)$/i,gt=/^(?:a|area)$/i;function vt(e){return(e.match(P)||[]).join(" ")}function yt(e){return e.getAttribute&&e.getAttribute("class")||""}function mt(e){return Array.isArray(e)?e:"string"==typeof e&&e.match(P)||[]}S.fn.extend({prop:function(e,t){return $(this,S.prop,e,t,1<arguments.length)},removeProp:function(e){return this.each(function(){delete this[S.propFix[e]||e]})}}),S.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&S.isXMLDoc(e)||(t=S.propFix[t]||t,i=S.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=S.find.attr(e,"tabindex");return t?parseInt(t,10):ht.test(e.nodeName)||gt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),y.optSelected||(S.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),S.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){S.propFix[this.toLowerCase()]=this}),S.fn.extend({addClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){S(this).addClass(t.call(this,e,yt(this)))});if((e=mt(t)).length)while(n=this[u++])if(i=yt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=e[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},removeClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){S(this).removeClass(t.call(this,e,yt(this)))});if(!arguments.length)return this.attr("class","");if((e=mt(t)).length)while(n=this[u++])if(i=yt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=e[a++])while(-1<r.indexOf(" "+o+" "))r=r.replace(" "+o+" "," ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},toggleClass:function(i,t){var o=typeof i,a="string"===o||Array.isArray(i);return"boolean"==typeof t&&a?t?this.addClass(i):this.removeClass(i):m(i)?this.each(function(e){S(this).toggleClass(i.call(this,e,yt(this),t),t)}):this.each(function(){var e,t,n,r;if(a){t=0,n=S(this),r=mt(i);while(e=r[t++])n.hasClass(e)?n.removeClass(e):n.addClass(e)}else void 0!==i&&"boolean"!==o||((e=yt(this))&&Y.set(this,"__className__",e),this.setAttribute&&this.setAttribute("class",e||!1===i?"":Y.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&-1<(" "+vt(yt(n))+" ").indexOf(t))return!0;return!1}});var xt=/\r/g;S.fn.extend({val:function(n){var r,e,i,t=this[0];return arguments.length?(i=m(n),this.each(function(e){var t;1===this.nodeType&&(null==(t=i?n.call(this,e,S(this).val()):n)?t="":"number"==typeof t?t+="":Array.isArray(t)&&(t=S.map(t,function(e){return null==e?"":e+""})),(r=S.valHooks[this.type]||S.valHooks[this.nodeName.toLowerCase()])&&"set"in r&&void 0!==r.set(this,t,"value")||(this.value=t))})):t?(r=S.valHooks[t.type]||S.valHooks[t.nodeName.toLowerCase()])&&"get"in r&&void 0!==(e=r.get(t,"value"))?e:"string"==typeof(e=t.value)?e.replace(xt,""):null==e?"":e:void 0}}),S.extend({valHooks:{option:{get:function(e){var t=S.find.attr(e,"value");return null!=t?t:vt(S.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!A(n.parentNode,"optgroup"))){if(t=S(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=S.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=-1<S.inArray(S.valHooks.option.get(r),o))&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),S.each(["radio","checkbox"],function(){S.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=-1<S.inArray(S(e).val(),t)}},y.checkOn||(S.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),y.focusin="onfocusin"in C;var bt=/^(?:focusinfocus|focusoutblur)$/,wt=function(e){e.stopPropagation()};S.extend(S.event,{trigger:function(e,t,n,r){var i,o,a,s,u,l,c,f,p=[n||E],d=v.call(e,"type")?e.type:e,h=v.call(e,"namespace")?e.namespace.split("."):[];if(o=f=a=n=n||E,3!==n.nodeType&&8!==n.nodeType&&!bt.test(d+S.event.triggered)&&(-1<d.indexOf(".")&&(d=(h=d.split(".")).shift(),h.sort()),u=d.indexOf(":")<0&&"on"+d,(e=e[S.expando]?e:new S.Event(d,"object"==typeof e&&e)).isTrigger=r?2:3,e.namespace=h.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,e.result=void 0,e.target||(e.target=n),t=null==t?[e]:S.makeArray(t,[e]),c=S.event.special[d]||{},r||!c.trigger||!1!==c.trigger.apply(n,t))){if(!r&&!c.noBubble&&!x(n)){for(s=c.delegateType||d,bt.test(s+d)||(o=o.parentNode);o;o=o.parentNode)p.push(o),a=o;a===(n.ownerDocument||E)&&p.push(a.defaultView||a.parentWindow||C)}i=0;while((o=p[i++])&&!e.isPropagationStopped())f=o,e.type=1<i?s:c.bindType||d,(l=(Y.get(o,"events")||Object.create(null))[e.type]&&Y.get(o,"handle"))&&l.apply(o,t),(l=u&&o[u])&&l.apply&&V(o)&&(e.result=l.apply(o,t),!1===e.result&&e.preventDefault());return e.type=d,r||e.isDefaultPrevented()||c._default&&!1!==c._default.apply(p.pop(),t)||!V(n)||u&&m(n[d])&&!x(n)&&((a=n[u])&&(n[u]=null),S.event.triggered=d,e.isPropagationStopped()&&f.addEventListener(d,wt),n[d](),e.isPropagationStopped()&&f.removeEventListener(d,wt),S.event.triggered=void 0,a&&(n[u]=a)),e.result}},simulate:function(e,t,n){var r=S.extend(new S.Event,n,{type:e,isSimulated:!0});S.event.trigger(r,null,t)}}),S.fn.extend({trigger:function(e,t){return this.each(function(){S.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return S.event.trigger(e,t,n,!0)}}),y.focusin||S.each({focus:"focusin",blur:"focusout"},function(n,r){var i=function(e){S.event.simulate(r,e.target,S.event.fix(e))};S.event.special[r]={setup:function(){var e=this.ownerDocument||this.document||this,t=Y.access(e,r);t||e.addEventListener(n,i,!0),Y.access(e,r,(t||0)+1)},teardown:function(){var e=this.ownerDocument||this.document||this,t=Y.access(e,r)-1;t?Y.access(e,r,t):(e.removeEventListener(n,i,!0),Y.remove(e,r))}}});var Tt=C.location,Ct={guid:Date.now()},Et=/\?/;S.parseXML=function(e){var t;if(!e||"string"!=typeof e)return null;try{t=(new C.DOMParser).parseFromString(e,"text/xml")}catch(e){t=void 0}return t&&!t.getElementsByTagName("parsererror").length||S.error("Invalid XML: "+e),t};var St=/\[\]$/,kt=/\r?\n/g,At=/^(?:submit|button|image|reset|file)$/i,Nt=/^(?:input|select|textarea|keygen)/i;function Dt(n,e,r,i){var t;if(Array.isArray(e))S.each(e,function(e,t){r||St.test(n)?i(n,t):Dt(n+"["+("object"==typeof t&&null!=t?e:"")+"]",t,r,i)});else if(r||"object"!==w(e))i(n,e);else for(t in e)Dt(n+"["+t+"]",e[t],r,i)}S.param=function(e,t){var n,r=[],i=function(e,t){var n=m(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(null==e)return"";if(Array.isArray(e)||e.jquery&&!S.isPlainObject(e))S.each(e,function(){i(this.name,this.value)});else for(n in e)Dt(n,e[n],t,i);return r.join("&")},S.fn.extend({serialize:function(){return S.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=S.prop(this,"elements");return e?S.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!S(this).is(":disabled")&&Nt.test(this.nodeName)&&!At.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=S(this).val();return null==n?null:Array.isArray(n)?S.map(n,function(e){return{name:t.name,value:e.replace(kt,"\r\n")}}):{name:t.name,value:n.replace(kt,"\r\n")}}).get()}});var jt=/%20/g,qt=/#.*$/,Lt=/([?&])_=[^&]*/,Ht=/^(.*?):[ \t]*([^\r\n]*)$/gm,Ot=/^(?:GET|HEAD)$/,Pt=/^\/\//,Rt={},Mt={},It="*/".concat("*"),Wt=E.createElement("a");function Ft(o){return function(e,t){"string"!=typeof e&&(t=e,e="*");var n,r=0,i=e.toLowerCase().match(P)||[];if(m(t))while(n=i[r++])"+"===n[0]?(n=n.slice(1)||"*",(o[n]=o[n]||[]).unshift(t)):(o[n]=o[n]||[]).push(t)}}function Bt(t,i,o,a){var s={},u=t===Mt;function l(e){var r;return s[e]=!0,S.each(t[e]||[],function(e,t){var n=t(i,o,a);return"string"!=typeof n||u||s[n]?u?!(r=n):void 0:(i.dataTypes.unshift(n),l(n),!1)}),r}return l(i.dataTypes[0])||!s["*"]&&l("*")}function $t(e,t){var n,r,i=S.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&S.extend(!0,e,r),e}Wt.href=Tt.href,S.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Tt.href,type:"GET",isLocal:/^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Tt.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":It,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":S.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?$t($t(e,S.ajaxSettings),t):$t(S.ajaxSettings,e)},ajaxPrefilter:Ft(Rt),ajaxTransport:Ft(Mt),ajax:function(e,t){"object"==typeof e&&(t=e,e=void 0),t=t||{};var c,f,p,n,d,r,h,g,i,o,v=S.ajaxSetup({},t),y=v.context||v,m=v.context&&(y.nodeType||y.jquery)?S(y):S.event,x=S.Deferred(),b=S.Callbacks("once memory"),w=v.statusCode||{},a={},s={},u="canceled",T={readyState:0,getResponseHeader:function(e){var t;if(h){if(!n){n={};while(t=Ht.exec(p))n[t[1].toLowerCase()+" "]=(n[t[1].toLowerCase()+" "]||[]).concat(t[2])}t=n[e.toLowerCase()+" "]}return null==t?null:t.join(", ")},getAllResponseHeaders:function(){return h?p:null},setRequestHeader:function(e,t){return null==h&&(e=s[e.toLowerCase()]=s[e.toLowerCase()]||e,a[e]=t),this},overrideMimeType:function(e){return null==h&&(v.mimeType=e),this},statusCode:function(e){var t;if(e)if(h)T.always(e[T.status]);else for(t in e)w[t]=[w[t],e[t]];return this},abort:function(e){var t=e||u;return c&&c.abort(t),l(0,t),this}};if(x.promise(T),v.url=((e||v.url||Tt.href)+"").replace(Pt,Tt.protocol+"//"),v.type=t.method||t.type||v.method||v.type,v.dataTypes=(v.dataType||"*").toLowerCase().match(P)||[""],null==v.crossDomain){r=E.createElement("a");try{r.href=v.url,r.href=r.href,v.crossDomain=Wt.protocol+"//"+Wt.host!=r.protocol+"//"+r.host}catch(e){v.crossDomain=!0}}if(v.data&&v.processData&&"string"!=typeof v.data&&(v.data=S.param(v.data,v.traditional)),Bt(Rt,v,t,T),h)return T;for(i in(g=S.event&&v.global)&&0==S.active++&&S.event.trigger("ajaxStart"),v.type=v.type.toUpperCase(),v.hasContent=!Ot.test(v.type),f=v.url.replace(qt,""),v.hasContent?v.data&&v.processData&&0===(v.contentType||"").indexOf("application/x-www-form-urlencoded")&&(v.data=v.data.replace(jt,"+")):(o=v.url.slice(f.length),v.data&&(v.processData||"string"==typeof v.data)&&(f+=(Et.test(f)?"&":"?")+v.data,delete v.data),!1===v.cache&&(f=f.replace(Lt,"$1"),o=(Et.test(f)?"&":"?")+"_="+Ct.guid+++o),v.url=f+o),v.ifModified&&(S.lastModified[f]&&T.setRequestHeader("If-Modified-Since",S.lastModified[f]),S.etag[f]&&T.setRequestHeader("If-None-Match",S.etag[f])),(v.data&&v.hasContent&&!1!==v.contentType||t.contentType)&&T.setRequestHeader("Content-Type",v.contentType),T.setRequestHeader("Accept",v.dataTypes[0]&&v.accepts[v.dataTypes[0]]?v.accepts[v.dataTypes[0]]+("*"!==v.dataTypes[0]?", "+It+"; q=0.01":""):v.accepts["*"]),v.headers)T.setRequestHeader(i,v.headers[i]);if(v.beforeSend&&(!1===v.beforeSend.call(y,T,v)||h))return T.abort();if(u="abort",b.add(v.complete),T.done(v.success),T.fail(v.error),c=Bt(Mt,v,t,T)){if(T.readyState=1,g&&m.trigger("ajaxSend",[T,v]),h)return T;v.async&&0<v.timeout&&(d=C.setTimeout(function(){T.abort("timeout")},v.timeout));try{h=!1,c.send(a,l)}catch(e){if(h)throw e;l(-1,e)}}else l(-1,"No Transport");function l(e,t,n,r){var i,o,a,s,u,l=t;h||(h=!0,d&&C.clearTimeout(d),c=void 0,p=r||"",T.readyState=0<e?4:0,i=200<=e&&e<300||304===e,n&&(s=function(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}(v,T,n)),!i&&-1<S.inArray("script",v.dataTypes)&&(v.converters["text script"]=function(){}),s=function(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}(v,s,T,i),i?(v.ifModified&&((u=T.getResponseHeader("Last-Modified"))&&(S.lastModified[f]=u),(u=T.getResponseHeader("etag"))&&(S.etag[f]=u)),204===e||"HEAD"===v.type?l="nocontent":304===e?l="notmodified":(l=s.state,o=s.data,i=!(a=s.error))):(a=l,!e&&l||(l="error",e<0&&(e=0))),T.status=e,T.statusText=(t||l)+"",i?x.resolveWith(y,[o,l,T]):x.rejectWith(y,[T,l,a]),T.statusCode(w),w=void 0,g&&m.trigger(i?"ajaxSuccess":"ajaxError",[T,v,i?o:a]),b.fireWith(y,[T,l]),g&&(m.trigger("ajaxComplete",[T,v]),--S.active||S.event.trigger("ajaxStop")))}return T},getJSON:function(e,t,n){return S.get(e,t,n,"json")},getScript:function(e,t){return S.get(e,void 0,t,"script")}}),S.each(["get","post"],function(e,i){S[i]=function(e,t,n,r){return m(t)&&(r=r||n,n=t,t=void 0),S.ajax(S.extend({url:e,type:i,dataType:r,data:t,success:n},S.isPlainObject(e)&&e))}}),S.ajaxPrefilter(function(e){var t;for(t in e.headers)"content-type"===t.toLowerCase()&&(e.contentType=e.headers[t]||"")}),S._evalUrl=function(e,t,n){return S.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,converters:{"text script":function(){}},dataFilter:function(e){S.globalEval(e,t,n)}})},S.fn.extend({wrapAll:function(e){var t;return this[0]&&(m(e)&&(e=e.call(this[0])),t=S(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(n){return m(n)?this.each(function(e){S(this).wrapInner(n.call(this,e))}):this.each(function(){var e=S(this),t=e.contents();t.length?t.wrapAll(n):e.append(n)})},wrap:function(t){var n=m(t);return this.each(function(e){S(this).wrapAll(n?t.call(this,e):t)})},unwrap:function(e){return this.parent(e).not("body").each(function(){S(this).replaceWith(this.childNodes)}),this}}),S.expr.pseudos.hidden=function(e){return!S.expr.pseudos.visible(e)},S.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},S.ajaxSettings.xhr=function(){try{return new C.XMLHttpRequest}catch(e){}};var _t={0:200,1223:204},zt=S.ajaxSettings.xhr();y.cors=!!zt&&"withCredentials"in zt,y.ajax=zt=!!zt,S.ajaxTransport(function(i){var o,a;if(y.cors||zt&&!i.crossDomain)return{send:function(e,t){var n,r=i.xhr();if(r.open(i.type,i.url,i.async,i.username,i.password),i.xhrFields)for(n in i.xhrFields)r[n]=i.xhrFields[n];for(n in i.mimeType&&r.overrideMimeType&&r.overrideMimeType(i.mimeType),i.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest"),e)r.setRequestHeader(n,e[n]);o=function(e){return function(){o&&(o=a=r.onload=r.onerror=r.onabort=r.ontimeout=r.onreadystatechange=null,"abort"===e?r.abort():"error"===e?"number"!=typeof r.status?t(0,"error"):t(r.status,r.statusText):t(_t[r.status]||r.status,r.statusText,"text"!==(r.responseType||"text")||"string"!=typeof r.responseText?{binary:r.response}:{text:r.responseText},r.getAllResponseHeaders()))}},r.onload=o(),a=r.onerror=r.ontimeout=o("error"),void 0!==r.onabort?r.onabort=a:r.onreadystatechange=function(){4===r.readyState&&C.setTimeout(function(){o&&a()})},o=o("abort");try{r.send(i.hasContent&&i.data||null)}catch(e){if(o)throw e}},abort:function(){o&&o()}}}),S.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),S.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return S.globalEval(e),e}}}),S.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),S.ajaxTransport("script",function(n){var r,i;if(n.crossDomain||n.scriptAttrs)return{send:function(e,t){r=S("<script>").attr(n.scriptAttrs||{}).prop({charset:n.scriptCharset,src:n.url}).on("load error",i=function(e){r.remove(),i=null,e&&t("error"===e.type?404:200,e.type)}),E.head.appendChild(r[0])},abort:function(){i&&i()}}});var Ut,Xt=[],Vt=/(=)\?(?=&|$)|\?\?/;S.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=Xt.pop()||S.expando+"_"+Ct.guid++;return this[e]=!0,e}}),S.ajaxPrefilter("json jsonp",function(e,t,n){var r,i,o,a=!1!==e.jsonp&&(Vt.test(e.url)?"url":"string"==typeof e.data&&0===(e.contentType||"").indexOf("application/x-www-form-urlencoded")&&Vt.test(e.data)&&"data");if(a||"jsonp"===e.dataTypes[0])return r=e.jsonpCallback=m(e.jsonpCallback)?e.jsonpCallback():e.jsonpCallback,a?e[a]=e[a].replace(Vt,"$1"+r):!1!==e.jsonp&&(e.url+=(Et.test(e.url)?"&":"?")+e.jsonp+"="+r),e.converters["script json"]=function(){return o||S.error(r+" was not called"),o[0]},e.dataTypes[0]="json",i=C[r],C[r]=function(){o=arguments},n.always(function(){void 0===i?S(C).removeProp(r):C[r]=i,e[r]&&(e.jsonpCallback=t.jsonpCallback,Xt.push(r)),o&&m(i)&&i(o[0]),o=i=void 0}),"script"}),y.createHTMLDocument=((Ut=E.implementation.createHTMLDocument("").body).innerHTML="<form></form><form></form>",2===Ut.childNodes.length),S.parseHTML=function(e,t,n){return"string"!=typeof e?[]:("boolean"==typeof t&&(n=t,t=!1),t||(y.createHTMLDocument?((r=(t=E.implementation.createHTMLDocument("")).createElement("base")).href=E.location.href,t.head.appendChild(r)):t=E),o=!n&&[],(i=N.exec(e))?[t.createElement(i[1])]:(i=xe([e],t,o),o&&o.length&&S(o).remove(),S.merge([],i.childNodes)));var r,i,o},S.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return-1<s&&(r=vt(e.slice(s)),e=e.slice(0,s)),m(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),0<a.length&&S.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?S("<div>").append(S.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},S.expr.pseudos.animated=function(t){return S.grep(S.timers,function(e){return t===e.elem}).length},S.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l=S.css(e,"position"),c=S(e),f={};"static"===l&&(e.style.position="relative"),s=c.offset(),o=S.css(e,"top"),u=S.css(e,"left"),("absolute"===l||"fixed"===l)&&-1<(o+u).indexOf("auto")?(a=(r=c.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),m(t)&&(t=t.call(e,n,S.extend({},s))),null!=t.top&&(f.top=t.top-s.top+a),null!=t.left&&(f.left=t.left-s.left+i),"using"in t?t.using.call(e,f):("number"==typeof f.top&&(f.top+="px"),"number"==typeof f.left&&(f.left+="px"),c.css(f))}},S.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){S.offset.setOffset(this,t,e)});var e,n,r=this[0];return r?r.getClientRects().length?(e=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}:void 0},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===S.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===S.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=S(e).offset()).top+=S.css(e,"borderTopWidth",!0),i.left+=S.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-S.css(r,"marginTop",!0),left:t.left-i.left-S.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===S.css(e,"position"))e=e.offsetParent;return e||re})}}),S.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,i){var o="pageYOffset"===i;S.fn[t]=function(e){return $(this,function(e,t,n){var r;if(x(e)?r=e:9===e.nodeType&&(r=e.defaultView),void 0===n)return r?r[i]:e[t];r?r.scrollTo(o?r.pageXOffset:n,o?n:r.pageYOffset):e[t]=n},t,e,arguments.length)}}),S.each(["top","left"],function(e,n){S.cssHooks[n]=$e(y.pixelPosition,function(e,t){if(t)return t=Be(e,n),Me.test(t)?S(e).position()[n]+"px":t})}),S.each({Height:"height",Width:"width"},function(a,s){S.each({padding:"inner"+a,content:s,"":"outer"+a},function(r,o){S.fn[o]=function(e,t){var n=arguments.length&&(r||"boolean"!=typeof e),i=r||(!0===e||!0===t?"margin":"border");return $(this,function(e,t,n){var r;return x(e)?0===o.indexOf("outer")?e["inner"+a]:e.document.documentElement["client"+a]:9===e.nodeType?(r=e.documentElement,Math.max(e.body["scroll"+a],r["scroll"+a],e.body["offset"+a],r["offset"+a],r["client"+a])):void 0===n?S.css(e,t,i):S.style(e,t,n,i)},s,n?e:void 0,n)}})}),S.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){S.fn[t]=function(e){return this.on(t,e)}}),S.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)},hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,n){S.fn[n]=function(e,t){return 0<arguments.length?this.on(n,null,e,t):this.trigger(n)}});var Gt=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;S.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),m(e))return r=s.call(arguments,2),(i=function(){return e.apply(t||this,r.concat(s.call(arguments)))}).guid=e.guid=e.guid||S.guid++,i},S.holdReady=function(e){e?S.readyWait++:S.ready(!0)},S.isArray=Array.isArray,S.parseJSON=JSON.parse,S.nodeName=A,S.isFunction=m,S.isWindow=x,S.camelCase=X,S.type=w,S.now=Date.now,S.isNumeric=function(e){var t=S.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},S.trim=function(e){return null==e?"":(e+"").replace(Gt,"")},"function"==typeof define&&define.amd&&define("jquery",[],function(){return S});var Yt=C.jQuery,Qt=C.$;return S.noConflict=function(e){return C.$===S&&(C.$=Qt),e&&C.jQuery===S&&(C.jQuery=Yt),S},"undefined"==typeof e&&(C.jQuery=C.$=S),S});

/* qrjs2 | https://github.com/englishextra/qrjs2
Kommentar: Hiermit wird der QR-Code erzeugt
*/
!function(r,b){"use strict";for(var x="length",m=[null,[[10,7,17,13],[1,1,1,1],[]],[[16,10,28,22],[1,1,1,1],[4,16]],[[26,15,22,18],[1,1,2,2],[4,20]],[[18,20,16,26],[2,1,4,2],[4,24]],[[24,26,22,18],[2,1,4,4],[4,28]],[[16,18,28,24],[4,2,4,4],[4,32]],[[18,20,26,18],[4,2,5,6],[4,20,36]],[[22,24,26,22],[4,2,6,6],[4,22,40]],[[22,30,24,20],[5,2,8,8],[4,24,44]],[[26,18,28,24],[5,4,8,8],[4,26,48]],[[30,20,24,28],[5,4,11,8],[4,28,52]],[[22,24,28,26],[8,4,11,10],[4,30,56]],[[22,26,22,24],[9,4,16,12],[4,32,60]],[[24,30,24,20],[9,4,16,16],[4,24,44,64]],[[24,22,24,30],[10,6,18,12],[4,24,46,68]],[[28,24,30,24],[10,6,16,17],[4,24,48,72]],[[28,28,28,28],[11,6,19,16],[4,28,52,76]],[[26,30,28,28],[13,6,21,18],[4,28,54,80]],[[26,28,26,26],[14,7,25,21],[4,28,56,84]],[[26,28,28,30],[16,8,25,20],[4,32,60,88]],[[26,28,30,28],[17,8,25,23],[4,26,48,70,92]],[[28,28,24,30],[17,9,34,23],[4,24,48,72,96]],[[28,30,30,30],[18,9,30,25],[4,28,52,76,100]],[[28,30,30,30],[20,10,32,27],[4,26,52,78,104]],[[28,26,30,30],[21,12,35,29],[4,30,56,82,108]],[[28,28,30,28],[23,12,37,34],[4,28,56,84,112]],[[28,30,30,30],[25,12,40,34],[4,32,60,88,116]],[[28,30,30,30],[26,13,42,35],[4,24,48,72,96,120]],[[28,30,30,30],[28,14,45,38],[4,28,52,76,100,124]],[[28,30,30,30],[29,15,48,40],[4,24,50,76,102,128]],[[28,30,30,30],[31,16,51,43],[4,28,54,80,106,132]],[[28,30,30,30],[33,17,54,45],[4,32,58,84,110,136]],[[28,30,30,30],[35,18,57,48],[4,28,56,84,112,140]],[[28,30,30,30],[37,19,60,51],[4,32,60,88,116,144]],[[28,30,30,30],[38,19,63,53],[4,28,52,76,100,124,148]],[[28,30,30,30],[40,20,66,56],[4,22,48,74,100,126,152]],[[28,30,30,30],[43,21,70,59],[4,26,52,78,104,130,156]],[[28,30,30,30],[45,22,74,62],[4,30,56,82,108,134,160]],[[28,30,30,30],[47,24,77,65],[4,24,52,80,108,136,164]],[[28,30,30,30],[49,25,81,68],[4,28,56,84,112,140,168]]],l=/^\d*$/,s=/^[A-Za-z0-9 $%*+\-./:]*$/,c=/^[A-Z0-9 $%*+\-./:]*$/,v=[],h=[-1],t=0,e=1;t<255;++t)v.push(e),h[e]=t,e=2*e^(128<=e?285:0);for(var p=[[]],n=0;n<30;++n){for(var a=p[n],o=[],u=0;u<=n;++u){var i=u<n?v[a[u]]:0,f=v[(n+(a[u-1]||0))%255];o.push(h[i^f])}p.push(o)}for(var d={},g=0;g<45;++g)d["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".charAt(g)]=g;function w(r){return 6<r}function F(r,t){var e=-8&function(r){var t=m[r],e=16*r*r+128*r+64;return w(r)&&(e-=36),t[2][x]&&(e-=25*t[2][x]*t[2][x]-10*t[2][x]-55),e}(r),n=m[r];return e-=8*n[0][t]*n[1][t]}function N(r,t){switch(t){case 1:return r<10?10:r<27?12:14;case 2:return r<10?9:r<27?11:13;case 4:return r<10?8:16;case 8:return r<10?8:r<27?10:12}}function A(r,t,e){var n=F(r,e)-4-N(r,t);switch(t){case 1:return 3*(n/10|0)+(n%10<4?0:n%10<7?1:2);case 2:return 2*(n/11|0)+(n%11<6?0:1);case 4:return n/8|0;case 8:return n/13|0}}function C(r,t){for(var e=r.slice(0),n=r[x],a=t[x],o=0;o<a;++o)e.push(0);for(var u=0;u<n;){var i=h[e[u++]];if(0<=i)for(var f=0;f<a;++f)e[u+f]^=v[(i+t[f])%255]}return e.slice(n)}function S(r,t,e,n){for(var a=r<<n,o=t-1;0<=o;--o)a>>n+o&1&&(a^=e<<o);return r<<n|a}function y(r,t,e){for(var n=L[e],a=r[x],o=0;o<a;++o)for(var u=0;u<a;++u)t[o][u]||(r[o][u]^=n(o,u));return r}function M(r,t,e,n){for(var a=r[x],o=21522^S(e<<3|n,5,1335,10),u=0;u<15;++u){var i=[a-1,a-2,a-3,a-4,a-5,a-6,a-7,a-8,7,5,4,3,2,1,0][u];r[[0,1,2,3,4,5,7,8,a-7,a-6,a-5,a-4,a-3,a-2,a-1][u]][8]=r[8][i]=o>>u&1}return r}function E(r){for(var t=function(r){for(var t=0,e=0;e<r[x];++e)5<=r[e]&&(t+=r[e]-5+3);for(var n=5;n<r[x];n+=2){var a=r[n];r[n-1]===a&&r[n-2]===3*a&&r[n-3]===a&&r[n-4]===a&&(r[n-5]>=4*a||r[n+1]>=4*a)&&(t+=40)}return t},e=r[x],n=0,a=0,o=0;o<e;++o){var u,i=r[o];u=[0];for(var f=0;f<e;){var l;for(l=0;f<e&&i[f];++l)++f;for(u.push(l),l=0;f<e&&!i[f];++l)++f;u.push(l)}n+=t(u),u=[0];for(var s=0;s<e;){var c;for(c=0;s<e&&r[s][o];++c)++s;for(u.push(c),c=0;s<e&&!r[s][o];++c)++s;u.push(c)}n+=t(u);var v=r[o+1]||[];a+=i[0];for(var h=1;h<e;++h){var p=i[h];a+=p,i[h-1]===p&&v[h]===p&&v[h-1]===p&&(n+=3)}}return n+=10*(Math.abs(a/e/e-.5)/.05|0)}function k(r,t,e,n,a){var o=m[t],u=function(r,t,e,n){function a(r,t){if(i<=t){for(o.push(u|r>>(t-=i));8<=t;)o.push(r>>(t-=8)&255);u=0,i=8}0<t&&(u|=(r&(1<<t)-1)<<(i-=t))}var o=[],u=0,i=8,f=e[x],l=N(r,t);switch(a(t,4),a(f,l),t){case 1:for(var s=2;s<f;s+=3)a(parseInt(e.substring(s-2,s+1),10),10);a(parseInt(e.substring(s-2),10),[0,4,7][f%3]);break;case 2:for(var c=1;c<f;c+=2)a(45*d[e.charAt(c-1)]+d[e.charAt(c)],11);f%2==1&&a(d[e.charAt(c-1)],6);break;case 4:for(var v=0;v<f;++v)a(e[v],8)}for(a(0,4),i<8&&o.push(u);o[x]+1<n;)o.push(236,17);return o[x]<n&&o.push(236),o}(t,e,r,F(t,n)>>3);u=function(r,t,e){for(var n=[],a=r[x]/t|0,o=0,u=t-r[x]%t,i=0;i<u;++i)n.push(o),o+=a;for(var f=u;f<t;++f)n.push(o),o+=1+a;n.push(o);for(var l=[],s=0;s<t;++s)l.push(C(r.slice(n[s],n[s+1]),e));for(var c=[],v=r[x]/t|0,h=0;h<v;++h)for(var p=0;p<t;++p)c.push(r[n[p]+h]);for(var d=u;d<t;++d)c.push(r[n[d+1]-1]);for(var g=0;g<e[x];++g)for(var m=0;m<t;++m)c.push(l[m][g]);return c}(u,o[1][n],p[o[0][n]]);var i=function(r){for(var t=m[r],e=function(r){return 4*r+17}(r),i=[],f=[],n=0;n<e;++n)i.push([]),f.push([]);function a(r,t,e,n,a){for(var o=0;o<e;++o)for(var u=0;u<n;++u)i[r+o][t+u]=a[o]>>u&1,f[r+o][t+u]=1}a(0,0,9,9,[127,65,93,93,93,65,383,0,64]),a(e-8,0,8,9,[256,127,65,93,93,93,65,127]),a(0,e-8,9,8,[254,130,186,186,186,130,254,0,0]);for(var o=9;o<e-8;++o)i[6][o]=i[o][6]=1&~o,f[6][o]=f[o][6]=1;for(var u=t[2],l=u[x],s=0;s<l;++s)for(var c=0===s?l-1:l,v=0===s||s===l-1?1:0;v<c;++v)a(u[s],u[v],5,5,[31,17,21,17,31]);if(w(r))for(var h=S(r,6,7973,12),p=0,d=0;d<6;++d)for(var g=0;g<3;++g)i[d][e-11+g]=i[e-11+g][d]=h>>p++&1,f[d][e-11+g]=f[e-11+g][d]=1;return{matrix:i,reserved:f}}(t),f=i.matrix,l=i.reserved;if(function(r,t,e){for(var n=r[x],a=0,o=-1,u=n-1;0<=u;u-=2){6===u&&--u;for(var i=o<0?n-1:0,f=0;f<n;++f){for(var l=u;u-2<l;--l)t[i][l]||(r[i][l]=e[a>>3]>>(7&~a)&1,++a);i+=o}o=-o}}(f,l,u),a<0){y(f,l,0),M(f,0,n,0);var s=0,c=E(f);for(y(f,l,0),a=1;a<8;++a){y(f,l,a),M(f,0,n,a);var v=E(f);v<c&&(c=v,s=a),y(f,l,a)}a=s}return y(f,l,a),M(f,0,n,a),f}var L=[function(r,t){return(r+t)%2==0},function(r){return r%2==0},function(r,t){return t%3==0},function(r,t){return(r+t)%3==0},function(r,t){return((r/2|0)+(t/3|0))%2==0},function(r,t){return r*t%2+r*t%3==0},function(r,t){return(r*t%2+r*t%3)%2==0},function(r,t){return((r+t)%2+r*t%3)%2==0}],R={generate:function(r,t){var e=t||{},n={numeric:1,alphanumeric:2,octet:4},a={L:1,M:0,Q:3,H:2},o=e.version||-1,u=a[(e.ecclevel||"L").toUpperCase()],i=e.mode?n[e.mode.toLowerCase()]:-1,f="mask"in e?e.mask:-1;if(i<0)i="string"==typeof r?r.match(l)?1:r.match(c)?2:4:4;else if(1!==i&&2!==i&&4!==i)throw"invalid or unsupported mode";if(null===(r=function(r,t){switch(r){case 1:return t.match(l)?t:null;case 2:return t.match(s)?t.toUpperCase():null;case 4:if("string"!=typeof t)return t;for(var e=[],n=0;n<t[x];++n){var a=t.charCodeAt(n);a<128?e.push(a):a<2048?e.push(192|a>>6,128|63&a):a<65536?e.push(224|a>>12,128|a>>6&63,128|63&a):e.push(240|a>>18,128|a>>12&63,128|a>>6&63,128|63&a)}return e}}(i,r)))throw"invalid data format";if(u<0||3<u)throw"invalid ECC level";if(o<0){for(o=1;o<=40&&!(r[x]<=A(o,i,u));++o);if(40<o)throw"too large data"}else if(o<1||40<o)throw"invalid version";if(-1!==f&&(f<0||8<f))throw"invalid mask";return k(r,o,i,u,f)},generateHTML:function(r,t){for(var e=t||{},n=e.fillcolor?e.fillcolor:"#FFFFFF",a=e.textcolor?e.textcolor:"#000000",o=R.generate(r,e),u=Math.max(e.modulesize||5,.5),i=Math.max(null!==e.margin?e.margin:4,0),f=b.createElement("div"),l=o[x],s=['<table border="0" cellspacing="0" cellpadding="0" style="border:'+u*i+"px solid "+n+";background:"+n+'">'],c=0;c<l;++c){s.push("<tr>");for(var v=0;v<l;++v)s.push('<td style="width:'+u+"px;height:"+u+"px"+(o[c][v]?";background:"+a:"")+'"></td>');s.push("</tr>")}f.className="qrcode";var h=b.createRange();h.selectNodeContents(f);var p=h.createContextualFragment(s.join("")+"</table>");return f.appendChild(p),f},generateSVG:function(r,t){var e=t||{},n=e.fillcolor?e.fillcolor:"#FFFFFF",a=e.textcolor?e.textcolor:"#000000",o=R.generate(r,e),u=o[x],i=Math.max(e.modulesize||5,.5),f=Math.max(e.margin?e.margin:4,0),l=i*(u+2*f),s=b.createElementNS("http://www.w3.org/2000/svg","svg");s.setAttributeNS(null,"viewBox","0 0 "+l+" "+l),s.setAttributeNS(null,"style","shape-rendering:crispEdges");var c="qrcode"+Date.now();s.setAttributeNS(null,"id",c);var v=b.createDocumentFragment(),h=b.createElementNS("http://www.w3.org/2000/svg","style");h.appendChild(b.createTextNode("#"+c+" .bg{fill:"+n+"}#"+c+" .fg{fill:"+a+"}")),v.appendChild(h);function p(r,t,e,n,a){var o=b.createElementNS("http://www.w3.org/2000/svg","rect")||"";return o.setAttributeNS(null,"class",r),o.setAttributeNS(null,"fill",t),o.setAttributeNS(null,"x",e),o.setAttributeNS(null,"y",n),o.setAttributeNS(null,"width",a),o.setAttributeNS(null,"height",a),o}v.appendChild(p("bg","none",0,0,l));for(var d=f*i,g=0;g<u;++g){for(var m=f*i,w=0;w<u;++w)o[g][w]&&v.appendChild(p("fg","none",m,d,i)),m+=i;d+=i}return s.appendChild(v),s},generatePNG:function(r,t){var e,n=t||{},a=n.fillcolor||"#FFFFFF",o=n.textcolor||"#000000",u=R.generate(r,n),i=Math.max(n.modulesize||5,.5),f=Math.max(null!==n.margin&&void 0!==n.margin?n.margin:4,0),l=u[x],s=i*(l+2*f),c=b.createElement("canvas");if(c.width=c.height=s,!(e=c.getContext("2d")))throw"canvas support is needed for PNG output";e.fillStyle=a,e.fillRect(0,0,s,s),e.fillStyle=o;for(var v=0;v<l;++v)for(var h=0;h<l;++h)u[v][h]&&e.fillRect(i*(f+h),i*(f+v),i,i);return c.toDataURL()}};r.QRCode=R}("undefined"!=typeof window?window:this,document);


/****************************
 * Methods
 ****************************/

const displayMarkers = function(displayValue) {
    const links_list = document.getElementsByClassName("divLinks")
    for(i=0, len=links_list.length; i<len; i++) {
        if (displayValue == "hide") {
            links_list[i].style.display = "none"
        }
        if (displayValue == "show") {
            links_list[i].style.display = ""
        }
    }
    const rechts_list = document.getElementsByClassName("divRechts")
    for(i=0, len=links_list.length; i<len; i++) {
        if (displayValue == "hide") {
            rechts_list[i].style.display = "none"
        }
        if (displayValue == "show") {
            rechts_list[i].style.display = "flex"
        }
    }
}

const postData = function(data) {
    const url = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postInputs"
    sendPostHttpRequest(url, data)
}
const sendPostHttpRequest = function(url, data) {
    console.log("POST...")
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("POST", url)
        xhr.responseType = "text"
        // xhr.setRequestHeader = "Content-Type", "application/json"
        xhr.setRequestHeader = "Content-Type", "text/plain"
        xhr.onload = function() { //onload -> wenn Response kommt
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() {
            reject("Keine Antwort vom Server :-(")
        }
        xhr.send(JSON.stringify(data))
    })
    return promise
}
const getCheckConnection = function() {
    const url = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/getCheckConnection"
    const p_connectionL2P = document.getElementById("p_connectionL2P")
    sendGetHttpRequest(url).then(responseData => {
        document.getElementById("svg_rectString").setAttributeNS(null, "fill", "green")
        console.log(responseData)
        // return responseData
    }).catch(err => {
        document.getElementById("svg_rectString").setAttributeNS(null, "fill", "red")
        console.log(err)
        // p_connectionL2P.innerHTML += "false"
        // return false
    })
}

const sendGetHttpRequest = function(url) {
    // console.log("GET...")
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", url)
        xhr.responseType = "text"
        xhr.onload = function() { //onload -> wenn Response kommt (auch bei "Fail"-Response)
            resolve(xhr.response)
        }
        xhr.onerror = function() { //onerror -> wenn gar keine Response kommt
            reject("No response from the server :-(")
        }
        xhr.send()
    })
    return promise
}

const checkConnectionToL2PService = function() {
    
    const answer = getCheckConnection()
    console.log("Answer = " + answer)
    if(answer == "Dies ist ein String als Inhalt einer Antwort.") {
        p_connectionL2P.innerHTML += "true"
    }else {
        p_connectionL2P.innerHTML += "false"
    }
}

const infiniteLoop = function() {
    // console.log("Infinite Loop")
    antiOnyx()
    getCheckConnection()
    setTimeout(infiniteLoop, 3000)
}

const getFragebogenTitle = function() {
    try {
        const element_navbar = document.getElementsByClassName("nav-header")[0]
        const title = element_navbar.getElementsByTagName("h1")[0].innerHTML
        console.log("Fragebogen-Title is " + title)
        return title
    }
    catch(err) {
        console.log("No Fragebogen-Title exists.")
        return "None"
    }
}

const crawlElementId = function(element) {
    // This Method is used on an input-element and climbs up the hierarchy tree to find its elementId which it was tagged with 
    while(element.parentElement) { //solange element ein Parent hat
        // console.log(element)
        if(element.getAttribute("markerId") != null) {
            const markerId = element.getAttribute("markerId")
            return markerId
        } else {
            element = element.parentNode
        }
    }
    return 999
}

const createDebuggingHeader = function() {
    console.log("Create Debugging Header")

    //DebuggingHeader
    const element_pageHeader = document.getElementsByTagName("header")[0]
    const debuggingHeader = document.createElement("div")
    debuggingHeader.id = "debugging-header"
    element_pageHeader.appendChild(debuggingHeader)
    debuggingHeader.style.backgroundColor = "#87CEEB"
    
    debuggingHeader_html = `
        <div class="connectionL2P">
            <p id="p_connectionL2P">
                Connection to L2P-Service = 
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><rect id="svg_rectString" width="15" height="15" fill="black" /></svg>
            </p>
        <div>
        <div class = codes_checkboxes>
            <label for="db_qrcodes">qrCodes</label>
            <input type="checkbox" id="db_qrcodes" name="db_qrcodes" checked="checked">
            <label for="db_arucomarker">AruCo Marker</label>
            <input type="checkbox" id="db_arucomarker" name="db_arucomarker" checked="checked">
        </div>
    `
    debuggingHeader.innerHTML = debuggingHeader_html

    // for code_checkboxes
    var checkbox = document.querySelector("input[name=db_qrcodes]");
    checkbox.addEventListener('change', function() {
    if (this.checked) {
        displayMarkers("show")
        localStorage.setItem("showMarker", "show")
    } else {
        displayMarkers("hide")
        localStorage.setItem("showMarker", "hide")
    }
    })
}

const createDebuggings = function(tasks) {
    const element_pageHeader = document.getElementsByClassName("page-header-additional")[0]
    const element_btn_printJsons = document.createElement("button")
    element_btn_printJsons.setAttribute("type", "button")
    element_btn_printJsons.innerHTML = "print jsons"
    element_btn_printJsons.addEventListener("click", function() {
        console.log("Button geklickt :-)")
        // tasks.createJsonForAllTasks()
        globalTimeline.printJson()
    })
    element_pageHeader.appendChild(element_btn_printJsons)
}
const resetLocalStorage = function() {
    localStorage.setItem("username", "")
    localStorage.setItem("startTimestamp", "")
    localStorage.setItem("timelineInputs", "")
}
const hideElements = function() {
    const hide_testabschliessen = function() {
        const element_btn_testAbschliessen = document.getElementsByClassName("btn btn-highlight")[0]
        if(element_btn_testAbschliessen != undefined) {
            element_btn_testAbschliessen.style.visibility = "hidden"
        }
    }
    const hide_navContainer = function() {
        const element_navContainer = document.getElementsByTagName("nav")
        console.log("Navigationscontainer-Länge=" + element_navContainer.length)
        if(element_navContainer != undefined) {
            element_navContainer[0].style.visibility = "hidden"
        }
    }
    const hide_antwortAbgeben = function() {
        const element_btn_antwortAbgeben = document.getElementsByClassName("btn btn-outline-secondary")
        for(let i=0, len=element_btn_antwortAbgeben.length; i<len; i++) {
            element_btn_antwortAbgeben[i].style.visibility = "hidden"
        }
        const element_btn_weiter = document.getElementsByClassName("btn btn-outline-secondary forward")
        if(element_btn_weiter.length > 0) {
            element_btn_weiter[0].style.visibility = "visible"
        }
    }
    const downloadJsonsOnUnload = function() {
        var btn_finish = document.getElementById("finish")
        var btn_forward = document.getElementById("forward")
        btn_finish.addEventListener("click", function() {
            console.log("Finish-Button geklickt :-)")
            globalTimeline.createJsonFromLocalStorage()
            resetLocalStorage()
        })
        if(btn_forward != null) {
            btn_forward.addEventListener("click", function() {
                console.log("Forward-Button geklickt :-)")
                // globalTimeline.createJson()
            })
        } else {
            console.log("Forward-Button does not exist.")
        }
    }
    hide_testabschliessen()
    hide_antwortAbgeben()
    downloadJsonsOnUnload()
}

const antiOnyx = function() {
    // make navbar-width smaller
    try {
        const element_navContainer = document.getElementsByTagName("nav")
        if(element_navContainer != undefined) {
            element_navContainer[0].style.width = "200px"
        }
    }
    catch(err) {
        console.log(err)
    }

    var divContent = document.getElementsByClassName("content")[0]
    divContent.style.height = "560px"
    divContent.style.overflowY = "auto"
    divContent.style.marginLeft = "0px"
    var divHeaderContainer = document.getElementsByClassName("header-container")[0]
    divHeaderContainer.style.position = "relative"
    divHeaderContainer.style.left = "0px"

    // Drag&Drop-Tasks width minimieren
    const ims = document.getElementsByClassName("interaction-match")
    for(let i=0, len=ims.length; i<len; i++) {
        im = ims[i]
        im.style.width = "auto"
    }
}

const appendAsFirstChild = function(parent, newChild) {
    parent.before(newChild, parent.firstChild)
}

const createPElement = function(text, color) {
    const pElement = document.createElement("p")
    const textNode = document.createTextNode("\"" + text + "\"")
    pElement.appendChild(textNode)
    pElement.style.color = color
    return pElement
}

const moveChildNodes = function(oldParent, newParent) {
    // for(let i=0, len=oldParent.childNodes.length; i<len; i++) {
    //     newParent.appendChild(oldParent.childNodes[0])
    // }
    while (oldParent.childNodes.length > 0) {
        newParent.appendChild(oldParent.childNodes[0])
    }
}

const jsonDownloadEvent = function(exportObj, exportName, clickElement) {
    clickElement.addEventListener("click", function() {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj))
        var downloadAnchorNode = document.createElement('a')
        downloadAnchorNode.setAttribute("href", dataStr)
        downloadAnchorNode.setAttribute("download", exportName + ".json")
        document.body.appendChild(downloadAnchorNode)
        downloadAnchorNode.click()
        downloadAnchorNode.remove()
    })
}

const jsonDownload = function(exportObj, exportName){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj))
    var downloadAnchorNode = document.createElement('a')
    downloadAnchorNode.setAttribute("href", dataStr)
    downloadAnchorNode.setAttribute("download", exportName + ".json")
    document.body.appendChild(downloadAnchorNode)
    downloadAnchorNode.click()
    downloadAnchorNode.remove()
}

/**************************************************************************************************/
function generateMarkerSvg(width, height, bits) {
	var svg = $('<svg/>').attr({
		viewBox: '0 0 ' + (width + 2) + ' ' + (height + 2),
		xmlns: 'http://www.w3.org/2000/svg',
		'shape-rendering': 'crispEdges' // disable anti-aliasing to avoid little gaps between rects
	});
	$('<rect/>').attr({ // Background rect
		x: 0,
		y: 0,
		width: width + 2,
		height: height + 2,
		fill: 'black'
	}).appendTo(svg);
	for (var i = 0; i < height; i++) { // "Pixels"
		for (var j = 0; j < width; j++) {
			var color = bits[i * height + j] ? 'white' : 'black';
			var pixel = $('<rect/>').attr({
				width: 1,
				height: 1,
				x: j + 1,
				y: i + 1,
				fill: color
			});
			pixel.appendTo(svg)
		}
	}
	return svg
}

function generateArucoMarker(width, height, dictName, id) {
    // console.log('Generate ArUco marker ' + dictName + ' ' + id);
    
    const dict = {"aruco":[[132,33,8,0],[132,33,11,1],[132,33,4,1],[132,33,7,0],[132,33,120,0],[132,33,123,1],[132,33,116,1],[132,33,119,0],[132,32,152,0],[132,32,155,1],[132,32,148,1],[132,32,151,0],[132,32,232,0],[132,32,235,1],[132,32,228,1],[132,32,231,0],[132,47,8,0],[132,47,11,1],[132,47,4,1],[132,47,7,0],[132,47,120,0],[132,47,123,1],[132,47,116,1],[132,47,119,0],[132,46,152,0],[132,46,155,1],[132,46,148,1],[132,46,151,0],[132,46,232,0],[132,46,235,1],[132,46,228,1],[132,46,231,0],[132,19,8,0],[132,19,11,1],[132,19,4,1],[132,19,7,0],[132,19,120,0],[132,19,123,1],[132,19,116,1],[132,19,119,0],[132,18,152,0],[132,18,155,1],[132,18,148,1],[132,18,151,0],[132,18,232,0],[132,18,235,1],[132,18,228,1],[132,18,231,0],[132,29,8,0],[132,29,11,1],[132,29,4,1],[132,29,7,0],[132,29,120,0],[132,29,123,1],[132,29,116,1],[132,29,119,0],[132,28,152,0],[132,28,155,1],[132,28,148,1],[132,28,151,0],[132,28,232,0],[132,28,235,1],[132,28,228,1],[132,28,231,0],[133,225,8,0],[133,225,11,1],[133,225,4,1],[133,225,7,0],[133,225,120,0],[133,225,123,1],[133,225,116,1],[133,225,119,0],[133,224,152,0],[133,224,155,1],[133,224,148,1],[133,224,151,0],[133,224,232,0],[133,224,235,1],[133,224,228,1],[133,224,231,0],[133,239,8,0],[133,239,11,1],[133,239,4,1],[133,239,7,0],[133,239,120,0],[133,239,123,1],[133,239,116,1],[133,239,119,0],[133,238,152,0],[133,238,155,1],[133,238,148,1],[133,238,151,0],[133,238,232,0],[133,238,235,1],[133,238,228,1],[133,238,231,0],[133,211,8,0],[133,211,11,1],[133,211,4,1],[133,211,7,0],[133,211,120,0],[133,211,123,1],[133,211,116,1],[133,211,119,0],[133,210,152,0],[133,210,155,1],[133,210,148,1],[133,210,151,0],[133,210,232,0],[133,210,235,1],[133,210,228,1],[133,210,231,0],[133,221,8,0],[133,221,11,1],[133,221,4,1],[133,221,7,0],[133,221,120,0],[133,221,123,1],[133,221,116,1],[133,221,119,0],[133,220,152,0],[133,220,155,1],[133,220,148,1],[133,220,151,0],[133,220,232,0],[133,220,235,1],[133,220,228,1],[133,220,231,0],[130,97,8,0],[130,97,11,1],[130,97,4,1],[130,97,7,0],[130,97,120,0],[130,97,123,1],[130,97,116,1],[130,97,119,0],[130,96,152,0],[130,96,155,1],[130,96,148,1],[130,96,151,0],[130,96,232,0],[130,96,235,1],[130,96,228,1],[130,96,231,0],[130,111,8,0],[130,111,11,1],[130,111,4,1],[130,111,7,0],[130,111,120,0],[130,111,123,1],[130,111,116,1],[130,111,119,0],[130,110,152,0],[130,110,155,1],[130,110,148,1],[130,110,151,0],[130,110,232,0],[130,110,235,1],[130,110,228,1],[130,110,231,0],[130,83,8,0],[130,83,11,1],[130,83,4,1],[130,83,7,0],[130,83,120,0],[130,83,123,1],[130,83,116,1],[130,83,119,0],[130,82,152,0],[130,82,155,1],[130,82,148,1],[130,82,151,0],[130,82,232,0],[130,82,235,1],[130,82,228,1],[130,82,231,0],[130,93,8,0],[130,93,11,1],[130,93,4,1],[130,93,7,0],[130,93,120,0],[130,93,123,1],[130,93,116,1],[130,93,119,0],[130,92,152,0],[130,92,155,1],[130,92,148,1],[130,92,151,0],[130,92,232,0],[130,92,235,1],[130,92,228,1],[130,92,231,0],[131,161,8,0],[131,161,11,1],[131,161,4,1],[131,161,7,0],[131,161,120,0],[131,161,123,1],[131,161,116,1],[131,161,119,0],[131,160,152,0],[131,160,155,1],[131,160,148,1],[131,160,151,0],[131,160,232,0],[131,160,235,1],[131,160,228,1],[131,160,231,0],[131,175,8,0],[131,175,11,1],[131,175,4,1],[131,175,7,0],[131,175,120,0],[131,175,123,1],[131,175,116,1],[131,175,119,0],[131,174,152,0],[131,174,155,1],[131,174,148,1],[131,174,151,0],[131,174,232,0],[131,174,235,1],[131,174,228,1],[131,174,231,0],[131,147,8,0],[131,147,11,1],[131,147,4,1],[131,147,7,0],[131,147,120,0],[131,147,123,1],[131,147,116,1],[131,147,119,0],[131,146,152,0],[131,146,155,1],[131,146,148,1],[131,146,151,0],[131,146,232,0],[131,146,235,1],[131,146,228,1],[131,146,231,0],[131,157,8,0],[131,157,11,1],[131,157,4,1],[131,157,7,0],[131,157,120,0],[131,157,123,1],[131,157,116,1],[131,157,119,0],[131,156,152,0],[131,156,155,1],[131,156,148,1],[131,156,151,0],[131,156,232,0],[131,156,235,1],[131,156,228,1],[131,156,231,0],[188,33,8,0],[188,33,11,1],[188,33,4,1],[188,33,7,0],[188,33,120,0],[188,33,123,1],[188,33,116,1],[188,33,119,0],[188,32,152,0],[188,32,155,1],[188,32,148,1],[188,32,151,0],[188,32,232,0],[188,32,235,1],[188,32,228,1],[188,32,231,0],[188,47,8,0],[188,47,11,1],[188,47,4,1],[188,47,7,0],[188,47,120,0],[188,47,123,1],[188,47,116,1],[188,47,119,0],[188,46,152,0],[188,46,155,1],[188,46,148,1],[188,46,151,0],[188,46,232,0],[188,46,235,1],[188,46,228,1],[188,46,231,0],[188,19,8,0],[188,19,11,1],[188,19,4,1],[188,19,7,0],[188,19,120,0],[188,19,123,1],[188,19,116,1],[188,19,119,0],[188,18,152,0],[188,18,155,1],[188,18,148,1],[188,18,151,0],[188,18,232,0],[188,18,235,1],[188,18,228,1],[188,18,231,0],[188,29,8,0],[188,29,11,1],[188,29,4,1],[188,29,7,0],[188,29,120,0],[188,29,123,1],[188,29,116,1],[188,29,119,0],[188,28,152,0],[188,28,155,1],[188,28,148,1],[188,28,151,0],[188,28,232,0],[188,28,235,1],[188,28,228,1],[188,28,231,0],[189,225,8,0],[189,225,11,1],[189,225,4,1],[189,225,7,0],[189,225,120,0],[189,225,123,1],[189,225,116,1],[189,225,119,0],[189,224,152,0],[189,224,155,1],[189,224,148,1],[189,224,151,0],[189,224,232,0],[189,224,235,1],[189,224,228,1],[189,224,231,0],[189,239,8,0],[189,239,11,1],[189,239,4,1],[189,239,7,0],[189,239,120,0],[189,239,123,1],[189,239,116,1],[189,239,119,0],[189,238,152,0],[189,238,155,1],[189,238,148,1],[189,238,151,0],[189,238,232,0],[189,238,235,1],[189,238,228,1],[189,238,231,0],[189,211,8,0],[189,211,11,1],[189,211,4,1],[189,211,7,0],[189,211,120,0],[189,211,123,1],[189,211,116,1],[189,211,119,0],[189,210,152,0],[189,210,155,1],[189,210,148,1],[189,210,151,0],[189,210,232,0],[189,210,235,1],[189,210,228,1],[189,210,231,0],[189,221,8,0],[189,221,11,1],[189,221,4,1],[189,221,7,0],[189,221,120,0],[189,221,123,1],[189,221,116,1],[189,221,119,0],[189,220,152,0],[189,220,155,1],[189,220,148,1],[189,220,151,0],[189,220,232,0],[189,220,235,1],[189,220,228,1],[189,220,231,0],[186,97,8,0],[186,97,11,1],[186,97,4,1],[186,97,7,0],[186,97,120,0],[186,97,123,1],[186,97,116,1],[186,97,119,0],[186,96,152,0],[186,96,155,1],[186,96,148,1],[186,96,151,0],[186,96,232,0],[186,96,235,1],[186,96,228,1],[186,96,231,0],[186,111,8,0],[186,111,11,1],[186,111,4,1],[186,111,7,0],[186,111,120,0],[186,111,123,1],[186,111,116,1],[186,111,119,0],[186,110,152,0],[186,110,155,1],[186,110,148,1],[186,110,151,0],[186,110,232,0],[186,110,235,1],[186,110,228,1],[186,110,231,0],[186,83,8,0],[186,83,11,1],[186,83,4,1],[186,83,7,0],[186,83,120,0],[186,83,123,1],[186,83,116,1],[186,83,119,0],[186,82,152,0],[186,82,155,1],[186,82,148,1],[186,82,151,0],[186,82,232,0],[186,82,235,1],[186,82,228,1],[186,82,231,0],[186,93,8,0],[186,93,11,1],[186,93,4,1],[186,93,7,0],[186,93,120,0],[186,93,123,1],[186,93,116,1],[186,93,119,0],[186,92,152,0],[186,92,155,1],[186,92,148,1],[186,92,151,0],[186,92,232,0],[186,92,235,1],[186,92,228,1],[186,92,231,0],[187,161,8,0],[187,161,11,1],[187,161,4,1],[187,161,7,0],[187,161,120,0],[187,161,123,1],[187,161,116,1],[187,161,119,0],[187,160,152,0],[187,160,155,1],[187,160,148,1],[187,160,151,0],[187,160,232,0],[187,160,235,1],[187,160,228,1],[187,160,231,0],[187,175,8,0],[187,175,11,1],[187,175,4,1],[187,175,7,0],[187,175,120,0],[187,175,123,1],[187,175,116,1],[187,175,119,0],[187,174,152,0],[187,174,155,1],[187,174,148,1],[187,174,151,0],[187,174,232,0],[187,174,235,1],[187,174,228,1],[187,174,231,0],[187,147,8,0],[187,147,11,1],[187,147,4,1],[187,147,7,0],[187,147,120,0],[187,147,123,1],[187,147,116,1],[187,147,119,0],[187,146,152,0],[187,146,155,1],[187,146,148,1],[187,146,151,0],[187,146,232,0],[187,146,235,1],[187,146,228,1],[187,146,231,0],[187,157,8,0],[187,157,11,1],[187,157,4,1],[187,157,7,0],[187,157,120,0],[187,157,123,1],[187,157,116,1],[187,157,119,0],[187,156,152,0],[187,156,155,1],[187,156,148,1],[187,156,151,0],[187,156,232,0],[187,156,235,1],[187,156,228,1],[187,156,231,0],[76,33,8,0],[76,33,11,1],[76,33,4,1],[76,33,7,0],[76,33,120,0],[76,33,123,1],[76,33,116,1],[76,33,119,0],[76,32,152,0],[76,32,155,1],[76,32,148,1],[76,32,151,0],[76,32,232,0],[76,32,235,1],[76,32,228,1],[76,32,231,0],[76,47,8,0],[76,47,11,1],[76,47,4,1],[76,47,7,0],[76,47,120,0],[76,47,123,1],[76,47,116,1],[76,47,119,0],[76,46,152,0],[76,46,155,1],[76,46,148,1],[76,46,151,0],[76,46,232,0],[76,46,235,1],[76,46,228,1],[76,46,231,0],[76,19,8,0],[76,19,11,1],[76,19,4,1],[76,19,7,0],[76,19,120,0],[76,19,123,1],[76,19,116,1],[76,19,119,0],[76,18,152,0],[76,18,155,1],[76,18,148,1],[76,18,151,0],[76,18,232,0],[76,18,235,1],[76,18,228,1],[76,18,231,0],[76,29,8,0],[76,29,11,1],[76,29,4,1],[76,29,7,0],[76,29,120,0],[76,29,123,1],[76,29,116,1],[76,29,119,0],[76,28,152,0],[76,28,155,1],[76,28,148,1],[76,28,151,0],[76,28,232,0],[76,28,235,1],[76,28,228,1],[76,28,231,0],[77,225,8,0],[77,225,11,1],[77,225,4,1],[77,225,7,0],[77,225,120,0],[77,225,123,1],[77,225,116,1],[77,225,119,0],[77,224,152,0],[77,224,155,1],[77,224,148,1],[77,224,151,0],[77,224,232,0],[77,224,235,1],[77,224,228,1],[77,224,231,0],[77,239,8,0],[77,239,11,1],[77,239,4,1],[77,239,7,0],[77,239,120,0],[77,239,123,1],[77,239,116,1],[77,239,119,0],[77,238,152,0],[77,238,155,1],[77,238,148,1],[77,238,151,0],[77,238,232,0],[77,238,235,1],[77,238,228,1],[77,238,231,0],[77,211,8,0],[77,211,11,1],[77,211,4,1],[77,211,7,0],[77,211,120,0],[77,211,123,1],[77,211,116,1],[77,211,119,0],[77,210,152,0],[77,210,155,1],[77,210,148,1],[77,210,151,0],[77,210,232,0],[77,210,235,1],[77,210,228,1],[77,210,231,0],[77,221,8,0],[77,221,11,1],[77,221,4,1],[77,221,7,0],[77,221,120,0],[77,221,123,1],[77,221,116,1],[77,221,119,0],[77,220,152,0],[77,220,155,1],[77,220,148,1],[77,220,151,0],[77,220,232,0],[77,220,235,1],[77,220,228,1],[77,220,231,0],[74,97,8,0],[74,97,11,1],[74,97,4,1],[74,97,7,0],[74,97,120,0],[74,97,123,1],[74,97,116,1],[74,97,119,0],[74,96,152,0],[74,96,155,1],[74,96,148,1],[74,96,151,0],[74,96,232,0],[74,96,235,1],[74,96,228,1],[74,96,231,0],[74,111,8,0],[74,111,11,1],[74,111,4,1],[74,111,7,0],[74,111,120,0],[74,111,123,1],[74,111,116,1],[74,111,119,0],[74,110,152,0],[74,110,155,1],[74,110,148,1],[74,110,151,0],[74,110,232,0],[74,110,235,1],[74,110,228,1],[74,110,231,0],[74,83,8,0],[74,83,11,1],[74,83,4,1],[74,83,7,0],[74,83,120,0],[74,83,123,1],[74,83,116,1],[74,83,119,0],[74,82,152,0],[74,82,155,1],[74,82,148,1],[74,82,151,0],[74,82,232,0],[74,82,235,1],[74,82,228,1],[74,82,231,0],[74,93,8,0],[74,93,11,1],[74,93,4,1],[74,93,7,0],[74,93,120,0],[74,93,123,1],[74,93,116,1],[74,93,119,0],[74,92,152,0],[74,92,155,1],[74,92,148,1],[74,92,151,0],[74,92,232,0],[74,92,235,1],[74,92,228,1],[74,92,231,0],[75,161,8,0],[75,161,11,1],[75,161,4,1],[75,161,7,0],[75,161,120,0],[75,161,123,1],[75,161,116,1],[75,161,119,0],[75,160,152,0],[75,160,155,1],[75,160,148,1],[75,160,151,0],[75,160,232,0],[75,160,235,1],[75,160,228,1],[75,160,231,0],[75,175,8,0],[75,175,11,1],[75,175,4,1],[75,175,7,0],[75,175,120,0],[75,175,123,1],[75,175,116,1],[75,175,119,0],[75,174,152,0],[75,174,155,1],[75,174,148,1],[75,174,151,0],[75,174,232,0],[75,174,235,1],[75,174,228,1],[75,174,231,0],[75,147,8,0],[75,147,11,1],[75,147,4,1],[75,147,7,0],[75,147,120,0],[75,147,123,1],[75,147,116,1],[75,147,119,0],[75,146,152,0],[75,146,155,1],[75,146,148,1],[75,146,151,0],[75,146,232,0],[75,146,235,1],[75,146,228,1],[75,146,231,0],[75,157,8,0],[75,157,11,1],[75,157,4,1],[75,157,7,0],[75,157,120,0],[75,157,123,1],[75,157,116,1],[75,157,119,0],[75,156,152,0],[75,156,155,1],[75,156,148,1],[75,156,151,0],[75,156,232,0],[75,156,235,1],[75,156,228,1],[75,156,231,0],[116,33,8,0],[116,33,11,1],[116,33,4,1],[116,33,7,0],[116,33,120,0],[116,33,123,1],[116,33,116,1],[116,33,119,0],[116,32,152,0],[116,32,155,1],[116,32,148,1],[116,32,151,0],[116,32,232,0],[116,32,235,1],[116,32,228,1],[116,32,231,0],[116,47,8,0],[116,47,11,1],[116,47,4,1],[116,47,7,0],[116,47,120,0],[116,47,123,1],[116,47,116,1],[116,47,119,0],[116,46,152,0],[116,46,155,1],[116,46,148,1],[116,46,151,0],[116,46,232,0],[116,46,235,1],[116,46,228,1],[116,46,231,0],[116,19,8,0],[116,19,11,1],[116,19,4,1],[116,19,7,0],[116,19,120,0],[116,19,123,1],[116,19,116,1],[116,19,119,0],[116,18,152,0],[116,18,155,1],[116,18,148,1],[116,18,151,0],[116,18,232,0],[116,18,235,1],[116,18,228,1],[116,18,231,0],[116,29,8,0],[116,29,11,1],[116,29,4,1],[116,29,7,0],[116,29,120,0],[116,29,123,1],[116,29,116,1],[116,29,119,0],[116,28,152,0],[116,28,155,1],[116,28,148,1],[116,28,151,0],[116,28,232,0],[116,28,235,1],[116,28,228,1],[116,28,231,0],[117,225,8,0],[117,225,11,1],[117,225,4,1],[117,225,7,0],[117,225,120,0],[117,225,123,1],[117,225,116,1],[117,225,119,0],[117,224,152,0],[117,224,155,1],[117,224,148,1],[117,224,151,0],[117,224,232,0],[117,224,235,1],[117,224,228,1],[117,224,231,0],[117,239,8,0],[117,239,11,1],[117,239,4,1],[117,239,7,0],[117,239,120,0],[117,239,123,1],[117,239,116,1],[117,239,119,0],[117,238,152,0],[117,238,155,1],[117,238,148,1],[117,238,151,0],[117,238,232,0],[117,238,235,1],[117,238,228,1],[117,238,231,0],[117,211,8,0],[117,211,11,1],[117,211,4,1],[117,211,7,0],[117,211,120,0],[117,211,123,1],[117,211,116,1],[117,211,119,0],[117,210,152,0],[117,210,155,1],[117,210,148,1],[117,210,151,0],[117,210,232,0],[117,210,235,1],[117,210,228,1],[117,210,231,0],[117,221,8,0],[117,221,11,1],[117,221,4,1],[117,221,7,0],[117,221,120,0],[117,221,123,1],[117,221,116,1],[117,221,119,0],[117,220,152,0],[117,220,155,1],[117,220,148,1],[117,220,151,0],[117,220,232,0],[117,220,235,1],[117,220,228,1],[117,220,231,0],[114,97,8,0],[114,97,11,1],[114,97,4,1],[114,97,7,0],[114,97,120,0],[114,97,123,1],[114,97,116,1],[114,97,119,0],[114,96,152,0],[114,96,155,1],[114,96,148,1],[114,96,151,0],[114,96,232,0],[114,96,235,1],[114,96,228,1],[114,96,231,0],[114,111,8,0],[114,111,11,1],[114,111,4,1],[114,111,7,0],[114,111,120,0],[114,111,123,1],[114,111,116,1],[114,111,119,0],[114,110,152,0],[114,110,155,1],[114,110,148,1],[114,110,151,0],[114,110,232,0],[114,110,235,1],[114,110,228,1],[114,110,231,0],[114,83,8,0],[114,83,11,1],[114,83,4,1],[114,83,7,0],[114,83,120,0],[114,83,123,1],[114,83,116,1],[114,83,119,0],[114,82,152,0],[114,82,155,1],[114,82,148,1],[114,82,151,0],[114,82,232,0],[114,82,235,1],[114,82,228,1],[114,82,231,0],[114,93,8,0],[114,93,11,1],[114,93,4,1],[114,93,7,0],[114,93,120,0],[114,93,123,1],[114,93,116,1],[114,93,119,0],[114,92,152,0],[114,92,155,1],[114,92,148,1],[114,92,151,0],[114,92,232,0],[114,92,235,1],[114,92,228,1],[114,92,231,0],[115,161,8,0],[115,161,11,1],[115,161,4,1],[115,161,7,0],[115,161,120,0],[115,161,123,1],[115,161,116,1],[115,161,119,0],[115,160,152,0],[115,160,155,1],[115,160,148,1],[115,160,151,0],[115,160,232,0],[115,160,235,1],[115,160,228,1],[115,160,231,0],[115,175,8,0],[115,175,11,1],[115,175,4,1],[115,175,7,0],[115,175,120,0],[115,175,123,1],[115,175,116,1],[115,175,119,0],[115,174,152,0],[115,174,155,1],[115,174,148,1],[115,174,151,0],[115,174,232,0],[115,174,235,1],[115,174,228,1],[115,174,231,0],[115,147,8,0],[115,147,11,1],[115,147,4,1],[115,147,7,0],[115,147,120,0],[115,147,123,1],[115,147,116,1],[115,147,119,0],[115,146,152,0],[115,146,155,1],[115,146,148,1],[115,146,151,0],[115,146,232,0],[115,146,235,1],[115,146,228,1],[115,146,231,0],[115,157,8,0],[115,157,11,1],[115,157,4,1],[115,157,7,0],[115,157,120,0],[115,157,123,1],[115,157,116,1],[115,157,119,0],[115,156,152,0],[115,156,155,1],[115,156,148,1],[115,156,151,0],[115,156,232,0],[115,156,235,1],[115,156,228,1],[115,156,231,0]],"4x4_1000":[[181,50],[15,154],[51,45],[153,70],[84,158],[121,205],[158,46],[196,242],[254,218],[207,86],[249,145],[17,167],[14,183],[42,15],[36,177],[38,62],[70,101],[102,0],[108,94],[118,175],[134,139],[176,43],[204,213],[221,130],[254,71],[148,113],[172,228],[165,84],[33,35],[52,111],[68,21],[87,178],[158,207],[240,203],[8,174],[9,41],[24,117],[4,255],[13,246],[28,90],[23,24],[42,40],[50,140],[56,178],[36,232],[46,235],[45,63],[75,100],[80,46],[80,19],[81,148],[85,104],[93,65],[95,151],[104,1],[104,103],[97,36],[97,233],[107,18],[111,229],[103,223],[126,27],[128,160],[131,68],[139,162],[147,122],[132,108],[133,42],[133,156],[156,137],[159,161],[187,124],[188,4],[182,91],[191,200],[183,171],[202,31],[201,98],[217,88],[211,213],[204,152],[199,160],[197,55],[233,93],[249,37],[251,187],[238,42],[247,77],[53,117],[138,173],[118,23],[10,207],[6,75],[45,193],[73,216],[67,244],[79,54],[79,211],[105,228],[112,199],[122,110],[180,234],[237,79],[252,231],[254,166],[0,37],[0,67],[10,136],[10,134],[2,111],[0,28],[0,151],[8,55],[10,49],[9,198],[11,1],[9,251],[11,88],[16,130],[24,45],[16,120],[16,115],[18,116],[18,177],[26,249],[19,6],[12,14],[12,241],[4,51],[12,159],[14,242],[14,253],[7,76],[15,164],[7,47],[5,181],[15,145],[7,219],[30,228],[20,57],[29,128],[21,200],[31,139],[21,186],[29,177],[32,128],[40,233],[34,162],[40,83],[42,240],[34,247],[41,64],[33,70],[41,185],[43,156],[43,178],[56,202],[56,46],[48,7],[56,231],[58,73],[58,101],[50,93],[59,136],[57,29],[59,211],[38,71],[39,128],[47,170],[45,20],[37,222],[37,83],[47,119],[52,72],[60,168],[60,65],[52,13],[52,251],[54,154],[61,224],[53,106],[61,9],[61,237],[63,196],[63,108],[55,206],[61,92],[61,118],[55,176],[63,23],[63,255],[72,229],[66,104],[74,45],[65,96],[73,81],[65,221],[75,223],[88,79],[90,72],[88,22],[80,93],[90,250],[90,181],[81,35],[91,138],[89,25],[81,53],[76,105],[70,193],[78,11],[68,95],[78,89],[77,131],[77,125],[71,216],[71,115],[92,133],[94,68],[86,43],[92,187],[85,195],[95,110],[95,235],[93,18],[85,94],[98,112],[98,21],[97,194],[107,32],[99,69],[107,92],[107,91],[120,12],[122,207],[120,127],[121,128],[113,229],[113,116],[121,182],[113,211],[123,51],[100,106],[102,168],[110,167],[110,145],[101,34],[109,203],[103,141],[109,49],[126,128],[126,226],[126,141],[116,210],[124,50],[126,53],[117,171],[119,5],[127,43],[125,218],[127,146],[128,117],[128,243],[129,166],[137,237],[129,252],[152,166],[154,32],[145,67],[153,249],[145,147],[155,212],[132,9],[132,107],[134,196],[142,100],[134,26],[133,78],[141,203],[133,103],[133,175],[133,215],[135,179],[156,225],[156,242],[148,23],[149,0],[149,162],[157,35],[159,98],[157,82],[149,218],[160,197],[170,205],[162,216],[162,87],[169,61],[169,87],[171,82],[163,54],[163,89],[176,244],[184,18],[176,191],[178,157],[187,237],[185,114],[185,150],[164,195],[172,210],[174,177],[165,130],[175,101],[165,123],[175,250],[180,100],[188,98],[180,129],[182,160],[190,238],[190,13],[188,217],[190,248],[181,40],[183,9],[183,210],[192,234],[192,25],[192,253],[200,211],[202,90],[193,77],[201,180],[193,87],[195,152],[195,29],[216,128],[216,239],[218,43],[208,30],[209,5],[211,173],[219,167],[196,201],[204,120],[205,69],[197,11],[207,207],[220,172],[212,2],[220,99],[212,39],[212,245],[214,120],[222,184],[221,230],[213,93],[221,189],[223,29],[226,202],[234,107],[224,180],[226,56],[226,212],[227,34],[225,216],[240,3],[242,204],[248,246],[241,73],[243,234],[241,156],[249,245],[241,59],[236,141],[238,201],[230,15],[228,247],[231,96],[239,232],[237,178],[229,21],[239,209],[244,134],[252,1],[246,195],[244,124],[252,147],[245,66],[253,152],[245,61],[2,189],[0,225],[2,226],[2,174],[8,120],[0,116],[8,158],[8,209],[8,125],[10,50],[10,222],[2,81],[1,162],[3,128],[11,131],[11,75],[11,39],[11,239],[9,182],[9,89],[9,147],[11,248],[3,217],[3,241],[16,196],[24,171],[26,160],[26,4],[26,108],[26,174],[18,137],[16,23],[26,243],[25,64],[17,2],[17,43],[17,207],[27,34],[19,46],[17,21],[19,187],[12,32],[12,201],[12,220],[12,54],[6,20],[6,114],[13,97],[5,13],[13,143],[15,224],[15,73],[7,133],[5,144],[13,51],[15,150],[15,118],[20,96],[28,141],[20,218],[28,115],[30,148],[30,186],[22,217],[30,61],[22,251],[29,233],[29,254],[31,159],[40,139],[32,175],[34,14],[34,169],[42,141],[42,163],[42,239],[40,144],[40,59],[42,88],[34,51],[33,160],[33,2],[33,165],[33,199],[43,3],[35,103],[41,48],[41,210],[43,25],[43,155],[43,151],[56,40],[56,165],[58,134],[50,1],[56,159],[50,210],[58,153],[58,213],[57,232],[59,193],[51,67],[59,231],[49,154],[51,144],[59,158],[36,196],[44,74],[44,173],[44,207],[44,103],[38,234],[46,229],[44,112],[46,18],[46,209],[46,57],[37,100],[37,231],[47,204],[45,188],[45,113],[37,213],[37,155],[39,16],[47,124],[39,242],[39,58],[47,182],[39,211],[47,179],[39,31],[60,75],[54,192],[54,238],[62,233],[52,184],[60,20],[60,82],[52,114],[52,126],[52,191],[62,113],[62,83],[61,140],[53,162],[53,46],[53,45],[55,172],[53,112],[55,250],[63,241],[63,219],[72,196],[72,233],[74,194],[74,65],[66,235],[72,19],[74,216],[66,253],[74,23],[73,99],[67,110],[65,58],[73,177],[65,61],[75,146],[75,155],[67,63],[88,34],[80,170],[88,39],[82,200],[82,132],[82,10],[90,15],[88,152],[88,92],[80,219],[80,247],[90,244],[81,236],[81,66],[81,13],[91,3],[83,235],[81,118],[89,113],[81,147],[83,249],[91,179],[83,151],[76,76],[68,75],[76,35],[70,140],[78,39],[70,144],[78,212],[69,206],[69,229],[69,39],[79,193],[71,5],[69,52],[69,114],[92,200],[92,14],[84,235],[86,137],[86,67],[94,231],[92,112],[84,178],[94,121],[86,243],[93,163],[93,242],[85,29],[93,157],[87,252],[87,210],[95,115],[104,45],[104,195],[104,135],[106,74],[98,105],[96,185],[104,255],[106,220],[106,218],[106,62],[106,81],[106,49],[98,215],[97,204],[107,130],[107,227],[105,58],[97,158],[97,149],[97,117],[105,95],[105,55],[99,218],[112,2],[120,99],[112,79],[114,202],[122,173],[112,123],[122,20],[122,249],[122,211],[122,187],[121,226],[113,41],[123,103],[113,208],[121,57],[115,48],[115,185],[115,83],[115,255],[108,136],[100,9],[108,67],[102,6],[102,131],[100,176],[100,218],[110,159],[103,200],[111,238],[109,59],[111,210],[116,128],[124,171],[126,104],[126,2],[124,156],[116,54],[124,17],[126,222],[126,182],[118,219],[125,196],[125,138],[117,109],[119,136],[119,32],[119,65],[117,56],[117,190],[125,155],[119,87],[136,40],[128,172],[136,13],[136,103],[130,78],[138,161],[130,43],[128,24],[136,249],[128,157],[138,156],[130,49],[138,117],[130,151],[129,9],[129,235],[129,7],[139,40],[139,172],[131,46],[131,229],[129,80],[137,50],[139,122],[139,150],[131,125],[144,135],[154,252],[146,245],[145,170],[147,65],[147,37],[155,235],[153,52],[145,247],[155,218],[147,86],[132,66],[140,129],[140,79],[134,72],[134,166],[142,3],[134,227],[134,111],[142,175],[132,94],[132,119],[134,250],[142,30],[142,55],[135,10],[143,138],[143,38],[135,33],[135,13],[133,114],[135,62],[156,67],[158,97],[148,88],[148,248],[156,50],[148,118],[148,177],[148,221],[148,155],[156,219],[158,156],[158,210],[150,25],[158,177],[149,105],[159,109],[151,43],[149,182],[149,185],[157,61],[157,87],[168,236],[168,37],[162,172],[162,2],[170,102],[170,143],[170,231],[168,48],[168,122],[168,246],[168,147],[162,20],[170,52],[162,114],[170,242],[162,241],[161,64],[169,10],[161,38],[169,197],[169,207],[161,52],[169,18],[161,250],[171,152],[163,247],[176,6],[176,69],[184,141],[178,132],[184,240],[184,85],[178,118],[186,145],[178,113],[185,192],[185,66],[185,42],[179,140],[179,202],[187,102],[179,15],[177,218],[187,20],[187,246],[179,19],[164,104],[172,44],[172,161],[172,235],[172,199],[164,103],[166,192],[174,224],[166,35],[173,232],[165,204],[167,236],[173,124],[165,26],[165,145],[173,25],[165,151],[180,109],[190,203],[188,58],[188,245],[190,189],[190,243],[181,37],[181,143],[183,104],[191,228],[189,254],[189,157],[181,245],[181,243],[191,176],[183,90],[191,62],[183,57],[191,213],[183,29],[191,53],[183,127],[200,1],[192,165],[194,130],[200,189],[194,252],[202,145],[194,91],[201,68],[193,42],[195,192],[201,122],[193,185],[201,117],[193,247],[203,177],[208,108],[216,135],[208,175],[218,196],[210,12],[218,9],[208,48],[216,148],[208,58],[208,182],[208,117],[210,118],[218,93],[218,53],[210,23],[217,2],[211,232],[211,229],[209,154],[209,246],[209,81],[219,20],[211,62],[211,211],[196,96],[204,167],[198,66],[198,71],[206,231],[196,92],[204,29],[204,53],[198,188],[205,168],[197,12],[197,228],[197,194],[205,45],[205,89],[205,149],[197,147],[199,95],[212,197],[222,136],[214,36],[222,236],[214,226],[222,198],[222,35],[220,220],[220,26],[212,17],[222,84],[214,148],[222,157],[221,129],[213,165],[215,172],[215,102],[223,169],[213,220],[221,31],[223,240],[226,72],[226,232],[226,7],[224,93],[234,245],[235,38],[235,237],[225,82],[225,126],[233,219],[248,6],[240,238],[248,161],[250,0],[250,194],[240,155],[250,244],[250,60],[242,252],[242,189],[242,147],[241,96],[249,236],[241,70],[249,225],[243,72],[243,174],[243,193],[243,139],[243,167],[241,115],[241,151],[243,244],[251,50],[228,7],[230,77],[236,85],[237,192],[237,133],[239,162],[231,78],[229,213],[239,80],[244,34],[244,137],[244,41],[246,106],[254,11],[254,111],[244,149],[244,53],[244,31],[246,176],[245,232],[245,197],[253,35],[255,192],[247,204],[247,233],[245,188],[253,246],[245,217],[253,151],[253,63],[255,156],[255,90],[247,254],[255,17],[247,191]],"5x5_1000":[[162,217,94,0],[14,3,115,0],[215,135,110,1],[129,202,251,1],[215,90,146,0],[234,4,22,1],[105,235,246,0],[113,10,53,1],[134,176,153,0],[152,159,210,1],[158,119,1,1],[209,109,96,0],[243,21,136,1],[47,56,179,0],[254,126,84,0],[40,241,191,1],[75,211,172,0],[95,81,55,1],[123,38,226,0],[131,14,244,0],[150,237,58,1],[168,114,32,0],[181,134,80,1],[93,9,111,0],[206,104,17,1],[210,204,185,0],[225,231,69,1],[17,33,35,0],[29,203,57,0],[18,17,29,1],[19,155,183,0],[27,68,57,1],[32,104,103,0],[37,85,100,0],[35,33,221,0],[61,55,245,0],[76,197,86,0],[65,104,128,1],[77,86,142,1],[67,30,57,0],[86,148,18,1],[82,151,207,0],[108,36,251,1],[97,132,236,1],[109,63,24,1],[116,177,61,0],[116,220,203,1],[124,164,3,0],[122,200,146,1],[123,91,235,1],[141,172,114,0],[141,105,60,1],[143,28,5,1],[139,74,34,1],[151,253,165,0],[172,101,198,1],[172,195,248,0],[161,23,239,1],[167,9,19,1],[171,111,145,0],[185,237,248,1],[178,100,158,0],[190,93,195,0],[196,5,67,1],[200,163,238,1],[194,117,197,0],[198,194,214,1],[217,102,212,1],[221,94,185,1],[244,234,25,0],[243,178,148,0],[122,186,5,0],[216,141,41,1],[12,103,50,1],[21,89,12,1],[76,116,192,1],[84,3,14,0],[160,208,172,0],[194,152,166,0],[203,104,150,0],[253,105,209,0],[4,145,90,1],[12,222,112,0],[5,170,62,0],[1,99,183,1],[9,145,68,1],[9,105,83,1],[6,37,161,1],[3,61,226,1],[7,213,6,1],[15,143,170,0],[28,116,60,1],[25,29,145,1],[22,27,35,1],[22,210,141,1],[23,78,198,1],[19,226,177,0],[31,126,250,1],[41,15,112,0],[34,71,126,1],[42,23,192,1],[42,251,100,1],[35,240,4,0],[39,205,252,0],[35,106,172,0],[47,123,47,0],[52,144,196,0],[48,9,214,1],[52,94,16,1],[57,82,232,0],[50,0,113,0],[54,231,29,0],[68,138,4,1],[68,236,254,1],[69,31,114,1],[69,74,149,0],[70,19,190,1],[78,131,9,0],[71,114,153,1],[79,130,125,1],[92,55,141,1],[88,253,119,0],[81,183,248,0],[89,58,251,0],[90,153,153,0],[83,1,240,1],[83,204,103,1],[83,110,1,0],[91,37,175,1],[95,213,204,1],[101,102,66,1],[101,199,175,0],[102,55,81,0],[103,12,197,1],[112,150,93,1],[124,99,128,1],[121,171,169,0],[114,61,70,0],[115,229,178,1],[132,38,140,0],[137,241,1,0],[134,174,233,1],[138,35,249,1],[148,53,113,1],[156,69,27,0],[149,31,164,1],[150,141,144,0],[151,38,183,0],[155,55,103,0],[155,198,224,1],[160,154,110,1],[164,82,134,1],[168,176,133,1],[173,155,66,0],[175,240,88,1],[176,46,99,1],[176,15,24,0],[188,234,178,1],[190,196,76,1],[179,130,250,0],[179,88,60,1],[191,69,238,0],[191,86,51,1],[196,58,240,0],[204,233,131,0],[197,51,0,1],[205,34,99,1],[198,8,138,1],[198,148,63,0],[202,238,132,1],[207,167,18,1],[203,97,226,1],[208,2,233,0],[220,183,70,0],[217,139,132,1],[217,206,178,0],[210,153,64,1],[210,209,23,0],[222,90,110,1],[218,71,66,1],[219,60,16,0],[219,143,201,0],[232,24,227,1],[229,28,111,0],[225,174,56,1],[237,185,123,1],[233,224,50,0],[235,93,12,0],[239,249,157,0],[248,17,1,0],[248,7,211,0],[246,2,32,1],[246,27,95,1],[254,181,237,0],[250,51,56,1],[250,74,193,1],[247,47,112,1],[247,234,252,1],[255,24,148,1],[251,163,94,0],[104,184,47,0],[153,15,11,1],[153,216,38,1],[228,95,14,1],[29,16,110,1],[42,193,48,1],[52,65,99,1],[55,192,116,1],[63,53,203,1],[86,160,76,0],[87,56,57,1],[102,152,184,0],[115,165,23,0],[127,44,253,1],[139,71,233,0],[165,195,151,0],[169,169,235,0],[181,25,183,0],[178,218,153,1],[196,193,244,0],[202,185,30,0],[216,111,163,1],[223,141,142,0],[229,102,143,1],[237,71,26,1],[240,103,134,1],[4,41,238,1],[0,35,71,1],[0,162,251,0],[4,200,206,0],[0,210,225,1],[12,10,15,0],[8,139,198,0],[12,134,91,0],[5,129,253,0],[1,113,30,0],[1,194,21,1],[1,231,113,0],[9,41,191,0],[13,16,27,1],[13,5,24,0],[13,237,47,1],[13,102,222,0],[9,242,118,0],[2,208,130,1],[2,98,201,0],[6,243,169,0],[6,239,35,0],[14,62,18,0],[10,154,221,1],[10,97,109,1],[10,255,81,0],[7,230,114,1],[11,154,1,0],[15,208,142,0],[15,73,147,0],[15,247,123,0],[16,158,96,1],[16,81,237,0],[20,122,222,1],[28,133,62,1],[28,35,104,0],[24,199,52,0],[21,251,130,1],[17,78,147,1],[25,160,234,1],[18,24,76,0],[18,163,174,0],[30,172,60,0],[30,201,108,0],[30,233,163,1],[26,118,216,0],[19,182,7,1],[23,70,188,0],[31,21,121,0],[27,35,125,0],[27,191,146,0],[31,150,154,1],[27,72,235,0],[32,12,253,0],[32,23,86,0],[40,1,153,1],[44,50,55,1],[37,140,230,0],[33,30,140,0],[45,125,105,0],[41,192,73,0],[38,177,32,0],[38,128,28,1],[34,180,26,0],[38,42,68,1],[38,171,89,0],[38,201,132,0],[42,48,98,1],[46,50,173,0],[35,63,1,1],[35,131,80,0],[39,100,46,1],[39,212,176,1],[47,186,44,1],[43,38,186,1],[48,11,141,0],[52,38,146,1],[48,130,25,0],[48,123,44,1],[60,184,59,1],[60,38,15,1],[56,233,220,0],[60,192,247,0],[56,247,84,1],[49,5,180,1],[53,184,254,1],[53,178,211,1],[57,155,206,1],[57,109,46,0],[61,224,131,1],[57,244,208,0],[57,255,32,0],[57,199,136,1],[54,15,36,0],[50,30,213,0],[54,191,209,1],[54,70,235,0],[58,45,168,0],[58,147,187,0],[55,149,173,0],[55,4,147,0],[51,160,216,1],[55,67,167,0],[51,250,92,0],[63,131,110,0],[63,107,96,0],[64,225,205,0],[76,144,234,0],[72,154,105,1],[76,190,247,0],[72,235,138,1],[72,211,7,1],[76,90,152,1],[65,36,19,1],[77,11,248,0],[73,91,189,1],[70,141,101,0],[66,181,107,1],[70,92,131,0],[70,244,103,0],[70,121,187,0],[74,163,157,1],[78,65,78,1],[78,75,194,0],[67,137,211,1],[71,170,200,1],[67,59,19,0],[71,74,105,1],[79,22,235,1],[79,69,160,0],[80,145,108,1],[84,16,187,0],[80,90,77,1],[80,114,212,0],[84,234,241,1],[88,9,195,1],[92,89,121,0],[88,107,27,0],[81,52,246,1],[85,169,118,1],[85,151,183,1],[85,72,67,1],[81,203,28,0],[89,103,105,1],[89,87,155,0],[86,160,171,1],[82,76,88,0],[86,87,68,1],[94,1,56,0],[90,1,255,0],[90,53,19,1],[94,29,215,1],[94,146,70,1],[90,108,162,0],[87,57,5,0],[87,185,238,0],[83,27,44,1],[87,134,8,0],[87,179,31,1],[87,175,125,0],[83,240,235,0],[91,30,236,0],[95,6,166,1],[95,46,87,1],[91,89,192,0],[91,221,125,1],[91,78,47,0],[95,111,137,1],[91,99,218,0],[96,32,36,1],[100,167,30,1],[96,84,84,1],[104,166,103,0],[108,235,188,1],[105,70,205,1],[109,243,169,1],[109,94,31,0],[102,32,99,0],[102,219,83,1],[98,215,16,1],[106,165,84,1],[110,46,41,1],[103,54,146,0],[103,196,202,0],[99,83,34,1],[107,59,196,1],[111,22,80,1],[107,88,37,1],[111,99,213,1],[112,192,248,1],[116,215,208,0],[113,43,110,1],[113,121,27,1],[113,66,210,0],[113,99,253,0],[117,254,122,1],[125,152,140,0],[125,185,247,0],[125,19,147,1],[121,71,103,0],[125,223,195,0],[121,239,82,1],[114,0,182,0],[118,68,152,1],[118,122,66,0],[126,11,181,1],[115,58,225,1],[119,139,59,1],[119,93,234,0],[127,76,0,1],[123,117,224,1],[127,226,186,0],[128,176,67,1],[128,188,61,0],[128,50,88,0],[128,92,26,1],[132,251,36,0],[128,127,245,1],[136,45,197,1],[136,153,224,1],[136,25,149,0],[133,29,199,1],[129,187,135,0],[129,225,96,1],[133,126,33,1],[129,242,146,0],[137,61,108,1],[137,100,178,1],[141,212,30,1],[137,94,209,1],[138,4,172,0],[142,200,22,0],[142,242,252,1],[131,11,137,0],[135,43,109,0],[135,69,49,0],[131,108,120,1],[135,238,13,0],[139,177,43,1],[139,145,216,0],[139,89,102,0],[143,229,85,0],[143,243,101,1],[148,26,21,0],[144,225,167,0],[144,71,255,0],[156,188,213,0],[152,146,244,0],[156,83,168,1],[152,195,107,0],[152,114,63,1],[145,19,213,0],[149,191,251,0],[149,69,22,1],[149,238,216,0],[157,214,4,0],[157,98,112,0],[150,175,206,0],[150,186,24,1],[150,217,61,1],[158,33,46,0],[154,185,132,0],[154,129,79,1],[154,59,54,1],[158,195,37,0],[147,149,40,0],[151,50,46,1],[151,211,152,0],[155,157,163,1],[159,28,240,0],[159,51,82,1],[155,250,202,0],[159,250,148,0],[160,60,203,1],[164,168,105,0],[160,179,245,0],[164,106,191,0],[160,91,120,1],[168,104,152,1],[172,205,54,1],[172,220,121,1],[161,73,35,0],[161,67,217,0],[173,176,209,0],[169,155,28,1],[169,138,215,1],[173,84,106,1],[173,235,81,1],[166,29,142,0],[166,92,102,1],[174,188,36,0],[170,171,48,0],[174,204,216,0],[170,237,187,0],[174,227,205,1],[167,54,8,1],[163,236,229,0],[163,193,155,1],[167,250,58,0],[175,165,105,1],[175,200,189,1],[175,239,164,0],[171,103,246,0],[180,28,82,0],[180,63,68,1],[180,240,202,0],[184,24,45,0],[188,191,136,1],[184,142,4,1],[188,120,40,1],[184,200,67,1],[188,243,60,1],[181,19,12,0],[177,63,106,0],[177,101,123,1],[177,203,237,0],[177,194,103,1],[177,90,242,1],[181,238,31,1],[185,144,112,1],[185,163,64,1],[185,120,90,0],[185,247,203,0],[182,5,1,1],[178,255,194,0],[178,123,21,0],[178,110,90,1],[190,181,243,1],[186,167,161,0],[186,114,10,1],[179,48,64,0],[183,181,52,1],[183,105,76,0],[183,197,193,0],[179,87,222,0],[187,84,251,0],[187,202,108,1],[187,251,225,1],[191,246,134,1],[196,45,124,0],[200,28,52,0],[204,67,229,1],[197,53,174,1],[193,146,192,0],[197,35,247,0],[193,159,254,0],[193,84,97,0],[193,87,130,0],[193,199,55,1],[201,140,239,1],[205,240,111,0],[201,95,103,1],[201,119,241,0],[205,226,149,1],[198,191,7,1],[198,191,184,0],[198,219,201,1],[202,49,136,0],[206,222,45,1],[199,120,44,1],[203,171,75,1],[207,67,191,0],[208,174,86,1],[208,115,51,0],[216,20,152,0],[220,96,159,1],[213,117,88,0],[209,224,215,0],[213,251,177,0],[213,218,116,1],[221,60,116,1],[221,151,109,0],[217,27,49,0],[217,98,166,0],[210,171,124,1],[210,197,236,0],[210,202,106,0],[210,107,214,0],[222,155,52,0],[222,146,158,0],[218,96,216,1],[215,57,178,1],[215,95,38,0],[223,1,65,1],[223,121,15,1],[219,247,116,0],[224,67,79,1],[224,198,227,0],[236,56,189,1],[236,81,253,0],[229,141,36,0],[229,5,121,1],[225,78,248,0],[229,87,220,1],[225,247,242,1],[226,133,77,0],[226,24,26,0],[226,43,203,0],[230,236,2,0],[230,98,129,0],[234,38,64,0],[234,224,196,0],[234,114,148,1],[227,17,63,1],[231,11,230,1],[227,100,241,1],[231,250,226,1],[227,219,243,0],[239,135,98,0],[239,93,112,0],[235,102,14,0],[239,74,60,0],[240,178,33,1],[240,175,79,0],[244,159,60,1],[240,245,4,0],[240,242,120,0],[248,52,99,0],[252,219,143,0],[252,254,77,1],[253,176,69,1],[253,51,235,0],[249,196,217,1],[242,35,145,1],[246,210,251,1],[254,239,211,1],[243,176,58,1],[247,138,77,1],[243,143,157,1],[247,242,39,0],[255,189,133,1],[255,38,197,1],[251,224,97,0],[255,220,26,0],[25,248,99,1],[169,93,31,1],[0,184,72,1],[0,236,225,0],[8,97,102,0],[8,244,131,0],[12,248,157,0],[8,192,62,1],[1,57,192,0],[1,40,10,0],[1,190,179,1],[13,128,85,0],[13,175,99,1],[6,181,236,0],[2,153,54,1],[6,182,143,0],[6,125,64,1],[2,114,104,1],[6,203,237,1],[14,15,61,0],[14,213,203,0],[3,125,39,1],[3,233,143,0],[7,250,159,1],[11,76,16,0],[16,185,87,1],[16,22,41,0],[16,98,139,1],[24,157,182,0],[24,109,0,1],[28,118,42,0],[24,95,228,1],[28,66,22,0],[17,60,169,0],[17,152,57,1],[21,58,99,0],[21,18,117,1],[21,222,162,0],[21,106,24,1],[29,209,40,0],[29,113,148,1],[18,57,139,1],[18,5,152,0],[22,2,152,1],[26,52,127,1],[30,220,239,1],[30,112,115,0],[26,67,149,0],[26,126,27,1],[23,173,194,0],[19,25,222,1],[27,168,147,1],[27,147,73,1],[27,208,27,0],[31,66,219,1],[32,185,170,0],[36,196,186,0],[40,173,5,0],[44,35,100,1],[44,14,154,1],[44,81,3,1],[37,197,207,1],[37,200,211,1],[33,119,232,1],[34,141,218,1],[34,10,238,0],[46,134,44,0],[42,182,47,1],[39,60,45,0],[39,157,74,0],[39,155,7,1],[35,87,132,0],[35,78,67,1],[47,171,185,1],[48,176,46,1],[48,40,122,0],[52,253,160,1],[60,214,137,0],[56,126,182,0],[49,0,109,0],[49,23,219,1],[49,167,119,1],[61,128,10,1],[61,226,157,0],[54,106,119,1],[54,199,190,1],[62,187,41,0],[62,104,154,0],[62,95,2,1],[58,86,206,1],[62,95,252,0],[51,185,172,0],[51,84,174,0],[63,25,160,0],[59,173,97,1],[59,112,47,1],[59,235,58,1],[64,83,90,0],[65,152,22,0],[65,143,177,0],[69,217,33,1],[69,235,1,0],[73,157,82,0],[66,56,79,1],[74,166,163,1],[74,197,42,1],[74,200,115,0],[71,180,198,1],[67,147,93,0],[67,120,117,0],[79,58,175,1],[79,135,68,1],[79,135,223,0],[79,196,222,1],[84,31,71,0],[80,31,148,0],[92,145,67,0],[88,189,239,1],[92,221,7,1],[92,102,88,1],[92,219,50,1],[85,69,212,0],[81,68,62,0],[81,231,230,0],[89,11,167,0],[93,186,56,1],[93,166,55,1],[89,249,149,0],[93,255,236,0],[82,157,58,0],[86,58,151,0],[82,99,34,0],[82,203,49,1],[90,139,204,1],[83,33,106,0],[87,245,17,0],[95,220,33,0],[96,140,28,0],[96,162,213,1],[96,222,5,1],[108,76,110,0],[104,82,12,0],[101,59,167,0],[97,170,234,0],[101,113,110,1],[101,124,89,1],[105,131,46,1],[109,196,12,0],[109,210,20,1],[98,150,165,1],[98,117,38,1],[98,100,214,1],[98,91,220,1],[110,51,2,1],[110,90,137,0],[99,209,212,0],[99,75,129,1],[107,56,145,1],[107,122,42,0],[116,56,51,0],[112,6,44,0],[116,170,141,1],[116,76,143,0],[124,183,167,0],[120,3,22,1],[124,199,198,1],[117,182,4,1],[113,254,101,0],[113,79,154,1],[121,32,119,0],[125,121,205,0],[125,67,92,0],[125,127,59,0],[118,85,8,0],[122,152,199,0],[126,19,192,0],[126,94,89,1],[115,36,132,0],[115,46,39,1],[115,139,84,1],[132,4,34,0],[128,232,46,0],[128,237,48,0],[140,53,96,0],[140,120,240,1],[136,194,128,1],[129,26,100,1],[129,38,163,0],[133,154,140,1],[137,19,23,1],[130,9,37,0],[130,183,201,1],[142,185,191,1],[138,159,236,0],[142,228,226,0],[135,32,232,0],[135,205,96,1],[135,71,172,1],[139,10,91,0],[139,92,190,1],[143,213,180,0],[139,118,41,0],[148,51,175,0],[144,85,72,1],[156,62,39,0],[152,151,159,0],[152,88,144,1],[149,182,31,0],[145,64,55,0],[157,179,128,0],[153,65,137,1],[146,173,38,0],[146,101,228,1],[146,111,141,1],[150,94,153,0],[154,11,77,0],[154,221,17,0],[147,246,156,1],[159,52,189,1],[159,80,9,0],[160,153,27,1],[164,15,234,0],[160,15,83,1],[168,2,105,1],[172,27,205,1],[172,135,77,0],[172,118,200,0],[165,68,167,0],[161,244,92,0],[169,157,169,0],[169,135,255,0],[169,71,98,1],[166,129,114,1],[166,249,172,1],[162,196,38,0],[170,53,101,1],[170,168,102,0],[170,200,143,0],[167,110,148,1],[175,4,36,1],[176,65,44,0],[176,208,227,0],[176,237,129,1],[180,231,236,0],[184,160,236,1],[184,188,60,1],[177,102,224,1],[189,49,10,1],[189,63,186,1],[185,190,220,0],[178,58,240,1],[182,207,169,0],[186,35,226,1],[183,36,171,1],[183,41,251,0],[179,174,8,0],[183,86,202,1],[191,168,244,0],[191,148,18,1],[191,122,64,1],[196,133,200,0],[192,14,9,1],[192,112,254,0],[204,116,17,0],[193,8,90,1],[193,47,130,1],[193,35,120,0],[198,52,219,1],[198,164,244,1],[194,228,25,0],[195,29,35,0],[195,242,196,1],[199,95,223,0],[203,101,189,0],[203,199,15,1],[207,246,185,0],[212,84,239,0],[212,251,220,0],[220,232,84,0],[213,177,150,0],[209,159,7,1],[209,93,126,1],[217,164,134,1],[221,9,48,1],[217,180,57,1],[221,171,105,1],[217,203,66,0],[210,33,13,0],[214,73,4,0],[210,216,219,0],[218,63,69,1],[218,125,218,0],[218,211,230,0],[211,51,189,0],[215,225,237,1],[219,197,122,0],[224,128,59,1],[224,207,105,1],[228,71,48,1],[232,29,86,1],[232,243,72,1],[232,254,175,0],[236,231,159,0],[225,60,212,1],[229,204,106,1],[233,175,154,0],[227,0,237,0],[231,177,14,0],[227,192,126,0],[235,140,163,0],[235,159,109,1],[239,65,11,0],[239,194,195,0],[240,168,32,0],[244,25,211,0],[244,133,191,1],[240,106,204,0],[240,123,107,1],[244,251,170,0],[252,198,47,0],[248,111,16,0],[252,127,120,1],[241,171,217,0],[245,81,143,1],[241,248,166,1],[241,86,24,0],[253,132,196,0],[249,141,114,1],[253,15,85,1],[242,156,111,1],[246,136,24,1],[246,7,197,0],[242,65,215,0],[246,241,27,0],[242,230,203,1],[250,213,32,1],[254,67,233,0],[243,251,142,0],[255,187,218,0],[251,201,14,1]],"6x6_1000":[[30,61,216,42,6],[14,251,163,137,1],[21,144,126,172,13],[201,27,48,105,14],[214,7,214,225,5],[216,232,224,230,8],[66,104,180,31,5],[136,165,15,41,10],[48,125,82,79,13],[60,47,52,179,12],[69,223,199,78,3],[72,216,91,37,7],[113,5,88,252,6],[134,220,250,208,7],[141,114,169,63,6],[162,184,157,205,14],[9,253,30,156,4],[21,77,189,24,15],[48,10,49,14,2],[72,7,239,175,13],[86,223,17,219,6],[102,136,50,116,12],[118,232,203,120,1],[154,83,217,207,3],[169,203,132,2,4],[198,117,73,73,0],[193,210,136,148,1],[231,72,8,82,11],[234,47,202,132,8],[233,99,183,123,1],[250,54,101,42,15],[6,91,255,123,13],[5,65,215,45,6],[12,247,36,106,2],[19,56,163,158,11],[21,168,147,231,4],[58,65,126,233,14],[79,17,226,108,0],[83,13,182,210,0],[88,155,250,227,4],[100,9,232,160,11],[96,83,122,137,1],[97,89,6,155,10],[107,255,120,215,11],[112,173,150,164,15],[117,132,111,113,10],[122,149,25,47,12],[134,9,118,10,10],[138,45,68,195,15],[147,235,120,177,4],[152,141,168,77,4],[158,222,43,60,8],[165,41,224,123,8],[181,147,184,85,15],[183,248,228,38,15],[188,32,82,37,14],[192,68,135,118,5],[196,195,36,37,9],[197,169,27,216,13],[206,115,230,178,12],[205,12,166,39,2],[201,67,93,68,13],[207,190,128,243,4],[229,125,21,135,7],[239,198,133,142,9],[247,126,243,119,2],[44,228,63,37,4],[43,220,255,75,3],[55,199,221,189,10],[161,162,84,224,15],[169,130,193,187,5],[216,27,73,176,8],[3,88,41,248,6],[7,196,9,95,12],[15,226,102,23,11],[20,72,54,68,1],[16,173,95,251,7],[18,130,149,83,15],[22,225,49,132,12],[24,122,73,107,0],[26,232,134,17,2],[25,19,174,10,1],[27,103,181,161,7],[37,220,149,240,11],[40,137,97,247,6],[51,84,20,106,10],[49,193,108,31,7],[51,203,24,198,6],[62,207,228,144,15],[70,69,24,163,15],[68,186,112,182,7],[65,156,98,62,8],[72,209,145,74,1],[84,244,153,246,13],[87,90,156,129,3],[85,131,85,178,12],[87,183,118,16,15],[92,52,54,254,4],[92,72,252,119,14],[94,110,239,64,2],[95,35,59,111,15],[91,116,42,99,2],[101,15,163,58,14],[101,211,23,92,12],[106,156,36,90,14],[105,197,243,4,2],[105,210,72,78,10],[116,121,226,222,6],[114,207,35,234,11],[119,177,220,65,4],[126,12,7,33,7],[122,105,112,100,7],[120,178,216,112,7],[121,197,133,121,4],[134,111,89,252,6],[130,246,114,127,5],[133,78,47,65,4],[154,17,133,147,4],[156,113,96,201,7],[157,209,148,253,8],[162,30,18,227,8],[174,112,28,130,12],[173,1,33,156,1],[176,53,31,158,14],[182,74,216,13,4],[181,55,49,75,4],[190,170,199,227,11],[187,104,61,188,15],[198,114,247,44,1],[193,231,77,186,11],[203,85,238,89,13],[203,160,83,114,4],[208,9,15,207,1],[208,108,58,213,4],[211,241,32,87,4],[230,227,59,26,7],[227,83,62,164,10],[232,6,142,177,4],[236,7,192,89,7],[234,243,128,61,10],[246,59,39,216,8],[243,7,152,55,9],[254,75,186,155,9],[171,165,125,134,11],[192,209,98,90,11],[19,206,123,174,7],[78,129,253,97,7],[86,224,118,50,0],[106,112,138,84,0],[114,168,152,161,8],[129,93,66,248,0],[207,76,195,213,15],[214,187,101,134,4],[236,211,19,163,1],[245,33,245,32,7],[249,31,165,223,7],[0,36,244,122,7],[0,8,77,136,2],[4,60,194,242,9],[4,123,80,33,1],[6,122,228,193,13],[0,170,150,138,3],[4,209,56,233,4],[5,16,168,13,10],[1,64,176,0,7],[1,157,156,238,1],[8,16,87,227,11],[8,107,151,182,6],[14,232,184,96,10],[11,108,118,185,11],[15,220,185,140,11],[15,202,207,58,0],[20,36,159,217,8],[20,7,32,31,13],[21,9,16,213,7],[19,92,215,48,7],[17,71,154,187,6],[28,185,169,35,8],[28,221,7,118,6],[31,46,124,36,11],[25,102,66,71,7],[25,87,212,200,4],[31,168,244,240,4],[27,130,70,237,8],[27,174,225,15,14],[34,164,182,60,10],[34,191,144,18,15],[35,44,21,180,0],[37,90,169,102,12],[39,165,175,169,7],[37,244,14,66,5],[40,102,85,205,14],[44,66,126,14,0],[42,185,124,189,0],[41,70,225,210,3],[45,166,40,65,0],[43,251,32,154,6],[54,140,214,107,12],[52,135,119,124,7],[52,221,235,132,0],[55,145,247,111,1],[58,34,142,23,5],[62,19,189,64,8],[60,152,67,202,2],[57,88,157,23,9],[57,116,218,238,11],[63,109,188,115,1],[61,107,192,80,12],[57,171,39,73,7],[70,2,78,37,14],[70,130,186,11,12],[66,233,205,90,14],[68,201,183,179,15],[64,199,212,30,9],[70,210,180,204,14],[67,25,83,86,11],[65,34,230,221,9],[71,83,165,154,11],[78,30,241,224,8],[78,74,192,150,0],[78,95,170,6,15],[74,141,50,148,3],[73,21,148,179,9],[77,77,219,98,1],[75,167,97,232,1],[73,212,131,216,14],[86,41,14,246,12],[83,126,213,255,12],[85,245,167,175,10],[85,213,234,100,15],[88,27,171,29,10],[94,190,146,109,13],[95,16,249,155,5],[93,30,223,165,12],[95,113,141,240,2],[93,225,30,70,8],[96,51,187,36,7],[100,88,26,254,1],[99,200,221,167,6],[97,218,61,143,13],[110,58,34,175,10],[110,97,5,183,1],[106,137,169,232,12],[106,151,34,79,5],[107,18,195,128,1],[107,104,75,34,10],[111,148,193,87,9],[109,166,254,160,13],[111,234,202,69,7],[112,61,56,166,0],[118,108,53,231,8],[112,74,13,255,6],[117,120,169,200,0],[113,74,112,19,8],[117,127,140,187,9],[124,35,104,51,1],[124,181,167,211,1],[124,248,44,237,14],[127,36,226,52,15],[127,71,41,141,8],[134,216,3,209,9],[131,139,27,161,3],[135,162,121,197,9],[138,67,100,140,14],[136,147,59,76,8],[143,33,223,78,3],[141,132,53,114,9],[141,136,215,31,13],[137,159,120,252,13],[146,107,22,121,12],[148,142,34,241,2],[144,229,230,49,7],[150,216,133,42,1],[149,57,59,164,6],[149,60,251,77,13],[145,62,170,18,6],[151,111,90,175,9],[145,178,41,253,10],[145,211,250,118,1],[154,112,134,200,8],[152,142,205,3,1],[152,199,16,151,10],[157,203,235,70,6],[164,40,245,182,14],[163,55,241,121,3],[163,68,64,245,10],[161,127,173,133,8],[167,210,150,35,13],[168,69,112,43,11],[174,72,127,160,9],[172,79,182,214,8],[168,168,211,133,3],[169,139,10,203,8],[173,254,140,222,2],[180,239,46,46,14],[183,153,137,199,0],[190,12,162,14,12],[188,112,34,122,9],[190,188,47,145,10],[184,233,10,152,3],[189,10,48,236,8],[194,2,224,243,1],[194,107,50,227,7],[198,202,66,106,8],[199,30,238,104,14],[199,125,46,145,3],[206,60,32,116,2],[204,74,185,197,7],[206,247,99,220,13],[205,67,34,202,2],[207,183,204,29,0],[201,206,200,53,10],[207,243,75,113,3],[214,46,123,112,13],[212,23,75,59,4],[215,141,250,151,14],[209,216,245,85,1],[213,207,225,211,9],[218,22,168,204,9],[216,76,68,133,9],[220,217,114,142,13],[223,103,17,126,8],[219,153,125,230,7],[221,171,142,49,14],[224,25,8,76,13],[230,54,218,82,1],[226,172,199,155,0],[228,141,33,98,0],[226,254,208,197,1],[225,58,125,2,10],[231,208,91,142,5],[236,48,156,107,5],[236,170,73,210,0],[238,179,122,196,6],[232,224,103,46,2],[234,229,213,36,12],[237,107,28,44,2],[235,200,175,29,6],[242,5,98,212,5],[246,25,188,251,2],[246,163,92,109,11],[244,241,189,15,0],[241,106,155,67,5],[241,178,145,41,14],[250,84,91,243,5],[254,110,134,124,6],[249,13,185,67,2],[249,105,102,43,13],[251,65,203,72,4],[253,87,191,152,5],[251,152,144,126,6],[255,234,33,198,3],[163,165,111,69,0],[161,152,104,48,2],[15,55,131,43,0],[38,236,72,39,2],[65,152,184,168,15],[78,181,67,138,4],[99,197,227,123,10],[110,89,221,230,12],[128,212,89,240,8],[152,8,136,159,13],[163,6,103,166,15],[2,25,166,20,7],[2,21,202,78,2],[0,104,204,57,9],[0,161,19,254,8],[2,185,86,117,5],[2,198,187,83,2],[2,243,31,29,9],[7,42,193,126,3],[7,55,141,151,2],[1,111,31,231,5],[1,119,48,21,5],[7,149,114,65,10],[7,200,163,134,14],[5,254,251,247,9],[12,21,243,16,1],[10,68,98,226,14],[8,94,55,238,9],[8,82,222,18,13],[12,102,136,3,9],[8,140,186,71,15],[12,172,39,30,5],[8,179,56,11,15],[12,162,165,217,4],[8,227,82,192,1],[14,255,68,245,14],[14,255,93,234,11],[11,52,109,201,14],[13,9,254,187,3],[15,16,170,146,6],[13,31,101,167,14],[15,177,95,160,0],[13,162,5,35,3],[13,175,35,219,11],[11,240,165,238,4],[13,200,153,251,14],[15,253,48,39,7],[11,199,93,86,2],[18,47,163,1,13],[18,117,72,114,5],[16,79,174,98,7],[16,140,138,232,11],[16,243,244,46,14],[19,55,238,112,2],[23,11,35,235,0],[21,2,111,27,11],[17,161,171,203,2],[19,166,74,200,13],[23,142,53,205,3],[21,139,229,157,12],[23,166,249,125,4],[21,220,164,180,4],[17,223,5,67,12],[21,210,1,147,5],[24,31,173,250,10],[28,64,14,171,13],[30,76,93,61,2],[24,111,246,127,9],[24,87,52,184,15],[30,86,137,227,4],[26,149,209,132,5],[24,167,255,2,10],[24,158,177,201,14],[28,178,10,96,10],[28,146,53,136,1],[28,147,183,214,13],[26,202,188,88,1],[28,226,172,181,8],[27,121,237,6,4],[31,106,57,19,2],[29,152,56,117,0],[32,13,193,3,15],[38,53,212,232,0],[38,117,44,47,8],[32,83,133,143,6],[34,136,31,122,3],[39,44,250,229,5],[33,107,76,67,11],[37,188,103,11,10],[33,163,233,176,14],[33,150,242,145,15],[39,162,43,140,10],[39,159,72,50,8],[35,226,188,201,7],[33,194,87,244,1],[39,207,186,248,11],[42,47,189,228,11],[42,84,58,140,12],[42,209,187,151,15],[40,255,58,99,1],[43,84,197,185,8],[45,97,175,26,10],[43,188,219,62,6],[45,159,154,13,2],[47,187,114,106,3],[43,217,204,255,7],[41,198,223,142,12],[52,12,63,195,5],[48,3,27,40,14],[48,113,29,236,3],[52,76,222,162,10],[50,122,138,139,12],[54,87,99,215,2],[52,141,50,170,6],[54,129,250,177,1],[54,153,111,21,13],[50,204,108,49,14],[54,224,7,77,4],[51,64,141,156,5],[51,125,174,182,10],[49,94,255,61,1],[53,164,249,40,11],[51,134,161,198,4],[53,232,26,158,12],[55,216,88,186,3],[56,21,51,89,12],[56,27,98,106,12],[58,54,80,219,10],[56,136,209,242,9],[58,204,141,200,2],[60,252,249,7,11],[60,246,137,57,12],[59,15,174,199,9],[61,11,95,245,6],[57,73,131,170,6],[61,92,175,229,6],[59,111,209,254,2],[61,114,229,206,7],[59,165,24,48,4],[59,215,215,116,14],[66,21,7,134,1],[68,42,51,204,5],[64,105,62,32,12],[68,76,84,241,12],[64,78,207,5,5],[64,223,74,196,6],[70,218,103,26,14],[67,32,239,178,13],[65,99,121,242,7],[71,127,169,44,10],[67,168,36,115,10],[69,174,2,166,1],[72,29,125,107,6],[72,68,41,14,4],[76,93,176,242,6],[72,79,166,76,0],[72,129,242,45,11],[72,140,105,154,11],[76,230,39,197,2],[79,61,163,205,7],[73,73,253,184,8],[77,89,111,25,7],[79,79,213,39,11],[75,148,100,228,11],[77,160,106,169,14],[75,146,209,251,2],[79,237,128,190,11],[73,250,110,175,3],[84,32,172,167,7],[84,25,164,142,8],[86,116,218,31,8],[82,70,69,212,11],[80,153,112,192,3],[82,162,196,106,5],[80,232,170,66,4],[82,242,173,89,8],[82,218,235,246,10],[85,46,248,226,2],[81,65,96,182,2],[85,104,6,21,8],[83,74,126,75,13],[83,107,211,224,11],[87,114,130,210,7],[87,129,213,88,2],[87,164,195,74,8],[81,151,175,148,8],[87,143,23,115,11],[85,194,224,207,0],[87,246,164,229,1],[92,5,94,2,5],[94,56,204,77,0],[94,101,102,31,5],[90,165,235,123,5],[94,173,81,224,13],[88,179,133,252,1],[90,197,248,110,0],[88,241,107,96,0],[91,48,177,32,8],[95,52,239,231,11],[89,64,115,102,9],[93,105,19,173,1],[89,82,185,227,11],[93,70,244,172,2],[95,128,157,46,8],[95,224,251,80,14],[95,235,211,215,0],[100,105,134,115,5],[96,188,232,205,8],[100,158,227,5,6],[98,212,165,37,13],[98,246,143,142,3],[101,96,72,36,7],[99,180,12,145,8],[99,159,233,153,13],[101,237,229,156,7],[103,235,231,112,4],[106,1,200,157,4],[104,61,242,11,0],[104,24,102,125,13],[108,84,97,80,15],[104,75,24,245,13],[104,79,21,129,0],[110,110,244,89,0],[110,152,128,38,5],[110,144,99,51,3],[110,241,228,10,7],[108,195,36,252,0],[105,0,81,91,15],[107,64,14,250,12],[107,124,181,68,7],[105,172,245,202,2],[105,167,102,247,12],[109,235,50,140,11],[116,23,252,110,12],[112,97,170,185,4],[116,67,17,82,1],[112,185,187,138,9],[118,148,62,229,4],[118,188,96,18,9],[112,155,7,165,4],[116,139,191,1,3],[114,248,93,79,11],[118,211,218,167,15],[117,36,208,249,9],[117,60,188,48,6],[119,3,201,213,15],[113,69,59,42,5],[115,108,25,223,1],[113,72,238,44,10],[115,111,34,124,0],[115,145,99,174,14],[117,131,158,142,9],[119,191,100,123,6],[115,229,189,18,10],[115,207,104,128,3],[113,247,74,10,0],[120,45,207,39,6],[124,2,58,157,7],[122,92,206,58,4],[122,114,16,7,7],[126,178,36,208,11],[120,253,197,213,2],[126,232,117,158,9],[123,28,43,68,14],[127,4,160,250,2],[123,39,78,235,3],[125,38,127,122,1],[127,50,169,94,10],[121,180,170,170,7],[125,176,215,37,3],[127,147,79,12,2],[125,252,16,66,2],[125,213,216,211,15],[128,56,32,103,11],[132,17,185,214,3],[128,3,252,100,10],[130,59,15,187,10],[134,42,167,117,10],[132,27,89,75,7],[130,68,49,200,10],[130,89,216,117,3],[132,72,31,220,14],[130,181,229,107,4],[128,183,10,183,0],[134,178,80,25,10],[128,204,47,123,8],[134,253,182,100,8],[129,121,60,62,9],[133,96,132,133,11],[131,98,22,146,5],[131,128,229,221,15],[135,160,76,203,2],[135,184,138,180,13],[142,2,6,56,4],[140,54,125,172,7],[140,65,233,44,2],[140,152,59,175,12],[136,244,107,79,11],[143,44,197,204,4],[141,55,126,115,3],[139,69,79,63,7],[141,86,37,150,0],[139,191,24,57,2],[141,166,67,9,4],[141,208,202,166,8],[139,246,152,79,4],[144,18,153,165,4],[150,46,4,135,5],[146,92,163,212,0],[146,133,143,119,10],[148,169,61,69,10],[145,32,243,51,2],[145,39,109,79,3],[151,51,253,233,10],[151,72,243,129,3],[151,159,160,6,9],[147,208,63,215,12],[149,252,208,110,0],[147,195,178,11,1],[145,195,66,1,14],[158,10,212,208,9],[158,62,24,70,8],[154,97,122,220,9],[152,200,135,101,13],[152,193,223,48,9],[158,249,40,250,12],[158,240,171,22,1],[152,251,117,9,13],[159,32,1,53,4],[153,18,120,7,12],[155,131,245,126,11],[153,249,200,173,5],[157,228,46,236,2],[155,219,144,210,3],[159,254,132,153,15],[162,66,140,215,9],[164,205,25,53,7],[166,213,162,25,0],[160,223,192,176,6],[163,49,180,144,4],[167,32,134,58,15],[167,56,45,40,13],[163,63,204,220,14],[165,62,179,132,11],[161,112,26,189,7],[167,103,101,55,9],[165,123,102,174,4],[161,213,29,147,2],[163,248,233,155,14],[165,211,131,218,3],[165,214,235,188,6],[170,38,227,151,9],[174,30,201,63,12],[172,119,184,237,2],[170,169,238,77,15],[170,128,121,170,6],[174,194,96,202,1],[169,58,152,98,11],[171,18,81,200,12],[173,102,219,216,13],[173,82,221,74,1],[169,182,113,16,8],[173,186,226,53,0],[173,151,65,223,12],[171,243,182,44,7],[182,34,108,113,12],[180,30,37,122,7],[182,55,195,138,3],[176,76,212,55,0],[178,127,139,5,3],[176,173,216,34,5],[180,155,130,43,11],[176,211,78,194,3],[177,61,126,195,14],[177,6,164,99,14],[179,43,116,63,14],[183,31,214,70,15],[177,110,241,244,5],[177,126,83,138,14],[183,98,223,55,7],[177,169,248,148,9],[179,183,217,210,4],[179,238,187,76,6],[184,44,165,82,4],[184,20,13,235,15],[188,25,220,199,7],[188,84,38,185,6],[190,103,55,196,5],[184,250,232,211,5],[190,194,26,55,8],[187,24,207,164,11],[189,1,126,246,13],[189,34,47,210,7],[189,2,85,133,5],[185,114,78,96,5],[189,119,92,22,15],[185,136,172,46,1],[191,160,38,103,5],[185,130,150,168,6],[189,135,23,24,10],[191,190,239,45,7],[189,250,159,0,7],[194,112,53,187,8],[196,108,5,172,13],[196,121,84,220,3],[194,106,81,13,8],[198,164,159,104,4],[192,158,44,235,4],[198,146,33,73,13],[196,186,131,207,8],[198,231,133,67,11],[198,215,175,70,4],[197,54,85,77,11],[195,147,255,13,7],[193,245,112,165,10],[204,57,113,197,0],[200,2,137,73,2],[200,124,100,74,5],[202,103,225,13,7],[200,153,231,66,2],[200,170,178,16,5],[204,166,213,159,3],[202,229,134,251,14],[202,217,124,49,15],[206,228,202,14,6],[203,45,164,50,12],[203,87,214,130,2],[201,144,33,177,0],[201,130,95,176,10],[203,158,212,36,7],[205,134,171,157,13],[201,201,176,119,5],[201,250,31,99,2],[203,247,64,6,9],[205,218,13,28,15],[208,11,130,83,15],[212,59,207,214,15],[214,98,171,209,11],[212,102,200,58,15],[209,32,28,120,10],[211,57,65,195,9],[209,7,160,253,4],[209,149,58,14,2],[215,189,66,29,13],[215,192,166,80,13],[215,229,141,245,12],[215,197,83,246,7],[213,254,112,184,8],[216,12,185,190,6],[222,52,186,37,6],[220,14,198,139,14],[222,3,247,164,11],[218,93,11,138,2],[216,113,249,127,15],[220,189,130,231,2],[220,176,90,229,5],[218,147,222,80,0],[218,183,241,191,8],[222,159,2,50,14],[218,196,13,85,0],[220,197,171,128,14],[218,194,178,58,10],[218,218,88,253,0],[217,37,142,168,1],[221,5,118,59,14],[221,46,153,8,12],[217,85,104,199,5],[219,80,109,76,3],[223,104,170,56,8],[219,66,135,167,5],[219,129,40,179,13],[217,163,194,250,13],[221,179,56,211,8],[217,202,123,155,1],[219,246,158,176,7],[226,8,108,175,8],[224,27,171,150,4],[224,72,115,243,0],[228,117,169,91,14],[230,128,236,116,9],[226,175,118,72,7],[224,162,200,17,13],[226,203,113,80,12],[224,210,73,197,15],[225,12,194,130,9],[225,127,68,52,3],[231,173,69,177,14],[225,182,90,159,9],[231,252,32,65,12],[238,51,65,56,1],[238,124,54,51,4],[236,112,123,248,10],[232,123,59,230,14],[232,149,232,57,1],[238,172,9,125,5],[236,161,201,55,4],[232,151,189,197,5],[232,204,18,29,0],[238,193,29,105,8],[237,7,255,219,10],[237,105,243,54,11],[237,118,20,181,12],[239,132,33,209,7],[235,245,218,120,2],[233,249,229,214,13],[235,211,15,145,9],[233,219,241,32,9],[244,41,139,109,8],[246,10,52,34,5],[244,88,85,155,1],[242,118,55,97,3],[244,75,10,224,6],[246,110,129,75,12],[246,67,29,76,6],[244,87,34,238,1],[244,153,83,229,11],[240,252,47,27,5],[247,9,30,9,12],[247,38,87,194,0],[241,133,27,198,13],[243,136,98,232,1],[247,169,183,234,6],[247,138,138,92,2],[243,237,194,21,2],[245,245,148,242,4],[248,15,62,15,4],[252,190,99,125,14],[248,237,33,73,11],[250,216,194,128,14],[254,229,5,156,6],[254,196,253,27,8],[250,210,225,31,4],[252,195,230,115,2],[249,63,162,110,11],[251,86,194,94,3],[255,78,140,162,15],[255,79,112,226,4],[249,177,42,52,9],[255,135,146,120,1],[249,195,188,9,10],[255,202,25,209,12],[255,239,23,165,10],[253,254,218,140,1],[6,66,233,9,7],[3,36,36,70,13],[7,94,92,135,12],[7,136,66,250,6],[10,7,178,27,11],[16,148,220,241,14],[22,162,90,176,15],[16,253,203,109,14],[17,52,172,162,12],[17,166,62,16,1],[30,88,44,226,2],[24,99,58,139,0],[26,99,1,47,13],[26,241,71,206,15],[25,108,177,139,1],[29,149,207,92,12],[25,158,56,146,2],[31,252,86,35,10],[36,27,41,179,3],[38,231,136,133,0],[33,7,54,188,1],[40,77,70,86,10],[46,236,227,69,8],[45,79,225,77,4],[45,138,40,60,7],[52,248,62,55,1],[56,99,93,125,8],[63,162,28,196,12],[66,221,151,174,14],[68,223,18,214,11],[65,58,205,236,8],[65,192,219,73,4],[76,180,0,200,13],[73,35,72,149,2],[82,55,185,92,6],[82,81,56,221,10],[94,113,146,65,3],[92,178,113,235,13],[91,238,55,54,6],[96,5,132,21,2],[100,8,128,51,8],[110,40,135,140,3],[108,85,84,181,7],[104,186,254,70,1],[104,179,85,202,7],[117,156,155,102,9],[124,11,200,10,0],[126,89,104,196,9],[120,111,61,52,5],[120,193,191,255,10],[122,218,185,69,10],[128,60,123,156,0],[135,20,88,249,7],[131,27,5,165,13],[136,59,180,103,4],[140,169,136,156,0],[142,177,3,26,11],[143,39,191,54,4],[143,22,179,202,0],[148,24,222,74,0],[147,69,36,197,0],[156,253,156,216,5],[155,34,55,120,13],[153,250,163,209,6],[162,159,28,84,3],[172,79,91,26,11],[172,191,109,54,6],[169,185,76,64,8],[182,40,233,2,12],[180,109,218,222,0],[178,110,246,10,1],[179,37,153,105,14],[179,80,40,17,5],[190,28,27,59,6],[188,127,85,99,14],[188,175,230,141,5],[189,37,146,140,4],[196,27,214,183,1],[198,99,190,252,14],[198,86,130,19,13],[198,139,73,36,10],[193,61,206,190,2],[197,25,109,109,1],[199,77,61,239,11],[193,90,43,220,9],[202,35,114,33,12],[207,199,244,213,9],[208,49,39,226,6],[212,119,84,14,0],[209,134,49,90,2],[219,76,100,122,11],[219,132,135,144,8],[223,222,6,112,0],[226,41,186,96,0],[225,64,224,141,6],[225,154,144,165,2],[231,242,192,250,9],[238,173,190,131,8],[240,28,242,124,1],[247,101,168,38,4],[247,236,195,164,13],[248,45,84,113,4],[254,133,143,205,11],[248,227,91,11,6],[254,214,62,31,15]],"7x7_1000":[[221,92,108,165,202,10,1],[228,27,241,62,64,171,0],[158,170,43,172,93,39,1],[166,103,5,183,233,76,0],[198,188,123,19,50,86,0],[88,128,20,35,89,238,0],[211,107,190,111,84,72,0],[60,161,109,136,139,219,0],[137,7,31,86,150,158,0],[187,101,177,141,110,63,0],[245,0,209,130,244,144,1],[2,38,112,184,84,26,0],[84,243,107,151,72,240,1],[158,36,136,70,35,153,1],[189,255,50,107,201,203,1],[206,17,5,176,90,151,1],[211,240,97,114,31,139,0],[1,225,158,206,200,237,1],[38,203,204,42,112,120,1],[56,125,77,118,65,19,0],[58,144,74,233,225,233,1],[70,212,132,123,60,221,0],[72,146,167,222,158,43,1],[94,118,195,228,152,168,0],[116,162,222,250,47,136,1],[148,93,226,40,19,89,0],[155,18,149,100,237,163,1],[161,212,148,45,130,248,0],[187,120,230,49,108,208,1],[203,181,221,203,219,24,0],[211,229,238,183,131,51,0],[242,148,35,63,209,156,1],[253,214,209,159,59,230,1],[58,70,36,238,132,238,1],[98,24,126,36,59,15,0],[10,205,67,111,157,195,0],[14,64,212,195,142,2,0],[20,159,217,160,98,36,0],[25,162,225,26,101,237,0],[43,80,130,126,27,52,0],[41,105,56,146,159,163,0],[44,247,84,219,244,86,1],[53,233,63,158,54,124,0],[63,116,171,216,61,243,1],[68,208,41,244,255,64,0],[72,64,228,132,42,245,0],[95,25,19,104,52,2,0],[94,178,114,64,61,93,1],[111,155,36,31,21,194,0],[114,170,249,32,168,227,1],[121,136,117,59,134,54,1],[123,231,19,174,121,81,1],[133,106,70,201,89,224,0],[142,63,238,242,217,81,1],[173,56,205,33,115,103,1],[177,55,37,48,185,63,1],[186,195,128,210,81,219,0],[193,117,109,93,24,166,1],[206,97,211,126,229,17,0],[210,92,152,54,221,91,1],[208,106,145,233,239,206,1],[73,228,227,141,169,90,1],[82,209,159,40,31,179,0],[2,24,175,24,115,160,1],[5,192,75,56,81,119,0],[15,88,169,33,137,159,0],[15,206,165,68,68,26,0],[21,238,211,115,20,216,1],[22,241,226,252,69,158,1],[30,94,76,65,59,115,0],[31,194,82,102,200,118,1],[39,33,22,246,49,159,1],[39,155,122,137,27,220,0],[46,12,48,75,106,108,0],[47,169,226,121,230,43,0],[44,207,211,2,147,19,1],[49,83,3,2,18,230,1],[48,120,115,90,253,41,1],[55,226,67,44,167,160,1],[76,11,79,138,255,84,0],[78,218,184,202,73,33,1],[76,218,221,21,152,110,0],[86,123,85,187,37,96,1],[84,151,117,241,41,203,0],[84,152,140,205,4,162,0],[94,110,164,168,119,169,0],[99,4,81,119,99,178,0],[105,226,58,120,116,228,1],[109,23,226,74,80,28,1],[112,62,128,226,203,177,0],[119,80,90,139,20,43,1],[130,100,225,43,36,151,1],[131,253,167,209,241,197,0],[147,189,107,77,96,125,1],[148,216,165,194,122,237,1],[148,247,189,26,15,79,1],[152,241,170,62,148,248,0],[169,138,223,140,87,40,0],[177,14,54,223,65,63,0],[185,234,28,100,226,107,0],[188,38,74,240,242,183,1],[189,63,146,218,126,170,0],[192,53,46,74,202,87,0],[194,54,220,36,22,128,1],[211,79,245,172,136,118,1],[215,6,210,240,31,150,0],[216,50,245,201,86,5,0],[216,217,65,4,131,170,0],[224,211,100,108,77,209,1],[225,232,37,60,152,205,0],[229,118,79,139,232,157,1],[232,80,250,159,250,74,0],[235,128,234,240,130,92,1],[239,108,113,174,69,200,1],[247,217,64,237,177,12,0],[254,214,40,241,92,60,1],[20,199,225,220,124,3,1],[34,236,79,160,255,225,0],[164,134,72,34,231,234,0],[179,172,156,128,134,12,1],[235,197,247,106,12,68,0],[0,36,28,209,199,244,0],[0,65,85,60,228,236,0],[2,119,133,135,91,48,1],[1,218,232,155,2,227,0],[4,40,5,26,111,159,0],[7,145,0,75,83,171,0],[10,89,153,210,227,215,1],[9,178,243,249,248,177,1],[13,49,205,127,92,218,1],[14,110,159,79,116,37,1],[23,114,128,242,245,110,0],[22,255,97,63,186,238,0],[25,50,221,83,147,173,1],[30,55,90,161,138,28,1],[30,243,155,67,46,40,0],[35,152,218,66,102,77,0],[35,183,98,39,73,166,1],[39,232,89,247,30,7,0],[41,207,36,233,56,245,0],[43,202,225,232,241,71,1],[46,52,167,60,136,145,0],[44,130,54,130,20,241,1],[52,119,39,34,124,128,0],[62,106,78,239,207,36,0],[61,189,0,186,135,236,0],[65,89,70,17,248,154,0],[66,160,192,80,251,19,1],[66,215,59,80,17,218,1],[71,21,159,236,40,89,1],[72,72,74,156,121,141,1],[75,141,239,56,120,19,0],[72,197,79,119,42,255,1],[73,205,149,222,250,38,0],[74,248,19,154,40,250,0],[76,39,45,47,65,165,0],[76,63,202,163,60,106,1],[76,105,150,233,240,91,0],[83,59,100,210,198,162,0],[81,70,77,104,180,53,1],[81,86,86,98,113,201,1],[80,115,53,140,113,223,1],[84,74,231,46,2,158,1],[84,219,22,238,94,71,0],[86,240,240,30,140,100,1],[91,106,3,196,23,123,1],[91,249,239,82,25,108,1],[92,116,249,2,251,190,0],[94,157,11,47,79,122,1],[99,67,213,185,201,130,0],[96,93,164,245,22,155,1],[100,136,47,135,72,47,0],[104,106,145,148,102,64,0],[108,55,209,238,204,101,1],[109,89,245,135,43,42,0],[115,31,7,158,40,12,0],[114,38,22,146,200,199,1],[115,131,239,207,199,93,1],[113,246,244,181,170,13,0],[118,16,2,241,170,59,0],[118,84,255,84,38,33,0],[122,227,200,30,38,54,0],[126,32,91,27,109,102,0],[124,113,102,127,179,75,1],[126,136,249,129,71,26,0],[127,189,182,243,152,70,0],[131,186,63,66,156,103,0],[131,179,163,188,159,85,1],[134,184,56,238,195,91,1],[138,251,116,46,23,43,0],[142,5,134,252,131,42,0],[143,72,60,30,32,231,0],[142,83,181,252,28,224,0],[141,157,35,130,73,96,0],[141,185,144,117,146,156,1],[144,19,230,225,162,176,0],[147,155,55,123,26,144,1],[151,14,49,237,242,168,1],[151,52,94,59,153,170,0],[149,122,253,44,125,85,0],[151,221,14,6,20,80,1],[154,154,4,227,189,68,1],[155,190,173,173,13,248,1],[157,103,144,38,24,93,0],[159,129,201,246,175,127,0],[159,247,196,109,149,97,1],[167,3,62,38,186,122,0],[166,79,109,144,30,111,0],[166,84,136,155,103,3,0],[164,250,171,52,23,138,0],[169,35,165,79,219,255,1],[175,22,89,55,116,14,1],[174,60,2,96,17,226,1],[174,166,70,29,70,134,0],[174,169,188,206,254,64,0],[177,170,233,41,210,190,0],[179,236,199,247,61,40,0],[179,242,249,209,5,96,1],[183,83,178,95,132,147,0],[180,99,115,84,106,222,0],[182,97,89,33,192,136,0],[186,184,208,95,68,186,1],[186,192,40,124,16,3,1],[188,9,19,77,135,103,0],[190,60,15,68,249,12,0],[193,39,249,24,172,4,0],[194,54,103,175,248,99,1],[194,202,232,228,90,50,0],[198,18,22,32,228,28,0],[199,229,93,208,65,150,0],[202,89,204,102,248,236,0],[200,251,13,58,124,119,1],[204,35,191,4,244,230,0],[204,185,29,222,17,77,0],[214,71,53,203,221,81,1],[212,80,13,82,140,243,1],[214,126,50,1,136,117,0],[214,239,103,48,199,69,1],[217,10,193,255,8,72,0],[218,68,144,181,192,149,0],[218,142,240,138,31,78,1],[226,186,72,154,71,244,1],[226,251,142,197,152,25,0],[229,62,6,191,131,16,1],[230,135,176,105,180,112,0],[229,161,35,223,231,210,0],[228,231,74,54,176,3,0],[235,25,74,181,219,227,0],[235,104,214,214,135,188,0],[239,76,157,25,92,242,0],[240,140,65,178,161,79,1],[245,42,227,102,145,91,0],[247,127,207,238,89,167,0],[248,218,47,9,127,235,0],[252,25,236,171,9,228,1],[147,75,97,136,48,21,0],[200,173,102,37,128,21,1],[28,218,21,111,160,160,1],[35,147,244,147,165,30,0],[47,88,146,189,228,140,1],[67,161,247,150,104,223,0],[73,80,111,179,116,206,1],[82,96,94,14,152,127,0],[82,145,24,227,252,161,0],[101,223,133,82,42,63,1],[113,72,12,215,43,160,0],[142,6,96,125,160,37,0],[150,170,33,3,84,112,1],[161,37,174,137,93,100,1],[188,234,86,10,46,231,1],[194,210,70,164,139,68,0],[218,226,149,139,69,176,0],[236,63,99,213,29,216,0],[255,105,45,60,107,15,0],[0,28,79,167,70,146,1],[2,24,244,93,202,150,0],[3,209,163,72,235,88,0],[4,27,131,253,41,154,1],[9,141,142,139,148,215,1],[8,163,222,26,89,139,0],[11,188,12,8,32,24,0],[8,180,118,44,124,199,0],[8,252,181,200,85,47,1],[14,22,54,84,254,34,0],[18,44,177,159,7,34,1],[18,69,156,171,22,232,0],[19,147,126,245,174,67,1],[22,41,244,18,48,140,0],[21,124,103,88,122,66,0],[21,204,177,89,131,214,1],[23,227,47,161,221,185,0],[27,70,90,142,239,10,1],[27,105,78,73,179,188,1],[27,196,203,219,144,138,1],[31,44,109,141,119,17,1],[31,87,63,15,8,234,1],[28,109,100,176,216,126,1],[31,214,51,233,39,78,1],[31,237,94,0,105,171,1],[32,25,133,43,191,166,0],[34,61,51,120,7,207,0],[32,99,33,28,107,49,0],[34,110,87,72,118,216,1],[32,116,241,178,248,194,1],[33,172,61,93,237,110,0],[33,195,23,29,144,62,1],[39,47,226,196,67,84,0],[39,159,50,224,232,133,0],[38,190,33,114,154,9,1],[42,3,57,247,158,125,1],[43,24,147,239,72,235,0],[41,20,232,100,42,8,0],[41,104,166,84,120,185,0],[42,193,129,37,248,55,0],[40,212,32,190,102,221,1],[41,251,7,40,147,64,0],[45,79,27,209,68,171,0],[44,77,58,93,125,209,0],[46,201,163,115,80,78,1],[47,242,157,56,166,39,0],[51,14,242,29,118,78,0],[51,46,161,171,153,4,0],[49,41,200,243,224,220,1],[53,0,254,245,196,8,0],[55,36,110,62,36,63,0],[53,81,121,197,146,92,1],[54,184,189,91,81,60,0],[54,221,188,202,44,119,0],[55,221,249,250,116,140,0],[57,8,64,158,68,97,1],[59,1,93,14,177,201,1],[58,14,216,225,88,0,1],[56,110,40,10,241,243,1],[57,97,243,168,199,88,1],[57,175,5,221,138,171,1],[57,172,253,140,32,166,1],[56,215,227,25,189,206,1],[62,29,177,174,155,223,1],[63,57,54,16,131,58,1],[61,68,172,103,73,121,1],[62,216,120,160,212,247,1],[67,45,92,111,10,140,0],[66,52,3,145,156,82,1],[64,59,107,130,137,239,0],[67,109,145,73,70,116,0],[67,151,133,107,233,44,1],[71,29,159,177,209,0,1],[70,43,251,166,87,121,0],[70,92,73,248,0,58,1],[69,118,247,32,48,38,1],[70,168,40,69,154,252,0],[68,160,107,94,1,10,0],[69,185,78,113,53,37,0],[75,113,215,160,253,197,1],[79,60,121,82,108,209,1],[79,178,80,33,70,98,0],[79,239,177,247,108,238,1],[82,75,82,250,189,204,0],[83,200,244,221,123,200,1],[87,5,99,141,134,48,0],[85,75,225,85,227,0,0],[85,138,47,175,183,98,0],[84,153,49,31,160,83,0],[88,13,255,122,69,176,1],[90,18,17,208,247,52,1],[95,17,24,22,24,164,0],[93,61,87,179,227,47,0],[93,74,155,244,162,166,0],[94,137,78,179,16,89,0],[95,182,4,116,142,30,1],[95,252,162,7,10,47,1],[98,20,52,163,198,43,1],[96,69,249,159,75,213,0],[96,94,235,202,23,75,1],[97,111,101,43,244,79,0],[96,111,231,237,175,53,0],[98,227,54,39,167,213,0],[101,50,13,146,242,6,0],[101,174,116,178,81,246,0],[106,37,233,73,31,2,1],[105,154,22,139,171,78,0],[104,159,185,186,26,80,0],[104,172,27,248,226,27,1],[107,204,220,27,224,85,0],[110,70,68,174,42,27,1],[110,91,221,156,85,145,1],[110,114,103,14,62,109,0],[111,134,147,35,45,171,0],[108,137,244,64,218,114,1],[111,156,82,187,178,211,1],[111,148,127,90,90,175,1],[110,168,22,55,188,108,1],[110,177,138,108,23,81,0],[111,200,142,200,247,162,0],[115,81,80,190,35,239,0],[112,139,71,240,27,189,0],[112,149,37,251,26,110,1],[114,158,255,114,99,90,0],[112,171,227,124,150,208,1],[114,245,151,82,179,142,0],[118,5,164,230,226,221,0],[116,17,6,76,105,165,0],[118,36,17,60,244,246,1],[116,101,193,88,187,101,1],[119,125,91,58,188,225,0],[119,182,15,188,123,59,0],[118,180,133,0,61,168,1],[117,238,187,75,14,98,1],[118,250,208,216,178,184,0],[120,75,97,130,218,8,1],[122,187,251,47,52,31,1],[127,39,5,215,84,100,0],[125,227,180,31,248,152,0],[127,237,141,135,251,207,1],[126,246,82,55,17,197,0],[129,31,240,203,185,104,1],[128,46,234,6,149,52,0],[130,73,255,59,138,203,1],[129,200,212,49,219,37,0],[132,43,15,88,160,122,0],[133,43,127,179,216,14,0],[134,32,167,236,235,192,1],[134,89,195,75,94,119,1],[135,96,6,117,87,9,0],[132,219,148,26,247,200,0],[138,22,181,10,57,125,0],[138,63,78,17,183,8,0],[137,70,134,138,98,190,0],[139,122,129,163,127,219,0],[137,134,220,73,174,214,0],[137,153,52,70,112,178,1],[139,205,202,224,85,9,1],[139,222,241,118,82,104,1],[142,18,98,26,198,81,1],[141,46,148,181,204,168,0],[140,75,185,11,82,34,0],[141,153,201,208,216,171,1],[144,17,129,73,60,185,1],[146,45,168,84,94,84,1],[144,78,120,161,56,250,1],[144,151,90,16,127,241,0],[144,179,61,187,100,42,0],[147,192,227,79,19,241,0],[151,73,34,81,12,38,0],[151,74,87,164,6,195,0],[150,115,104,38,246,150,1],[149,138,223,218,96,3,1],[150,175,46,35,58,45,0],[155,7,68,229,222,47,0],[152,75,88,148,206,180,0],[153,122,202,55,11,119,1],[156,0,144,208,133,65,1],[157,117,4,69,227,242,1],[160,66,244,31,20,137,1],[160,154,32,41,137,97,0],[161,151,65,181,6,20,0],[162,199,179,237,218,219,0],[163,237,115,3,171,156,0],[164,14,153,123,197,24,0],[166,46,211,181,170,239,0],[166,72,83,81,97,91,1],[166,88,46,241,183,120,1],[167,125,62,203,3,105,0],[165,183,40,12,100,157,0],[164,207,91,78,212,244,0],[171,21,211,205,48,133,0],[171,47,103,30,208,8,1],[168,43,177,100,80,17,1],[168,67,58,121,199,250,1],[168,95,145,245,169,81,0],[168,126,123,132,128,94,1],[168,132,121,195,96,56,1],[171,163,68,207,130,112,0],[171,230,49,247,179,118,0],[172,22,16,22,73,151,0],[174,39,216,18,235,36,1],[175,59,230,2,186,159,1],[174,70,26,234,137,71,1],[173,66,113,218,119,159,1],[173,88,23,206,43,193,1],[172,151,210,208,179,77,0],[173,190,197,66,11,202,0],[175,250,80,85,120,101,0],[176,31,206,26,12,146,1],[177,112,134,190,143,137,0],[183,222,54,61,41,2,0],[180,229,14,234,89,126,0],[186,59,44,235,91,138,0],[187,123,82,243,209,20,0],[186,116,182,71,89,86,1],[186,139,73,97,25,111,1],[187,152,233,200,28,89,0],[188,30,177,81,156,163,1],[188,42,52,17,27,68,1],[189,81,101,27,30,213,0],[190,112,117,173,51,180,1],[189,255,164,161,70,198,1],[193,4,151,159,155,192,0],[193,30,33,36,254,38,1],[192,237,72,64,106,165,0],[192,234,184,166,4,229,0],[198,87,250,41,77,82,1],[200,111,63,193,206,104,0],[200,99,86,204,12,4,1],[201,215,21,97,151,171,1],[203,227,118,18,26,242,1],[204,23,188,202,55,164,1],[204,223,166,39,145,190,0],[204,235,235,67,36,93,0],[209,53,155,82,5,119,0],[209,91,212,112,60,106,0],[215,26,117,66,163,79,1],[215,230,123,63,234,215,1],[219,2,186,238,81,189,1],[216,126,223,70,165,187,1],[216,131,133,71,132,38,1],[218,189,22,192,207,231,0],[219,226,77,128,236,124,1],[217,244,154,237,44,69,0],[222,24,187,178,78,206,1],[221,25,226,31,178,162,1],[222,44,65,181,149,43,0],[220,75,143,150,13,97,0],[221,148,173,142,18,115,1],[221,209,152,178,105,248,1],[225,16,129,210,125,199,0],[226,29,244,60,230,69,1],[227,42,142,26,117,17,0],[230,127,34,189,228,63,1],[230,201,244,50,47,175,1],[233,65,95,6,96,115,0],[232,162,221,84,147,113,0],[232,216,146,243,185,171,1],[234,224,242,13,25,46,0],[237,183,135,120,221,244,0],[237,243,41,3,202,39,1],[242,9,23,170,178,81,0],[242,87,191,49,98,230,1],[240,161,59,114,216,194,0],[242,204,10,145,3,251,0],[240,197,80,127,169,160,1],[241,197,159,71,93,153,1],[240,244,101,147,182,176,1],[244,0,37,19,37,72,0],[244,111,108,190,223,216,1],[247,104,114,223,78,225,0],[251,140,211,46,207,23,0],[249,145,73,232,47,176,0],[251,204,144,194,16,228,0],[248,252,106,92,170,113,1],[254,15,171,22,40,191,1],[252,66,14,134,208,220,0],[252,124,113,120,44,31,0],[255,159,183,150,227,134,1],[112,127,0,71,111,110,0],[131,238,196,68,55,228,0],[1,32,180,5,180,95,1],[0,59,124,20,69,167,0],[3,79,186,190,163,12,0],[1,86,37,1,21,142,0],[1,143,230,168,33,42,0],[0,175,103,1,210,134,0],[0,217,16,247,55,249,0],[3,240,9,255,186,56,0],[5,92,205,107,57,177,1],[6,109,146,36,128,158,1],[4,166,38,247,185,66,1],[4,196,160,187,5,176,0],[9,4,240,102,157,166,0],[8,63,162,23,251,204,1],[10,107,164,59,63,71,1],[9,147,68,160,208,252,0],[8,210,94,4,114,98,1],[14,39,43,135,201,75,1],[15,42,254,150,218,227,0],[13,94,114,56,152,3,0],[14,87,112,167,99,99,1],[13,106,169,220,27,63,0],[13,118,154,128,113,208,1],[14,124,197,35,226,204,1],[15,139,106,20,246,21,0],[13,171,134,215,206,17,1],[15,203,199,97,60,150,1],[14,236,63,51,174,17,1],[17,15,152,165,71,82,0],[17,119,210,131,226,153,0],[17,151,181,190,81,161,0],[18,155,152,58,105,111,1],[19,233,65,189,245,218,0],[22,13,83,212,169,87,0],[22,61,21,111,231,73,0],[20,130,100,0,95,151,1],[21,130,161,143,42,56,1],[21,153,34,53,74,250,0],[23,156,89,25,214,46,0],[21,165,87,175,25,244,1],[22,185,129,147,151,243,0],[20,212,174,31,83,198,0],[22,222,188,166,177,46,1],[21,215,231,50,136,117,0],[27,5,137,184,14,199,1],[24,76,13,17,16,216,1],[27,80,19,3,222,147,1],[26,117,67,117,230,58,0],[25,139,44,109,101,206,0],[24,157,237,154,191,3,0],[26,163,189,21,22,74,1],[24,191,221,217,136,80,1],[26,187,197,128,248,166,1],[24,202,200,115,174,12,1],[26,251,85,224,195,153,1],[25,252,172,250,123,235,0],[31,33,77,51,43,129,0],[29,104,97,97,80,45,1],[29,113,17,105,159,205,1],[28,115,52,93,58,61,1],[30,167,7,22,61,22,1],[32,22,241,69,239,16,1],[33,37,79,144,49,77,0],[35,45,115,235,220,62,1],[35,72,62,43,100,184,1],[35,86,58,73,248,94,1],[34,138,184,17,107,194,0],[32,167,72,150,66,82,1],[34,164,248,245,231,39,1],[33,239,227,53,220,27,1],[33,255,170,148,240,254,0],[37,48,68,183,73,106,0],[38,55,142,202,57,200,0],[39,67,65,27,226,165,0],[39,119,180,37,12,229,0],[36,140,97,44,211,5,0],[39,181,134,58,242,110,1],[37,192,47,169,170,233,1],[37,193,244,243,24,172,1],[38,223,180,89,168,216,1],[36,229,181,254,134,140,0],[36,250,147,106,49,84,1],[41,33,120,64,98,223,1],[42,47,218,50,70,201,1],[40,62,63,42,103,4,0],[43,205,185,118,1,186,0],[40,234,218,205,222,126,1],[44,77,140,242,231,58,1],[46,88,0,228,62,206,0],[45,129,187,116,79,227,1],[44,145,67,129,237,29,0],[45,180,8,215,116,177,1],[44,224,23,61,89,3,1],[48,9,23,86,236,208,1],[51,70,13,15,126,150,0],[50,113,60,116,164,89,0],[48,167,239,179,52,231,1],[48,183,244,73,95,202,0],[51,236,16,56,154,82,0],[53,106,133,153,65,91,0],[54,125,199,249,16,137,1],[53,145,236,15,52,5,1],[58,31,122,71,185,141,0],[58,86,77,209,166,203,1],[57,105,99,159,57,176,1],[59,96,103,17,227,243,0],[57,204,214,65,214,205,1],[60,51,21,240,128,231,0],[60,51,197,75,3,115,1],[61,98,186,75,99,200,0],[61,117,9,162,109,95,1],[63,152,179,12,62,51,1],[62,205,175,189,212,43,0],[63,213,180,83,119,231,1],[60,233,181,48,105,180,0],[64,3,93,254,66,40,1],[67,106,171,8,205,110,1],[67,113,229,169,206,30,1],[65,168,233,191,52,145,0],[65,202,197,166,227,19,0],[66,247,60,189,2,52,0],[68,39,29,179,182,219,1],[68,76,27,64,24,97,1],[70,74,152,175,201,220,0],[70,131,6,249,199,224,1],[68,140,202,221,114,250,1],[70,158,58,59,116,65,1],[73,31,143,197,190,227,0],[73,75,34,203,253,35,1],[73,91,8,111,19,202,1],[74,177,68,70,10,33,1],[75,195,24,213,28,201,0],[76,116,244,77,39,23,1],[78,157,120,2,250,136,1],[79,168,131,36,88,72,0],[77,204,62,66,47,44,0],[77,205,182,47,105,192,1],[76,252,81,246,35,2,0],[77,248,189,181,78,179,0],[78,254,206,197,81,2,1],[83,45,244,148,190,11,1],[83,55,48,177,237,76,1],[81,138,115,76,78,26,0],[81,173,41,83,250,51,0],[80,244,139,181,87,23,1],[83,242,216,169,125,63,0],[87,46,51,92,65,229,1],[85,97,138,153,243,43,1],[84,114,176,221,66,144,1],[87,120,188,172,178,210,1],[85,178,111,85,255,238,1],[87,215,193,128,15,12,1],[85,243,37,159,247,53,0],[90,0,169,237,210,158,0],[89,39,61,104,89,78,1],[89,73,179,211,30,79,0],[91,92,30,37,185,241,1],[89,81,133,145,139,249,0],[91,156,92,115,202,248,1],[90,148,209,233,34,152,1],[88,170,155,107,27,52,0],[90,202,163,22,250,87,1],[88,216,190,3,241,57,0],[90,253,27,96,216,252,0],[93,40,12,244,83,170,1],[94,52,215,24,145,91,0],[94,140,231,129,51,103,0],[92,240,52,170,32,5,0],[99,47,8,198,1,227,0],[97,34,240,131,154,222,0],[97,64,6,28,66,91,1],[99,104,252,248,31,16,0],[98,163,80,160,5,206,1],[96,207,8,137,207,134,1],[100,31,102,147,38,250,0],[103,84,36,66,155,194,1],[103,94,109,245,161,167,0],[100,112,128,63,112,33,0],[101,113,143,62,145,182,1],[103,153,192,236,194,232,1],[102,181,191,1,79,214,0],[103,209,57,91,135,184,0],[101,214,185,83,116,18,0],[107,53,69,174,190,183,0],[106,85,31,116,156,15,1],[105,214,240,35,1,52,0],[107,214,206,166,84,232,1],[108,83,126,168,65,235,0],[110,99,164,20,64,36,1],[108,120,204,22,132,88,1],[108,206,226,122,51,47,0],[111,226,98,8,71,155,0],[113,2,15,225,198,243,0],[113,1,66,223,219,44,0],[114,10,186,206,130,96,0],[114,76,255,13,215,153,0],[112,148,200,167,175,228,0],[115,219,92,88,31,37,1],[113,220,240,226,63,213,1],[115,240,217,222,113,35,1],[117,16,178,106,60,109,0],[117,125,232,146,6,63,0],[116,159,153,133,17,253,0],[122,14,219,24,154,237,0],[121,22,143,58,72,165,1],[121,188,129,6,112,53,0],[125,77,194,163,143,129,1],[124,135,26,99,181,78,0],[127,162,123,214,2,146,1],[125,171,223,59,221,197,1],[125,187,65,173,85,124,1],[131,4,66,138,233,199,0],[131,43,42,216,218,107,1],[129,42,82,146,228,127,0],[129,56,22,164,149,242,0],[129,100,221,191,156,110,1],[129,123,93,252,187,253,1],[130,178,225,68,80,78,0],[128,247,188,97,25,21,0],[135,52,106,165,125,200,0],[132,140,23,210,149,239,1],[132,150,182,186,170,44,0],[135,156,185,242,219,180,0],[132,196,118,79,240,21,1],[135,196,142,129,238,18,1],[133,225,245,45,64,89,1],[137,14,35,66,210,193,1],[138,56,218,197,237,134,1],[138,149,152,168,110,21,0],[141,29,47,20,46,123,1],[141,111,99,41,14,170,1],[142,178,140,83,176,124,1],[142,222,235,43,219,30,1],[141,245,6,243,158,250,0],[144,23,149,47,152,143,0],[145,35,7,27,254,232,0],[144,37,85,177,206,2,1],[144,40,188,66,77,232,0],[144,57,208,138,32,54,1],[147,79,149,211,130,90,0],[146,164,210,234,226,74,0],[145,180,112,9,138,92,1],[147,222,25,219,41,219,1],[145,229,150,87,118,92,0],[146,236,144,218,63,135,0],[146,252,112,168,201,176,0],[149,25,170,136,106,158,1],[148,82,41,183,65,127,0],[148,87,59,92,68,38,1],[151,82,5,249,168,20,0],[150,156,251,61,143,160,1],[151,163,111,26,69,144,1],[151,170,229,229,236,98,0],[151,229,170,59,113,88,1],[154,32,49,130,105,146,1],[153,69,56,75,61,53,1],[154,177,236,143,208,111,1],[154,213,151,178,63,108,1],[153,249,19,79,121,230,0],[158,24,232,0,191,106,1],[159,72,93,163,29,40,1],[159,84,21,142,137,140,0],[157,106,155,82,57,78,1],[158,111,156,10,159,118,0],[160,9,100,5,91,91,0],[162,60,219,252,54,178,0],[163,91,247,118,247,143,0],[162,157,115,18,132,59,0],[160,206,10,169,116,87,0],[160,220,172,38,43,179,1],[162,245,118,116,106,43,1],[166,47,214,238,159,142,1],[165,56,116,248,96,115,1],[164,94,71,141,231,74,0],[167,93,87,124,48,116,1],[166,114,149,34,69,134,0],[165,131,104,82,20,90,0],[164,155,84,147,194,3,0],[166,189,42,131,213,14,0],[169,7,89,5,251,125,0],[169,21,217,154,37,235,1],[171,34,146,112,50,2,0],[169,55,228,166,33,245,1],[170,114,189,81,74,123,0],[168,137,199,107,240,9,0],[170,213,171,110,131,212,1],[168,211,200,181,190,175,0],[174,20,19,54,167,164,0],[173,100,135,249,112,220,1],[175,121,76,217,234,209,1],[173,154,157,184,0,254,1],[175,156,136,245,213,82,0],[173,207,238,79,108,35,0],[173,247,163,231,58,61,1],[178,65,252,215,31,187,0],[179,66,216,120,217,241,1],[178,97,81,226,141,99,0],[179,99,173,194,60,10,1],[179,122,143,78,111,189,1],[176,177,13,218,203,9,1],[176,187,235,133,79,183,0],[177,195,46,176,79,2,0],[177,202,162,122,198,206,0],[180,119,47,73,43,245,0],[181,174,77,39,89,131,1],[183,210,205,142,206,219,0],[182,235,156,93,104,151,0],[180,244,103,243,78,87,0],[184,15,212,222,49,7,1],[184,57,126,65,128,2,0],[184,56,131,182,233,190,0],[184,108,6,172,40,232,0],[185,136,183,154,126,192,1],[184,156,165,183,60,206,0],[186,207,131,167,229,28,1],[184,231,17,147,88,172,1],[189,114,116,100,13,206,1],[188,125,134,57,75,186,1],[195,30,73,175,1,169,1],[192,46,91,239,41,95,0],[193,78,242,15,34,121,1],[193,129,72,108,154,70,1],[193,153,177,237,205,191,1],[193,146,177,38,19,223,1],[195,183,213,56,210,51,1],[193,215,25,188,235,129,1],[193,246,67,78,186,232,1],[196,56,145,7,35,108,1],[198,77,74,227,205,183,0],[199,83,172,246,95,75,1],[197,126,74,113,2,75,0],[198,116,217,128,138,199,1],[197,136,152,47,230,202,1],[197,174,39,8,53,94,1],[199,188,103,248,187,33,1],[196,190,140,164,243,99,0],[203,15,36,114,82,53,0],[201,72,89,111,111,39,1],[200,85,15,64,161,129,0],[203,90,139,223,86,169,0],[200,115,24,175,214,16,0],[201,159,158,95,56,56,0],[202,191,51,44,49,240,0],[201,233,181,7,221,84,0],[203,240,30,59,146,129,0],[204,2,72,129,217,10,0],[206,44,78,124,45,3,1],[205,131,179,65,153,204,1],[206,128,171,157,219,17,1],[204,183,70,234,75,14,1],[207,228,35,113,192,123,1],[205,246,59,148,188,171,1],[211,23,149,186,69,222,1],[208,58,34,105,80,179,0],[211,54,150,219,2,36,1],[210,86,224,221,145,27,0],[210,188,170,228,3,129,0],[211,196,238,80,249,166,0],[211,228,7,252,5,226,1],[211,252,28,167,191,2,0],[215,68,167,168,172,223,0],[214,159,244,68,89,83,0],[212,175,147,230,193,68,0],[217,18,102,139,15,210,1],[216,81,88,45,224,190,1],[216,99,102,115,129,229,0],[219,108,88,112,132,206,1],[217,99,155,60,255,211,0],[216,138,98,229,71,244,0],[217,153,210,99,213,126,1],[217,162,40,145,224,209,0],[219,244,50,154,65,205,0],[220,29,107,96,189,237,0],[222,26,217,103,197,109,0],[221,114,161,214,214,204,1],[221,116,213,91,237,200,1],[221,134,26,161,4,19,1],[223,175,27,5,181,189,1],[222,176,237,214,204,22,1],[223,230,10,66,93,241,0],[221,251,88,81,243,143,0],[227,22,68,69,140,160,0],[226,26,171,99,248,21,1],[226,90,168,144,16,221,1],[224,126,198,224,106,118,1],[225,117,204,107,230,146,0],[224,221,231,62,185,106,0],[231,26,89,61,43,68,0],[230,61,186,78,33,23,0],[231,80,110,107,241,111,0],[229,83,212,72,59,179,0],[228,120,9,177,15,166,1],[229,122,161,185,186,74,1],[231,124,180,146,88,174,1],[230,164,245,240,132,66,1],[229,213,42,118,142,25,1],[231,215,202,210,228,123,1],[228,239,89,211,239,61,1],[235,27,228,239,142,205,0],[233,50,9,4,85,129,1],[235,52,254,217,172,58,1],[234,78,19,245,78,30,0],[233,176,103,32,6,254,0],[234,213,177,80,109,191,0],[239,10,161,134,54,174,0],[237,54,239,15,39,238,1],[237,74,130,29,33,213,0],[236,96,189,109,140,6,0],[239,130,202,100,252,71,0],[238,161,127,189,151,92,0],[238,209,152,137,82,229,1],[241,9,85,233,4,138,1],[243,42,138,178,12,104,1],[243,71,148,129,100,1,1],[243,136,79,37,132,41,1],[240,162,31,6,126,77,1],[243,170,11,255,165,205,0],[240,175,124,121,142,228,0],[240,217,16,212,202,31,1],[245,75,179,178,85,245,1],[246,81,96,41,26,234,1],[246,117,82,145,217,185,1],[245,114,198,173,192,65,0],[247,124,210,85,227,70,0],[246,132,78,31,171,28,0],[248,61,136,57,165,1,1],[251,54,195,223,220,215,1],[248,76,172,3,76,20,1],[251,160,208,185,211,75,1],[250,195,132,204,125,54,1],[249,231,61,60,30,97,1],[248,255,88,101,6,119,1],[253,11,219,85,100,56,1],[253,22,174,228,229,62,1],[254,73,163,120,194,52,0],[253,132,226,198,107,168,1],[252,149,79,148,230,207,0],[254,165,35,88,48,164,1],[254,168,217,255,176,118,0],[255,195,135,255,162,252,1],[95,114,124,241,17,248,0],[135,81,63,164,61,190,1],[198,147,77,53,31,189,0],[231,230,150,226,6,159,1]]}
	var bytes = dict[dictName][id];
	var bits = [];
	var bitsCount = width *  height;

	// Parse marker's bytes
	for (var byte of bytes) {
		var start = bitsCount - bits.length;
		for (var i = Math.min(7, start - 1); i >= 0; i--) {
			bits.push((byte >> i) & 1);
		}
	}
	return generateMarkerSvg(width, height, bits);
}

function createArucoMarkerElement(divMarker, markerId) {
	// define marker attributes
	var size = 8
	var dictName = "4x4_1000"
	var width = 4
	var height = 4

	var svg = generateArucoMarker(width, height, dictName, markerId, size);
	svg.attr({
		width: size + 'mm',
		height: size + 'mm'
	});

	const idName = "#" + divMarker.id
	console.log(idName)
	$(idName).html(svg[0].outerHTML) //append created svg-Marker to marker-container
}

function returnArucoMarkerElement(markerId) {
	// define marker attributes
	var size = 8
	var dictName = "4x4_1000"
	var width = 4
	var height = 4

	var svg = generateArucoMarker(width, height, dictName, markerId, size);
	svg.attr({
		width: size + 'mm',
		height: size + 'mm'
	});

    body = document.body

    const arucoMarkerPlaceholder = document.createElement("div")
    arucoMarkerPlaceholder.setAttribute("id", "arucoMarkerPlaceholder")

    body.appendChild(arucoMarkerPlaceholder)

	$("#arucoMarkerPlaceholder").html(svg[0].outerHTML) //append created svg-Marker to marker-container
    const svgElement = document.getElementById(arucoMarkerPlaceholder.id).firstElementChild
    return svgElement
}

const inlineMarkingElement = function(element, markerId) {
    element.setAttribute("markerId", markerId)
    const arucoMarkerLeft = returnArucoMarkerElement(markerId)
    const arucoMarkerRight = returnArucoMarkerElement(markerId)
    element.before(arucoMarkerLeft)
    element.after(arucoMarkerRight)
}

const adaptiveMarkingElement = function(taskNumber, element, markerId) {
    /**
     * In dieser Methode werden für ein zu markierendes Element links und rechts davon div-Container erzeugt, um darin die Marker einzusetzen.
     */

    // Element-Hierarchie erzeugen
    const taggedArea = document.createElement("div")
    taggedArea.className = "taggedArea"
    taggedArea.setAttribute("markerId", markerId)
    const divLinks = document.createElement("div")
    divLinks.className = "divLinks"
    const divCodeLinks = document.createElement("div")
    divCodeLinks.setAttribute("id", "divCodeLinks_" + taskNumber + "_" + markerId)
    const divMitte = document.createElement("div")
    divMitte.className = "divMitte"
    const divRechts = document.createElement("div")
    divRechts.className = "divRechts"
    const divCodeRechts = document.createElement("div")
    divCodeRechts.setAttribute("id", "divCodeRechts_" + taskNumber + "_" + markerId)
    element.before(taggedArea)
    divMitte.appendChild(element)
    taggedArea.appendChild(divLinks)
    taggedArea.appendChild(divMitte)
    taggedArea.appendChild(divRechts)
    divLinks.appendChild(divCodeLinks)
    divRechts.appendChild(divCodeRechts)
    createArucoMarkerElement(divCodeLinks, markerId)
    createArucoMarkerElement(divCodeRechts, markerId)
    
    //CSS
    taggedArea.style.display = "flex"
    divRechts.style.display = "flex"
    divCodeLinks.style.position = "sticky"
    divCodeLinks.style.top = "10px"
    divCodeLinks.style.margin = "10px"
    divCodeRechts.style.marginTop = "auto"
    divCodeRechts.style.marginLeft = "10px"
    divCodeRechts.style.marginBottom = "10px"
    divCodeRechts.style.marginRight = "10px"
    divCodeRechts.style.position = "sticky"
    divCodeRechts.style.bottom = "10px"
    divCodeRechts.style.marginBottom = "10px"
    divMitte.style.padding = "10px"
    divMitte.style.overflowX = "auto" // in ArUCo
}

/****************************
 * Klassen
 ****************************/
class Timeline {
    constructor() {
        this.jsonVersion = "0"
        this.startTimestamp = localStorage.getItem("startTimestamp")
        this.userId = String(localStorage.getItem("username"))
        this.fragebogenTitle = getFragebogenTitle()
        this.taskTitle = ""
        this.studyName = "test_study" //hardcoded
    }
    push(timelineElement) {
        this.pushToL2P(timelineElement)
        this.pushToLocalStorage(timelineElement)
    }
    pushToL2P(timelineElement) {
        console.log("Push to LRS...")
        // This is an example for the jsonData
        const jsonDataExample = {
            "userId":"999999",
            "studyName":"test_study",
            
            "fragebogenTitle":"Aufgabensammlung",
            "taskId":"idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
            "taskTitle":"no title",
            "taskTypeIndex":"1",
            "taskElementId":"1",
    
            "inputFieldIndex":"1",
            "taskInputValue":"check0",
            "inputCounter":"0",

            "timestamp":"1627565441592",
            "startTimestamp":"1627565441591",
            "jsonVersion":"1",
        }
        // collecting the information in variables
        const userId = this.userId
        const studyName = this.studyName

        const fragebogenTitle = this.fragebogenTitle
        const taskId = timelineElement.taskId
        const taskTitle = timelineElement.taskTitle
        const taskTypeIndex = timelineElement.taskTypeIndex
        const taskElementId = timelineElement.taskElementId

        const inputFieldIndex = timelineElement.inputFieldIndex
        const taskInputValue = timelineElement.taskInputValue
        const inputCounter = String(timelineElement.inputCounter)
        
        const timestamp = String(timelineElement.timestamp)
        const startTimestamp = this.startTimestamp
        const jsonVersion = String(this.jsonVersion)
        // const jsonVersion = this.jsonVersion

        // creating an e-element with the data of the variables
        let e = {}
        e.userId = userId
        e.studyName = studyName
        e.fragebogenTitle = fragebogenTitle
        e.taskId = taskId
        e.taskTitle = taskTitle
        e.taskTypeIndex = taskTypeIndex
        e.taskElementId = taskElementId
        e.inputFieldIndex = inputFieldIndex
        e.taskInputValue = taskInputValue
        e.inputCounter = inputCounter
        e.timestamp = timestamp
        e.startTimestamp = startTimestamp
        e.jsonVersion = jsonVersion

        console.log("Pushing to LRS:")
        console.log(e)
        postData(e)

        // postData(jsonDataExample)
    }
    pushToLocalStorage(timelineElement) {
        let timelineInputsStored = localStorage.getItem("timelineInputs")
        let newInputs = {
            "inputs": []
        }
        if(timelineInputsStored === null || timelineInputsStored === "" || timelineInputsStored === "null") {
            // console.log("TimelineInputs ist leer")
        } else {
            // console.log("TimelineInputs ist nicht leer.")
            const currentInputs = JSON.parse(timelineInputsStored).inputs
            // console.log(currentInputs)
            // console.log(currentInputs.length)
            for(let i=0, len=currentInputs.length; i<len; i++) {
                newInputs.inputs.push(currentInputs[i])
            }
        }
        
        timelineElement.jsonVersion = this.jsonVersion
        timelineElement.startTimestamp = this.startTimestamp
        timelineElement.userId = this.userId
        timelineElement.fragebogenTitle = this.fragebogenTitle
        newInputs.inputs.push(timelineElement)
        console.log(newInputs)
        
        localStorage.setItem("timelineInputs", JSON.stringify(newInputs))
    }
    createJsonFromLocalStorage() {
        this.userId = localStorage.getItem("username")
        this.startTimestamp = localStorage.getItem("startTimestamp")
        this.fragebogenTitle = getFragebogenTitle()

        let json = JSON.parse(localStorage.getItem("timelineInputs"))
        jsonDownload(json, "Timeline_LocalStorage_" + this.userId + "_" + this.fragebogenTitle)
    }
    printJson() {
        console.log(this)
    }
    createJson() {
        this.userId = localStorage.getItem("username")
        this.startTimestamp = localStorage.getItem("startTimestamp")
        this.fragebogenTitle = getFragebogenTitle()

        const jsonString = JSON.stringify(this)
        console.log(jsonString)
        
        jsonDownload(this, "Timeline" + "_" + this.userId)
    }
}
class Timeline_Element {
    constructor(taskId, taskTitle, taskElementId, taskTypeIndex, typeName, inputFieldIndex, timestamp, taskInputValue) {
        this.taskId = String(taskId)
        this.taskTitle = String(taskTitle)
        this.taskElementId = String(taskElementId)
        this.taskTypeIndex = String(taskTypeIndex)
        // this.typeName = typeName
        this.inputFieldIndex = String(inputFieldIndex)
        this.inputCounter = this.getInputCounter()
        this.timestamp = String(timestamp)
        this.taskInputValue = String(taskInputValue)
    }
    getInputCounter() {
        globalInputCounter += 1
        return globalInputCounter
    }
}
class ArucoNumberingGroup {
    // This class is used in the ArucoNumberingSheme-Class for every group that needs a numbering system
    constructor(startingNumber, slots, scheme) {
        this.startingNumber = startingNumber
        this.slots = slots // size of the number range the element is allowed to have, eg. explainationSection has 1
        this.counter = 0
        this.scheme = scheme // saves scheme-obj into variable for use of checkIfNumberIsValid()-function
    }
    getNumber() {
        // supplies a number for the aruco marker
        if(this.slots == 0) {
            console.log("No slots left for this type of numbering.")
            return 999
        }
        
        const markerId = this.startingNumber + this.counter
        this.counter += 1
        this.slots -= 1

        if(this.scheme.checkIfNumberIsValid(markerId)) {
            return markerId
        } else {
            throw "Err: The following number is invalid:" + String(markerId)
        }
    }
}
class ArucoNumberingScheme {
    constructor() {
        // define all ArucoNumberingGroups there are
        this.explainationSection = new ArucoNumberingGroup(0, 1, this)
        this.interactionSection = new ArucoNumberingGroup(1, 1, this)
        this.figure = new ArucoNumberingGroup(10, 10, this)
        this.textfield = new ArucoNumberingGroup(20, 80, this)

        this.numberArray = [] // saves all numbers which were used to check if the current number is valid
    }
    checkIfNumberIsValid(number) {
        if(number > 999) {
            console.log("Err: Invalid Number is bigger than 999.")
            return false
        } else if (typeof this.numberArray[number] !== 'undefined') {
            console.log("Err: Number has already been taken.")
            return false
        } else {
            return true
        }

    }
}
class Matrix {
    constructor() {
        this.radiosMatrix = null
        this.timelineInputs = []
    }
}
class Radios {
    constructor() {
        this.radios = []
        this.timelineInputs = []
    }
}
class Radio {
    constructor(element, radioValue, elementName) {
        this.element = element
        this.pValue = ""
        this.radioValue = radioValue
        this.elementName = elementName
        this.inputs = []
    }
}
class RadioInput {
    constructor(element, number, timestamp) {
        this.element = element
        this.number = number
        this.pValue = ""
        this.timestamp = timestamp
    }
}
class Textarea {
    constructor(element) {
        this.element = element
        this.inputs = []
    }
}
class Textfields {
    constructor() {
        this.textfields = []
        this.timelineInputs = []
    }
}
class Hottexts {
    constructor() {
        this.hottexts = []
        this.timelineInputs = []
    }
}
class Hottext {
    constructor(element, number) {
        this.element = element
        this.number = number
        this.inputs = []
        inlineMarkingElement(element, number)
    }
}
class Textfield {
    constructor(element, number) {
        this.element = element
        this.number = number
        this.inputs = []
        // returnArucoMarkerElement(this.element, number)
        inlineMarkingElement(element, number)
    }
}
class TextfieldInput {
    constructor(currentString, timestamp) {
        this.currentString = currentString
        this.timestamp = timestamp
    }
}
class Tasks {
    constructor(taskList) {
        this.taskList = taskList
    }
    createJsonForAllTasks() {
        console.log("CreateJsonForAllTasks " + this.taskList.length)
        for(let i=0, len=this.taskList.length; i<len; i++) {
            const task = this.taskList[i]
            task.createJson()
        }
    }
    create_qr_codes_ForAllTasks() {
        for(let i=0, len=this.taskList.length; i<len; i++) {
            console.log("CreateArucosForAllTasks")
            const task = this.taskList[i]
            task.create_qr_codes()
        }
    }
}

class Task {
    constructor(elementItemContainer, number) {
        this.id = null
        this.number = number
        this.title = ""
        this.typeIndex = ""
        this.typeName = ""
        this.typeId = ""
        this.elementItemContainer = elementItemContainer
        this.elementItemContent = elementItemContainer.getElementsByClassName("item-content")[0]
        this.elementInteractionContainers = this.elementItemContainer.getElementsByClassName("interaction-container")
        this.taskElementList = []

        // Tagging
        this.arucoNumberingScheme = new ArucoNumberingScheme()
        this.e_explainationSection = null
        this.e_interactionSection = null

        // JSON
        this.interactionElement = null
        this.interaction = []
        this.radioInteractions = []
        this.jsonInteractionFormat = null
    }
    create_qr_codes() {
        // Structure
        const itemContainer = this.elementItemContainer
        const divMainTask = itemContainer.parentElement //Dies ist der Hauptcontainer einer Aufgabe mit eigener ID, die wir aber nicht nutzen
        const divAufgabenblock = document.createElement("div")
        divAufgabenblock.className = "aufgabenblock"
        divAufgabenblock.style.display = "flex"
        divMainTask.appendChild(divAufgabenblock)

        const divLinks = document.createElement("div")
        divLinks.className = "divLinks"
        divLinks.style.width = "var(--qr-width)"
        // divLinks.style.width = "200px"
        divAufgabenblock.appendChild(divLinks)

        const divQRCodeLinks = document.createElement("div")
        divQRCodeLinks.id = "qrCodeLinks".concat(String(this.number))
        divLinks.appendChild(divQRCodeLinks)

        const divMitte = document.createElement("div")
        divMitte.className = "mitte"
        divMitte.style.width = "100%"
        // divMitte.style.overflowX = "auto" // in QR-Codes
        divAufgabenblock.appendChild(divMitte)
        divMitte.appendChild(itemContainer)

        const divRechts = document.createElement("div")
        divRechts.className = "divRechts"
        divRechts.style.width = "var(--qr-width)"
        // divRechts.style.width = "200px"
        divRechts.style.display = "flex"
        divAufgabenblock.appendChild(divRechts)

        const divQRCodeRechts = document.createElement("div")
        divQRCodeRechts.id = "qrCodeRechts".concat(String(this.number))
        divRechts.appendChild(divQRCodeRechts)

        // QR-Codes Creation
        const idTop = this.id + " top"
        const idBot = this.id + " bot"
        var qrcodeTop = document.createElement("img"),
        sTop = QRCode.generatePNG(idTop, {
            //options here
            ecclevel: "Q",
            format: "html",
            margin: 0,
            modulesize: 8
        });
        var qrcodeBot = document.createElement("img"),
        sBot = QRCode.generatePNG(idBot, {
            //options here
            ecclevel: "Q",
            format: "html",
            margin: 0,
            modulesize: 8
        });
        qrcodeTop.src = sTop
        qrcodeBot.src = sBot
        divQRCodeLinks.appendChild(qrcodeTop)
        divQRCodeRechts.appendChild(qrcodeBot)

        //CSS Styles
        divQRCodeLinks.style.position = "sticky"
        divQRCodeLinks.style.top = "10px"
        divQRCodeLinks.style.margin = "10px"
        divQRCodeRechts.style.position = "sticky"
        divQRCodeRechts.style.marginTop = "auto"
        divQRCodeRechts.style.paddingTop = "10px" // padding weil marginTop auf "auto" muss
        divQRCodeRechts.style.marginLeft = "10px"
        divQRCodeRechts.style.marginBottom = "10px"
        divQRCodeRechts.style.marginRight = "10px"
        divQRCodeRechts.style.bottom = "10px"
        const QRCodeLinksImg = divQRCodeLinks.getElementsByTagName("img")[0]
        QRCodeLinksImg.style.width = "160px"
        QRCodeLinksImg.style.height = "100%"
        const QRCodeRechtsImg = divQRCodeRechts.getElementsByTagName("img")[0]
        QRCodeRechtsImg.style.width = "160px"
        QRCodeRechtsImg.style.height = "100%"

    }
    pPrintTaskInfo() {
        let color = "yellow"
        if(this.typeName == "") {
            color = "red"
        } else {
            color = "green"
        }
        const pElement = createPElement("title = " + this.title + " | " + "taskTypeName = " + this.typeName, color)
        const element_item_content = this.elementItemContainer.getElementsByClassName("item-content")[0]
        appendAsFirstChild(element_item_content, pElement)
        
    }
    toString() {
        console.log("toString()")
        console.log("title=" + this.title)
        console.log("id=" + this.id)
        console.log("typeName=" + this.typeName)
        console.log("typeId=" + this.typeId)
    }
    extractEverything() {
        this.extractId()
        this.extractTitle()
        this.extractTaskType()
        this.extractInteractionElement()
        this.taskTypeCustomCssStyles()
    }
    extractId() {
        const a_array = this.elementItemContainer.getElementsByTagName("a")
        const id = a_array[0].getAttribute("name")
        if(id.substring(0,2) == "id") {
            this.id = id
        }
    }
    extractTitle() {
        const element_content_header = this.elementItemContainer.getElementsByClassName("content-header")
        const element_h2 = element_content_header[0].getElementsByTagName("h2")
        this.title = element_h2[0].textContent
    }
    extractTaskType() {
        const generalTaskTypes = [
            ["0", "Auswahlaufgabe", "interaction-choice", "0"],
            ["1", "Lückentextaufgabe | Nummerische Eingabe | Berechnung | Formelvergleich | Regulärer Ausdruck", "interaction-textentry", "1"],
            ["2", "Freitextaufgabe", "interaction-extendedtext", "0"],
            ["3", "Einfache Zuordnung Drag and Drop", "interaction-match", "0"],
            ["4", "Mehrfache Zuordnung (Matrix)", "interaction-matrix", "0"],
            ["5", "Grafische Zuordnung", "ia_graphic_gap_match outer", "0"],
            ["6", "Reihenfolge Aufgabe", "interaction-match order", "0"],
            ["7", "Hotspotaufgabe", "ia_hotspot", "0"],
            ["8", "Fehlertextaufgabe", "hottextInteraction", "1"],
            ["9", "Textboxaufgabe", "dropdown inline-input", "1"],
            ["10", "Uploadaufgabe", "uploadwrapper", "0"],
            ["11", "Programmieraufgabe", "ace_gutter", "0"],

        ]
        const ic = this.elementInteractionContainers[0]
        for (let i=0, len=generalTaskTypes.length; i<len; i++) { //iteriere durch die Generalliste
            const taskTypeIndex = parseInt(generalTaskTypes[i][0])
            const taskTypeName = generalTaskTypes[i][1]
            const taskTypeClassName = generalTaskTypes[i][2]
            const taskTypeId = generalTaskTypes[i][3]
            // console.log(taskTypeName, taskTypeClassName, taskTypeId)

            const match = ic.getElementsByClassName(taskTypeClassName)
            // console.log("Match mit", this.title, match.length, taskTypeName)
            if(match.length > 0) {
                // console.log("Aktuell untersuchte task " + this.title + " hat den Klassennamen " + taskTypeClassName)
                // console.log("== 1", "taskTypesName = " + taskTypesName + " taskTypesClassName = " + taskTypesClassName + " taskTypeId=" + taskTypeId + " hinzugefügt.")
                this.typeIndex = taskTypeIndex
                this.typeName = taskTypeName
                this.typeId = taskTypeId
            }
        }
    }
    // extractRadioAnswers() {
    //     console.log("Extracting radio answers ...")
    //     if(this.typeIndex == "0") {
    //         const ic = this.elementInteractionContainers[0]
    //         const element_p_array = ic.getElementsByTagName("p")
    //         const taskRadio = new TaskRadio()

    //         for(let i=0, len=element_p_array.length; i<len; i++) { //iteriere durch alle p-Elemente
    //             string = element_p_array[i].innerHTML
    //             taskRadio.answerOptions.push(string)
    //         }
    //     }
    // }
    taskTypeCustomCssStyles() {
        if(this.typeIndex == "3") { // Einfache Zuordnung Drag & Drop
            this.elementInteractionContainers[0].style.background = "green"
            const im = this.elementInteractionContainers[0].getElementsByClassName("interaction-match")[0]
            console.log(im)
            im.style.width = "auto"
            console.log(im)
        }
        if(this.typeIndex == "4") { // Mehrfach Zuordnung (Matrix)

        }
        if(this.typeIndex == "11") { // Freitext, Programmieraufgabe
            // Programmierfeld wird nicht angezeigt. deshalb wird die Breite auf 400px festgelegt. Ist nicht elegant, weil bei einer kleineren Browserfenstergröße könnte der rechte QR-Code verdeckt werden
            try {
                const programmField = this.elementInteractionContainers[0].getElementsByClassName("interaction-extendedtext")[0].children[0]
                programmField.style.width = "400px"
            } catch {
                console.log("Freitextaufgabe und keine Programmieraufgabe")
            }
        }
    }
    extractInteractionElement() {
        // In dieser Methode werden die Informationen der Aufgabenelemente extrahiert, sodass man beispielsweise die HTML-Elemente in Instanzen speichert. Anschließend folgt die addListener-Methode
        console.log("Extracting Interaction Elements...")
        if(this.typeIndex == "0") { //Auswahlaufgabe
            const radios = new Radios()
            const element_input_array = this.elementInteractionContainers[0].getElementsByTagName("input")
            for(let i=0, len=element_input_array.length; i<len; i++) {
                const input = element_input_array[i]
                const elementName = input.getAttribute("name")
                const radioValue = input.getAttribute("value")
                const radio = new Radio(input, radioValue, elementName)

                // pValue zu radio hinzufügen
                // const ic = this.elementInteractionContainers[0]
                // const element_p_array = ic.getElementsByTagName("p")
                // console.log("Kindelemente Anzahl = " + element_p_array[0].children.length)
                // if(element_p_array[0].children.length == 0) {
                //     const pValue = this.elementInteractionContainers[0].getElementsByTagName("p")[i].innerHTML
                //     radio.pValue = pValue
                //     console.log(pValue)
                // }
                
                // console.log("new Radio:", i, radioValue, elementName)
                radios.radios.push(radio) // strukturiert nach radio
            }
            this.interactionElement = radios
        }
        if(this.typeIndex == "1") { //Lückentext
            const textfields = new Textfields()
            for(let i=0, len=this.elementInteractionContainers.length; i<len; i++) {
                const element_input = this.elementInteractionContainers[i].getElementsByTagName("input")[0]
                const number = i
                const textfield = new Textfield(element_input, number)
                // console.log(textfield.number)
                textfields.textfields[i] = textfield
            }
            this.interactionElement = textfields
            console.log("Number erstes Textfield-Elementes", this.interactionElement.textfields[0].number)
        }
        if(this.typeIndex == "2") {
            const element_input = this.elementInteractionContainers[0].getElementsByTagName("textarea")
            const textarea = new Textarea(element_input)
            this.interactionElement = textarea
        }
        if(this.typeIndex == "8") {
            console.log("extract Fehlertextaufgabe")
            const hottexts = new Hottexts()
            const element_hottexts = this.elementInteractionContainers[0].getElementsByClassName("hottext")
            for(let i=0, len=element_hottexts.length; i<len; i++) {
                const element_input = element_hottexts[i]
                const number = i
                const hottext = new Hottext(element_input, number)
                hottexts.hottexts[i] = hottext // in's array speichern
            }
            this.interactionElement = hottexts // ich glaub das ist nur für die legacy timeline datenspeicherung wichtig
        }
        if(this.typeIndex == "4") {
            console.log("Matrix")
            const matrix = new Matrix()
            const element_matrixRow_array = this.elementInteractionContainers[0].getElementsByClassName("matrix-row")
            let radiosMatrix = new Array(element_matrixRow_array.length)
            for(let i=0, len=element_matrixRow_array.length; i<len; i++) {
                //console.log(i, "matrix-row")
                const element_matrixCell_array = element_matrixRow_array[i].getElementsByClassName("matrix-cell")
                radiosMatrix[i] = new Array(element_matrixCell_array.length) 
                for(let j=0, len=element_matrixCell_array.length; j<len; j++) {
                    //console.log(j, "matrix-cell")
                    const element_input_array = element_matrixCell_array[j].getElementsByTagName("input")
                    const element = element_input_array[0]
                    const radioValue = element.getAttribute("value")
                    const elementName = element.getAttribute("name")
                    const radio = new Radio(element, radioValue, elementName)

                    radiosMatrix[i][j] = radio
                }
            }
            matrix.radiosMatrix = radiosMatrix
            this.interactionElement = matrix
            console.log(radiosMatrix)
        }
        if(this.typeIndex == "11") { // Programmieraufgabe -funktioniert nicht-
            const element_input = this.elementInteractionContainers[0].getElementsByClassName("form-control")
            console.log(element_input)
            const textarea = new Textarea(element_input)
            this.interactionElement = textarea
        }
        console.log("End extracting interaction elements")
        
    }
    picture() {
        this.elementItemContainer.getElementsByTagName("img")
    }
    divideIntoExplainationAndInteractionSection() {
        
        if(this.typeId != 1) {
            let itemContent = this.elementItemContainer.getElementsByClassName("item-content")[0]
            
            // taskExplainationSection
            let explainationSection = document.createElement("div")
            explainationSection.className = "explainationSection"
            this.elementItemContainer.appendChild(explainationSection)
            explainationSection.append(...itemContent.childNodes)
            itemContent.append(explainationSection)
            this.e_explainationSection = explainationSection

            // interactionSection
            let interactionSection = document.createElement("div")
            interactionSection.className = "interactionSection"
            this.elementItemContainer.appendChild(interactionSection)
            let interactionContainer = this.elementInteractionContainers[0]
            interactionSection.appendChild(interactionContainer)
            itemContent.append(interactionSection)
            this.e_interactionSection = interactionSection

            // console.log("test" + String(explainationSection.childElementCount))
            if(explainationSection.childElementCount > 0) { //Falls eine explainationSection existiert
                adaptiveMarkingElement(this.number, explainationSection, this.arucoNumberingScheme.explainationSection.getNumber())
            }
            // Hier werden alle InteractionSection-Elemente markiert
            adaptiveMarkingElement(this.number, interactionSection, this.arucoNumberingScheme.interactionSection.getNumber())
        }
        if(this.typeIndex == "9") {
            let itemContent = this.elementInteractionContainers[0].getElementsByClassName("hottext")
            
        }
        
    }
    markFigures() {
        const img_array = this.elementItemContainer.getElementsByTagName("img")
        for(let i=0, len=img_array.length; i<len; i++) {
            const img = img_array[i]
            const taskNumber = this.number
            adaptiveMarkingElement(taskNumber, img, this.arucoNumberingScheme.figure.getNumber())
        }
    }
    addListeners() {
        // Die addListener-Methode 
        console.log("adding listeners...")
        const tl_taskId = this.id
        const tl_taskTitle = this.title
        const tl_typeName = this.typeName
        const tl_typeIndex = this.typeIndex


        if(this.typeIndex == "0") { // Auswahlaufgabe
            console.log("Auswahlaufgabe")
            const eventListenerInteractionElement = this.interactionElement
            const inputNameTag = eventListenerInteractionElement.radios[0].elementName
            
            $("input[name=\"" + inputNameTag + "\"]").change(function() {
                const radios = eventListenerInteractionElement
                const checkNumber = parseInt(this.value.substring(5, this.value.length))
                const checkNumberFirstRadioValue = parseInt(radios.radios[0].radioValue.substring(5, this.value.length))
                const number = checkNumber - checkNumberFirstRadioValue
                const timestamp = Date.now()
                // pValue zu radioInput hinzufügen
                /*
                const element_p_array = ic.getElementsByTagName("p")
                console.log(element_p_array.length)
                if(element_p_array.length != 0) {
                    // const pValue = document.querySelectorAll("label[for="+ this.id +"]")[0].getElementsByTagName("p")[0].innerHTML
                    const pValue = "none"
                    radioInput.pValue = pValue
                }
                */
                const radioInput = new RadioInput(this, number, Date.now())
                console.log(this)
                
                // Add current Input-Values to Attribute - without JSON-Formatting
                // Ordered by radio
                const radio = radios.radios[number]
                console.log("check:", radio.radioValue)
                radio.inputs.push(radioInput)
                
                // Ordered by time for timelineInputs
                radios.timelineInputs.push(radioInput)

                // Ordered by global timeline
                const tl_number = number
                const tl_timestamp = timestamp
                const tl_inputValue = radio.radioValue //eher unwichtig
                const tl_elementId = crawlElementId(this)
                const timelineElement = new Timeline_Element(tl_taskId, tl_taskTitle, tl_elementId, tl_typeIndex, tl_typeName, tl_number, tl_timestamp, tl_inputValue)
                globalTimeline.push(timelineElement)
            })
        }
        
        if(this.typeIndex == "1") { // Lückentext
            console.log("Lückentextaufgabe")
            const element_input_array = this.elementItemContainer.getElementsByTagName("input")
            console.log("Die Aufgabe besteht aus " + element_input_array.length + " Eingabefeldern.")
            
            //var textfieldList = []
            //this.interaction = textfieldList
            for(let i=0, len = element_input_array.length; i<len; i++) {
                const input = element_input_array[i]
                // console.log(input.getAttribute("type"))
                const eventListenerInteractionElement = this.interactionElement

                input.addEventListener("input", function() {
                    const inputNumber = i
                    const currentValue = input.value
                    const timestamp = Date.now()
                    
                    // Add current Input-Values to Attribute - without JSON-Formatting
                    // Ordered by textfield
                    const textfields = eventListenerInteractionElement
                    const textfield = textfields.textfields[i]
                    const textfieldInput = new TextfieldInput(currentValue, timestamp)
                    textfield.inputs.push(textfieldInput)

                    // Ordered by time for timelineInputs
                    const timelineInputs = textfields.timelineInputs
                    console.log("Save to timelineInputs-Attribute: " + inputNumber, currentValue, timestamp)
                    timelineInputs.push([inputNumber, currentValue, timestamp])

                    // Ordered by global timeline
                    const tl_number = inputNumber
                    const tl_timestamp = timestamp
                    const tl_inputValue = currentValue
                    const tl_elementId = crawlElementId(this)
                    const timelineElement = new Timeline_Element(
                        tl_taskId, tl_taskTitle, tl_elementId,
                        tl_typeIndex, tl_typeName, tl_number, tl_timestamp,
                        tl_inputValue) 
                    globalTimeline.push(timelineElement)
                }, true)
            }
        }
        if(this.typeIndex == "2") { // Freitextaufgabe
            console.log("Freitextaufgabe")
            const element_input = this.elementItemContainer.getElementsByTagName("textarea")[0]
            const input = element_input
            // input.addEventListener("")
            input.addEventListener("input", function() {
                const currentValue = input.value
                const timestamp = Date.now()

                // Ordered by global timeline
                const tl_number = 0
                const tl_timestamp = timestamp
                const tl_inputValue = currentValue
                const tl_elementId = crawlElementId(this)
                const timelineElement = new Timeline_Element(
                    tl_taskId, tl_taskTitle, tl_elementId,
                    tl_typeIndex, tl_typeName, tl_number, tl_timestamp,
                    tl_inputValue)
                globalTimeline.push(timelineElement)
            }, true)
        }

        if(this.typeIndex == "4") { // Matrix
            console.log("Matrix")
            const element_input_array = this.elementItemContainer.getElementsByClassName("matrix-cell")
            console.log("Die Aufgabe" + this.title + " besteht aus " + element_input_array.length + " Matrix-Zellen.")

            const eventListenerInteractionElement = this.interactionElement
            const radiosMatrix = eventListenerInteractionElement.radiosMatrix
            const timelineInputs = eventListenerInteractionElement.timelineInputs
            for(let i=0, len=radiosMatrix.length; i<len; i++) {
                for(let j=0, len=radiosMatrix[i].length; j<len; j++) {
                    const id = radiosMatrix[i][j].element.getAttribute("id")
                    const element_radio = radiosMatrix[i][j].element
                    $("input[id=\"" + id + "\"]").change(function() {
                        let self = this
                        console.log("value = " + self.value)
                        const checkNumber = parseInt(self.value.substring(5, self.value.length))
                        const checkNumberFirstRadioValue = parseInt(radiosMatrix[0][0].radioValue.substring(5, self.value.length))
                        const number = checkNumber - checkNumberFirstRadioValue
                        
                        const rowNumber = number % radiosMatrix[i].length
                        const colNumber = Math.floor(number / radiosMatrix[i].length)
                        const radioInput = new RadioInput(this, [rowNumber, colNumber], Date.now())
                        console.log(number, rowNumber, colNumber)
                        timelineInputs.push(radioInput)

                        // Ordered by global timeline
                        const tl_elementId = crawlElementId(this)
                        // const tl_number = [rowNumber, colNumber]
                        const tl_number = String(rowNumber) + " " + String(colNumber)
                        const tl_timestamp = Date.now()
                        const tl_inputValue = self.value
                        const timelineElement = new Timeline_Element(tl_taskId, tl_taskTitle, tl_elementId, tl_typeIndex, tl_typeName, tl_number, tl_timestamp, tl_inputValue) 
                        globalTimeline.push(timelineElement)

                    })
                }
            }
        }
        if(this.typeIndex == "8") { // Fehlertext
            console.log("Fehlertext")
            const inputs = this.elementInteractionContainers[0].getElementsByTagName("input")
            console.log(inputs.length)
            const hottexts = this.interactionElement.hottexts
            console.log(hottexts.length)
            for(let i=0, len=hottexts.length; i<len; i++) {
                const input = hottexts[i].element
                input.addEventListener("click", function() {
                    console.log("Fehlertext-Input geklickt")
                    const inputNumber = i
                    const currentValue = inputs[i].value
                    const timestamp = Date.now()

                    // Ordered by global timeline
                    const tl_number = inputNumber
                    const tl_timestamp = timestamp
                    const tl_inputValue = currentValue
                    const tl_elementId = crawlElementId(this)
                    const timelineElement = new Timeline_Element(
                        tl_taskId, tl_taskTitle, tl_elementId,
                        tl_typeIndex, tl_typeName, tl_number, tl_timestamp,
                        tl_inputValue)
                    globalTimeline.push(timelineElement)
                })
            }
            
        }
        if(this.typeIndex == "11") { // Programmieraufgabe
            console.log("Programmieraufgabe")
            const element_input = this.elementItemContainer.getElementsByClassName("form-control")[0]
            console.log("asdf " + element_input)
            element_input.addEventListener("change", function() {
                const currentValue = input.value
                const timestamp = Date.now()

                // Ordered by global timeline
                const tl_number = 0
                const tl_timestamp = timestamp
                const tl_inputValue = currentValue
                const tl_elementId = crawlElementId(this)
                const timelineElement = new Timeline_Element(
                    tl_taskId, tl_taskTitle, tl_elementId,
                    tl_typeIndex, tl_typeName, tl_number, tl_timestamp,
                    tl_inputValue)
                globalTimeline.push(timelineElement)
            }, true)
        }

    }
    createJson() {
        console.log("Create single json for " + this.title)

        let jsonObj = {
            "id": this.id,
            "title": this.title,
            "typeIndex": this.typeIndex,
            "typeName": this.typeName,
            "interaction": {}
        }

        let radioJsonObj = {
            "id": this.id,
            "title": this.title,
            "typeIndex": this.typeIndex,
            "typeName": this.typeName,
            "interaction": {
                "radios": {
                    "timelineInputs": {}
                }
            }
        }

        let lueckentextJsonObj = {
            "id": this.id,
            "title": this.title,
            "typeIndex": this.typeIndex,
            "typeName": this.typeName,
            "interaction": {
                "textfields": {
                    "timeline": {

                    },
                    "textfield0": {

                    }
                }
            }
        }

        // Pack Interaction Tree

        if(this.typeIndex == "0") { // Auswahlaufgabe
            const radios = this.interactionElement
            console.log("Radios-Anzahl:" + radios.radios.length)
            console.log("Radio-Timeline zeigen:" + radios.timelineInputs.length)
            jsonObj.interaction["timeline"] = radios.timelineInputs // Timeline-Daten in json einfügen

            jsonObj.interaction["radios"] = {}
            for(let i=0, len=radios.radios.length; i<len; i++) { //Daten pro Textfield in json einfügen
                const radio = radios.radios[i]
                // console.log(radio.inputs)
                const radioName = "radio_" + i

                jsonObj.interaction.radios[radioName] = {
                    "number": i,
                    "inputs": {}
                }
                jsonObj.interaction.radios[radioName]["inputs"] = radio.inputs
            }

        }

        if(this.typeIndex == "1") { //Lückentext
            jsonObj.interaction["textfields"] = {}
            jsonObj.interaction["timeline"] = {}
            const textfields = this.interactionElement.textfields
            
            const timelineInputs = this.interactionElement.timelineInputs
            jsonObj.interaction.textfields.timeline = timelineInputs // Timeline-Daten in json einfügen
            
            for(let i=0, len=textfields.length; i<len; i++) { //Daten pro Textfield in json einfügen
                // console.log(i)
                const textfield = textfields[i]
                const textfieldName = "textfield_" + i
                jsonObj.interaction.textfields[textfieldName] = {
                    "number": textfield.number,
                    "inputs": {}
                }
                jsonObj.interaction.textfields[textfieldName]["inputs"] = textfield.inputs
            }
        }

        if(this.typeIndex == "4") { // Matrix
            jsonObj.interaction["timeline"] = {}
            const timelineInputs = this.interactionElement.timelineInputs // Timeline-Daten in json einfügen
            jsonObj.interaction.timeline = timelineInputs
        }

        // Print
        console.log("Print Json for " + this.title)
        console.log(jsonObj)
        
        // Download
        const exportObj = jsonObj
        const exportName = this.title
        var downloadAnchorNodeFinish = document.getElementById("finish")
        var forward = document.getElementById("forward")
        jsonDownloadEvent(exportObj, exportName, forward)
        jsonDownloadEvent(exportObj, exportName, downloadAnchorNodeFinish)
    }
}
/****************************
 * Variablen
 ****************************/

const globalStartTimestamp = Date.now()
let globalTimeline = null
let globalInputCounter = 0
 
/****************************
 * Executed Code
 ****************************/

const checkMarker = function() {

    var body = document.body

    if(body != null) { // Wenn Body des iFrames aufgebaut ist
        console.log("iframe geladen")

        let usernameStored = localStorage.getItem("username")
        if(usernameStored === null || usernameStored === "" || usernameStored === "null") {
            let username = window.prompt("Bitte trage deine User-ID in das Feld. Die User-ID bleibt so lange gespeichert, wie der Tab offen bleibt.")
            localStorage.setItem("username", username)
        }
        let startTimestampStored = localStorage.getItem("startTimestamp")
        if(startTimestampStored === null || startTimestampStored === "" || startTimestampStored === "null") {
            localStorage.setItem("startTimestamp", globalStartTimestamp)
        }

        globalTimeline = new Timeline()
        
        hideElements()
        antiOnyx()
        createDebuggingHeader()

        // Create Tasks
        const element_item_container = document.getElementsByClassName("item-container")
        const taskList = []
        for (let i=0, len=element_item_container.length; i<len; i++) {
            const task = new Task(element_item_container[i], i)
            task.extractEverything()
            task.divideIntoExplainationAndInteractionSection()
            task.markFigures()
            console.log("---task " + i + ": " + task.title)
            task.addListeners()
            task.pPrintTaskInfo()
            taskList.push(task)
        }
        const tasks = new Tasks(taskList)

        tasks.create_qr_codes_ForAllTasks()
        console.log("showMarker = " + localStorage.getItem("showMarker"))
        if(localStorage.getItem("showMarker") == "hide") {
            displayMarkers("hide")
        }

        // createDebuggings(tasks)
        console.log("Code finished.")
        infiniteLoop()
    }
    else {
        console.log("iframe ist noch nicht geladen.")
        setTimeout(checkMarker, 100) // check again in a second // this is a recursive call of the method "check"
    }
}
window.onload = function() {
    setTimeout(function() {
        checkMarker()
    }, 2000)
}
window.onunload = function() {
    console.log("onunload is triggered")
    // resetLocalStorage()
}

window.onbeforeunload = function(){
    console.log("onbeforeunload is triggered")
    // resetLocalStorage()
 }
 