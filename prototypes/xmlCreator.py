import xml.etree.cElementTree as ET

session = ET.Element("session")
person = ET.SubElement(session, "person")
mail = ET.SubElement(person, "mail").text = "surname.lastname@stud.htwk-leipzig.de"
records = ET.SubElement(session, "records")
tasks = ET.SubElement(records, "tasks")
task = ET.SubElement(tasks, "task")
id = ET.SubElement(task, "id").text = "ideae724db-7c63-4b49-bcd9-c6dd9d812af0"
framecounters = ET.SubElement(task, "framecounters")
visable = ET.SubElement(framecounters, "visable").text = "800"
focused = ET.SubElement(framecounters, "focused").text = "500"
unfocused = ET.SubElement(framecounters, "unfocused").text = "400"

tree = ET.ElementTree(session)
tree.write("session.xml")

"""
<?xml version="1.0"?>
<session>
    <person>
        <mail>surname.lastname@stud.htwk-leipzig.de</mail>
    </person>
    <records>
        <tasks>
            <task>
                <id>ideae724db-7c63-4b49-bcd9-c6dd9d812af0</id>
                <framecounters>
                    <visable>800</visable>
                    <focused>381</focused>
                    <unfocused>381</unfocused>
                </framecounters>
            </task>
            <task>
                <id>ideae724db-7c63-4b49-bcd9-c6dd9d812af0</id>
                <framecounters>
                    <visable>999</visable>
                    <focused>444</focused>
                    <unfocused>333</unfocused>
                </framecounters>
            </task>
        </tasks>
    </records>
</session>
"""