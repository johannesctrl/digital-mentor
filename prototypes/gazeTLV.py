import csv
path = "D:/Git-Clones/digitalMentor/ressources/pilotstudie/002/exports/000/iMotions_21_05_2021_10_14_59/gaze.tlv"

def importGazePositionTLV(path):
    x = []
    y = []
    world_index = -1
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter="\t")
        csv.list_dialects()
        header = next(csv_reader)
        for line in csv_reader:
            mediaFrameIndex = int(line[2])
            if (world_index == mediaFrameIndex):
                try:
                    next(csv_reader)
                except:
                    print("End of CSV reached")
                    break
            else:
                world_index += 1
                norm_pos_x = float(line[6])
                norm_pos_y = float(line[7])
                print(norm_pos_x, norm_pos_y)
                x.append(norm_pos_x)
                y.append(norm_pos_y)
    return x, y


x, y = importGazePositionTLV(path)
print(x)
print(y)