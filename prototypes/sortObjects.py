

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"


p1 = Point(0,0)
p2 = Point(-0.1, 1)
p3 = Point(1,1)
p4 = Point(1.1, 0.1)
pointList = [p1, p2, p3, p4]

def sortPoints(pointList):
    # Punkte nach x-Achse sortieren von links nach rechts
    pointListX = sorted(pointList, key=lambda point: point.x)
    print(pointList)
    print(pointListX)

    topLeft, botLeft, topRight, botRight = 0, 0, 0, 0

    # Punkte gruppieren: beide linken und beide rechten Punkte
    if pointListX[0].y < pointListX[1].y: # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster stelle kommen
        topLeft = pointListX[0]
        botLeft = pointListX[1]
    else:
        botLeft = pointListX[0] # bot left
        topLeft = pointListX[1] # top left

    if pointListX[2].y < pointListX[3].y:
        topRight = pointListX[2]
        botRight = pointListX[3]
    else:
        botRight = pointListX[2]
        topRight = pointListX[3]

    rectSorted = [topLeft, topRight, botLeft, botRight]
    return rectSorted

sortPoints(pointList)
