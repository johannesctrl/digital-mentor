import cv2

cap = cv2.VideoCapture(0)

# tracker = cv2.TrackerMOSSE_create()
tracker = cv2.TrackerCSRT_create()
ret, frame = cap.read()
bbox = cv2.selectROI("Tracking", frame, False) # innerhalb des Windows "Tracking" eine Rechteck-Auswahl machen
tracker.init(frame, bbox)

def drawBox(frame, bbox):
    x, y, width, height = int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])
    cv2.rectangle(frame, (x, y), (x + width, y + height), (0, 255, 0), 3, 1)
    cv2.putText(frame, "Tracking", (75, 75), cv2.FONT_HERSHEY_COMPLEX, 0.7, (0, 255, 0), 2)

while True:
    timer = cv2.getTickCount()
    ret, frame = cap.read()

    ret, bbox = tracker.update(frame) # bbox einfügen

    if ret:
        drawBox(frame, bbox)

    else:
        cv2.putText(frame, "Lost", (75, 75), cv2.FONT_HERSHEY_COMPLEX, 0.7, (0, 0, 255), 2)

    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
    cv2.putText(frame, str(int(fps)) + " fps", (75, 50), cv2.FONT_HERSHEY_COMPLEX, 0.7, (0,0,0), 2)
    cv2.imshow("Tracking", frame)

    if cv2.waitKey(1) & 0xff == ord("q"):
        break

