import tkinter as tk
from tkinter.messagebox import showinfo

# --- classes ---

class PopupWindow():

    def __init__(self, master):
        self.master = master
        window = tk.Toplevel(master)

        label = tk.Label(window, text="Hello World!")
        label.pack(fill='x', padx=50, pady=5)

        button_close = tk.Button(window, text="Close", command=window.destroy)
        button_close.pack(fill='x')


class App():

    def __init__(self):
        self.root = tk.Tk()

        button_bonus = tk.Button(self.root, text="Window", command=self.popup_window)
        button_bonus.pack(fill='x')

        button_showinfo = tk.Button(self.root, text="ShowInfo", command=self.popup_showinfo)
        button_showinfo.pack(fill='x')

        button_close = tk.Button(self.root, text="Close", command=self.root.destroy)
        button_close.pack(fill='x')

    def run(self):
        self.root.mainloop()

    def popup_window(self):
        PopupWindow(self.root)

    def popup_showinfo(self):
        showinfo("ShowInfo", "Hello World!")

# --- main ---

app = App()
app.run()