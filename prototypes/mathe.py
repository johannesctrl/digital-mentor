import sympy

p1 = (0, 0)
p2 = (2, 1)

def line(p1, p2, x):
    x1 = p1[0] # p1
    y1 = p1[1]
    x2 = p2[0] # p2
    y2 = p2[1]
    delta_x = x2-x1
    delta_y = y2-y1
    m = delta_y/delta_x
    def f(x):
        return m * (x - x1) + y1
    return f(x)

x = line(p1, p2, 3)
print(x)


###############################
p1 = sympy.Point(0, 0)
p2 = sympy.Point(2, 1)
p3 = sympy.Point(4, 4)
l1 = sympy.Line(p1, p2)
l2 = sympy.Line(p1, p3)

isParallel = sympy.Line.is_parallel(l1, l2)
print(l1.intersection(l2))
