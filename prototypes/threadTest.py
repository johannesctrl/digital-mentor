import datetime
import threading
from queue import Queue
from threading import Thread
import asyncio
import aiohttp
import time
import json
import requests


url_eyeTracking = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postEyeTracking"
_sentinel = object()


# A thread that produces data
def producer(out_q):
    frame_counter = 0
    while frame_counter < 10:
        frame_counter += 1
        json_data = {
            "userId": "999999",
            "studyName": "sync " + str(frame_counter),
            "startTimestamp": "1631614338679",
            "timestamp": "1631614338879",
            "taskId": "idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
            "taskElementId": "None"
        }
        # json_data = json.dumps(json_data)
        startTime = time.time()

        out_q.put((json_data, url_eyeTracking))

        duration = time.time() - startTime
        print("loop " + str(frame_counter) + " : POST took " + str(duration) + " seconds.")
    out_q.put(_sentinel)


# A thread that consumes data
def consumer(in_q):
    requestList = []
    while True:
        # Get some data
        data = in_q.get()
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        else:
            url = data[0]
            json_data = data[1]
            print("url = ", url, "json_data = ", json_data)
            print(json_data["studyName"] + " :" + str(threading.get_ident()))

        # requestList.append(data)
        # if (datetime.datetime.now() - data["timestamp"]) > datetime.timedelta(0, 30):
        sendToL2P(str(url), json.dumps(json_data))
        # requestList = []

        in_q.task_done()
    print("END Consumer")


def sendToL2P(url, data):
    print(data)
    r = requests.post(url=url, data=data)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")


# Create the shared queue and launch both threads
q = Queue()
t1 = Thread(target=consumer, args=(q,))
t2 = Thread(target=producer, args=(q,))
# t3 = Thread(target=consumer, args=(q,))
t1.start()
t2.start()
# t3.start()

# Wait for all produced items to be consumed
print("join")
t1.join()
t2.join()
print("End")

