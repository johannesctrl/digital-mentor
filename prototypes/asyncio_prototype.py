import time
import requests
import json

#####################################################################################
# Sequential Loop
#####################################################################################
url_eyeTracking = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postEyeTracking"


def infiniteLoop():
    frame_counter = 0
    while True:
        frame_counter += 1
        json_data = {
            "userId": "999999",
            "studyName": "sync " + str(frame_counter),
            "startTimestamp": "1631614338679",
            "timestamp": str(frame_counter),
            "taskId": "idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
            "taskElementId": "None"
        }
        json_data = json.dumps(json_data)
        startTime = time.time()

        sendToL2P(url_eyeTracking, json_data)

        time.sleep(3)
        duration = time.time() - startTime - 3.0
        print("loop " + str(frame_counter) + " : POST took " + str(duration) + " seconds.")


def sendToL2P(url, data):
    print(data)
    r = requests.post(url=url, data=data)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")


infiniteLoop()


