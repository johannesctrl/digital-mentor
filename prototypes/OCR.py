import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

img = cv2.imread("../ressources/2.PNG")
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# print(pytesseract.image_to_string(img))

### Detecting Characters
"""
print(pytesseract.image_to_boxes(img))
hImg, wImg, _ = img.shape
boxes = pytesseract.image_to_boxes(img)
for b in boxes.splitlines():
    # print(b)
    b = b.split(" ") 
    print(b)
    x, y, w, h = int(b[1]), int(b[2]), int(b[3]), int(b[4])
    cv2.rectangle(img,(x,hImg-y), (w, hImg-h), (0,0,255), 1)
    cv2.putText(img, b[0], (x, hImg-y + 25 ), cv2.FONT_HERSHEY_COMPLEX, 0.5, (50, 50, 255),1)
"""

### Detecting Words
hImg = img.shape[0]
wImg = img.shape[1]
print(img.shape)
boxes = pytesseract.image_to_data(img)
print(boxes)
for x,b in enumerate(boxes.splitlines()):
    b = b.split()  # unterteilt zeilenweise den boxes.splitLines() Output
    leereZeile = False
    headerZeile = False

    if len(b) == 11:
        leereZeile = True
        print("Leere Zeile.")

    if x == 0:
        headerZeile = True
        print("Header-Zeile.")

    if not leereZeile and not headerZeile:
        x, y, w, h = int(b[6]), int(b[7]), int(b[8]), int(b[9])
        text = b[11]
        cv2.rectangle(img, (x, y), (w + x, h + y), (0, 0, 255), 3)
        cv2.putText(img, text, (x,y), cv2.FONT_HERSHEY_COMPLEX, 1, (50,50,255), 2)
        print(b)

cv2.imshow("Result", img)
cv2.waitKey(0)