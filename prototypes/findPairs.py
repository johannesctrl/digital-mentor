from collections import defaultdict

class X:
    def __init__(self, param):
        self.param = param

my_list = [X(1), X(2), X(3), X(2), X(3), X(3), X(1), X(1), X(1), X(1)]

d = defaultdict(list)

for i in my_list:
    d[i.param].append(i)

for i in d:
    print("Index", i, "hat", len(d[i]), "Elemente.")
