import cv2
import cv2.aruco as aruco
import ttClasses
import numpy as np

class TaskElement:
    def __init__(self, id, area, leftMarkerCoordinates, rightMarkerCoordinates):
        self.id = id
        self.area = area
        self.leftMarkerCoordinates = leftMarkerCoordinates
        self.rightMarkerCoordinates = rightMarkerCoordinates

class TaskElements:
    def __init__(self):
        self.list = []

capture = cv2.VideoCapture(0)
capture.set(3, 1920)
capture.set(4, 1080)

# ArUCo-Setup
arucoDict = aruco.Dictionary_get(aruco.DICT_4X4_1000)
arucoParams = aruco.DetectorParameters_create()

while capture.isOpened:
    success, frame = capture.read()
    dframe = frame.copy()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frameRate = int(capture.get(cv2.CAP_PROP_FPS))
    cv2.putText(dframe, "frameRate = " + str(frameRate), (int(30), int(30)), 1, 1.4, (0, 0, 255), 1)

    # Decoding
    corners, ids, rejectedMarkers = aruco.detectMarkers(frame, arucoDict, parameters=arucoParams)
    cv2.putText(dframe, "MarkerCount = " + str(len(corners)), (int(30), int(60)), 1, 1.4, (0, 0, 255), 1)

    taskElements = TaskElements()
    # Corners
    if len(corners) > 0:
        print("Corner:")
        counter = 0
        for i, corner in enumerate(corners):
            counter += 1
            print(counter)
            print(corners[0][0])
            topRight = ttClasses.Point(int(corners[i][0][0][0]), int(corners[i][0][0][1]))
            botRight = ttClasses.Point(int(corners[i][0][1][0]), int(corners[i][0][1][1]))
            botLeft = ttClasses.Point(int(corners[i][0][2][0]), int(corners[i][0][2][1]))
            topLeft = ttClasses.Point(int(corners[i][0][3][0]), int(corners[i][0][3][1]))
            rectangle = [topLeft, topRight, botLeft, botRight]

            # Draw
            pts = np.array([[topLeft.x, topLeft.y], [topRight.x, topRight.y], [botRight.x, botRight.y], [botLeft.x, botLeft.y]], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], isClosed=True, color=(0, 0, 255), thickness=5)  # Rechteck malen

            id = int(ids[i])
            print(id)
            cv2.putText(dframe, str(id), (botLeft.x + 15, botLeft.y + 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, (0, 255, 255), 2)

    cv2.imshow("frame", ttClasses.scaleImage(img=dframe, scale_percent=60))

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        cv2.destroyWindow("frame")
        break
    cv2.waitKey(1)