
import numpy as np
import cv2
from pyzbar.pyzbar import decode


img = cv2.imread("../ressources/barcode3.PNG")

cap = cv2.VideoCapture(1)
cap.set(3, 1920) # ID bei set für die breite ist 3
cap.set(4, 1080)

while True:
    success, frame = cap.read()

    for barcode in decode(frame):
        decodedText = barcode.data.decode("UTF-8")
        print(decodedText)
        pts = np.array([barcode.polygon], np.int32)
        pts = pts.reshape((-1, 1, 2))
        cv2.polylines(frame, [pts], True, (255, 0, 255), 5)
        pts2 = barcode.rect
        cv2.putText(frame, decodedText, (pts2[0] - 20, pts2[1] - 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, (255, 0, 255), 2)
        color = (0, 0, 255)
        for i, point in enumerate(pts):  # iteriert durch alle 4 Punkte des QR-Codes
            px = point[0][0]
            py = point[0][1]
            pointText = str(i) + " : " + str(px) + " | " + str(py)
            cv2.putText(frame, pointText, (px + 30, py + 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color, 2)

    cv2.imshow("Result", frame)
    cv2.waitKey(1)



