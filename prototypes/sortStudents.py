class Student:
    def __init__(self, name, grade, age):
            self.name = name
            self.grade = grade
            self.age = age
    def __repr__(self):
            return repr((self.name, self.grade, self.age))
    def weighted_grade(self):
            return 'CBA'.index(self.grade) / float(self.age)

student_objects = [
    Student('john', 'A', 15),
    Student('xenia', 'B', 12),
    Student('dave', 'B', 10),
]

print(student_objects)
newList = sorted(student_objects, key=lambda asdf: asdf.name)   # sort by age
print(newList)