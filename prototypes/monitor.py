import random
from itertools import count
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.figure import Figure
import time

def animate(i):
    x_vals.append(next(index))
    y_vals.append(random.randint(0, 5))
    plt.cla()
    plt.plot(x_vals, y_vals)

plt.style.use("fivethirtyeight")
x_vals = []
y_vals = []

fig_ani = Figure(figsize=(5, 4), dpi=100)


index = count()

print("anfang")

while True:
    time.sleep(1)
    print("test")
    animate(index)
    plt.show()

print("ende")


ani = FuncAnimation(plt.gcf(), animate, interval=20)

plt.tight_layout()
plt.show()