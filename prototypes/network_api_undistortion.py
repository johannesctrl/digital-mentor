"""
Receive world camera data from Pupil using ZMQ.
Make sure the frame publisher plugin is loaded and confugured to gray or rgb
"""
import json
from datetime import datetime
import requests
import pyautogui
import zmq
import time
import numpy as np
import tkinter
from msgpack import unpackb, packb, loads
import numpy as np
import cv2
from scipy.signal import band_stop_obj

run_once = True
context = zmq.Context()
# open a req port to talk to pupil
addr = "127.0.0.1"  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string("SUB_PORT")
sub_port = req.recv_string()

# request requirements
url_multi = "https://las2peer.tech4comp.dbis.rwth-aachen.de/eyeTrackingProxyService/postRawDataArray"

def sendToL2P(url, data):
    r = requests.post(url=url, data=data)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")



# send notification:
def notify(notification):
    """Sends ``notification`` to Pupil Remote"""
    topic = "notify." + notification["subject"]
    payload = packb(notification, use_bin_type=True)
    req.send_string(topic, flags=zmq.SNDMORE)
    req.send(payload)
    return req.recv_string()


# open a sub port to listen to pupil
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))

# set subscriptions to topics
# recv just pupil/gaze/notifications
sub.setsockopt_string(zmq.SUBSCRIBE, "frame.")
sub.setsockopt_string(zmq.SUBSCRIBE, "gaze.")

# undistortion section

# K Camera intrinsic matrix \f$cameramatrix{K}\f$.
# D Input vector of distortion coefficients \f$\distcoeffsfisheye\f$.
# R Rectification transformation in the object space: 3x3 1-channel, or vector: 3x1/1x3

fisheye_properties = {
    "(1920, 1080)": {
            "dist_coefs": [
                [
                    -0.13648546769272826,
                    -0.0033787366635030644,
                    -0.002343859061730869,
                    0.001926274947199097,
                ]
            ],
            "camera_matrix": [
                [793.8052697386686, 0.0, 953.2237035923064],
                [0.0, 792.3104221704713, 572.5036513432223],
                [0.0, 0.0, 1.0],
            ],
            "cam_type": "fisheye"
    }
}
D = fisheye_properties["(1920, 1080)"]["dist_coefs"]
D = np.array(D)
K = fisheye_properties["(1920, 1080)"]["camera_matrix"]
print(K, type(K))
K =  np.array(K).reshape(3, 3)
print(K, type(K))
resolution = (1920, 1080)
#print(fisheye_properties.keys()[0])

def undistort(img):
    """
    Undistortes an image based on the camera model.
    :param img: Distorted input image
    :return: Undistorted image
    """
    R = np.eye(3)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(
        np.array(K),
        np.array(D),
        R,
        np.array(K),
        resolution,
        cv2.CV_16SC2,
    )

    undistorted_img = cv2.remap(
        img,
        map1,
        map2,
        interpolation=cv2.INTER_LINEAR,
        borderMode=cv2.BORDER_CONSTANT,
    )

    return undistorted_img

def unprojectPoints(pts_2d, use_distortion = True, normalize = False):
    """
    Undistorts points according to the camera model. cv2.fisheye.undistortPoints
    does *NOT* perform the same unprojection step the original cv2.unprojectPoints
    does. Thus we implement this function ourselves.
    :param pts_2d, shape: Nx2
    :return: Array of unprojected 3d points, shape: Nx3
    """

    pts_2d = np.array(pts_2d, dtype=np.float32)

    # Delete any posibly wrong 3rd dimension
    if pts_2d.ndim == 1 or pts_2d.ndim == 3:
        pts_2d = pts_2d.reshape((-1, 2))

    eps = np.finfo(np.float32).eps

    f = np.array((K[0, 0], K[1, 1])).reshape(1, 2)
    c = np.array((K[0, 2], K[1, 2])).reshape(1, 2)
    if use_distortion:
        k = D.ravel().astype(np.float32)
    else:
        k = np.asarray(
            [1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0], dtype=np.float32
        )

    pi = pts_2d.astype(np.float32)
    pw = (pi - c) / f

    theta_d = np.linalg.norm(pw, ord=2, axis=1)
    theta = theta_d
    for j in range(10):
        theta2 = theta ** 2
        theta4 = theta2 ** 2
        theta6 = theta4 * theta2
        theta8 = theta6 * theta2
        theta = theta_d / (
            1 + k[0] * theta2 + k[1] * theta4 + k[2] * theta6 + k[3] * theta8
        )

    scale = np.tan(theta) / (theta_d + eps)

    pts_2d_undist = pw * scale.reshape(-1, 1)

    pts_3d = cv2.convertPointsToHomogeneous(pts_2d_undist)
    pts_3d.shape = -1, 3

    if normalize:
        pts_3d /= np.linalg.norm(pts_3d, axis=1)[:, np.newaxis]
    print(pts_2d_undist, type(pts_2d_undist))
    print(pts_2d_undist[0][0], pts_2d_undist[0][1] )
    return pts_2d_undist[0][0], pts_2d_undist[0][1]
    #return pts_3d

def recv_from_sub():
    """Recv a message with topic, payload.
    Topic is a utf-8 encoded string. Returned as unicode object.
    Payload is a msgpack serialized dict. Returned as a python dict.
    Any addional message frames will be added as a list
    in the payload dict with key: '__raw_data__' .
    """
    topic = sub.recv_string()
    payload = unpackb(sub.recv(), raw=False)
    extra_frames = []
    while sub.get(zmq.RCVMORE):
        extra_frames.append(sub.recv())
    if extra_frames:
        payload["__raw_data__"] = extra_frames
    return topic, payload


def has_new_data_available():
    # Returns True as long subscription socket has received data queued for processing
    return sub.get(zmq.EVENTS) & zmq.POLLIN

def scaleImage(img, scale_percent=60):
    """ Skaliert ein Bild um x Prozent. """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img

terminator = 0
recent_world = None
recent_eye0 = None
recent_eye1 = None
res_x = 0
res_y = 0
global_norm_pos_x = 0
global_norm_pos_y = 0
start_time = time.time()

FRAME_FORMAT = "bgr"

# Set the frame format via the Network API plugin
notify({"subject": "frame_publishing.set_format", "format": FRAME_FORMAT})
records = []


try:
    while True:
        # The subscription socket receives data in the background and queues it for
        # processing. Once the queue is full, it will stop receiving data until the
        # queue is being processed. In other words, the code for processing the queue
        # needs to be faster than the incoming data.
        # e.g. we are subscribed to scene (30 Hz) and eye images (2x 120 Hz), resulting
        # in 270 images per second. Displays typically only have a refresh rate of
        # 60 Hz. As a result, we cannot draw all frames even if the network was fast
        # enough to transfer them. To avoid that the processing can keep up, we only
        # display the most recently received images *after* the queue has been emptied.



        while has_new_data_available():
            topic, msg = recv_from_sub()

            if topic.startswith("gaze."):
                global_norm_pos_x, global_norm_pos_y = msg['norm_pos']
                current_timestamp = msg['timestamp']
                local_time = round(time.time() * 1000, 0)
                base_data = msg['base_data']
                try:
                    right_diameter = base_data[0].get('diameter')
                except IndexError:
                    right_diameter = 'N/A'
                try:
                    left_diameter = base_data[1].get('diameter')
                except IndexError:
                    left_diameter = 'N/A'
                #print(f"norm_pos_x: {global_norm_pos_x}, norm_pos_y: {global_norm_pos_y}, timestamp: {current_timestamp}, right-diameter: {right_diameter}, left-diameter: {left_diameter}")

                single_record = {
                    "system_time": str(local_time)
                    , "pupil_timestamp": str(current_timestamp)
                    , "norm_pos_x": str(global_norm_pos_x)
                    , "norm_pos_y": str(global_norm_pos_y)
                    , "right_diameter": str(right_diameter)
                    , "left_diameter": str(left_diameter)
                }

                records.append(single_record)

                if (time.time() - start_time) >= 5 and terminator < 3:
                    print("Five seconds passed.")
                    data_array = {
                        "userId": "123123",
                        "studyName": "test_study",
                        "records": records
                    }
                    print(f"Record-length: {len(records)}")
                    terminator += 1
                    #sendToL2P(url_multi, json.dumps(data_array))
                    records = [] # clear array
                    start_time = time.time()


            if topic.startswith("frame.") and msg["format"] != FRAME_FORMAT:
                print(
                    f"different frame format ({msg['format']}); "
                    f"skipping frame from {topic}"
                )
                continue

            if topic == "frame.world":
                recent_world = np.frombuffer(
                    msg["__raw_data__"][0], dtype=np.uint8
                ).reshape(msg["height"], msg["width"], 3)

                res_x = msg['width']
                res_y = msg['height']

        if recent_world is not None:
            x = int(global_norm_pos_x * res_x)
            #  invert y axis for gaze data and screen mapping
            y = int(res_y - (global_norm_pos_y * res_y))
            cv2.circle(recent_world, (x, y), 15, (255, 0, 0), -1)
            cv2.imshow("recent_world", scaleImage(recent_world, 50))

            undist_rec_world = undistort(recent_world.copy())
            points_2d = (global_norm_pos_x, global_norm_pos_y)

            glob_ud_norm_pos_x, glob_ud_norm_pos_y = unprojectPoints(points_2d)
            x_ud = int(glob_ud_norm_pos_x * res_x)
            y_ud = int(res_y - (glob_ud_norm_pos_y * res_y))
            cv2.circle(undist_rec_world, (x_ud, y_ud), 15, (255, 0, 0), -1)
            cv2.circle(undist_rec_world, (x, y), 15, (255, 0, 255), -1)
            cv2.imshow("undistorted", scaleImage(undist_rec_world, 50))

            pressedKey = cv2.waitKey(1) & 0xFF
            if pressedKey == ord('q'):  # schließen
                cv2.destroyWindow("recent_world")
                cv2.destroyWindow("undistorted")
                break
            pass  # here you can do calculation on the 3 most recent world, eye0 and eye1 images

except KeyboardInterrupt:
    pass
finally:
    cv2.destroyAllWindows()
