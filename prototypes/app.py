import tkinter as tk
import tkinter.font as font
# from shapely.geometry.polygon import Polygon
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
import ttClasses as ttc
import tkinter
import cv2
import PIL.Image, PIL.ImageTk


class App:
    def __init__(self, root, root_title, video_source=0):
        self.root = root
        self.root.title(root_title)

        # open video source
        self.vid = MyVideoCapture(video_source)

        # Create a cavnas that can fit the above video source size
        #self.videoCanvas = tkinter.Canvas(root, width=self.vid.width, height=self.vid.heigth)
        self.videoCanvas = tkinter.Canvas(root, width=1280, height=720)
        self.videoCanvas.pack()
        self.buttonsCanvas = tkinter.Canvas(root)
        self.buttonsCanvas.pack()
        self.listCanvas = tkinter.Canvas(root)
        self.listCanvas.pack()

        # Buttons
        fontStyle = font.Font(family='Helvetica')
        changeCamera = tk.Button(self.buttonsCanvas, text="Change Camera", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc")
        changeCamera.pack()
        close = tk.Button(self.buttonsCanvas, text="Close", font=fontStyle, padx=10, pady=10, fg="white", bg="#ffcccc", width=20, command=self.close)
        close.pack()
        #self.btn_snapshot = tkinter.Button(window, text="Snapshot", width=50, command=self.snapshot)
        #self.btn_snapshot.pack(anchor=tkinter.CENTER, expand=True)

        # After it is called once, the update method will be automatically called every delay
        self.eyes = ttc.Eyes()  # Die Augen sind in der Mitte der Bild-Aufnahme
        self.taskRecords = ttc.TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
        self.update()

        self.root.mainloop()

    def update(self, delay=15):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()
        if ret:
            dframe = frame.copy()

            cv2.circle(dframe, (int(self.eyes.x), int(self.eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

            ##############################################
            # QR-Scanner
            ##############################################
            decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
            taskListPerFrame = ttc.TasksPerFrame()
            taskListPerFrame.decodeFrame(decodedFrame)

            taskListPerFrame.checkIfFocused_ForAllTasks(self.eyes)
            taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
            taskListPerFrame.drawRect_ForAllTasks(dframe)
            taskListPerFrame.drawRectPoint_ForAllTasks(dframe)

            self.taskRecords.importTasksPerFrame(taskListPerFrame)
            self.taskRecords.drawTasks(dframe)
            
            sframe = ttc.scaleImage(dframe, 50)
            sframe = cv2.cvtColor(sframe, cv2.COLOR_BGR2RGB)

            height = int(sframe.shape[0])
            width = int(sframe.shape[1])

            self.videoCanvas.config(width=width, height=height)
            self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(sframe))
            self.videoCanvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)

        self.root.after(delay, self.update)

    def close(self):
        self.root.quit()


class MyVideoCapture:
    def __init__(self, video_source=0):
        # Open the video source
        self.capture = cv2.VideoCapture(video_source)
        if not self.capture.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and heigth
        self.capture.set(3, 1920)  # ID bei set für die Breite ist 3
        self.capture.set(4, 1080)
        self.width = self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.heigth = self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.capture.isOpened():
            ret, frame = self.capture.read()
            if ret:
                # Return a boolean success flag and the current frame converted to BGR
                return (ret, frame)
            else:
                return (ret, None)
        else:
            return (ret, None)

    def __del__(self):
        if self.capture.isOpened():
            self.capture.release()

root = tk.Tk()
App(root, "Digital Mentor")



'''
capture = cv2.VideoCapture(0)
capture.set(3, 1920) # ID bei set für die Breite ist 3
capture.set(4, 1080)

while capture.isOpened:
    success, frame = capture.read()
    dframe = frame.copy()

    windowsize = 60
    cv2.imshow("frame", ttClasses.scaleImage(dframe, windowsize))

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        cv2.destroyAllWindows()
        break


#####################
# Main
#####################

root = tk.Tk()
root.resizable(True, False)

canvas = tk.Canvas(root, height=400, width=250, bg="#ffcccc")
canvas.pack()

# Design Choices
fontStyle = font.Font(family='Helvetica')

# Buttons
changeCamera = tk.Button(canvas, text="Change Camera", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc")
changeCamera.pack()

openCameraView = tk.Button(canvas, text="Camera View", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc")
openCameraView.pack()

exportAsXml = tk.Button(canvas, text="Export XML", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc")
exportAsXml.pack()

close = tk.Button(canvas, text="Close", font=fontStyle, padx=10, pady=10, fg="white", bg="#ffcccc", width=20, command=root.quit)
close.pack()


root.mainloop()




class App:
    def __init__(self, window, window_title, video_source=0):
        self.window = window
        self.window.title(window_title)
        self.video_source = video_source

        # open video source (by default this will try to open the computer webcam)
        self.vid = MyVideoCapture(self.video_source)

        # Create a canvas that can fit the above video source size
        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height)
        self.canvas.pack()

        # Button that lets the user take a snapshot
        self.btn_snapshot=tkinter.Button(window, text="Snapshot", width=50, command=self.snapshot)
        self.btn_snapshot.pack(anchor=tkinter.CENTER, expand=True)

        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update()

        self.window.mainloop()

    def snapshot(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))

    def update(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self, video_source=0):
        # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

# Create a window and pass it to the Application object
App(tkinter.Tk(), "Tkinter and OpenCV")

'''