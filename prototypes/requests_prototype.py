import requests
import json

json_data = {
    "userId": "999999",
    "studyName": "test_study",

    "fragebogenTitle": "Aufgabensammlung",
    "taskId": "idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
    "taskTitle": "no title",
    "taskTypeIndex": 1,
    "taskElementId": 1,

    "inputFieldIndex": 1,
    "taskInputValue": "check0",

    "timestamp": "1624520785981",
    "startTimestamp": "1624520785281",
    "jsonVersion": 1
}
json_data = json.dumps(json_data)

r = requests.get("http://localhost:8080/uitProxy/get")
print(r.ok)

r_post = requests.post(url="http://localhost:8080/uitProxy/postInputs", data=json_data)
