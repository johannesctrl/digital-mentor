import cv2

capture = cv2.VideoCapture(1)

while capture.isOpened:
    success, frame = capture.read()
    cv2.imshow("frame", frame)

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        cv2.destroyWindow("frame")
        break