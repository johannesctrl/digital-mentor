from matplotlib import pyplot as plt
from itertools import count

dev_x = []
dev_y = []

index = count()

dev_y.append("a")
dev_x.append(next(index))
dev_y.append("b")
dev_x.append(next(index))

fig, axs = plt.subplots(2, sharex=True)
fig.suptitle("Title")
axs[0].plot(dev_x, dev_y)
axs[1].plot(dev_x, dev_y)
plt.show()

"""
plt.plot(dev_x, dev_y)
plt.xlabel("Zeit t")
plt.ylabel("Region")
plt.title("Dies ist der Titel")
plt.show()
"""
