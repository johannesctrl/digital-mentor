import cv2
import pytesseract
from PIL import Image
from difflib import SequenceMatcher



class Video:
    def __init__(self, path):
        self.path = path


class Task:
    """  """
    def __init__(self, id, wordList, start_x, start_y, end_x, end_y):
        self.id = id
        self.wordList = wordList
        self.start_x = start_x
        self.start_y = start_y
        self.end_x = end_x
        self.end_y = end_y
    def wordListToString(self):
        return ''.join(str(e) for e in self.wordList)

class TextDetection:
    """ List in einem Bild den Text aus. """
    def __init__(self, img):
        self.img = img.copy()
        pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

    def detectTasks(self, taskID):
        taskList = []
        startFound = False
        endFound = False
        taskID = taskID

        width, height = Image.fromarray(self.img).size
        start_x, start_y, end_x, end_y = 0, 0, width, height

        img = self.img.copy()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        hImg = img.shape[0]
        wImg = img.shape[1]
        print(img.shape)
        boxes = pytesseract.image_to_data(img)
        print(boxes)
        for i, b in enumerate(boxes.splitlines()):
            b = b.split()  # unterteilt zeilenweise den boxes.splitLines() Output
            leereZeile = False
            headerZeile = False
            indexAnfang = 0
            indexEnde = 0

            if len(b) == 11:
                leereZeile = True
                # print("Leere Zeile.")

            if i == 0:
                headerZeile = True
                print("Header-Zeile.")

            if not leereZeile and not headerZeile:
                x, y, w, h = int(b[6]), int(b[7]), int(b[8]), int(b[9])
                text = b[11]

                if SequenceMatcher(None, text, "Punkte").ratio() >= 0.8:
                    print("Anfang gefunden!")
                    startFound = True
                    indexAnfang = i
                    start_y = y
                    print(b)

                if text == "Frage":
                    print("Ende gefunden!")
                    endFound = True
                    indexEnde = i
                    end_y = y + h
                    print(b)

                if startFound and endFound:
                    print("Anfang und Ende gefunden: ", start_x, start_y, end_x, end_y)
                    taskWordList = b[indexAnfang:indexEnde]
                    print("TaskText:", taskWordList)
                    task = Task(taskID, taskWordList, start_x, start_y, end_x, end_y)
                    taskID = taskID + 1
                    taskList.append(task)
                    startFound = False
                    endFound = False

        return taskList

"""
textDetection = TextDetection(cv2.imread("2.PNG"))
cv2.imshow("Result", textDetection.detectWords())
cv2.waitKey(0)
"""