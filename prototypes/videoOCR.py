import cv2
import time
from prototypes import classes

capture = cv2.VideoCapture("ressources/video01.mkv")

frameRate = capture.get(cv2.CAP_PROP_FPS)
timeFrameRateWait = 1./frameRate

print("FrameRate =", frameRate)
print("timeFrameRateWait =", timeFrameRateWait)

taskListInVideo = []
taskIDCounter = len(taskListInVideo) + 1
taskListPerFrame = []

currentSecond = 0
secondsBeforeDetection = 4

textDetection = None

while True:
    startTimeCode = time.time()

    ret, frame = capture.read() # ret ist True, solange es ein Bild gibt, frame ist das Bild
    frameNumber = int(capture.get(cv2.CAP_PROP_POS_FRAMES))

    # PRO SEKUNDE
    hitSecond = bool((frameNumber % frameRate) == 0)
    if hitSecond: # Dies soll jede Sekunde passieren
        currentSecond = frameNumber / frameRate
        print("currentSecond:", currentSecond)

        # PRO SECONDS_BEFORE_DETECTION
        hitSecondsBeforeDetection = bool((currentSecond % secondsBeforeDetection) == 0)
        if hitSecondsBeforeDetection:  # Dies soll alle x Sekunden passieren
            print("Hit", hitSecondsBeforeDetection, "!")
            # Text Detection
            textDetection = classes.TextDetection(frame)
            taskListPerFrame.clear()
            taskListPerFrame = textDetection.detectTasks(taskIDCounter)
            taskListInVideo.extend(taskListPerFrame)
            print(len(taskListPerFrame), "Tasks gefunden!")

    # Paint into Video
    for task in taskListPerFrame:
        cv2.rectangle(frame, (task.start_x, task.start_y), (task.end_x, task.end_y), (0, 0, 255), 3)
        # text = "Aufgabe " + str(task.id)
        text = "Aufgabe"
        cv2.putText(frame, text, (task.start_x, task.start_y), cv2.FONT_HERSHEY_COMPLEX, 1, (50, 50, 255), 2)

    # cv2.imshow("frame", textDetection.detectWords())
    cv2.imshow("frame", frame)

    # Video Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'): # schließen
        break
    elif pressedKey == ord('p'): # pausieren
        print("Pausiert")
        # pressedKey = cv2.waitKey(1) & 0xFF
        if cv2.waitKey(0):
            pass

    # Wartezeit
    endTimeCode = time.time()
    deltaTimeCode = endTimeCode - startTimeCode # Die Zeit die es gebraucht hat den Code auszuführen
    timeToWait = timeFrameRateWait - deltaTimeCode
    if timeToWait > 0:
        time.sleep(timeToWait)

capture.release()
cv2.destroyAllWindows()