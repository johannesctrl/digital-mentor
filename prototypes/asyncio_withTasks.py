import asyncio
import aiohttp
import time
import json

#####################################################################################
# Async with tasks
#####################################################################################
url_eyeTracking = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postEyeTracking"


async def infiniteLoop():
    tasks = []
    async with aiohttp.ClientSession() as session:
        frame_counter = 0
        while True:
            frame_counter += 1
            json_data = {  # create json
                "userId": "999999",
                "studyName": "async_tasks " + str(frame_counter),
                "startTimestamp": "1631614338679",
                "timestamp": str(frame_counter),
                "taskId": "idb7cc5078-4aa6-4eb5-89ef-a1ae234587d8",
                "taskElementId": "None"
            }
            json_data = json.dumps(json_data)  # convert to json
            startTime = time.time()

            tasks.append(session.post(url=url_eyeTracking, data=json_data, ssl=False))

            time.sleep(3)
            duration = time.time() - startTime - 3.0
            print("loop " + str(frame_counter) + " in " + str(duration))


async def sendToL2P(url, data, session):
    await session.post(url=url, data=data, ssl=False)

loop = asyncio.get_event_loop()  # loop that checks if an async-Method is done
loop.run_until_complete(infiniteLoop())  # declare which method should be run in the loop
loop.close()
