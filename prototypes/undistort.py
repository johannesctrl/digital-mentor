import os
import numpy as np
from cv2 import cv2


def main():
    capture = cv2.VideoCapture(1, cv2.CAP_DSHOW)
    capture.set(3, 1920)
    capture.set(4, 1080)
    while capture.isOpened:
        success, frame = capture.read()
        if not success:
            print("Camera", "The camera is disconnected.")
            capture.release()
            cv2.destroyWindow("frame")
            return

        undistorted_frame = undistort(frame.copy())
        cv2.imshow("frame", undistorted_frame)
        cv2.imshow("frame2", frame)

        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            cv2.destroyWindow("frame")
            cv2.destroyWindow("frame2")
            break


def _process_frame(capture, frame):
    """
    Processing function for IsolatedFrameExporter.
    Removes camera lens distortions.
    """
    return capture.intrinsics.undistort(frame.img)


# K Camera intrinsic matrix \f$cameramatrix{K}\f$.
# D Input vector of distortion coefficients \f$\distcoeffsfisheye\f$.
# R Rectification transformation in the object space: 3x3 1-channel, or vector: 3x1/1x3

resolution = (1920, 1080)
K = camera_matrix = [
            [1000, 0.0, resolution[0] / 2.0],
            [0.0, 1000, resolution[1] / 2.0],
            [0.0, 0.0, 1.0],
        ]
D = dist_coefs = [[0.0, 0.0, 0.0, 0.0, 0.0]]

dict = {
    "(1920, 1080)": {
            "dist_coefs": [
                [
                    -0.13648546769272826,
                    -0.0033787366635030644,
                    -0.002343859061730869,
                    0.001926274947199097,
                ]
            ],
            "camera_matrix": [
                [793.8052697386686, 0.0, 953.2237035923064],
                [0.0, 792.3104221704713, 572.5036513432223],
                [0.0, 0.0, 1.0],
            ],
            "cam_type": "fisheye"
    }
}
D = dict["(1920, 1080)"]["dist_coefs"]
K = dict["(1920, 1080)"]["camera_matrix"]
resolution = (1920, 1080)
print(D)

def undistort(img):
    """
    Undistortes an image based on the camera model.
    :param img: Distorted input image
    :return: Undistorted image
    """
    R = np.eye(3)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(
        np.array(K),
        np.array(D),
        R,
        np.array(K),
        resolution,
        cv2.CV_16SC2,
    )

    undistorted_img = cv2.remap(
        img,
        map1,
        map2,
        interpolation=cv2.INTER_LINEAR,
        borderMode=cv2.BORDER_CONSTANT,
    )

    return undistorted_img


main()