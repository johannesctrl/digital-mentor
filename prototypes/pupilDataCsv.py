import cv2
import csv
import os
import numpy as np
import matplotlib.pyplot as plt

pathFolderRecording = "ressources/recording_24_03/"
capture = cv2.VideoCapture(pathFolderRecording + "world.mp4")
x = []
y = []
world_index = -1
with open("../ressources/recording_24_03/exports/000/gaze_positions.csv") as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader)
    for line in csv_reader:
        if(world_index == int(line[1])):
            next(csv_reader)
            print(world_index)
        else:
            world_index += 1
            world_index = int(line[1])
            norm_pos_x = float(line[3])
            norm_pos_y = float(line[4])
            print(norm_pos_x, norm_pos_y)
            x.append(norm_pos_x)
            y.append(norm_pos_y)

print("Create Plot")
plt.scatter(x, y, s=80, facecolors='none', edgecolors='r')
plt.show()