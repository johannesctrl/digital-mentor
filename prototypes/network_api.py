"""
Receive world camera data from Pupil using ZMQ.
Make sure the frame publisher plugin is loaded and confugured to gray or rgb
"""
import json
from datetime import datetime
import requests
import pyautogui
import zmq
import time
import tkinter
from msgpack import unpackb, packb, loads
import numpy as np
import cv2
from scipy.signal import band_stop_obj
from queue import Queue
from threading import Thread
import threading

context = zmq.Context()
# open a req port to talk to pupil
addr = "127.0.0.1"  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string("SUB_PORT")
sub_port = req.recv_string()

# request requirements
url_multi = "https://las2peer.tech4comp.dbis.rwth-aachen.de/eyeTrackingProxyService/postRawDataArray"
_sentinel = object()


def sendToL2P(url, data):
    r = requests.post(url=url, data=data)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")


# send notification:
def notify(notification):
    """Sends ``notification`` to Pupil Remote"""
    topic = "notify." + notification["subject"]
    payload = packb(notification, use_bin_type=True)
    req.send_string(topic, flags=zmq.SNDMORE)
    req.send(payload)
    return req.recv_string()


# open a sub port to listen to pupil
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))

# set subscriptions to topics
# recv just pupil/gaze/notifications
sub.setsockopt_string(zmq.SUBSCRIBE, "frame.")
sub.setsockopt_string(zmq.SUBSCRIBE, "gaze.")


def recv_from_sub():
    """Recv a message with topic, payload.
    Topic is a utf-8 encoded string. Returned as unicode object.
    Payload is a msgpack serialized dict. Returned as a python dict.
    Any addional message frames will be added as a list
    in the payload dict with key: '__raw_data__' .
    """
    topic = sub.recv_string()
    payload = unpackb(sub.recv(), raw=False)
    extra_frames = []
    while sub.get(zmq.RCVMORE):
        extra_frames.append(sub.recv())
    if extra_frames:
        payload["__raw_data__"] = extra_frames
    return topic, payload


def has_new_data_available():
    # Returns True as long subscription socket has received data queued for processing
    return sub.get(zmq.EVENTS) & zmq.POLLIN


def consumer(in_q):
    while True:
        data = in_q.get()
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        else:
            url, json_data = data
            print("Thread:", str(threading.get_ident()))
            sendToL2P(str(url), json.dumps(json_data))

        in_q.task_done()


def producer(out_q):
    global_norm_pos_x = 0
    global_norm_pos_y = 0

    recent_world = None
    recent_eye0 = None
    recent_eye1 = None
    res_x = 0
    res_y = 0

    start_time = time.time()

    FRAME_FORMAT = "bgr"

    # Set the frame format via the Network API plugin
    notify({"subject": "frame_publishing.set_format", "format": FRAME_FORMAT})
    records = []
    try:
        while True:
            # The subscription socket receives data in the background and queues it for
            # processing. Once the queue is full, it will stop receiving data until the
            # queue is being processed. In other words, the code for processing the queue
            # needs to be faster than the incoming data.
            # e.g. we are subscribed to scene (30 Hz) and eye images (2x 120 Hz), resulting
            # in 270 images per second. Displays typically only have a refresh rate of
            # 60 Hz. As a result, we cannot draw all frames even if the network was fast
            # enough to transfer them. To avoid that the processing can keep up, we only
            # display the most recently received images *after* the queue has been emptied.

            while has_new_data_available():
                topic, msg = recv_from_sub()

                if topic.startswith("gaze."):
                    global_norm_pos_x, global_norm_pos_y = msg['norm_pos']
                    current_timestamp = msg['timestamp']
                    local_time = round(time.time() * 1000, 0)
                    base_data = msg['base_data']
                    try:
                        right_diameter = base_data[0].get('diameter')
                    except IndexError:
                        right_diameter = 'N/A'
                    try:
                        left_diameter = base_data[1].get('diameter')
                    except IndexError:
                        left_diameter = 'N/A'
                    # print(f"norm_pos_x: {global_norm_pos_x}, norm_pos_y: {global_norm_pos_y}, timestamp: {current_timestamp}, right-diameter: {right_diameter}, left-diameter: {left_diameter}")

                    single_record = {
                        "system_time": str(local_time)
                        , "pupil_timestamp": str(current_timestamp)
                        , "norm_pos_x": str(global_norm_pos_x)
                        , "norm_pos_y": str(global_norm_pos_y)
                        , "right_diameter": str(right_diameter)
                        , "left_diameter": str(left_diameter)
                    }

                    records.append(single_record)

                    if (time.time() - start_time) >= 5:
                        print("Five seconds passed.")
                        data_array = {
                            "userId": "123123",
                            "studyName": "test_study",
                            "records": records
                        }
                        print(f"Record-length: {len(records)}")
                        out_q.put((url_multi, data_array))
                        # sendToL2P(url_multi, json.dumps(data_array))
                        records = []
                        start_time = time.time()

                if topic.startswith("frame.") and msg["format"] != FRAME_FORMAT:
                    print(
                        f"different frame format ({msg['format']}); "
                        f"skipping frame from {topic}"
                    )
                    continue

                if topic == "frame.world":
                    recent_world = np.frombuffer(
                        msg["__raw_data__"][0], dtype=np.uint8
                    ).reshape(msg["height"], msg["width"], 3)

                    res_x = msg['width']
                    res_y = msg['height']

            if (
                    recent_world is not None
            ):
                x = int(global_norm_pos_x * res_x)
                y = int(global_norm_pos_y * res_y)
                cv2.circle(recent_world, (x, y), 5, (255, 255, 0), -1)
                cv2.imshow("world", recent_world)
                pressedKey = cv2.waitKey(1) & 0xFF
                if pressedKey == ord('q'):  # schließen
                    out_q.put(_sentinel)
                    cv2.destroyWindow("frame")
                    break
                pass  # here you can do calculation on the 3 most recent world, eye0 and eye1 images


    except KeyboardInterrupt:
        pass
    finally:
        cv2.destroyAllWindows()


# Create the shared queue
q = Queue()

# create and launch both threads
t1 = Thread(target=consumer, args=(q,))
t2 = Thread(target=producer, args=(q,))

t1.start()
t2.start()

# Wait for all produced items to be consumed
print("join")
t1.join()
t2.join()
print("End")
