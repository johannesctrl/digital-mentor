import cv2

# Initialisation
## Object-Detection
classFile = "../ressources/coco.names"
with open(classFile, "rt") as f:
    classNames = f.read().rstrip("\n").split("\n")

configPath = "../ressources/ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt"
weightsPath = "../ressources/frozen_inference_graph.pb"

net = cv2.dnn_DetectionModel(weightsPath, configPath)
net.setInputSize(320, 320)
net.setInputScale(1.0 / 127.5)
net.setInputMean((127.5, 127.5, 127.5, 127.5))
net.setInputSwapRB(True)

## Video-Camera
capture = cv2.VideoCapture(1)
capture.set(3, 640)
capture.set(4, 480)


while capture.isOpened:
    ret, frame = capture.read()

    classIDs, confs, bbox = net.detect(frame, confThreshold=0.5)
    print(classIDs, bbox)

    objectsAreDetected = bool(len(classIDs) > 1)
    if objectsAreDetected:
        for classID, confidence, box in zip(classIDs.flatten(), confs.flatten(), bbox):
            cv2.rectangle(frame, box, color=(0, 255, 0), thickness=3)
            x = box[0]
            y = box[1]
            objectName = str(classNames[classID -1])
            confidenceLevel = str(round(confidence * 100, 0)) + "%"
            text = objectName + " " + confidenceLevel
            print(text)
            cv2.putText(frame, text, (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0), 2)

    cv2.imshow("frame", frame)

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        break

capture.release()
cv2.destroyAllWindows()

