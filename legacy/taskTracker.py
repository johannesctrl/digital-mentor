
import numpy as np
import cv2
import ttClasses as ttc
from pyzbar.pyzbar import decode


class Task:
    """ Tasks sind die Aufgaben des Nutzers, die es gilt zu erkennen. """
    # ideae724db-7c63-4b49-bcd9-c6dd9d812af0 top
    def __init__(self, id, p0, p1, p2, p3):
        self.id = id
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def getPolygon(self):
        polygon = np.array([[self.p0.x, self.p0.y], [self.p1.x, self.p1.y], [self.p2.x, self.p2.y], [self.p3.x, self.p3.y]], np.int32)
        polygon = polygon.reshape((-1, 1, 2))
        return polygon

    def validateID(self, id):
        length = len(id)

##############################################
# Funktionen
##############################################

def lineIntersection(line1, line2):
    """ Diese Funktion gibt den Schnittpunkt zweier Geraden aus."""
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y

def sortPoints(pointList):
    """ Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück. Es wird nach der Perspektive der Points sortiert: 1) oben links 2) oben rechts 3) unten links 4) unten rechts """

    # Punkte nach x-Achse sortieren von links nach rechts
    pointListX = sorted(pointList, key=lambda point: point.x)

    # Punkte gruppieren: beide linken und beide rechten Punkte
    topLeft, botLeft, topRight, botRight = 0, 0, 0, 0
    if pointListX[0].y < pointListX[1].y: # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster stelle kommen
        topLeft = pointListX[0]
        botLeft = pointListX[1]
    else:
        botLeft = pointListX[0] # bot left
        topLeft = pointListX[1] # top left

    if pointListX[2].y < pointListX[3].y:
        topRight = pointListX[2]
        botRight = pointListX[3]
    else:
        botRight = pointListX[2]
        topRight = pointListX[3]

    rectSorted = [topLeft, topRight, botLeft, botRight]
    return rectSorted

def scaleImage(img, scale_percent):
    """ Skaliert ein Bild um x Prozent. """
    scale_percent = 60  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img

def getID(qrCode):
    return qrCode.data.decode("UTF-8")

##############################################
# Main
##############################################

capture = cv2.VideoCapture(0)
capture.set(3, 1920) # ID bei set für die Breite ist 3
capture.set(4, 1080)

eyes = ttc.Eyes()  # Die Augen sind in der Mitte der Bild-Aufnahme


while capture.isOpened:
    success, frame = capture.read()
    dframe = frame.copy()

    ##############################################
    # Eye-Tracking
    ##############################################
    eyes.setX(1920/2)
    eyes.setY(1080/2)
    cv2.circle(dframe, (int(1920/2), int(1080/2)), 10, (255,0,0), -1)

    ##############################################
    # QR-Scanner
    ##############################################
    decodedFrame = decode(frame)
    taskList = ttc.TasksPerFrame()
    # taskList.decodeFrame(decodedFrame)
    # Hier müssen noch alle QR-Codes sortiert werden und nur bei einem Pärchen ist eine Aufgabe definiert
    #decodedFrame.sort(key=getID)
    #print(len(decodedFrame))
    if len(decodedFrame) != 0 and len(decodedFrame) % 2 == 0: #Ein QR-Code wurde gefunden
        taskPoints = []
        taskPoints2 = []
        for qrCode in decodedFrame: # qrCode steht für jeden gefundenen qrCode im Frame
            decodedText = qrCode.data.decode("UTF-8")
            qrPosType = decodedText[len(decodedText) - 3:len(decodedText)] # Der Positions-Typ des QR-Codes, ob top oder bot
            #print("Polygon:", qrCode.polygon)

            # Rechteck malen von QR-Code
            print(qrCode.polygon)
            pts = np.array([qrCode.polygon], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], True, (255, 0, 255), 5)

            # Punkte malen von QR-Code
            for i,point in enumerate(pts): # iteriert durch alle 4 Punkte des QR-Codes
                px = point[0][0]
                py = point[0][1]
                pointText = str(i) + " : " + str(px) + " | " + str(py)
                cv2.putText(dframe, pointText, (px+30, py+30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, (123, 255, 132), 1)
            pts2 = qrCode.rect
            cv2.putText(dframe, decodedText, (pts2[0], pts2[1] - 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, (255, 0, 255), 1)

            # Punkte für Task-Rechteck berechnen
            ## Punkte aus Barcode zwischenspeichern
            ptBarcode1 = ttc.Point(qrCode.polygon[0].x, qrCode.polygon[0].y) # Rechteck-Punkte aus QR-Code extrahieren
            ptBarcode2 = ttc.Point(qrCode.polygon[1].x, qrCode.polygon[1].y)
            ptBarcode3 = ttc.Point(qrCode.polygon[2].x, qrCode.polygon[2].y)
            ptBarcode4 = ttc.Point(qrCode.polygon[3].x, qrCode.polygon[3].y)
            ptList = [ptBarcode1, ptBarcode2, ptBarcode3, ptBarcode4]
            ptListSorted = sortPoints(ptList)

            ## Pun
            if qrPosType == "top":
                taskPoints2.insert(0, ptListSorted[1])

            if qrPosType == "bot":
                taskPoints2.insert(0, ptListSorted[2])

        point1 = taskPoints2[0]
        point2 = taskPoints2[1]
        p0 = ttc.Point(point1.x, point1.y)
        p1 = ttc.Point(point1.x, point2.y)
        p2 = ttc.Point(point2.x, point2.y)
        p3 = ttc.Point(point2.x, point1.y)

        task = Task("1", p0, p1, p2, p3)

        """
        pt1 = Point(qrCode.polygon[0].x, qrCode.polygon[0].y)  # Rechteck-Punkte aus QR-Code extrahieren
        pt2 = Point(qrCode.polygon[1].x, qrCode.polygon[1].y)
        pt3 = Point(qrCode.polygon[2].x, qrCode.polygon[2].y)
        pt4 = Point(qrCode.polygon[3].x, qrCode.polygon[3].y)
        ptList = [pt1, pt2, pt3, pt4]
        ptListSorted = sortPoints(ptList)
        task = Task("1", ptListSorted[0], ptListSorted[1], ptListSorted[2], ptListSorted[3])
        """
        cv2.polylines(dframe, [task.getPolygon()], True, (255, 0, 255), 5)  # Rechteck malen



    cv2.imshow("frame", scaleImage(dframe, 60))

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        break


