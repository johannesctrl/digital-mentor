
import numpy as np
import cv2
import ttClasses as ttc
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol

##############################################
# Main
##############################################

capture = cv2.VideoCapture(1)
capture.set(3, 1920) # ID bei set für die Breite ist 3
capture.set(4, 1080)

eyes = ttc.Eyes()  # Die Augen sind in der Mitte der Bild-Aufnahme

taskRecords = ttc.TaskRecords() # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.

while capture.isOpened:
    success, frame = capture.read()
    dframe = frame.copy()

    cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1) # visualisiere Eye-Tracking

    # QR-Scanner
    decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
    taskListPerFrame = ttc.TasksPerFrame()
    taskListPerFrame.decodeFrame(decodedFrame)

    # Sind Tasks fokussiert?
    taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

    # Speichere Ergebnisse
    taskRecords.importTasksPerFrame(taskListPerFrame)
    taskRecords.drawTasks(dframe)

    # Zeichne
    taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
    taskListPerFrame.drawRect_ForAllTasks(dframe)
    taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
    taskListPerFrame.drawID_ForAllTasks(dframe)
    ttc.drawControls(dframe)
    windowsize = 60
    cv2.imshow("frame", ttc.scaleImage(dframe, windowsize))

    # Controlls
    pressedKey = cv2.waitKey(1) & 0xFF
    if pressedKey == ord('q'):  # schließen
        cv2.destroyAllWindows()
        break

    if pressedKey == ord('x'):  # createXML
        ttc.createXMLFromTaskRecords(taskRecords)

    def mousePoints(event, x, y, flags, params):
        if event == cv2.EVENT_LBUTTONDOWN:
            eyes.setX(x / (windowsize/100))
            eyes.setY(y / (windowsize/100))
            print(x, y)
    cv2.setMouseCallback("frame", mousePoints)
