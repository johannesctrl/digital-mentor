import numpy as np
import cv2
import ttClasses
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
import tkinter as tk
import tkinter.font as font
from tkinter import filedialog, Text
from tkinter import *
from tkinter import ttk
from tkinter import filedialog

##############################################
# Variables
##############################################



##############################################
# Methods
##############################################

def pickFolder():
    folderPath = tk.StringVar()
    folder_selected = filedialog.askdirectory()
    folderPath.set(folder_selected)
    return folderPath.get() + "/"
    # ttClasses.openPupilDataFishEye(folderPath.get() + "/")

##############################################
# Main
##############################################

class WindowPupilDataFishEye:
    def __init__(self, master):
        self.master = master
        global pdfepcs
        window = tk.Toplevel(master)
        window.title("WindowPupilDataFishEye")
        canvas = tk.Canvas(window, height=400, width=250, bg="#ffcccc")
        canvas.pack()

        labelList = Label(canvas, text="Record-List")
        labelList.grid(row=0, column=0)

        # folderPicker
        listbox = tk.Listbox(canvas, selectmode=MULTIPLE, width=40)
        listbox.grid(row=2, column=0)

        pdfepcs = StringVar()
        folderPicker = tk.Button(canvas, text="Pick Folder", padx=10, pady=10, fg="white", width=20, bg="#ffcccc",
                                 command=lambda: pdfepcs == ttClasses.tk_pupilDataFishEye_pickFolder(pickFolder(), listbox))
        folderPicker.grid(row=1, column=0)

        btn_startScanning = tk.Button(canvas, text="Start Scanning", padx=10, pady=10, fg="white", width= 20, bg="#ffcccc",
                              command=lambda: ttClasses.tk_startScanning(listbox, pdfepcs))
        btn_startScanning.grid(row=3, column=0)

        e = Entry(canvas, textvariable=pdfepcs)
        e.grid(row=4, column=0)


class App:
    def __init__(self):
        root = tk.Tk()
        self.root = root
        root.resizable(True, False)

        canvas = tk.Canvas(root, height=400, width=250, bg="#ffcccc")
        canvas.pack()

        # Design Choices
        fontStyle = font.Font(family='Helvetica')

        # Buttons
        liveVideo = tk.Button(canvas, text="Live Video", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc", command=ttClasses.openLiveWebcam)
        liveVideo.grid(row=0, column=0)
        pupilData = tk.Button(canvas, text="PupilData Recording", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc", command=ttClasses.openPupilData)
        pupilData.grid(row=1, column=0)
        pupilDataFishEye = tk.Button(canvas, text="PupilData Fish Eye Recording", font=fontStyle, padx=10, pady=10, fg="white", width=20, bg="#ffcccc", command=self.open_WindowPupilDataFishEye)
        pupilDataFishEye.grid(row=2, column=0)

        close = tk.Button(canvas, text="Close", font=fontStyle, padx=10, pady=10, fg="white", bg="#ffcccc", width=20, command=root.quit)
        close.grid(row=99, column=0)

    def run(self):
        self.root.mainloop()

    def open_WindowPupilDataFishEye(self):
        WindowPupilDataFishEye(self.root)

app = App()
app.run()